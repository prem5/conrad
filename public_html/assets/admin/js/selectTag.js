/*------------------------------------------------------------------
 [ selectize  Trigger Js]

 Project     :	Fickle Responsive Admin Template
 Version     :	1.0
 Author      : 	AimMateTeam
 URL         :   http://aimmate.com
 Support     :   aimmateteam@gmail.com
 Primary use :   use on Select & Tag
 -------------------------------------------------------------------*/


jQuery(document).ready(function($) {
    'use strict';

   select_state_call();
    
   
});
var eventHandler = function (name) {
    return function () {
        /*console.log(name, arguments);*/
    };
};


function select_state_call(){ 
    $('#select-state').selectize({ 
	plugins: ['remove_button'],
        delimiter: ',',
        persist: false,
        maxItems: null,
        create: false,
onDelete: function (values) {
           // return confirm(values.length > 1 ? 'Are you sure you want to remove these ' + values.length + ' items?' : 'Are you sure you want to remove "' + values[0] + '"?');
        }
    });
}
