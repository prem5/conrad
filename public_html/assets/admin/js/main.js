$(function() {

    var dat =$('#datepicker').val();
    console.log(dat);
    if($('#country_id').val()){
        $('#province_id').removeAttr('disabled');
    }
    if($('#province_id').val()){
        $('#city_id').removeAttr('disabled');
    }
    if($('#city_id').val()){
        $('#area_id').removeAttr('disabled');
    }
     var dat =$( "#datepicker").val();
    if(dat=='1970-01-01')
    {
    $( "#datepicker" ).datepicker({
        dateFormat: 'yy-mm-dd', minDate: 0
    }).datepicker("setDate", new Date());
    }
    else
    {
        $( "#datepicker" ).datepicker({
            dateFormat: 'yy-mm-dd', minDate: 0
        });
    }


    $('#country_id').change(function(){
        if($(this).val()!='')
        {
       var token = $("form input[name=_token]").val();
        $.ajax({
            url:'/ajax/province',
            data:'country_id='+$(this).val()+'&_token='+token,
            type:'post',
            dataType:'json',
            success:function(response)
            {
                $('#province_id').attr('disabled',false);
                $('#province_id').html(response['data']);
            }
        });
        }
        else
        {
            $('#province_id').attr('disabled',true);
            $('#city_id').attr('disabled',true);
            $('#area_id').attr('disabled',true);
        }
    });

    $('#province_id').change(function(){
        if($(this).val()!='')
        {
        var token = $("form input[name=_token]").val();
        $.ajax({
            url:'/ajax/city',
            data:'province_id='+$(this).val()+'&_token='+token,
            type:'post',
            dataType:'json',
            success:function(response)
            {
                $('#city_id').attr('disabled',false);
                $('#city_id').html(response['data']);
            }
        });
        }
        else
        {
            $('#city_id').attr('disabled',true);
            $('#area_id').attr('disabled',true);
        }
    });

    $('#city_id').change(function(){
        if($(this).val()!='')
        {
        var token = $("form input[name=_token]").val();
        $.ajax({
            url:'/ajax/area',
            data:'city_id='+$(this).val()+'&_token='+token,
            type:'post',
            dataType:'json',
            success:function(response)
            {
                $('#area_id').attr('disabled',false);
                $('#area_id').html(response['data']);
            }
        });

    }
    else
    {

        $('#area_id').attr('disabled',true);
    }
    });
    $('#carousel-listing').tableDnD({
        onDrop:function(table,row)
        {
            var data =$.tableDnD.serialize();
            var token = $("#carousel-index-form input[name=_token]").val();
            $.ajax({
                url:'/administrator/carousels/changeorder',
                data:data+'&_token='+token,
                dataType:'json',
                type:'post',
                success:function(response)
                {

                }
            })
        }
    });

    $('#featured-listing').tableDnD({
        onDrop:function(table,row)
        {
            var data =$.tableDnD.serialize();
            var token = $("#featured-index-form input[name=_token]").val();
            $.ajax({
                url:'/administrator/features/changeorder',
                data:data+'&_token='+token,
                dataType:'json',
                type:'post',
                success:function(response)
                {

                }
            })
        }
    });

    /*****Gallery Sortable***/
    $('#gallery-listing').tableDnD({
        onDrop:function(table,row)
        {
            var data =$.tableDnD.serialize();
            var token = $("#gallery-index-form input[name=_token]").val();
            $.ajax({
                url:'/administrator/galleries/changeorder',
                data:data+'&_token='+token,
                dataType:'json',
                type:'post',
                success:function(response)
                {

                }
            })
        }
    });

    $('#property_id').change(function(){
       var id = $(this).val();
        var pid  = id.split('_');
        var d = pid[1];
        var token = $("#carousel-index-form input[name=_token]").val();
        $.ajax({
            url : '/administrator/carousels/image',
            data : "property_id="+pid[0]+"&proimg="+d+"&_token="+token,
            /*dataType:'json',*/
            type:'get',
            success:function(res)
            {
               $('#Sthumbs').html(res);
                $('#choose').show();
            }
        });
    });

    $('#deluser').click(function(){
        var numberOfChecked = $('input:checkbox:checked').length;
       if(numberOfChecked>0)
       {
           return true;
       }
        else{
           confirm("Please select at least one contact before deleting.");
           return false;

       }

    });

});
function getAreaSlug(e)
{
    var title = $(e).val();
    $.ajax({
        type: "get",
        url: '/get-area-slug/' + title,
        //data: {'title':title},
        //cache: false,
        success: function (result) {

            $('#slug').val(result);
        }
    });
}
function getCitySlug(e)
{
    var title = $(e).val();
    $.ajax({
        type: "get",
        url: '/get-city-slug/' + title,
        //data: {'title':title},
        //cache: false,
        success: function (result) {

            $('#slug').val(result);
        }
    });
}
function getCountrySlug(e)
{
    var title = $(e).val();
    $.ajax({
        type: "get",
        url: '/get-country-slug/' + title,
        //data: {'title':title},
        //cache: false,
        success: function (result) {

            $('#slug').val(result);
        }
    });
}
function getProvinceSlug(e)
{
    var title = $(e).val();
    $.ajax({
        type: "get",
        url: '/get-province-slug/' + title,
        //data: {'title':title},
        //cache: false,
        success: function (result) {

            $('#slug').val(result);
        }
    });
}

function getTagSlug(e)
{
    var title = $(e).val();
    $.ajax({
        type: "get",
        url: '/get-tag-slug/' + title,
        //data: {'title':title},
        //cache: false,
        success: function (result) {

            $('#slug').val(result);
        }
    });
}
function getGuideSlug(e) {
    var title = $(e).val();
    $.ajax({
        type: "get",
        url: '/get-guide-slug/' + title,
        //data: {'title':title},
        //cache: false,
        success: function (result) {

            $('#slug').val(result);
        }
    });
}
function getBlogSlug(e) {
    var title = $(e).val();
    $.ajax({
        type: "get",
        url: '/get-blog-slug/' + title,
        //data: {'title':title},
        //cache: false,
        success: function (result) {

            $('#slug').val(result);
            $('#meta_title').val(title);
        }
    });
}

function getListingSlug(e,action){
    var title = $(e).val();
    $.ajax({
        type: "get",
        url: '/get-listing-slug/' + title,
        dataType:"json",
        //data: {'title':title},
        //cache: false,
        success: function (result) {

            $('#slug').val(result.slug);
            /*if(action=="add"){
            $('#reference_code').val(result.refe);
            }*/
        }
    });
}
function getPageSlug(e){
    var title = $(e).val();
    $.ajax({
        type: "get",
        url: '/get-page-slug/' + title,
        //data: {'title':title},
        //cache: false,
        success: function (result) {
            $('#slug').val(result);


        }
    });
}
// for filtering properties list in admin side
function fileterPropertyList() {
    var newUrl;
    newUrl = ($(location).attr('href').indexOf('?') > 0) ? $(location).attr('href').substring(0, $(location).attr('href').indexOf('?')) : $(location).attr('href');
    if (parseInt($('#propertyFilter').val()) ==1) {
        newUrl += '?published=1';
    }
    else if (parseInt($('#propertyFilter').val()) ==0){
        newUrl += '?published=0';
    }
    else {

    }
    window.location = newUrl;

}
