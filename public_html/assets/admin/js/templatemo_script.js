/* Credit: http://www.templatemo.com */
    
$(document).ready( function() {        

	// sidebar menu click
	$('.templatemo-sidebar-menu li.sub a.cat').click(function(){

        $('.templatemo-sidebar-menu li').removeClass('active');
		if($(this).parent().hasClass('open')) {
            $('.templatemo-sidebar-menu li.sub').removeClass('open');
			$(this).parent().removeClass('open');

		} else {
            $('.templatemo-sidebar-menu li.sub').removeClass('open');
			$(this).parent().addClass('open');
            $(this).parent().addClass('active');
		}
	});  // sidebar menu click

    $('.mobile-menu').click(function(){
        $('.collapse').slideToggle();
    });
}); // document.ready
