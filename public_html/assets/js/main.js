/*jQuery(window).load(function(){
 console.log('loaded');
 $('.loader').fadeOut(500);
 });*/
jQuery(document).ready(function () {
    $('.loader').remove();
    $(".pro-list li:nth-child(odd)").addClass('dark');
    /****** Sell form submission*****/

    $('#enquire_now').click(function () {
        jQuery('#MyForm').validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                },
                phone: "required",
                subject: "required",
                message: "required"
            },
            messages: {
                name: "Name is required.",
                email: {
                    required: "Email is required.",
                    email: "Please enter valid email."
                },
                phone: "Phone is required",
                subject: "Subject is required",
                message: "Message is required"
            }

        });
        if (!jQuery('#MyForm').valid()) return false;
        var data = $('#MyForm').serialize();
        var act = $('#MyForm').attr('action');
        $.ajax({
            url: act,
            type: 'POST',
            data: data,
            success: function (success) {

                jQuery('#MyForm').hide();
                jQuery('.succemsg').html(success);

            }
        });
        return false;
    });

    /**** *  Blog Comment ****/

    jQuery('.subcomment').click(function () {
        jQuery('#comment_form').validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                },
                message: "required"

            },
            messages: {
                name: "Name is required.",
                email: {
                    required: "Email is required.",
                    email: "Please enter valid email."
                },
                message: "Message is required"

            }
        });

        if (!jQuery('#comment_form').valid()) return false;

        var comm = $('#comment_form').serialize();
        var act = $('#comment_form').attr('action');
        jQuery.ajax({
            url: act,
            type: "POST",
            data: comm,
            success: function (response) {
                jQuery('.comment_respone').remove();
                jQuery('.comRes').html(response)
            }

        });
        return false;
    });

    /********** Footer subscribe form email validation***********/

    jQuery('#emailsub').click(function () {
        jQuery('#subs').validate({
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                required: "Email is required",
                email: "Please enter valid email"

            }
        });
        if (!jQuery('#subs').valid()) return false;
        var email = jQuery('#subs').serialize();
        var act = jQuery('#subs').attr('action');
        jQuery.ajax({
            url: act,
            type: "POST",
            data: email,
            success: function (response) {

                jQuery('.footmsg').html(response);
                jQuery('#femail').val('');
            }
        });
        return false;
    });

    /********** Footer subscribe form email validation***********/


    /**Rent and Buy Tabs**/
    $('ul.tabs').each(function () {

        var $active, $content, $links = $(this).find('a');
        $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
        $active.addClass('active');
        $content = $($active[0].hash);
        $links.not($active).each(function () {
            $(this.hash).hide();
        });

        // Bind the click event handler
        $(this).on('click', 'a', function (e) {
            // Make the old tab inactive.
            $active.removeClass('active');
            $content.hide();

            // Update the variables with the new link and content
            $active = $(this);
            $content = $(this.hash);

            // Make the tab active.
            $active.addClass('active');
            $content.show();

            // Prevent the anchor's default click action
            e.preventDefault();
        });
    });
    /**End **/

    /****SideNewsletter Form  Validation-**/
    jQuery('#newsfrm').click(function () {
        jQuery('#sidenewfrm').validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: "Name is required",
                required: "Email is required",
                email: "Please enter valid email"

            }
        });
        if (!jQuery('#sidenewfrm').valid())return false;
        var sidenews = jQuery('#sidenewfrm').serialize();
        var act_url = jQuery('#sidenewfrm').attr('action');

        jQuery.ajax({
            url: act_url,
            type: "POST",
            data: sidenews,
            success: function (response) {
                jQuery('.sidemsg').html(response);
                jQuery('#sname').val('');
                jQuery('#semail').val('');
            }

        });

        return false;

    });

    /**** End SideNewsletter Form  Validation-**/

    /***Newsletter Sell Form validation**/
    jQuery('#homenewsbtn').click(function () {
        jQuery('#homenews').validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: "Name is required",
                required: "Email is required",
                email: "Please enter valid email"

            }
        });

        if (!jQuery('#homenews').valid())return false;
        var hdata = jQuery('#homenews').serialize();
        var h_act = jQuery('#homenews').attr('action');
        jQuery.ajax({
            url: h_act,
            type: "POST",
            data: hdata,
            success: function (response) {
                jQuery('.homemsg').html(response);
                jQuery('#hname').val('');
                jQuery('#hemail').val('');
            }
        });
        return false;
    });
    jQuery('.close').click(function () {
        jQuery('.alert').hide();
    });

    if ($(window).width() < 800) {
        $('#slider-wrap .strip').insertAfter('#content-wrap .propertie-block');
        $('.offer-block .mid').insertBefore('.offer-block .centering');
        $('.off h3').click(function () {
            $('.off p').slideUp();
            $('.off h3').removeClass('active');
            $('.off').removeClass('active');
            $(this).parent('.off').addClass('active');
            $(this).addClass('active');
            if ($(this).parent('.off').find('p').is(':visible')) {
                $('.off h3').removeClass('active');

                $(this).parent('.off').find('p').slideUp();

            } else {
                $(this).parent('.off').find('p').slideToggle();
            }

        });
        $('.bx h3').click(function () {
            $('.bx .cntnts').slideUp();
            $('.bx h3').removeClass('active');
            $(this).addClass('active');
            if ($(this).parent('.bx').find('.cntnts').is(':visible')) {
                $('.bx h3').removeClass('active');
                $(this).parent('.bx').find('.cntnts').slideUp();
            } else {
                $(this).parent('.bx').find('.cntnts').slideToggle();
            }

        });
    }
    /***End Newsletter Sell Form validation**/

    /***Disable right mouseclick on images**/
    /*$('img').bind('contextmenu', function (e) {
        e.preventDefault();
    });*/
    /***End Disable right mouseclick on images**/

    $('.translation-list #selected').click(
        function () {
            $(this).parent('div').find('.translation-links').slideToggle();
            return false;
        });


    


    $('a').each(function() {
	if(typeof $(this).attr('href') !=='undefined'){
		var ssl = $(this).attr('href').replace('http://', 'https://');
		$(this).attr('href', ssl);
	}
    });
});
function footerproperty(s) {

    var token = $('#tok').val();
    var country = $(s).attr("data-country");
    var province = $(s).attr("data-province");
    var city = $(s).attr("data-city");
    var area = $(s).attr("data-area");
    var action = $(s).attr("data-action");
    var type = $(s).attr("data-pro");

    var data = $('#inset_form').html('<form action="/searchresult" method="post" id="link_form" style="display:none;">' +
        '<input type="hidden" name="country" value="' + country + '" />' +
        '<input type="hidden" name="province" value="' + province + '" />' +
        '<input type="hidden" name="city" value="' + city + '" />' +
        '<input type="hidden" name="place" value="' + area + '" />' +
        '<input type="hidden" name="action" value="' + action + '" />' +
        '<input type="hidden" name="type" value="' + type + '" />' +
        '<input type="hidden" name="_token" value="' + token + '">' +
        '</form>');
    $("#link_form").submit();

    /*
     $.ajax({
     url :"links/",
     type :"POST",
     data:"county="+country+"&province="+province+"&city="+city+"&place="+area+"&action="+action+"&type="+type+"&_token="+token,
     success:function(response)
     {

     }
     });*/
    return false;


}
function updateSearchPage() {
    var newUrl;
    var reload = false;
    newUrl = ($(location).attr('href').indexOf('?') > 0) ? $(location).attr('href').substring(0, $(location).attr('href').indexOf('?')) : $(location).attr('href');
    if (parseInt($('#display').val()) > 0) {

        newUrl += '?display=' + parseInt($('#display').val());
        reload = true;

    }
    if (parseInt($('#sortBy').val()) > 0) {
        var sortBy = parseInt($('#sortBy').val());
        if (reload) {
            newUrl += '&sortBy=' + sortBy;
        }
        else {
            newUrl += '?sortBy=' + sortBy;
        }
        reload = true;
    }
    if (!($('#currency').val() == undefined) && $('#currency').val().length > 1) {
        if (reload) {
            newUrl += '&currency=' + $('#currency').val();
        }
        else {
            newUrl += '?currency=' + $('#currency').val();
        }
        reload = true;
    }
    if (reload) {
        window.location = newUrl;
    }
}
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}
$('#mapToggle').slideToggle("slow")
var isMap = false;
function showMap() {
    if(!isMap){
        setupMap();
        isMap = true;
    }
    $('#mapToggle').slideToggle("slow");
}