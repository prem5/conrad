$(document).ready(function () {

    tinymce.init({
        selector: "textarea",
        height: 400,
        plugins: [
            "advlist autolink lists link  charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste jbimages"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
        protect: [
            /\<\/?(if|endif)\>/g, // Protect <if> & </endif>
            /\<xsl\:[^>]+\>/g, // Protect <xsl:...>
            /<\?php.*?\?>/g // Protect php code
        ],
        valid_elements: '*[*]',
        preformatted: true,
        content_css: "css/social-buttons.css,css/iconmono.css,css/slick.css,css/jquery.fancybox.css,css/style.css"
    });

    /*
     *
     * Zebra Datepicker
     *
     */
    $('.datepicker').Zebra_DatePicker({});

    /*
     *
     * Profile page
     * Clear password inputs
     *
     */
    $("input#passwordcheck, input#password1, input#password2").val("");

});