$(document).ready(function () {

    //$('form.required-form').simpleValidate({});

    $(".select").heapbox();
    $(".property-type").heapbox({
        'onChange':function(e,v){
            var value = $(v).find("option:selected").text();
            if(value=="Type")
            value = "Property";
            $('#text-type').html(value);
        }
    });

    $(".property-place").heapbox({
        'onChange':function(e,v){
            var value = $(v).find("option:selected").text();
            if(value=="Location")
                value = "Koh Samui";
            $('#text-place').html(value);
        }
    });
    $("#search-buy-rent").heapbox({
        'onChange':function(e,v)
        {
            var token = $('#property-search-form input[name=_token]').val();
            $.ajax({
                url:'/ajax/changesearchfield',
                data:'type='+e+'&_token='+token,
                type:'post',
                dataType:'json',
                success:function(response)
                {
                    //$('#property-price').heapbox("set",response['data']);
                    $('#property-price').html(response['content']);
                    $('#property-price').heapbox("update");
                }
            })

        }
    });
    $("#property-search-form .heapBox:nth-child(5) .heapOption a").each(function(){
        var value = $(this).attr('rel');
        if(value!='')
        {
            value = value.split('_');
            if(value[0]=='city')
                $(this).addClass('city');
        }
    });

    var slideshows = $('.asd').on('cycle-next cycle-prev', function (e, opts) {
        slideshows.not(this).cycle('goto', opts.currSlide);
    });

    $('#cycle-2 .cycle-slide').click(function () {
        var index = $('#cycle-2').data('cycle.API').getSlideIndex(this);
        slideshows.cycle('goto', index);
    });

    $('#opner').click(function (e) {
        e.preventDefault();
        $('.nav-block .nav,.nav-block .links').slideToggle();
    });

    $('.searchOpen').click(function(){
        $('.schfrm').slideToggle();
        $('.schfrm .heapBox').slideToggle();

    });

    if($(window).width() < 800){
        var ht = $('.contact-block').height();
            $('.contact-block .map').height(ht);
    }

    new WOW().init();

    $(".fancybox").fancybox();

    $("input[type=file]").nicefileinput();

    $('.slideshow').slick({
        infinite: false,
        speed: 300,
        /*autoplay: true,
        autoplaySpeed: 2000,*/
        prevArrow: '<a class="prev" href=# id=prev_1>&lt;</a>',
        nextArrow: '<a class="next" href=# id=next_1>&gt;</a>',
        appendArrows: '#slickarrow',
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 900,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });


    $('.np-block #share > a.share').click(function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $(link).slideToggle('fast');
    });

    /*$(".slider-block .heapBox:nth-child(3)").addClass("gray");
    $(".slider-block .heapBox:nth-child(5)").addClass("gray");*/

    $('.single-item').slick({
        autoplay: true,
        autoplaySpeed: 7000,
        infinite: true,
        speed: 1500,
        fade: true,
        dots: true,
        cssEase: 'linear'
    });

    // Click on heart
    $('span.heart').click(function (e) {
        e.preventDefault();
        var id = this.id;
        idd = id.split('-');
        $.ajax({
            url:'/properties/setfavourites',
            data:'property_id='+idd[1],
            type:'get',
            dataType:'json',
            success:function(response){
                if(response['status']==1)
                    $('.property-'+idd[1]).addClass('selected');
                else if(response['status']==0)
                    $('.property-'+idd[1]).removeClass('selected');
                $('#total-favourite-count').html(response['count']);
            }
        });
    });

    $('span.remove').click(function (e) {
        e.preventDefault();
        var id = this.id;
        idd = id.split('-');
        $.ajax({
            url:'/properties/removefavourites',
            data:'property_id='+idd[1],
            type:'get',
            dataType:'json',
            success:function(response){
                $('.favourite-'+idd[1]).remove();
                $('#total-favourite-count').html(response['count']);
            }
        });
    });
   // for custom drop down of google translater
    $('.translation-links a').click(function() {
        var lang = $(this).data('lang');
        var $frame = $('.goog-te-menu-frame:first');
        if (!$frame.size()) {
            alert("Error: Could not find Google translate frame.");
        }
        $(this).parent().parent().hide()
        if(lang == 'English'){
            $('#selected').attr('class', 'eng');
            $('#selected').html('ENG');
            $('#selectedMob').attr('class', 'eng');
            $('#selectedMob').html('ENG');

        }
        else if(lang == 'French'){
            $('#selected').attr('class', 'fra');
            $('#selected').html('FRA');
            $('#selectedMob').attr('class', 'fra');
            $('#selectedMob').html('FRA');

        }
        else if(lang == 'Thai'){
            $('#selected').attr('class', 'thai');
            $('#selected').html('THAI');
            $('#selectedMob').attr('class', 'thai');
            $('#selectedMob').html('THAI');

        }
        else if(lang == 'Chinese'){
            $('#selected').attr('class', 'chin');
            $('#selected').html('CHIN');
            $('#selectedMob').attr('class', 'chin');
            $('#selectedMob').html('CHIN');
        }
        else if(lang == 'Russian'){
            $('#selected').attr('class', 'rus');
            $('#selected').html('RUS');
            $('#selectedMob').attr('class', 'rus');
            $('#selectedMob').html('RUS');
        }
        else {}
        $frame.contents().find('.goog-te-menu2-item span.text:contains('+lang+')').get(0).click(); // to $('.goog-te-menu-frame:first').contents().find('.goog-te-menu2-item span.text').each(function(){ if( $(this).html() == lang ) $(this).click(); });

        return false;
    });
    if(getCookie('googtrans').length > 1){
        var newlang = getCookie('googtrans').substring(4);
        if(newlang == 'en'){
            $('#selected').attr('class', 'eng');
            $('#selected').html('ENG');
            $('#selectedMob').attr('class', 'eng');
            $('#selectedMob').html('ENG');

        }
        else if(newlang == 'fr'){
            $('#selected').attr('class', 'fra');
            $('#selected').html('FRA');
            $('#selectedMob').attr('class', 'fra');
            $('#selectedMob').html('FRA');

        }
        else if(newlang == 'th'){
            $('#selected').attr('class', 'thai');
            $('#selected').html('THAI');
            $('#selectedMob').attr('class', 'thai');
            $('#selectedMob').html('THAI');
        }
        else if(newlang == 'zh-CN'){
            $('#selected').attr('class', 'chin');
            $('#selected').html('CHIN');
            $('#selectedMob').attr('class', 'chin');
            $('#selectedMob').html('CHIN');
        }
        else if(newlang == 'ru'){
            $('#selected').attr('class', 'rus');
            $('#selected').html('RUS');
            $('#selectedMob').attr('class', 'rus');
            $('#selectedMob').html('RUS');
        }
        else {}
    }
    else{
        $('#selected').attr('class', 'eng');
        $('#selected').html('ENG');

    }
     $("#enquire_now_new").click(function(){
        debugger;
        return false;
        if(jQuery("#MyForm").validate({
                rules:{name:"required",email:{required:!0,email:!0},phone:"required",subject:"required",message:"required"},
                messages:{name:"Name is required.",email:{required:"Email is required.",email:"Please enter valid email."},phone:"Phone is required",subject:"Subject is required",message:"Message is required"}}),
                !jQuery("#MyForm").valid())return!1;
        jQuery('#enquery_now')..prop('disabled', true);
        var e=$("#MyForm").serialize(),r=$("#MyForm").attr("action");
        return $.ajax({url:r,type:"POST",data:e,success:function(e){jQuery("#MyForm").hide(),jQuery(".succemsg").html(e),jQuery('#enquery_now')..prop('disabled', false); }}),!1
    })


});



/*
function changeBuyUrl(){
    var sbr = $('#search-buy-rent').val();
    var stype = $('#search-type').val();
    var splace = $('#search-place').val();

    var base_url = $('#property-search-form').attr('action') +'/';
    var url = base_url;
    if(splace && sbr && stype){
        url = base_url + $('#search-place option:selected').text().toLowerCase() + '/' + $('#search-buy-rent').val().toLowerCase() + '/'+ $('#search-type option:selected').text().toLowerCase();
    }else if(splace && sbr){
        url = base_url + $('#search-place option:selected').text().toLowerCase() + '/' + $('#search-buy-rent').val().toLowerCase() + '/';
    }else if(splace && stype){
        url = base_url + $('#search-place option:selected').text().toLowerCase() + '/all/' + $('#search-type option:selected').text().toLowerCase();
    }else  if(sbr && stype){
        url = base_url +"all/"+ $('#search-place option:selected').text().toLowerCase() + '/'+ $('#search-type option:selected').text().toLowerCase();
    }

    */
/*console.log(splace, url);
    if(splace){
        url = url + $('#search-place option:selected').text().toLowerCase() + '/';
    }else{
        url = url + 'all/';
    }

    console.log(sbr, url);
    if(sbr){
        url = url + $('#search-buy-rent').val().toLowerCase() + '/';
    }else{
        url = url + 'all/';
    }

    console.log(stype, url);
    if(stype){
        url = url + $('#search-type option:selected').text().toLowerCase();
    }*//*


    $('#property-search-form').attr('action',url);
    console.log(url);
}*/
