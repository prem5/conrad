(function($) {
    function render_map( $el ) {
        var $markers = $el.find('.marker');
        var args = {
            zoom		: 18,
            center: new google.maps.LatLng(latitude,longitude),
            mapTypeId	: google.maps.MapTypeId.ROADMAP,
            styles: [{
                featureType: 'water',
                elementType: 'all',
                stylers: [
                    { hue: '#e0e0e0' },
                    { saturation: -100 },
                    { lightness: 18 },
                    { visibility: 'on' }
                ]
            },{
                featureType: 'landscape',
                elementType: 'all',
                stylers: [
                    { hue: '#ececec' },
                    { saturation: -100 },
                    { lightness: 54 },
                    { visibility: 'on' }
                ]

            },{
                featureType: 'road.highway',
                elementType: 'all',
                stylers: [
                    { hue: '#616161' },
                    { saturation: -100 },
                    { lightness: -41 },
                    { visibility: 'on' }
                ]
            },{
                featureType: 'poi',
                elementType: 'all',
                stylers: [
                    { hue: '#ececec' },
                    { saturation: -100 },
                    { lightness: 66 },
                    { visibility: 'on' }
                ]
            }
            ]};
        var map = new google.maps.Map( $el[0], args);
        map.markers = [];
        $markers.each(function(){
            add_marker( $(this), map );
        });
        center_map( map );
    }
    function add_marker( $marker, map ) {
        var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
        var marker = new google.maps.Marker({
            position	: latlng,
            map			: map
        });
        map.markers.push( marker );
        if( $marker.html() )
        {
            var infowindow = new google.maps.InfoWindow({
                content		: $marker.html(),
            });
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open( map, marker );
            });
        }
    }
    function center_map( map ) {
        var bounds = new google.maps.LatLngBounds();
        $.each( map.markers, function( i, marker ){
            var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
            bounds.extend( latlng );
        });
        if( map.markers.length == 1 )
        {
            map.setCenter( bounds.getCenter() );
            map.setZoom( 18 );
        }
        else
        {
            map.fitBounds( bounds );
        }
    }
    $(document).ready(function(){
        $('.contact-map').each(function(){
            render_map( $(this) );
        });
    });
})(jQuery);



var gmarkers = [];
var htmls = [];
var to_htmls = [];
var from_htmls = [];
var map = null;
//latitude = 10.551506;
longitude = 100.034031;
function createMarker(latlng, name, html) {
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(settings.latitude,settings.longitude),
        icon: '/assets/images/icon.png',
        map: map,
        zIndex: Math.round(latlng.lat()*-100000)<<5
    });

    var i = gmarkers.length;

    html = html + '' +
        '<form action="https://maps.google.com/maps" method="get"" target="_blank"><strong>Directions :</strong>' +
        '<input type="text" SIZE=40 MAXLENGTH=40 name="daddr" id="daddr" value="" placeholder="To:" /><br>' +
        '<INPUT value="Get Directions" TYPE="SUBMIT">' +
        '<input type="hidden" name="saddr" value="' + latlng.lat() + ',' + latlng.lng() + '"/>';

    var contentString = html;

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(html);
        infowindow.open(map,marker);
    });
    gmarkers.push(marker);
    htmls[i] = html;

}

function myclick(i) {
    google.maps.event.trigger(gmarkers[i], "click");
    map.setCenter(new google.maps.LatLng(9.551814680446547,100.03422653259302));
}

function tohere(i) {
    infowindow.setContent(to_htmls[i]);
    infowindow.open(map,gmarkers[i]);
}
function fromhere(i) {
    infowindow.setContent(from_htmls[i]);
    infowindow.open(map,gmarkers[i]);
}

function initialize() {
    debugger;
    var myOptions = {
        zoom: 18,
        scrollwheel : false,
        center: new google.maps.LatLng(latitude+50,longitude),
        mapTypeControl: true,
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [{
            featureType: 'water',
            elementType: 'all',
            stylers: [
                { hue: '#e0e0e0' },
                { saturation: -100 },
                { lightness: 18 },
                { visibility: 'on' }
            ]
        },{
            featureType: 'landscape',
            elementType: 'all',
            stylers: [
                { hue: '#ececec' },
                { saturation: -100 },
                { lightness: 54 },
                { visibility: 'on' }
            ]

        },{
            featureType: 'road.highway',
            elementType: 'all',
            stylers: [
                { hue: '#616161' },
                { saturation: -100 },
                { lightness: -41 },
                { visibility: 'on' }
            ]
        },{
            featureType: 'poi',
            elementType: 'all',
            stylers: [
                { hue: '#ececec' },
                { saturation: -100 },
                { lightness: 66 },
                { visibility: 'on' }
            ]
        }
        ]

    }
    map = new google.maps.Map(document.getElementById("map-container"), myOptions);
    var bounds = new google.maps.LatLngBounds();


    google.maps.event.addListener(map, 'click', function() {
        infowindow.close();
    });

    // obtain the attribues of each marker 9.558545, 100.045357
    var lat = parseFloat(latitude);
    var lng = parseFloat(longitude);
    var point = new google.maps.LatLng(latitude,longitude);
    var html = "<p><strong>Conrad Properties,</strong><br>" + settings.address + "</p>";
    var label = "";
    // create the marker
    var marker = createMarker(point,label,html);
    bounds.extend(point);
    myclick(0);

    zoomChangeBoundsListener = google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
        if (num_markers = 1){
            this.setZoom(18);
            new google.maps.event.trigger( gmarkers[0], 'click' );
        }
    });
    setTimeout(function(){google.maps.event.removeListener(zoomChangeBoundsListener)}, 3000);

    var listener = google.maps.event.addListener(map, "idle", function () {
        google.maps.event.removeListener(listener);
    });
    new google.maps.event.trigger( gmarkers[0], 'click' );
}
var infowindow = new google.maps.InfoWindow({
    size: new google.maps.Size(150,50)
});

google.maps.event.addDomListener(window, 'load', initialize);
