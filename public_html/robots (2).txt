Sitemap: https://www.conradproperties.asia/sitemap.xml

User-agent: *
Disallow: /uploads/
Disallow: /blog-news/tag/
Disallow: /*buy?*
Disallow: /*rent?*
Disallow: /*apart-condos?*
Disallow: /assets/
Disallow: /administrator/

User-agent: *
Crawl-delay: 30