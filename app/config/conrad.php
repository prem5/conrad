<?php

return array(
    'blogs' => '/uploads/blogs/',
    'guides' => '/uploads/guides/',
    'properties' => '/uploads/properties/',
    'default' => '/assets/images/default_img.jpg',
    'buy_sell_select' => array('' => 'BUY or RENT', 'buy' => 'BUY', 'rent' => 'RENT'),

    'property_view'=>array(
      ""=>"None",
        "Sea View"=>"Sea View",
        "Mountain View"=>"Mountain View",
        "Pool View"=>"Pool view",
        "Garden View"=>"Garden View"
    ),

    'bedrooms_select' => array(
        '' => 'Bedrooms',
        ' '=>'Any', '1' => '1 bedroom',
        '2' => '2 bedroom',
        '3' => '3 bedroom',
        '4' => '4 bedroom',
        '5' => '5 bedroom',
        '>5' => 'More Than 5',
        '1-2'=> '1 -2 BED',
        '2-3'=> '2 -3 BED',
        '3-4'=> '3 -4 BED',
        '4-5'=> '4 -5 BED',
    ),
    'price_buy_select' => array(
        '' => 'Price',
        ' ' => 'Any',
        '<5' => '<5M THB',
        '5-10' => '5 - 10M THB',
        '10-20' => '10 - 20M THB',
        '20-30' => '20 - 30M THB',
        '>30'  =>  '>30M THB'

    ),

    'price_rent_select' => array(
            ''=>'PRICE',
            'daily'=>"DAILY",
           // 'daily-<10' =>'<10K THB',
            'daily-5-10'=>'5-10K THB',
            'daily-10-20'=>'10-20K THB',
            'daily-20-30'=>'20-30K THB',
            'daily-30-50'=>'30-50K THB',
//            'daily-50-70'=>'50-70K THB',
//            'daily-70-100'=>'70-100K THB',
            'daily->50'=>'>50K THB',
            'monthly'=>"MONTHLY",
//            'monthly-<20'=>'<20K THB',
            'monthly-30-50'=>'30-50K THB',
            'monthly-50-70'=>'50-70K THB',
//            'monthly-40-50'=>'50K THB',
            'monthly-70-100'=>'70 -100K THB',
            'monthly-100-150'=>'100 -150K THB',
            'monthly->150'=>'>150K THB',

        /*'' => 'Price',
        '15000–25000' => '15,000 – 25,000',
        '20000–30000' => '20,000 – 30,000',
        '25000–35000' => '25,000 – 35,000',
        '30000–40000' => '30,000 – 40,000',
        '35000–45000' => '35,000 – 45,000',
        '40000-60000' => '40,000 – 60,000',
        '50000-70000' => '50,000 – 70,000',
        '60000-80000' => '60,000 – 80,000',
        '70000-90000' => '70,000 – 90,000',
        '100000' => '100 +',*/
    ),

    'terms_select'=> array(
        ''=>'Terms',
        'sale'=>'FOR SALE Sale',
        'rent'=>'FOR RENT',
        'sale-rent'=>'BOTH'
    ),

    'contact_subject_select' => [
        'I am looking to buy property' => 'I am looking to buy property',
        'I am looking to rent a property' => 'I am looking to rent a property',
        'I would like to list my property' => 'I would like to list my property',
        'Other'=>'Other'
    ],

    'image_sizes_blog' => array(
        'blog' => '693x300', // min size for sb-admin2.js
        'blog@2' => '1386x600',

        'blog-thumb' => '225x135',
        'blog-thumb@2' => '450x270'
    ),
    'image_sizes_guide' => array(
        'guide' => '693x300', // min size for sb-admin2.js
        'guide@2' => '1386x600',

        'guide-thumb' => '225x135',
        'guide-thumb@2' => '450x270'
    ),
    'image_sizes_property' => array(
        'slider' => '1400x665',
        'slider@2' => '2800x1330',

        'property-small' => '312x192',
        'property-small@2' => '624x384',

        'property-main' => '741x391', // min size for sb-admin2.js
        'property-main@2' => '1482x982',

        'property-thumb' => '106x64',
        'property-thumb@2' => '212x128'
    ),

    'gmap_key' => '',
    'property-marker-count' => 12,
    'currencies' => array(
        'THB' => 'THAILAND',
        'GBP' => 'ENGLAND',
        'EUR' => 'EUROPE',
        'USD' => 'AMERICA',
        'CAD' => 'CANADA',
        'AUD' => 'AUSTRALIA',
        'RUB' => 'RUSSIAN',
        'SGD' => 'SINGAPORE',
        'HKD' => 'HONG KONG',
        'CNY' => 'CHINA',
        'MYR' => 'MALAYSIA',
        'INR' => 'INDIA',
        'KRW' => 'SWEDEN',
        'NOK' => 'NORWEGIAN',
        'CHF' => 'SWITZERLAND'
    ),
    'languages' => ['eng' => 'English','fra' => 'French','thai' => 'Thai','chin' =>'Chinese','rus' =>'Russian'],
    'short_options' => [
        '1' => 'Show All',
        '2' => 'Recommended',
        '3' => 'Price high - low',
        '4' => 'Price low - high',
        '5' => 'Featured'
    ],
);