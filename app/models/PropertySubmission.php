<?php


class PropertySubmission extends Eloquent
{


    protected $fillable = [
        "name",
        "email",
        "phone",
        "property_type",
        "terms",
        "location",
        "price",
        "image",
        "extra_information"

    ];

    public static $rules = array(
        "name" => "required",
        "email" => "required",
        "property_type" => "required",
        "location" => "required",
        "price" => "required",
        "image" => "image|mimes:jpg,jpeg,png",
        "extra_information" => "required"
    );

    protected function propertyTypes()
    {
        return $this->belongsTo('PropertyType', 'property_type');
    }

    public  function getLocation()
    {
        $plc = "";
        $loc =explode("_",$this->location);
        if($loc[0]=="area")
        {
           $plc = \Area::findOrFail($loc[1])->name;
        }
        if($loc[0]=="city")
        {
            $plc = \City::findOrFail($loc[1])->name;
        }

        return $plc;
    }
}