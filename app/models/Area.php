<?php

class Area extends \Eloquent
{
    use UpdatedByTrait;

    protected $fillable = array(
        'name',
        'city_id',
        'slug',
        'status'
    );
    public static $rules = array(
        'name' => 'required',
        'slug' => 'required',
        'city_id' => 'required'
    );

    public function city()
    {
        return $this->belongsTo('City', 'city_id');
    }

    public static function getAreaForSelect()
    {
        $data = array('' => 'Location Area');
        foreach (self::get(array('id', 'name')) as $v) {
            $data[$v->id] = $v->name;
        }
        return $data;
    }

    public static function getRandArea()
    {
        return self::where('status','=','1')->get();
    }

}