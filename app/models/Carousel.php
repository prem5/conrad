<?php

class Carousel extends \Eloquent
{

    protected $fillable = [
        'property_id',
        'order',
        'image'
    ];
    public static $rules = array(
        'property_id' => 'required',
        'order' => 'required',
        'image' =>'required',
    );

    protected static function bannerOrder()
    {
        return self::orderBy('order', 'Asc');
    }

    public static function getBanner()
    {
        $data = array();
        $banner = self::bannerOrder()->get();
        foreach ($banner as $b):
            $return = array();
            $return['images'] = Config::get('conrad.properties') . $b->property_id . '/thumb/' . $b->image;
            $property = \Property::find($b->property_id);
            $return['title'] = $property->title;
            $return['url'] = $property->getUrl();
            $return['address'] = $property->getFullAddress();
            $return['price'] = $property->getPrice();
            $data[] = $return;
        endforeach;
        return $data;


    }

    public static function getCarouselImage($id)
    {
        $banner = self::where('id','=',$id)->first();
        if($banner->image!='')
        {
        $newarray = explode(".", $banner->image);
        $thumb = "$newarray[0].$newarray[1]";
        $images = Config::get('conrad.properties') . $banner->property_id ."/thumb/" . $thumb;
        return url($images);
        }else{

                return url(Config::get('conrad.default'));
           
        }


    }
}