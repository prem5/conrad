<?php

class ExchangeRate extends \Eloquent
{


    protected $fillable = [
        'exchange_code',
        'rate',
        'date'
    ];

    public static $rules = array(
        'exchange_code' => 'required',
        'rate' => 'required',
        'data' => 'required'
    );
    public static  function getExchangeRate(){
        if(Input::has('currency') && array_key_exists(Input::get('currency'),Config::get('conrad.currencies')) ){
            $code = Input::get('currency');
        }
        elseif(Session::has('exchange_rate')){
            $code = Session::get('exchange_rate')['exchange_code'];
        }
        else{
            $code = \Setting::getDefaultCurrency();
        }

        if(\ExchangeRate::where('date',date("m/d/Y"))->where('exchange_code', $code )->count() < 1){

                $result = file_get_contents('http://www.apilayer.net/api/live?access_key=f53880369da0131049a4c43c7723ba0a');
                $data = json_decode($result,true);
                $new =[];
                foreach ($data['quotes'] as $index => $value){
                    // $array = ['THBTHB','THBGBP','THBEUR','THBUSD','THBCAD','THBAUD','THBRUB','THBSGD','THBHKD','THBCNY','THBMYR','THBINR','THBKRW','THBNOK','THBCHF'];
                    $array = ['USDTHB','USDGBP','USDEUR','USDUSD','USDCAD','USDAUD','USDRUB','USDSGD','USDHKD','USDCNY','USDMYR','USDINR','USDKRW','USDNOK','USDCHF'];
                    
                   if(in_array($index,$array)){
                        $new[$index] = $value;
                   }
                }
                foreach ($new as $key => $value) {
                  $exchange_rate = new \ExchangeRate ;
                    $exchange_rate->exchange_code = substr($key,3); //substr($rate[0],3,-2);
                    $exchange_rate->rate = ($value/$new['USDTHB']); //$rate[1];
                    $exchange_rate->date = date('m/d/Y');
                    $exchange_rate->save();
               }

            // $url =
            //     'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s=THBTHB=X,THBGBP=X,THBEUR=X,THBUSD=X,THBCAD=X,THBAUD=X,THBRUB=X,THBSGD=X,THBHKD=X,THBCNY=X,THBMYR=X,THBINR=X,THBKRW=X,THBNOK=X,THBCHF=X';
            // // $file = @fopen('http://localhost/quotes.csv', 'r');
            //     $file = @fopen('https://omniitworld.com/demo/quotes.csv','r');
            // $file = @fopen($url, 'r');
            // while(! feof($file))
            // {
            //     $rate = fgetcsv($file);
            //     $exchange_rate = new \ExchangeRate ;
            //     $exchange_rate->exchange_code = substr($rate[0],3,-2);
            //     $exchange_rate->rate = $rate[1];
            //     $exchange_rate->date = date('m/d/Y');
            //     $exchange_rate->save();
            // }
            // fclose($file);
        }
        $exchange_rate = ExchangeRate::where('exchange_code', $code )->orderBy('date','DESC')->first();
        return $exchange_rate;
    }
    public static  function getRate($exchange_code){
        return  ExchangeRate::where('exchange_code',$exchange_code)->orderBy('date','DESC')->first();
    }
}