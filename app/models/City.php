<?php

class City extends \Eloquent
{
    use UpdatedByTrait;

    protected $fillable = array(
        'name',
        'province_id',
        'slug',
        'status'
    );
    public static $rules = array(
        'name' => 'required',
        'slug' => 'required',
        'province_id' => 'required'
    );

    public function province()
    {
        return $this->belongsTo('Province', 'province_id');
    }

    public function area()
    {
        return $this->hasMany('Area', 'city_id', 'id');
    }

    public static function getCitiesForSelect()
    {
        $data = array('' => 'Location City');
        foreach (self::get(array('id', 'name')) as $v) {
            $data[$v->id] = $v->name;
        }
        return $data;
    }

    public static function getSearchSelect()
    {
        $data = array('' => 'Location',' '=>'ANY');
        foreach (self::orderBy('order','ASC')->get() as $v)  {
            $data['city_' . $v->id] = $v->name;
            if ($v->area()->count() > 0) {
                foreach ($v->area()->get() as $a) {
                    $data['area_' . $a->id] = $a->name;
                }
            }
        }
        return $data;
    }
}