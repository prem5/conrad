<?php


class PropertyType extends \Eloquent
{
    use UpdatedByTrait;

    protected $fillable = [
        'label',
        'status'
    ];

    public static $rules = array(
        'label' => 'required'
    );

    public static function getIdBySlug($label)
    {
        if($label == 'laxury'){
            $data = self::where('label','Villa/House')->first();
        }
        else{
            $data = self::where(DB::raw("REPLACE( `label` , '/', '' )"), str_replace('-', ' ', $label))->first();

        }
        if ($data) {
            return $data->id;
        }
        return $data;
    }

    public static function getTypeForSelect()
    {
        $data = array('' => 'Type',' '=>'ANY');
        foreach (self::orderBy('order','ASC')->where('status','=','1')->get() as $v) {
            $data[$v->id] = $v->label;
        }

        return $data;
    }
} 