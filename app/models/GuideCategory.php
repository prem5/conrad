<?php

/**
 * Created by PhpStorm.
 * User: bishnu
 * Date: 7/10/15
 * Time: 12:32 PM
 */
class GuideCategory extends \Eloquent
{
    use UpdatedByTrait;

    protected $fillable = array(
        'id',
        'name',
        'image'
    );
    public static $rules = array(
        'name' => 'required'
    );

    public static function getGuideCategories()
    {
        $data = array('' => 'Select Any');
        foreach (self::all() as $v) {
            $data[$v->id] = $v->name;
        }

        return $data;
    }
} 