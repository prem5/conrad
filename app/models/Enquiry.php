<?php

class Enquiry extends \Eloquent
{
    protected $fillable = [
        'name',
        'phone',
        'email',
        'property_id',
        'subject',
        'message'
    ];
    public static $rules = array(
        'name' => 'required',
        'phone' => 'required',
        'email' => 'required',
        'subject' => 'required',
        'message' => 'required'
    );

    public function getPropertyName()
    {
        return $this->hasMany('Property', 'id', 'property_id')->get();
    }
} 