<?php

use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\UserTrait;

class User extends Eloquent implements UserInterface, RemindableInterface
{

    use UserTrait,
        RemindableTrait,
        UpdatedByTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    protected $fillable = array('firstname', 'lastname', 'email', 'password', 'role_id','status');
    public static $rules = array(
        'firstname' => 'required|alpha|min:2',
        'lastname' => 'required|alpha|min:2',
        'email' => 'required|email|unique:users',
        'password' => 'required|alpha_num|between:6,12|confirmed',
        'password_confirmation' => 'required|alpha_num|between:6,12'
    );
    public static $edit_rules = array(
        'password' => 'required|alpha_num|between:6,12|confirmed',
        'password_confirmation' => 'required|alpha_num|between:6,12'
    );

    public static $profilerules = array(
        'firstname' => 'required|alpha|min:2',
        'lastname' => 'required|alpha|min:2'
    );

    public static $prules = array(
        'firstname' => 'required|alpha|min:2',
        'lastname' => 'required|alpha|min:2',
        'email' => 'required|email'
    );


    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Checks if user is admin
     *
     * @return boolean
     */
    public function isAdmin()
    {
        if ($this->role_id == 1 && $this->status == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isWriter()
    {
        if ($this->role_id == 2 && $this->status == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function isMaster(){
        if($this->role_id == 3 && $this->status == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function role()
    {
        return $this->belongsTo('Role', 'role_id')->first();
    }

}
