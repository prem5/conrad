<?php

/**
 * @property mixed restaurant_id
 * @property mixed photo
 */
class Gallery extends \Eloquent
{

    use UpdatedByTrait;

    protected $fillable = array(
        'property_id',
        'photo',
        'primary',
        'order',
        'status'
    );

    public static $rules = array(
        'photo' => 'required|mimes:jpg,jpeg,png'
    );

    public function property()
    {
        return $this->belongsTo('Property');
    }

    public function getPhotoPath()
    {
        return url(Config::get('conrad.properties') . $this->property_id . "/" . $this->photo);
    }

    public function getPhotoThumbPath()
    {
        $newarray = explode(".", $this->photo);
        if (count($newarray) > 1) {
            $thumb = "$newarray[0]-property-thumb.$newarray[1]";
            return url(Config::get('conrad.properties') . $this->property_id . "/thumb/" . $thumb);
        } else {
            return url(Config::get('conrad.default'));
        }

    }

    public function getPhotoSliderPath()
    {
        $newarray = explode(".", $this->photo);
        if (count($newarray) > 1) {
            $thumb = "$newarray[0]-property-main.$newarray[1]";
            $path = Config::get('conrad.properties') . $this->property_id . "/thumb/" . $thumb;

            if (file_exists(public_path() . $path))
                //dd(public_path() . Config::get('conrad.properties') . $this->property_id . "/ " . $this->photo)
                return url($path);
            else if (file_exists(public_path() . Config::get('conrad.properties') . $this->property_id . "/" . $this->photo))
                return url(Config::get('conrad.properties') . $this->property_id . "/" . $this->photo);
            else
                return url(Config::get('conrad.default'));
            //return url(Config::get('conrad.properties') . $this->property_id . "/" . $this->photo);
        } else {
            return url(Config::get('conrad.default'));
        }

    }

    public function getPhotoSliderThumbPath()
    {
        $newarray = explode(".", $this->photo);
        if (count($newarray) > 1) {
            $thumb = "$newarray[0]-property-thumb.$newarray[1]";
            return url(Config::get('conrad.properties') . $this->property_id . "/thumb/" . $thumb);
        } else {
            return url(Config::get('conrad.default'));
        }

    }

    public function deleteFiles()
    {
        $newarray = explode(".", $this->photo);
        $thumb = "$newarray[0]-thumb.$newarray[1]";

        File::delete(
            public_path() . '/' . Config::get('conrad.properties') . $this->property_id . "/" . $this->photo,
            public_path() . '/' . Config::get('conrad.properties') . $this->property_id . "/thumb/" . $thumb
        );
    }


}
