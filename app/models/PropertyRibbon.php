<?php

class PropertyRibbon extends \Eloquent
{
    use UpdatedByTrait;

    protected $fillable = [
        'label',
        'color',
        'status'
    ];

    public static $rules = array(
        'label' => 'required',
        'color' => 'required'
    );

    public static function getRibbonForSelect()
    {
        $data = array('' => 'Select Any');
        foreach (self::all() as $v) {
            $data[$v->id] = $v->label;
        }

        return $data;
    }
} 