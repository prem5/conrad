<?php

class Page extends \Eloquent
{

    use UpdatedByTrait;

    protected $fillable = [
        'name',
        'content',
        'meta_title',
        'slug',
        'meta_description'
    ];
    public static $rules = array(
        'name' => 'required',
        'content' => 'required',
        'meta_title' => 'required',
        'meta_description' => 'required',
        'slug' => 'required'
    );
    protected static function getActive()
    {
        return self::where('status', '=', 1);
    }
    public function getUrl()
    {
        return url($this->slug);
    }

}