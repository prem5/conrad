<?php

class Currency extends \Eloquent
{

    use UpdatedByTrait;

    protected $fillable = [
        'symbol',
        'rate',
        'status'
    ];

    public static $rules = array(
        'symbol' => 'required',
        'rate' => 'required'
    );

    public static function getDefaultCurrency()
    {
        $data = array('' => 'Select Any');
        foreach (self::all() as $v) {
            $data[$v->id] = $v->symbol;
        }

        return $data;
    }
}