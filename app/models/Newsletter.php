<?php

class Newsletter extends \Eloquent
{
    use UpdatedByTrait;

    protected $fillable = [
        'name',
        'email'
    ];

    public static $rules = array(
        'name' => 'required',
        'email' => 'required|email'
    );

    public static $erules = array(
        'email' => 'required|email'
    );
} 