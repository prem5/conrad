<?php

class Province extends \Eloquent
{

    use UpdatedByTrait;

    protected $fillable = array(
        'name',
        'code',
        'slug',
        'country_id',
        'status'
    );
    public static $rules = array(
        'name' => 'required',
        'slug' => 'required',
        'code'=>'required',
    );

    public function country()
    {
        return $this->belongsTo('Country', 'country_id');
    }

    public function city()
    {
        return $this->hasMany('City', 'province_id', 'id');
    }

    public static function getProvinceForSelect()
    {
        $data = array('' => 'Select Any');
        foreach (self::all() as $v) {
            $data[$v->id] = $v->name;
        }

        return $data;
    }
    public function getCountrySlug(){
        return $this->country()->first()->slug;
    }

}
