<?php

class Setting extends \Eloquent
{
    use UpdatedByTrait;

    protected $fillable = [
        'title',
        'description',
        'primary_email',
        'display_email',
        'display_phone',
        'additional_email',
        'address',
        'primary_currency',
        'analtyics_code',
        'twitter_url',
        'facebook_url',
        'instagram_url',
        'pinterest_url',
        'google_plus_url',
        'linkedin_url',
        'latitude',
        'longitude'
    ];

    public static $rules = array(
        'title' => 'required',
        'description' => 'required',
        'primary_email' => 'required',
        'display_phone' => 'required',
        'address' => 'required',
        'latitude'=>'required',
        'longitude'=>'required'
    );

    public function currency()
    {
        return $this->belongsTo('Currency', 'primary_currency');
    }

    public static function getAddressForContact()
    {
        $data = Setting::find(1);

        return ['address'=>nl2br($data->address),'latitude'=>$data->latitude,'longitude'=>$data->longitude];
    }

    public static function getDefaultCurrency(){
        $data = Setting::find(1);
        $curr = $data->currency()->first();
        if($curr){
            return $curr->symbol;
        }
        else{
            return 'THB';
        }

    }

} 