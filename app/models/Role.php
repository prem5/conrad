<?php

class Role extends \Eloquent
{

    use UpdatedByTrait;

    // Add your validation rules here
    public static $rules = [
        'name' => 'required'
    ];
    // Don't forget to fill this array
    protected $fillable = ['name'];

    public static function getRoleForSelect()
    {
        $data = array('' => 'Select Any');
        foreach (self::all() as $v) {
            $data[$v->id] = $v->name;
        }

        return $data;
    }

}
