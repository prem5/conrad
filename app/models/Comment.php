<?php

class Comment extends \Eloquent
{

    use UpdatedByTrait;

    protected $fillable = [
        'name',
        'email',
        'message',
        'blog_id',
        'status'
    ];

    public  static $rules = array(
        'name'=>'required',
        'email'=>'required',
        'message'=>'required'

    );

    public function blog()
    {
        return $this->belongsTo('Blog', 'blog_id', 'id');
    }

    protected static function getActive()
    {
        return self::where('status', '=', 1);
    }

    public static function getUnpublish()
    {
        return self::where('status', '=', '0');
    }
}