<?php

class Country extends \Eloquent
{

    use UpdatedByTrait;

    protected $fillable = array(
        'id',
        'name',
        'slug',
        'status'
    );
    public static $rules = array(
        'name' => 'required',
        'slug' => 'required'

    );

    public static function getcountryForSelect()
    {
        $data = array('' => 'Select Any');
        foreach (self::all() as $v) {
            $data[$v->id] = $v->name;
        }

        return $data;
    }

    public function province()
    {
        return $this->hasMany('Province', 'country_id', 'id');
    }

    public function getProvinceById()
    {

    }

    public static function getActive()
    {
        return self::where('status', '=', 1);
    }

}
