<?php

class Blog extends \Eloquent
{
    use UpdatedByTrait;
    protected $fillable = [
        'name',
        'slug',
        'content',
        'teaser',
        'meta_title',
        'meta_description',
        'photo',
        'status',
        'created_at'

    ];

    public static $rules = array(
        'name' => 'required',
        'slug' => 'required',
        'content' => 'required',
        'teaser' => 'required',
        'meta_title' => 'required',
        'meta_description' => 'required'


    );

    public static function inst(array $attributes)
    {
        $model = new static($attributes);
        return $model;
    }

    public function tags()
    {
        return $this->belongsToMany('Tag', 'blog_tag');
    }

    public static  function getBlogListing()
    {
        $date = date('Y-m-d');
        return self::where('created_at', '<=',$date )->orderBy('created_at','desc');
    }

    public function comments()
    {
        return $this->hasMany('Comment', 'blog_id', 'id');
    }

    protected static function getActive()
    {
        return self::where('status', '=', 1);
    }

    public function getActiveComments()
    {
        return $this->comments()->where('status', '=', 1);
    }


    public function getUrl()
    {
        return url("blog-news/" . str_replace( 'http://', 'https://', $this->slug ));
    }

    public function getBlogMainImagePath()
    {
        $newarray = explode(".", $this->photo);
        if (count($newarray) > 1) {
            $thumb = "$newarray[0]-blog.$newarray[1]";
            $path = Config::get('conrad.blogs') .$thumb;
            if (file_exists(public_path() . $path)) {
            return url($path);
            }
            else
            {
                return url(Config::get('conrad.blogs') . $this->photo);
            }
        } else {
            return url(Config::get('conrad.default'));
        }

    }

    public function getBlogThumbImagePath()
    {
        $newarray = explode(".", $this->photo);
        if (count($newarray) > 1) {
            $thumb = "$newarray[0]-blog-thumb.$newarray[1]";
            return url(Config::get('conrad.blogs') . $thumb);
        } else {
            return url(Config::get('conrad.default'));
        }

    }

    public static function getRandomBlogData()
    {
        $blogs = self::getActive();
        $blogs->orderBy(DB::raw('RAND()'));
        $blogs->take(7);
        return $blogs->get();
    }

    public function trimTeaser($char = 20)
    {
        return str_limit($this->teaser, $char, $end = '...');
    }


    /**** writer ***/
    public static function  getMyBlogs()
    {
        return self::where('created_by', '=', Auth::user()->id);
    }

    public function delete()
    {
        // delete all related properties carousel
        $this->comments()->delete();

        // delete the properties
        return parent::delete();
    }
}
