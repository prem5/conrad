<?php

class District extends \Eloquent
{

    use UpdatedByTrait;

    protected $fillable = array(
        'id',
        'name',
        'province_id',
        'parent_id',
        'status'
    );
    public static $rules = array(
        'name' => 'required'
    );

    public function alias()
    {
        return $this->hasMany('DistrictAlias', 'district_id', 'id');
    }

    public static function getDistrictForSelect($onlysubdisritct = true)
    {

        $data = array('' => 'Location');
        if ($onlysubdisritct) {
            foreach (self::where('parent_id', '0')->where('province_id', '1')->get() as $v) {
                $data[$v->id] = $v->name;
            }
        } else {
            foreach (self::get(array('id', 'name')) as $v) {
                $data[$v->id] = $v->name;
            }
        }


        return $data;
    }

    public static function getAllDistrict()
    {
        $data = array();

        $alias = self::all();

        foreach ($alias as $als) {
            $data["$als->id"] = $als->name;
        }

        return $data;
    }

    public static function getSubdistrictForSelect()
    {
        $data = array('' => 'Area');
        foreach (self::where('parent_id', '!=', '0')->orderBy('name')->get() as $v) {
            $data[$v->id] = $v->name;
        }
        return $data;
    }

    public static function getIdBySlug($place)
    {
        $data = self::where('name', str_replace('-', ' ', $place))->first();
        if ($data) {
            return $data->id;
        }
        return $data;
    }

}
