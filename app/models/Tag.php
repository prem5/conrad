<?php

class Tag extends \Eloquent
{

    protected $fillable = [
        'name',
        'slug'
    ];

    public static $rules = array(
        'name' => 'required',
        'slug' => 'required'
    );

    public function blog()
    {
        return $this->belongsToMany('Blog', 'blog_tag');
    }

    public static function getAllTag()
    {
        $data = array();

        $alias = self::all();

        foreach ($alias as $als) {
            $data["$als->id"] = $als->name;
        }

        return $data;
    }

    public function getUrl()
    {
        return url('blog-news/tag/' . $this->slug);
    }
}