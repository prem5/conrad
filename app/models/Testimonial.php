<?php

class Testimonial extends \Eloquent
{

    use UpdatedByTrait;

    protected $fillable = [
        'quote',
        'name',
        'status'
    ];

    public static $rules = array(
        'quote' => 'required',
        'name' => 'required'
    );
} 