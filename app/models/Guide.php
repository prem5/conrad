<?php

class Guide extends \Eloquent
{

    use UpdatedByTrait;
    protected $fillable = [
        'name',
        'slug',
        'content',
        'teaser',
        'guide_category_id',
        'meta_title',
        'meta_description',
        'photo',
        'status'

    ];
    public static $rules = array(
        'name' => 'required',
        'slug' => 'required',
        'content' => 'required',
        'guide_category_id' => 'required',
        'teaser' => 'required',
        'meta_title' => 'required',
        'meta_description' => 'required'


    );

    public function category()
    {
        return $this->belongsTo('GuideCategory', 'guide_category_id');
    }

    protected static function getActive()
    {
        return self::where('status', '=', 1);
    }

    public static function getAll()
    {
        return self::where('status', '=', 1)->orderBy('id')->get();
    }

    public function getUrl()
    {
        return url("buyers-guide/" . $this->slug);
    }

    public static  function getBuyerGuide()
    {
        return self::where('status', '=', 1)->where('name','!=','10 Step Property Buying Guide')->where('name','!=','10 Step Land Buying Guide')->orderBy('id')->get();
    }

    public function getNext()
    {
        if ($this->where('id', '>', $this->id)->first()) {
            return $this->where('id', '>', $this->id)->first();
        } else {
            return $this->first();
        }

    }

    public function getPrevious()
    {
        if ($this->where('id', '<', $this->id)->first()) {
            return $this->where('id', '<', $this->id)->first();
        } else {
            return $this->orderBy('id', 'desc')->first();
        }
    }

    public function getGuideMainImagePath()
    {
        $newarray = explode(".", $this->photo);
        if (count($newarray) > 1) {
            $thumb = "$newarray[0]-guide.$newarray[1]";
            return url(Config::get('conrad.guides') . $thumb);
        } else {
            return url(Config::get('conrad.default'));
        }

    }

    public function getGuideThumbImagePath()
    {
        $newarray = explode(".", $this->photo);
        if (count($newarray) > 1) {
            $thumb = "$newarray[0]-guide-thumb.$newarray[1]";
            return url(Config::get('conrad.guides') . $thumb);
        } else {
            return url(Config::get('conrad.default'));
        }

    }

} 