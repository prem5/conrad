<?php

class Template extends \Eloquent
{

    protected $fillable = [
        'name',
        'title',
        'subject',
        'message'
    ];
    public static $rules = array(
        'name' => 'required|unique:templates',
        'title' => 'required',
        'subject' => 'required',
        'message' => 'required',
    );

    public static function getTemplates($slug)
    {
        return self::where('name', '=', $slug);
    }
}