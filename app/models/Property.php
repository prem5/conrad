<?php

class Property extends \Eloquent
{

    use UpdatedByTrait;

    // Add your validation rules here


    // Don't forget to fill this array
    protected $fillable = [
        "title",
        "slug",
        "reference_code",
        "overview",
        "key_features",
        "rent",
        "sale",
        "monthly_rental_price_from",
        "monthly_rental_price_to",
        "daily_rental_price_from",
        "daily_rental_price_to",
        "daily_rent",
        "monthly_rent",
        "sale_price",
        "country_id",
        "province_id",
        "city_id",
        "area_id",
        "type",
        "min_bedroom",
        "max_bedroom",
        "bedrooms",
        "bathrooms",
        "view",
        "parking",
        "swimming_pool",
        "land_area",
        "building_area",
        "ribbon",
        "features",
        "community_features",
        "living_room",
        "dining_room",
        "family_room",
        "kitchen",
        "study_office",
        "floors",
        "pets",
        "terrace_rooftop",
        "library",
        "cinema_room",
        "security",
        "tennis",
        "furnished",
        "rental_program",
        "restaurant",
        "concierge",
        "year_build",
        "latitude",
        "longitude",
        "published",
        "featured",
        "featured_order",
        "default_currency",
        'is_sold'

    ];

    public static $rules = [
        "title" => 'required|max:150',
        "slug" => 'required',
        //"reference_code" => 'required',
        "overview" => 'required',
        "key_features" => 'required',
        "country_id" => 'required',
        "province_id" => 'required',
        "city_id" => 'required',
        "area_id" => 'required',
        "type" => 'required',
        //"bedrooms" => 'required|max:40',
        "min_bedroom" => 'required|max:40',
        "bathrooms" => 'required|max:40',
        //"view" => 'required|max:40',
        "parking" => 'max:40',
        //"swimming_pool" => 'required|max:40',
        //"land_area" => 'required|max:10',
        //"building_area" => 'required|max:10',
       // "community_features" => 'required|max:500',
        ///"features" => 'required|max:500',
        "living_room" => 'max:40',
        "dining_room" => 'max:40',
        "family_room" => 'max:40',
        "kitchen" => 'max:40',
        "study_office" => 'max:40',
        "floors" => 'max:40',
        "pets" => 'max:40',
        "terrace_rooftop" => 'max:40',
        "library" => 'max:40',
        "cinema_room" => 'max:40',
        "security" => 'max:40',
        "tennis" => 'max:40',
        "furnished" => 'max:40',
        "rental_program" => 'max:40',
        "restaurant" => 'max:40',
        "concierge" => 'max:40',
        "year_build" => 'max:4',
    ];

    public static function inst(array $attributes)
    {
        $model = new static($attributes);
        return $model;
    }

    public static function getAll()
    {
        return  self::orderBy('published','DESC');
    }

    public function gallery()
    {
        return $this->hasMany('Gallery', 'property_id', 'id');
    }

    public function sub_district()
    {
        return $this->belongsTo('District', 'subdistrict_id');
    }

    public function district()
    {
        return $this->belongsTo('District', 'district_id');
    }

    public function area()
    {
        return $this->belongsTo('Area', 'area_id');
    }

    public function city()
    {
        return $this->belongsTo('City', 'city_id');
    }

    public function province()
    {
        return $this->belongsTo('Province', 'province_id');
    }

    public function country()
    {
        return $this->belongsTo('Country', 'country_id');
    }

    public function ribbon()
    {
        return $this->belongsTo('PropertyRibbon', 'ribbon');
    }

    public function type()
    {
        return $this->belongsTo('PropertyType', 'type');
    }

    public function carousel()
    {
        return $this->hasMany('Carousel');
    }

    protected static function getActive()
    {
        return self::where('published', '=', 1);
    }

    public function getRibbon()
    {
        if ($this->ribbon()->first()) {
            return $this->ribbon()->first()->label;
        }
        return False;
    }

    public function getSubDistrict()
    {
        if ($this->sub_district()->first()) {
            return $this->sub_district()->first()->name;
        }
        return False;
    }

    public function getDistrict()
    {
        if ($this->district()->first()) {
            return $this->district()->first()->name;
        }
        return False;
    }

    public function getArea()
    {
        if ($this->area()->first()) {
            return $this->area()->first()->name;
        }
        return False;
    }

    public function getCity()
    {
        if ($this->city()->first()) {
            return $this->city()->first()->name;
        }
        return False;
    }

    public function getProvince()
    {
        if ($this->province()->first()) {
            return $this->province()->first()->name;
        }
        return False;
    }

    public function getCountry()
    {
        if ($this->country()->first()) {
            return $this->country()->first()->name;
        }
        return False;
    }

    public function getType()
    {
        if ($this->type()->first()) {
            return $this->type()->first()->label;
        }
        return False;
    }

    public function getSellOrRent()
    {
        $text = Null;
        if ($this->sale == 1) {
            $text = 'for Sale';
        }elseif($this->rent == 1){
            $text = 'for Rent';
        }

        return $text;
    }

    public function getFullAddress()
    {
        $add[] = ($this->getArea()) ? $this->getArea() : "";
        $add[] = ($this->getProvince()) ? $this->getProvince() : '';

        return implode(', ', $add);
    }

    public function getAddressUrl()
    {
        $url = '#';
        if ($this->getArea() && $this->getProvince()) {
            $url = 'search/' . \Str::slug($this->getCountry()) . '/' . \Str::slug($this->getProvince()) . '/' . \Str::slug($this->getCity()) . '/' . \Str::slug($this->getArea());
        }

        return url($url);
    }

    public function getSlug()
    {
        return Str::slug($this->name);
    }

    public function getUrl()
    {
        return url("properties/" . $this->slug);
    }

    public function getThumbImage()
    {
        $filename = $this->slug . ".-thumb";
        $path = Config::get('conrad.properties') . $this->id . "/" . $filename;

        $path = Config::get('conrad.properties') . '1/thumb/villa-with-sunset-views-property-small.jpg';
        if (!file_exists(public_path() . $path)) {
            return url(Config::get('conrad.default'));
        }
        return url($path);
    }

    public function getPrimaryImageThumb()
    {
        if ($this->gallery()->count() <= 0) {
            return url(Config::get('conrad.default'));
        }
        if ($this->gallery()->where('order', '=', 1)->count() > 0)
            $galleryImage = $this->gallery()->where('order', '=', 1)->first();
        else
            $galleryImage = $this->gallery()->first();

        $newarray = explode(".", $galleryImage->photo);
        $thumb = "$newarray[0]-property-thumb.$newarray[1]";

        $path = Config::get('conrad.properties') . $this->id . '/thumb/' . $thumb;
        if (!file_exists(public_path() . $path)) {
            return url(Config::get('conrad.default'));
        }

        return url($path);
    }

    public function getPrimaryImageSmall()
    {
        if ($this->gallery()->count() <= 0) {
            return url(Config::get('conrad.default'));
        }
        
        if ($this->gallery()->where('order', '=', 1)->count() > 0)
            $galleryImage = $this->gallery()->where('order', '=', 1)->first();
        
        else if ($this->gallery()->where('order', '=', 0)->count() > 0)
            $galleryImage = $this->gallery()->where('order', '=', 0)->first();
        
        else
            $galleryImage = $this->gallery()->first();


        $newarray = explode(".", $galleryImage->photo);
        $thumb = "$newarray[0]-property-small.$newarray[1]";

        $path = Config::get('conrad.properties') . $this->id . '/thumb/' . $thumb;
        if (!file_exists(public_path() . $path)) {
            return url(Config::get('conrad.default'));
        }

        return url($path);
    }

    public static function featured()
    {
        $list = self::getActive();

        return $list->where('featured', '=', 1)->orderBy('featured_order', 'ASC');
    }

    public static function notFeatured()
    {
        $list = self::getActive();

        return $list->where('featured', '=', 0);
    }


    public static function recent()
    {
        $list = self::getActive();
        return $list->orderBy('recent_log', 'desc')->take(5);
    }

    public static function getLocation()
    {
        dd(self::lists(['latitude', 'longitude']));
    }

    public function getNext()
    {
        if ($this->where('id', '>', $this->id)->first()) {
            return $this->where('id', '>', $this->id)->first();
        } else {
            return $this->first();
        }

    }

    public function getPrevious()
    {
        if ($this->where('id', '<', $this->id)->first()) {
            return $this->where('id', '<', $this->id)->first();
        } else {
            return $this->orderBy('id', 'desc')->first();
        }
    }

    public function propertyViewGallery()
    {
        return $this->gallery()->orderBy('order', 'ASC');
    }

    public static function getImageWidth()
    {
        $properties = self::getActive()->has('gallery')->get();
        $data = array();
        foreach ($properties as $prop):
            $gallery = $prop->gallery()->lists('photo');
            foreach ($gallery as $photo):
                $newarray = explode('.', $photo);
                $slidderImage = $newarray[0] . '-slider.' . $newarray[1];
                list($width, $length) = getimagesize(public_path() . '/' . Config::get('conrad.properties') . $prop->id . "/" . $photo);
                if ($width >= 1400) {
                    $data[$prop->id . '_' . $slidderImage] = $prop->title;
                    break;
                }
            endforeach;
        endforeach;
        return $data;
    }
/*Get Thumbnail images for carousel images*/
    public  function getBannerImageWidth()
    {
        $data = array();
            $gallery = $this->gallery()->lists('photo');
            foreach ($gallery as $photo):
                $newarray = explode('.', $photo);
                $slidderImage = $newarray[0] . '-slider.' . $newarray[1];
                list($width, $length) = getimagesize(public_path() . '/' . Config::get('conrad.properties') . $this->id . "/" . $photo);
                if ($width >= 1400) {
                    $newarray = explode(".", $photo);
                    $thumb = "$newarray[0]-property-small.$newarray[1]";
                    $data[$slidderImage] = array('path'=>url()  . Config::get('conrad.properties') . $this->id . "/thumb/" .  $thumb,'imgname'=>$slidderImage);


                }
        endforeach;
        return $data;
    }

    public function getPrice($action = Null, $type = Null)
    {  $exchange_rate = \ExchangeRate::getExchangeRate();
        if($action == 'rent'){
            $d = Session::get('price');

            $p = explode("-",$d);
            $type = $p[0];

            if($type == 'monthly'){
                $price =  CustomHelper::format_price(number_format($this->monthly_rental_price_from/1000, 2));
            }else if($type == 'daily'){
                $price =  CustomHelper::format_price(number_format($this->daily_rental_price_from/1000, 2));
            }else{

                if($this->monthly_rental_price_from > 0) {
                    $price =  CustomHelper::format_price(number_format($this->monthly_rental_price_from/1000, 2));
                    $type = 'monthly';

                }else if($this->daily_rental_price_from > 0) {
                    if($this->daily_rental_price_from>999)
                    {
                    $price =  CustomHelper::format_price(number_format($this->daily_rental_price_from/1000, 2));
                    $type = 'daily';
                    }
                    else{
                        $price =  $this->daily_rental_price_from;
                        $type = 'daily';
                    }
                }

            }
            $price *= ($exchange_rate != null)? $exchange_rate->rate : 1;
            $unit = 'K';
            $amount = $price;
            $unit .= ' '.(($type)? ucwords($type) :'');
            $type = '';
        }
        else{
            $amount = $this->sale_price;// CustomHelper::format_price(number_format($this->sale_price/1000000, 2)) ;
            //echo $amount;
            $unit = '';
            if($amount == 0 && ($this->monthly_rental_price_from > 0 || $this->daily_rental_price_from > 0)){

                $amount =  CustomHelper::format_price(number_format($this->monthly_rental_price_from/1000, 2));
                $unit = 'K';
                $type = 'Monthly';
                if($amount  == 0 && $this->daily_rental_price_from > 0){
                   if($this->daily_rental_price_from>999)
                   {
                       $amount = CustomHelper::format_price(number_format($this->daily_rental_price_from/1000, 2));
                       $unit = 'K';
                       $type = 'Daily';
                   }
                    else{
                        $amount = $this->daily_rental_price_from ;
                        $unit = '';
                        $type = 'Daily';
                    }


                }
            }

        }
        $amount *= ($exchange_rate != null)? $exchange_rate->rate : 1;

        if(!(Input::has('currency') || Session::get('exchange_rate'))){
            $currency = ($this->default_currency ) ? $this->default_currency  : $exchange_rate->exchange_code ;
        }
        else{

            if($this->default_currency){
                $rate = ExchangeRate::getRate($this->default_currency);
                  $amount = $amount/$rate->rate;
            }

            $currency = ($exchange_rate == null ) ? ($this->default_currency)? $this->default_currency  : \Setting::getDefaultCurrency() : $exchange_rate->exchange_code ;
        }

        if($amount > 0 && $amount < 1){
            $newData = $this->checkUnit($amount,$unit);

            $amount = $newData['amount'];
            $unit = $newData['unit'];
        }
        if($amount >= 1000){
            if($unit == ''){
                $amount = $amount / 1000;
                $unit = 'K';
            }
            if($unit == 'K' && $amount >= 1000){
                $amount = $amount / 1000;
                $unit = 'M';
            }

        }

        return $currency ." ".round($amount,2).' '.$unit.' '.$type ;
    }
    public  function  checkUnit($amount,$unit){
        if($amount <1){
            if($unit == 'M'){
                $amount *= 1000;
                $unit = 'K';
            }
            elseif($unit == 'K'){
                $amount *= 1000;
                $unit = '';

            }
            else{
                $result['amount'] = $amount;
                $result['unit'] = $unit;
                return $result;
            }

            if($amount >= 1 || $unit = ''){
                $result['amount'] = $amount;
                $result['unit'] = $unit;
                return $result;
            }
            else{
                echo "$amount -- $unit";
                $this->checkUnit($amount,$unit);
            }
        }
    elseif($amount > 999 && $unit != 'M'){
            if($unit == ''){
                $amount = $amount/1000;
                $unit = 'K';
                $this->checkUnit($amount,$unit);
            }
            elseif($unit == 'K'){
                echo "less";
                $amount = $amount/1000;
                $unit = 'M';
                $this->checkUnit($amount,'M');
            }
           // echo  "$amount -- $unit";
           // $this->checkUnit($amount,$unit);
        }
        else{
            echo "final";
            $result['amount'] = $amount;
            $result['unit'] = $unit;
            echo  "$amount -- $unit returning-";
            return 10;
        }
    }

//    public function getPrice_1($action = Null, $type = Null)
//    {
//        $exchange_rate = \ExchangeRate::getExchangeRate();
//        if($action == 'rent'){
//            $d = Session::get('price');
//
//            $p = explode("-",$d);
//            $type = $p[0];
//
//            if($type == 'monthly'){
//                $price =  CustomHelper::format_price(number_format($this->monthly_rental_price_from/1000, 2));
//            }else if($type == 'daily'){
//                $price =  CustomHelper::format_price(number_format($this->daily_rental_price_from/1000, 2));
//            }else{
//
//                if($this->monthly_rental_price_from > 0) {
//                    $price =  CustomHelper::format_price(number_format($this->monthly_rental_price_from/1000, 2));
//                    $type = 'monthly';
//
//                }else if($this->daily_rental_price_from > 0) {
//                    if($this->daily_rental_price_from>999)
//                    {
//                        $price =  CustomHelper::format_price(number_format($this->daily_rental_price_from/1000, 2));
//                        $type = 'daily';
//                    }
//                    else{
//                        $price =  $this->daily_rental_price_from;
//                        $type = 'daily';
//                    }
//                }
//
//            }
//            $price *= ($exchange_rate != null)? $exchange_rate->rate : 1;
//            if($this->default_currency){
//                $rate = ExchangeRate::getRate($this->default_currency);
//                $price = $price/$rate->rate;
//            }
//            echo $price;
//            $price .= 'K';
//            $price =  $price ." ".(($type)? ucwords($type) :'');
//        }else{
//            $price = ((Session::has('exchange_rate'))? ($exchange_rate->rate * CustomHelper::format_price(number_format($this->sale_price/1000000, 2)) ) : CustomHelper::format_price(number_format($this->sale_price/1000000, 2))) . 'M';
//            // $price = CustomHelper::format_price(number_format($this->sale_price/1000000, 2)) . 'M';
//            if($price == '0M' && ($this->monthly_rental_price_from > 0 || $this->daily_rental_price_from > 0)){
//                $price =  CustomHelper::format_price(number_format($this->monthly_rental_price_from/1000, 2)) .'K Monthly';
//                if($price == '0K Monthly' && $this->daily_rental_price_from > 0){
//                    if($this->daily_rental_price_from>999)
//                    {
//                        $price = (($exchange_rate != null)? ($exchange_rate->rate * CustomHelper::format_price(number_format($this->daily_rental_price_from/1000, 2))  ) : CustomHelper::format_price(number_format($this->daily_rental_price_from/1000, 2)) ) .'K Daily';
//                        //$price =  CustomHelper::format_price(number_format($this->daily_rental_price_from/1000, 2)) .'K Daily';
//                    }
//                    else{
//                        $price =  (($exchange_rate != null)? ($exchange_rate->rate * $this->daily_rental_price_from  ) : $this->daily_rental_price_from ) .' Daily';
////                        $price =  $this->daily_rental_price_from .' Daily';
//                    }
//
//
//                }
//                else{
//                    $price_1 = CustomHelper::format_price(number_format($this->monthly_rental_price_from/1000, 2)) ;
//                    echo $price_1."-";
//                    if($this->default_currency){
//                        $exchange = new ExchangeRate;
//                        $price_temp = $exchange->getRate($this->default_currency);
//                        $price_1 = ($price_1 / $price_temp->rate);
//                        echo $price_temp->rate;
//                    }
//                    $price = $price_1 . 'K Monthly';
//                }
//            }
//            else{
//                $price_1 = ((Session::has('exchange_rate'))? ($exchange_rate->rate * CustomHelper::format_price(number_format($this->sale_price/1000000, 2)) ) : CustomHelper::format_price(number_format($this->sale_price/1000000, 2))) ;
//                if($this->default_currency){
//                    $price_temp = ExchangeRate::getRate($this->defaul_currency);
//
//                    $price_1 = ($price_1 / $price_temp->rate);
//                }
//                $price = $price_1 . 'M';
//            }
//        }
//        if(!(Input::has('currency') || Session::get('exchange_rate'))){
//            $currency = ($this->default_currency ) ? $this->default_currency  : $exchange_rate->exchange_code ;
//        }
//        else{
//            $currency = ($exchange_rate == null ) ? ($this->default_currency)? $this->default_currency  : \Setting::getDefaultCurrency() : $exchange_rate->exchange_code ;
//        }
//        return $currency ." ".$price ;
//    }
    public function sale_p()
    {

        if($this->sale_price >0){
            return $this->sale_price;
            return CustomHelper::format_price(number_format($this->sale_price/1000000, 2));
        }else{
            return Null;
        }
    }


    public function getAllPrices($type)
    {

        $exchange_rate = \ExchangeRate::getExchangeRate();
        $payment_type = '';
        $amount = 0;
        if($type == 'sale'){
            if($this->sale_price > 0){
                $amount += CustomHelper::format_price(number_format($this->sale_price/1000000, 2));
                $unit = 'M';
            }else{
                return Null;
            }
        }else{
            if($this->{$type.'_rental_price_from'} >0){

//                $amount = ($exchange_rate->rate * CustomHelper::format_price(number_format($this->{$type.'_rental_price_from'}/1000, 2))) ;
                $amount = CustomHelper::format_price(number_format($this->{$type.'_rental_price_from'}/1000, 2));
                //echo 'test'.$amount.'--';
                $unit = 'K';
                $payment_type =  ucwords($type);
            }else{
                return null;
            }
        }
        if((Input::has('currency') || Session::get('exchange_rate'))){
            $currency = $exchange_rate->exchange_code ;
        }
        else{
            $currency =  ($this->default_currency)? $this->default_currency  : \Setting::getDefaultCurrency() ;
        }
        if($this->default_currency == $exchange_rate->exchange_code){
            $amount = $amount;
        }
        elseif($this->default_currency == 'USD'){
            $rate = ExchangeRate::getRate($this->default_currency);
            if($currency == $this->default_currency ){
                $amount = $amount;
            }
            else{
                $amount_thb = $amount/$rate->rate;
                $amount = ($exchange_rate->rate * $amount_thb );
            }
        }
        else{
            $amount *= $exchange_rate->rate;

        }
        if($amount > 0 && $amount < 1){
            $result = $this->checkUnit($amount,$unit);
            $amount = $result['amount'];
            $unit = $result['unit'];
        }
        return $currency.' '. round($amount,2) . $unit .' '. $payment_type;
    }

    public function getLatLong()
    {
        dd('sdfgsdfg');
    }

    public function delete()
    {
        // delete all related properties carousel
        $this->carousel()->delete();

        // delete the properties
        return parent::delete();
    }

    public function getTitle(){
        return $this->title;
    }
}