<?php

class BaseController extends Controller
{

    public $title = NULL;
    public $role = NULL;
    protected $settings;

    /**
     * Initializer.
     *
     * @access   public
     * @return \BaseController
     */
    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->settings = Setting::find(1);
        View::share('settings', $this->settings);
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);

            $this->layout->content = Null;
            $this->layout->script = Null;
            $this->layout->og_data = Null;

            if ($this->settings) {
                $this->layout->title = $this->settings->title;
                $this->layout->head_title = $this->settings->title;
                $this->layout->meta_description = $this->settings->description;
            } else {
                $this->layout->title = 'Conrad Properties Bangkok';
                $this->layout->head_title = 'Conrad Properties Bangkok';
                $this->layout->meta_description = "Conrad Properties - Thailand's leading independent property agency";
            }

            if ($this->role == 'administrator') {
                //dd(Route::currentRouteName());
                $controller = \Request::segment(2); // somecontroller
                $action = \Request::segment(3); // someaction
                if($controller || $action){
                    if ($controller == "propertiestype")
                        $controller = "Properties Type";
                    if ($controller == "propertiesribbon")
                        $controller = "Properties Ribbon";
                    if ($controller == "property-submission")
                        $controller = "Property Submission";
                    if ($controller == "property-enquiry")
                        $controller = "Property Enquiry";

                    if ($action == "")
                        $controller = $controller . ' : List';
                    if ($action == "create")
                        $controller = $controller . ' : Add';
                    if ($action == "update" || $action == "edit")
                        $controller = $controller . ' : Edit';
                    if ($action == "changepassword")
                        $controller = $controller . ' : Change Password';

                    if ($action == "dashboard")
                        $controller = '';
                }


                $this->layout->title = ($controller) ? ucwords($controller) : Null;
                $this->layout->head_title = 'Conrad Properties Bangkok';
                $this->layout->meta_description = "Conrad Properties - Thailand's leading independent property agency";
            }

        }
    }

}
