<?php

use Illuminate\Support\Facades\View;

class UserBaseController extends \BaseController
{
    function __construct()
    {
        parent::__construct();
        $this->layout = 'layouts.admin.layout';
        $this->role = 'administrator';

        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    public function getLogin()
    {
        $this->layout = View::make("layouts.front.login");
        $controller = Request::segment(1); // somecontroller
        $action = Request::segment(2); // someaction

        $this->layout->title = ($controller && $action) ? ucwords($controller) . " - " . ucwords($action) : Null;
        $this->layout->head_title = ucwords($controller);
        $this->layout->meta_description = "Conrad Properties - Thailand's leading independent property agency";
        $this->layout->og_data = Null;
        if (Auth::user()) {
            if (Auth::user()->role_id == 1) {
                return Redirect::to('/administrator/users/dashboard');
            } else {
                return Redirect::to($this->role . '/users/dashboard');
            }
        }
        $this->layout->content = View::make('users.login');
    }

    public function postSignin()
    {
        if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))) {
            @session_start();
            $_SESSION['IsAuthorized'] = True;
            return Redirect::to($this->role . '/users/dashboard')->with('message', 'You are now logged in!');
        } else {
            return Redirect::to($this->role . '/users/login')
                ->with('message', 'Your username/password combination was incorrect')
                ->withInput();
        }
    }

    public function getLogout()
    {
        Auth::logout();
        @session_start();
        $_SESSION['IsAuthorized'] = False;
        return Redirect::to($this->role . '/users/login')->with('message', 'Your are now logged out!');
    }

}
