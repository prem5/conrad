<?php

class FrontBaseController extends BaseController
{

    /**
     * Initializer.
     *
     * @return \FrontBaseController
     */
    public function __construct()
    {
        $this->layout = 'layouts.front.layout';
        parent::__construct();
    }

}
