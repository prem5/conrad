<?php

class HomeController extends \FrontBaseController
{

    function __construct()
    {
        parent::__construct();
        $this->layout = 'layouts.front.page';
        if (Route::getCurrentRoute()->getUri() == '/') {
            $this->layout = 'layouts.front.home';
        }
    }
    public function showWelcome()
    {
        $this->layout->home = true;
        $this->layout->content = '';
    }

    public function getPage($slug)
    {
        // Get this blog post data
        $page = Page::where('slug', '=', $slug)->first();

        if ($slug == 'contact-us') {
            $this->layout = View::make('layouts.front.contact');
            $this->layout->og_data = Null;
            $this->layout->script = '<script type="text/javascript" src="/assets/js/map-contact_updated.js"></script>';
        }
        // Check if the blog post exists
        if (!$page) {
            // 404 error page.
            return \App::abort(404);
        }
        $this->layout->head_title = $page->meta_title;
        $this->layout->meta_description = $page->meta_description;

        // Show the page
        $this->layout->content = View::make('page', compact('page'));
    }

    public function postContact()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ];
        $validator = Validator::make($data = Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        $templateDataUser = \Template::getTemplates('contact-us-user')->first();
        //$asd = $templateData->first();
        $templateMessageUser = $templateDataUser->message;
        $templateSubjectUser = $templateDataUser->subject;

        $templateDataAdministrator = \Template::getTemplates('contact-us-administrator')->first();
        //$asd = $templateData->first();
        $templateMessageAdministrator = $templateDataAdministrator->message;
        $templateSubjectAdministrator = $templateDataAdministrator->subject;

        $keywordToReplace = array('[NAME]','[EMAIL]','[PHONE]','[SUBJECT]','[MESSAGE]');
        $valueToReplace = array($data['name'],$data['email'],$data['phone'],$data['subject'],$data['message']);
        $templateMessageAdministrator = str_replace($keywordToReplace,$valueToReplace,$templateMessageAdministrator);
        try {


            $settings = Setting::find(1);
            // auto response message for booking user
            $adminEmail = ['email' =>$settings->primary_email,'name'=>''];
            $contactEmail = ['email' =>$settings->primary_email,'name'=>''];
            //mail to notify the admin for the contact request
            if($data['email'] != 'ante.b.tomasovic@gmail.com'){
                Mail::send('emails.email', array('body' => $templateMessageAdministrator, 'style'=>''), function ($message) use ($adminEmail, $contactEmail, $templateSubjectAdministrator, $templateMessageAdministrator) {
                    $message->from($adminEmail['email'], $adminEmail['name'])
                        ->to($contactEmail['email'], $contactEmail['name'])
                        ->subject( $templateSubjectAdministrator);
                    //->setBody($templateMessageAdministrator);
                });
            }

            //thankyou mail to the user
            Mail::send('emails.email', array('body' => $templateMessageUser, 'style'=>''), function ($message) use ($data, $adminEmail, $templateMessageUser,$templateSubjectUser) {
                $message->from($adminEmail['email'], $adminEmail['name'])
                    ->to($data['email'], $data['name'])
                    ->subject($templateSubjectUser);
                //->setBody($templateMessage);
            });
        } catch (Exception $e) {
            print $e->getMessage();exit;
        }
        echo  "<script language='javascript'>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        
        ga('create', 'UA-65003715-1', 'auto');
        ga('send', 'pageview');
        ga('send', 'event', 'Contact Form', 'submit', 'contact-us');
        </script>";

        return Redirect::back()->with('message', 'Thank you for getting in touch. We will do our best to get back to you within 24 hours !!');
    }

    public function postNewsletter()
    {

        $rules = [
            'email' => 'required|email'
        ];
        $validator = Validator::make($data = Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to(Redirect::getUrlGenerator()->previous() . "#subscribe")->withInput()->withErrors($validator);
        }

        //        try {
//            // auto notification message for site admin
//            Mail::send('emails.newsletter.notification', ['data' => $data], function ($message) use ($data) {
//                $message->alwaysFrom($data['email'], $data['email']);
//                $message->to(Config::get('conrad.admin.email'), Config::get('conrad.admin.name'))->subject("Newsletter Signup from {$data['name']}.");
//            });
//        } catch (Exception $e) {
//        }
        return Redirect::back()->with('message', 'Thank you for contacting !!');
    }

    public function postNews()
    {
        $data = \Input::all();
        $validator = Validator::make($data, \Newsletter::$erules);
        if ($validator->fails()) {
            return "fail";
        }
        $news = \Newsletter::firstOrCreate(['email'=>\Input::get('email')]);
        $name = \Input::get('name');
        if(!is_null($name) && ($name != "footernewslettersubscription" ) ){
            $news->name = \Input::get('name');
            $news->save();
        }
        if(isset($news->id))
        {
            //mail functionality
            try {
                $templateDataUser = \Template::getTemplates('subscriber-user')->first();
                //$asd = $templateData->first();
                $templateMessageUser = $templateDataUser->message;
                $templateSubjectUser = $templateDataUser->subject;

                $templateDataAdministrator = \Template::getTemplates('subscriber-administrator')->first();
                //$asd = $templateData->first();
                $templateMessageAdministrator = $templateDataAdministrator->message;
                $templateSubjectAdministrator = $templateDataAdministrator->subject;

                $keywordToReplace = array('[EMAIL]');
                $valueToReplace = array($data['email']);
                $templateMessageAdministrator = str_replace($keywordToReplace,$valueToReplace,$templateMessageAdministrator);



                $settings = \Setting::find(1);
                // auto response message for booking user
               // $adminEmail = ['email' =>$settings->primary_email,'name'=>''];
                $adminEmail = ['email' =>'noreply@conradproperties.asia','name'=>''];
                $contactEmail = ['email' =>$settings->primary_email,'name'=>''];
                // mail to notify the admin for the contact request
                /**
                Mail::send('emails.email', array('body' => $templateMessageAdministrator, 'style'=>''), function ($message) use ($adminEmail, $contactEmail, $templateSubjectAdministrator, $templateMessageAdministrator) {
                $message->from($adminEmail['email'], $adminEmail['name'])
                ->to($contactEmail['email'], $contactEmail['name'])
                ->subject( $templateSubjectAdministrator);
                //->setBody($templateMessageAdministrator);
                });
                 */
                //thankyou mail to the user
                Mail::send('emails.email', array('body' => $templateMessageUser, 'style'=>''), function ($message) use ($data, $adminEmail, $templateMessageUser,$templateSubjectUser) {
                    $message->from($adminEmail['email'], $adminEmail['name'])
                        ->to($data['email'], $data['name'])
                        ->subject($templateSubjectUser);
                    //->setBody($templateMessage);
                });
            } catch (Exception $e) {
                print $e->getMessage();exit;
            }

            echo  "<script language='javascript'>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            
            ga('create', 'UA-65003715-1', 'auto');
            ga('send', 'pageview');
            ga('send', 'event', 'Subscribe Footer Form', 'submit', 'subscribe-footer');
            </script>";


            return "Thank You ";
        }
        else
        {
            return  "Something went wrong";

        }


    }

    public function getFavourites()
    {
        $cookieData = \Cookie::get('favourites');
        if (!empty($cookieData)):
            $propertyId = array();
            foreach ($cookieData as $key => $cd):
                $propertyId[] = $key;
            endforeach;

            $data['sell'] = \Property::getActive()->whereIn('id', $propertyId)->where('sale', '=', 1)->get();
            $data['rent'] = \Property::getActive()->whereIn('id', $propertyId)->where('rent', '=', 1)->get();
        else:
            $data = array();
        endif;
        $this->layout->meta_title = 'Your favorite properties | Conrad Properties';
        $this->layout->meta_description = 'Your favorite properties saved from Conrad Properties, based in Koh Samui, Thailand.
        ';
        $this->layout->content = View::make('properties.favourites', compact('data'));

    }
    public function getSiteMap()
    {

        $sitemap = array();
        $pages = Page::getActive();
        foreach($pages->get() as $page)
        {
            $sitemap[]['url'] = array('loc'=>$page->getUrl());
        }

        $properties = Property::getActive();
        foreach($properties->get() as $property)
        {
            $sitemap[]['url'] = array('loc'=>$property->getUrl());
        }
        $xml = new SimpleXMLElement('<urlset/>');
        $xml->addAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $xml->addAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $xml->addAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $this->array_to_xml($sitemap, $xml);
        return Response::make($xml->asXML(), '200')->header('Content-Type', 'text/xml');
        //dd($sitemap);
    }

// function defination to convert array to xml
    public function array_to_xml( $data, &$xml_data ) {
        foreach( $data as $key => $value ) {
            if( is_array($value) ) {
                if( is_numeric($key) ){
                    $subnode = $xml_data;
                }else{
                    $subnode = $xml_data->addChild($key);
                }
                $this->array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

    public function find()
    {
        $property = Property::where('reference_code', \Input::get('ref'))->first();
        if ($property != null) {
            return Redirect::to('/properties/' . $property->slug);
        }
        return Redirect::to('');
    }
    /*public function getPad()
    {
        $pro =\Property::all();
        foreach($pro as $res)
        {
            $id =$res->id;
            $formatted_value = sprintf("%04d", $id);
            $code = "CP".$formatted_value;
            $res->reference_code = $code;
            $res->save();
        }
    }*/

    /* public function getXml()
     {
         ini_set('max_execution_time', 10000000);
         $xml=simplexml_load_file('http://www.xml2u.com/Xml/Conrad%20Properties%20Co.,%20Ltd._2425/4864_Default.xml') or die("Error: Cannot create object");
         $nonArrayElement = ['propertyid','lastUpdateDate','category'];
         $innerArrayElement = ['FloorSize','PlotSize','Features','image'];
         $allData = array();
         foreach($xml->Clients->Client->properties->Property as $prop):
             $data = array();
             $image = array();
             $alt = array();

             foreach($prop as $key=>$pr):

                 if(in_array($key,$nonArrayElement))
                     $data[$key] = (string)$pr;
                 else
                 {
                     foreach($pr as $k=>$p):
                         if(in_array($k,$innerArrayElement))
                         {
                             if($k=="image")
                             {
                                 foreach($p as $ke=>$pe):
                                     if($ke == "image")
                                     {
                                         $image[] = (string)$pe;

                                     }
                                     if($ke =="alttext" )
                                         $alt[] = (string)$pe;
                                 endforeach;
                                 $data['image'] = $image;
                                 $data['alt'] = $alt;

                             }
                             else
                             {
                                 foreach($p as $ke=>$pe):
                                     $data[$ke] = (string)$pe;
                                 endforeach;
                             }
                         }
                         else
                             $data[$k] = (string)$p;
                     endforeach;
                 }
             endforeach;
             $allData[] = $data;
         endforeach;

         //dd($allData);
          $areaArray = array();
         $i = 1;
          foreach($allData as $al):
              echo $al['reference'].'<br />';
          $i++;
              endforeach;
         die;*/

    /*$z=1;
    foreach(array_chunk($allData,6) as $all):
        foreach($all as $al):

            $insertData = array();
            $title = explode(',',$al['title']);
            $size = sizeof($title);
            if($size==3)
            {
                $area = trim($title[2]);
                if($area=="Bang Rak")
                    $area = "Bangrak";
                if($area=="Choeng Mon")
                    $area = "Cheong Mon";
                $areaData = Area::where('name','=',$area)->first();
                if(!is_null($areaData))
                {
                    $insertData['city_id'] = $areaData->city_id;
                    $insertData['area_id'] = $areaData->id;
                }
            }
            $insertData['country_id'] = 1;
            $insertData['province_id'] = 1;
            $insertData['title'] = $al['title'];
            $insertData['overview'] = $al['description'];
            $insetData['sale_price'] = $al['price'];
            $propertyType = trim($al['propertyType']);
            $propertyArray = ['Condo'=>'Apart/Condos','House'=>'Villa/Houses','Apartment'=>'Apart/Condos','Property'=>'Property','Townhouse'=>'Townhouse'];
            $propertyData = PropertyType::where('label','=',$propertyArray[$propertyType])->first();
            if(!empty($propertyData))
            {
                $propertyType_id = $propertyData->id;
            }
            else
            {
                $propertyInsertData['label'] = $propertyArray[$propertyType];
                $propertyInsertData['status'] = 1;
                $propertyInsert = PropertyType::create($propertyInsertData);
                $propertyType_id = $propertyInsert->id;
            }
            $insertData['type']= $propertyType_id;

            $features = "<ul>";
            for($i=1;$i<=10;$i++)
            {
                if($al['Feature'.$i]!="")

                    $features .="<li>".$al['Feature'.$i]."</li>";
            }
            $features .="</ul>";
            $insertData['key_features'] = $features;
            $insertData['bedrooms'] = $al['bedrooms'];
            $insertData['bathrooms'] = $al['fullBathrooms'];
            $insertData['kitchen'] = $al['fittedKitchen'];
            $insertData['terrace_rooftop'] = $al['terrace'];
            $insertData['furnished'] = $al['furnishings'];
            $insertData['year_build'] = $al['yearBuilt'];
            $insertData['land_area'] = $al['plotSize'];
            $insertData['building_area'] = $al['floorSize'];
            $insertData['reference_code'] = $this->random();
            $insertData['slug'] = $this->slug($insertData['title']);
            $insertData['parking'] = $al['carports'];
            $insertData['swimming_pool'] = $al['swimmingPool'];
            $insertData['sale_price'] = $al['price'];
            $insertData['reference'] = $al['reference'];
            $insertData['sale'] = 1;
            //$insertData['published'] = 1;
            $ref = Property::where('reference','=',$al['reference'])->first();
            if(is_null($ref))
            {
                $propertyid = Property::create($insertData);

                $path = Config::get('conrad.properties');
                $destinationPath = public_path() .$path.$propertyid->id."/";

                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath);
                    mkdir($destinationPath."/thumb");
                }
                $l=1;
                foreach($al['image'] as $img):
                    $source = public_path().'/images/'.$al['propertyid'].'/'.$l.'.jpg';
                    if(file_exists($source)):
                    $filename =$insertData['slug'].$l. '.jpg';
                    \File::move($source,$destinationPath.$insertData['slug'].$l.'.jpg');
                    //\Input::file('photo')->move($destinationPath, $filename);
                    \CustomHelper::genThumbs($destinationPath.$filename,$destinationPath."/thumb/",$insertData['slug'].$l,'property');
                    $data['photo'] = $filename;
                    $data['status'] = 1;
                    $data['property_id'] = $propertyid->id;
                    Gallery::create($data);
                        endif;
                    $l++;

                endforeach;
                die();
            }

             $z++;
        endforeach;
            sleep(60);
    endforeach;


}*/

    /*public function random()
    {
        $str = strtoupper(str_random(5));
        $available = Property::where('reference_code','=',$str)->first();
        if(is_null($available))
            return $str;
        else
            $this->random();
    }

    public function slug($title)
    {
        $slug = Str::slug($title);
        $slugCount = Property::where('slug', 'like %' . $slug . '%')->get()->count();
        $slugStr = ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
        return $slugStr;
    }*/

    public function sitemap()
    {
        $agent['agent_name'] = 'Conrad Properties Co., Ltd';
        $agent['agent_id'] = ' ';
        $agent['language'] = 'en';
        $agent['phone'] = '+66 92-959-1299';
        $agent['email'] = 'info@conradproperties.asia';
        $agent['website'] = 'https://www.conradproperties.asia';
        $agent['updated_at'] = date('d-m-Y s:i:h') ;
        $agent['country'] = 'Thailand' ;
        $agent['province'] = 'Surat Thani' ;
        $properties = Property::where('published', '=', 1)->orderBy('id','DESC')->get();


//        return Response::make($xml->asXML(), '200')->header('Content-Type', 'text/xml');
        return Response::view('pages.sitemap',compact('properties','agent'))->header('Content-Type', 'text/xml');
    }
    public function test(){
        return 980;
        print_r($_SESSION['timeout']);
        die();


        return '"THBTHB=X",1.0000,"10/16/2017","9:09am"
"THBGBP=X",0.0227,"10/16/2017","9:05am"
"THBEUR=X",0.0255,"10/16/2017","9:05am"
"THBUSD=X",0.0302,"10/16/2017","9:09am"
"THBCAD=X",0.0377,"10/16/2017","9:05am"
"THBAUD=X",0.0374,"10/16/2017","9:05am"
"THBRUB=X",1.7297,"10/16/2017","9:09am"
"THBSGD=X",0.0406,"10/16/2017","9:05am"
"THBHKD=X",0.2347,"10/16/2017","9:05am"
"THBCNY=X",0.1987,"10/16/2017","9:05am"
"THBMYR=X",0.1274,"10/16/2017","9:05am"
"THBINR=X",1.9581,"10/16/2017","9:05am"
"THBKRW=X",34.0540,"10/16/2017","9:05am"
"THBNOK=X",0.2389,"10/16/2017","9:05am"
"THBCHF=X",0.0282,"10/16/2017","9:05am"';
    }

}