<?php

class AjaxController extends \BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    /*     * ******Admin Section******** */

    public function postProvince()
    {
        $country_id = $_POST['country_id'];
        $data = \Country::find($country_id)->province()->get();
        $value = '<option value="">Select Province</option>';
        foreach ($data as $d):
            $value .= '<option value="' . $d->id . '">' . $d->name . '</option>';
        endforeach;
        $return['data'] = $value;
        return \Response::json($return);
    }

    public function postCity()
    {
        $province_id = $_POST['province_id'];
        $data = \Province::find($province_id)->city()->get();
        $value = '<option value="">Select Cities</option>';
        foreach ($data as $d):
            $value .= '<option value="' . $d->id . '">' . $d->name . '</option>';
        endforeach;
        $return['data'] = $value;
        return \Response::json($return);
    }

    public function getCity()
    {
        return "this is ajax";
    }
    public function postArea()
    {

        $city_id = $_POST['city_id'];
        $data = \City::find($city_id)->area()->get();

        $value = '<option value="">Select Area</option>';
        foreach ($data as $d):
            $value .= '<option value="' . $d->id . '">' . $d->name . '</option>';
        endforeach;

        $return['data'] = $value;

        return \Response::json($return);
    }


    /*     * *****End admin section******** */

    function postChangesearchfield()
    {
        $type = $_POST['type'];
        if ($type == "" || $type == "buy")
            $value = \Config::get('conrad.price_buy_select');
        else if ($type == "rent")
            $value = \Config::get('conrad.price_rent_select');
        $finalArray = array();
        $option = '';
        foreach ($value as $key => $val):
            $data = array();
            $data['value'] = $key;
            $data['text'] = $val;
            $option .= '<option value="' . $key . '">' . $val . '</option>';
            $finalArray[] = $data;
        endforeach;
        $return['data'] = $finalArray;
        $return['content'] = $option;
        echo json_encode($return);
        exit;
        //return \Response::json($return);
    }

}
