<?php
namespace Admin;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class PropertyTypeController extends \AdminBaseController {
    function __construct() {
        parent::__construct();
    }
    public function getIndex() {
        $roles = \PropertyType::all();
        $this->layout->content = View::make('propertiestype.list', compact('roles'));
    }

    public function getCreate() {
        $this->layout->content = View::make('propertiestype.create');
    }
    public function postStore() {
        $validator = Validator::make($data = \Input::all(), \PropertyType::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \PropertyType::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/propertiestype/');
    }

    public function getUpdate($id) {
        $data = \PropertyType::findOrFail($id);
        $this->layout->content = View::make('propertiestype.update', compact('data'));
    }

    public function postSave($id) {
        $status = \Input::get('status');
        $validator = Validator::make($data = \Input::all(), \PropertyType::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        $propertytype = \PropertyType::findOrFail($id);
        $propertytype->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
        /*Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/propertiestype/');*/
    }

    public function getDelete($id) {

        $type = \Property::where('type','=',$id)->count();
        if($type>0){
            Session::flash('message', "Cannot be deleted as it is already in use.");
            return Redirect::to('/administrator/propertiestype/');
        }
        $data = \PropertyType::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/propertiestype/');
    }




} 