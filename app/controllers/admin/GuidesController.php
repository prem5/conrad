<?php
namespace Admin;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class GuidesController extends \AdminBaseController {

    function __construct() {
        parent::__construct();
    }

    public function getIndex()
    {
        $roles = \Guide::all();
        $this->layout->content = View::make('guides.admin.list',compact('roles'));
    }
    public function getCreate() {
        $this->layout->content = View::make('guides.admin.create');
    }
    public function postStore() {
       /* $file = \Input::file('photo');
        $slug = \Input::get('slug');*/
        $validator = Validator::make($data = \Input::all(), \Guide::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
      /*  if($file !=null){
        $destinationPath = public_path() . '/uploads/guides/';
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath);
        }
        $filename =$slug. '.' . $file->getClientOriginalExtension();
        \Input::file('photo')->move($destinationPath, $filename);
        \CustomHelper::genThumbs(public_path() .'/uploads/guides/'.$filename,public_path() .'/uploads/guides/',$slug,'guide');
        $data['photo'] = $filename;
        }
        else
        {
            unset($data['photo']);
        }*/
        \Guide::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/guides/');
    }

    public function getUpdate($id) {
        $data = \Guide::findOrFail($id);
        $this->layout->content = View::make('guides.admin.update', compact('data'));
    }

    public function postSave($id) {

        $status = \Input::get('status');
        /*$file = \Input::file('photo');
        $slug = \Input::get('slug');*/
        $validator = Validator::make($data = \Input::all(), \Guide::$rules);
        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        /**To Do */
        /*
        if($file !=null){
        $destinationPath = public_path() . '/uploads/guides/';
        $filename =$slug. '.' . $file->getClientOriginalExtension();
        \Input::file('photo')->move($destinationPath, $filename);
        \CustomHelper::genThumbs(public_path() .'/uploads/guides/'.$filename,public_path() .'/uploads/guides/',$slug,'guide');
        $data['photo'] = $filename;
        }else
        {
            unset($data['photo']);
        }*/
        $guide = \Guide::findOrFail($id);
        //dd($data);
        $guide->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
        /*Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/guides/');*/
    }
    public function getDelete($id) {
        $data = \Guide::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/guides/');
    }

    public function getImage($id){
        $data = \Guide::findOrFail($id);
        $this->layout->content = View::make('guides.admin.guide-image', compact('data'));
    }

    public function postUpload($id){
        $slug = \Input::get('slug');
        $file = \Input::file('photo');
        if($file !=null){
            $destinationPath = public_path() . '/uploads/guides/';
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath);
            }
            $filename =$slug. '.' . $file->getClientOriginalExtension();
            \Input::file('photo')->move($destinationPath, $filename);
            \CustomHelper::genThumbs(public_path() .'/uploads/guides/'.$filename,public_path() .'/uploads/guides/',$slug,'guide');
            $data['photo'] = $filename;
        }

        $blogs = \Guide::findOrFail($id);
        $blogs->update($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::back();

    }
} 