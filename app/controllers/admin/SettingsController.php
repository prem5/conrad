<?php
namespace Admin;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class SettingsController extends \AdminBaseController{

    function __construct() {
        parent::__construct();
    }

    public function getIndex() {

        $data = \Setting::get()->first();;
        $this->layout->content = View::make('settings.update', compact('data'));
    }

    public function postSave($id) {
        $validator = Validator::make($data = \Input::all(), \Setting::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $setting = \Setting::findOrFail($id);
        $setting->update($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/setting/');
    }

} 