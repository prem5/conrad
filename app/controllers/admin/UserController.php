<?php

namespace Admin;

use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View as View;

class UserController extends \UserBaseController
{
    public function __construct(){
        parent::__construct();
    }

    public function getIndex()
    {
        $users = \User::all();

        $this->layout->content = View::make('users.list', compact('users'));
    }

    public function getDashboard()
    {
        $this->layout->content = View::make('admin.dashboard');
    }

    public function getCreate()
    {
        $this->layout->content = View::make('users.create');
    }

    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), \User::$rules);

        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }


        $data['username'] = strtolower($data['firstname'] . $data['lastname']);
        $data['password'] = \Hash::make($data['password']);
        unset($data['password_confirmation']);
        $user = \User::create($data);

        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/users/');
    }

    public function getChangepassword($uid)
    {
        $this->layout->content = View::make('users.change_password', compact('uid'));
    }

    public function postSavepassword($uid)
    {
        $validator = Validator::make($data = Input::all(), \User::$edit_rules);

        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $user = \User::findOrFail($uid);
        $data['password'] = \Hash::make($data['password']);
        unset($data['password_confirmation']);
        $user->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
        /*Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/users');*/
    }

    public function getProfile($id)
    {
        $data = \User::findOrFail($id);
        $this->layout->content = View::make('users.admin_profile',compact('data'));
    }

    public function postSave($id)
    {
        $status = \Input::get('status');
        $validator = Validator::make($data = \Input::all(), \User::$prules);
        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }

        $user = \User::findOrFail($id);
        $user->update($data);

        Session::flash('message', "Successfully Updated !");
        return Redirect::back();

    }

    public function getDelete($id)
    {
        $curid = Auth::user()->id;
        if($curid!=$id && $id!=1)
        {
        $data = \User::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/users/');
        }
        else
        {
            Session::flash('message', "You cannot Delete Super admin");
            return Redirect::back('/administrator/users/');
        }
    }

}
