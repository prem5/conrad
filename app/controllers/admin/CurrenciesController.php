<?php
namespace Admin;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class CurrenciesController extends \AdminBaseController {

    function __construct() {
        parent::__construct();
    }

    public function getIndex() {
        $roles = \Currency::all();
        $this->layout->content = View::make('currencies.list', compact('roles'));
    }

    public function getCreate() {
        $this->layout->content = View::make('currencies.create');
    }

    public function postStore() {
        $validator = Validator::make($data = \Input::all(), \Currency::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \Currency::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/currencies/');
    }

    public function getUpdate($id) {
        if($id == '1')
        {
            Session::flash('message', "You are not allowed to change default currency setting !");
            return Redirect::to('/administrator/currencies/');
        }
        $data = \Currency::findOrFail($id);
        $this->layout->content = View::make('currencies.update', compact('data'));
    }

    public function postSave($id) {
        $status = \Input::get('status');
        $validator = Validator::make($data = \Input::all(), \Currency::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        $currency = \Currency::findOrFail($id);
        $currency->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
       /* Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/currencies/');*/
    }

    public function getDelete($id) {
        if($id == '1')
        {
            Session::flash('message', "You are not allowed to change default currency setting !");
            return Redirect::to('/administrator/currencies/');
        }
        $data = \Currency::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/currencies/');
    }



} 