<?php
namespace Admin;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class PropertyEnquriesController extends \AdminBaseController {
    function __construct() {
        parent::__construct();
    }

    public function getIndex() {

        $roles = \Enquiry::paginate(20);
        $this->layout->content = View::make('properties.admin.enquiry', compact('roles'));
    }

    public function getDelete($id) {
        $data = \Enquiry::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/property-enquiry/');
    }
    public function getShow($id){
        $data = \Enquiry::findOrFail($id);
        $this->layout->content = View::make('properties.admin.enquiry_detail',compact('data'));
    }

} 