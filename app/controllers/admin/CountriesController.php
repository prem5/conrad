<?php

namespace Admin;

use Countries;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class CountriesController extends \AdminBaseController {

    function __construct() {
        parent::__construct();
    }

    public function getIndex() {
        $roles = \Country::all();
        $this->layout->content = View::make('countries.list', compact('roles'));
    }

    public function getCreate() {
        $this->layout->content = View::make('countries.create');
    }

    public function postStore() {
        $validator = Validator::make($data = \Input::all(), \Country::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \Country::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/countries/');
    }

    public function getUpdate($id) {
        $data = \Country::findOrFail($id);
        $this->layout->content = View::make('countries.update', compact('data'));
    }

    public function postSave($id) {
        $status = \Input::get('status');
        $validator = Validator::make($data = \Input::all(), \Country::$rules);
        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        $country = \Country::findOrFail($id);
        $country->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
       /* Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/countries/');*/
    }

    public function getDelete($id) {
        $country_id = \Property::where('country_id','=',$id)->count();
        if($country_id>0){
            Session::flash('message', "Cannot be Deleted !");
            return Redirect::to('/administrator/countries/');
        }
        $data = \Country::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/countries/');
    }

}
