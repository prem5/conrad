<?php
namespace Admin;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class TagsController extends \AdminBaseController {
    function __construct() {
        parent::__construct();
    }
    public function getIndex() {
        $roles = \Tag::all();
        $this->layout->content = View::make('tags.list', compact('roles'));
    }
	/**
	 * Show the form for creating a new resource.
	 * GET /tags/create
	 *
	 * @return Response
	 */
    public function getCreate() {
        $this->layout->content = View::make('tags.create');
    }


    public function postStore() {
        $validator = Validator::make($data = \Input::all(), \Tag::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \Tag::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/keyword/');
    }
	/**
	 * Display the specified resource.
	 * GET /tags/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function getUpdate($id) {
        $data = \Tag::findOrFail($id);
        $this->layout->content = View::make('tags.edit', compact('data'));
    }

    /**
	 * Show the form for editing the specified resource.
	 * GET /tags/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function postSave($id) {
        $validator = Validator::make($data = \Input::all(), \Tag::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $tag = \Tag::findOrFail($id);
        $tag->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
       /* Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/keyword/');*/
    }

    public function getDelete($id) {
        $data = \Tag::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/keyword/');
    }


    /**
	 * Remove the specified resource from storage.
	 * DELETE /tags/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}