<?php
namespace Admin;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class PropertySubmissionsController extends \AdminBaseController {
    function __construct() {
        parent::__construct();
    }
    public function getIndex() {
        $roles = \PropertySubmission::all();
        $this->layout->content = View::make('properties.admin.all', compact('roles'));
    }

    public function getView($id)
    {
        $data = \PropertySubmission::findOrFail($id);
        $this->layout->content = View::make('properties.admin.detail', compact('data'));

    }

    public function getDelete($id)
    {
        $data = \PropertySubmission::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/property-submission/');
    }
} 