<?php

namespace Admin;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View as View;

class RolesController extends \AdminBaseController
{
    function __construct() {
        parent::__construct();
    }

    public function getIndex()
    {
        $roles = \Role::all();

        $this->layout->content = View::make('roles.list', compact('roles'));
    }

    public function getCreate()
    {
        $this->layout->content = View::make('roles.create');
    }

    public function postStore()
    {
        $validator = Validator::make($data = \Input::all(), \Role::$rules);

        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }

        \Role::create($data);

        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/roles/');
    }

    public function getUpdate($id)
    {
        $data = \Role::findOrFail($id);

        $this->layout->content = View::make('roles.update', compact('data'));
    }

    public function postSave($id)
    {
        $validator = Validator::make($data = \Input::all(), \Role::$rules);

        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $user = \Role::findOrFail($id);
        $user->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);

        /*Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/roles');*/
    }

    public function getDelete($id)
    {
        $data = \Role::findOrFail($id);
        $data->delete();

        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/roles');
    }

}
