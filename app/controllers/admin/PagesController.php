<?php

namespace Admin;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class PagesController extends \AdminBaseController {

    function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * GET /pages
     *
     * @return Response
     */
    public function getIndex() {
        $roles = \Page::all();
        $this->layout->content = View::make('pages.list', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /pages/create
     *
     * @return Response
     */
    public function getCreate() {
        $this->layout->content = View::make('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /pages
     *
     * @return Response
     */
    public function postStore() {
        $validator = Validator::make($data = \Input::all(), \Page::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \Page::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/pages/');
    }

    /**
     * Display the specified resource.
     * GET /pages/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function getUpdate($id) {
        $data = \Page::findOrFail($id);
        $this->layout->content = View::make('pages.update', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     * GET /pages/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function postSave($id) {
        $status = \Input::get('status');
        $validator = Validator::make($data = \Input::all(), \Page::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        $page = \Page::findOrFail($id);
        $page->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
        /*Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/pages/');*/
    }

    /**
     * Update the specified resource in storage.
     * PUT /pages/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function getDelete($id) {
        $data = \Page::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/pages/');
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /pages/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function getDestroy($id) {
        //
    }

}
