<?php

namespace Admin;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\File;

class FeaturesController extends \AdminBaseController {
    function __construct() {
        parent::__construct();
    }

    function getIndex()
    {
        $data = \Property::featured()->orderBy('featured_order','Asc')->get();
        $this->layout->content = View::make('properties.admin.featuredlisting',compact('data'));
    }

    public function getCreate()
    {
        $data = [''=>'Select Property'] + \Property::notFeatured()->lists('title','id');
        $this->layout->content = View::make('properties.admin.addfeatured',compact('data'));
    }

    public function postStore()
    {
        $rules = [
            'property_id' => 'required'
        ];
        $validator = Validator::make($data = \Input::all(), $rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $property = \Property::findOrFail($data['property_id']);
        $updateData['featured'] = 1;
        $property->update($updateData);
        Session::flash('message', "Successfully Added to the Featured List !");
        return Redirect::to('/administrator/features/');
    }

    public function getRemovefeature($id)
    {
        $property = \Property::findOrFail($id);
        $updateData['featured'] = 0;
        $property->update($updateData);
        Session::flash('message', "Successfully Removed from the Featured List !");
        return Redirect::to('/administrator/features/');
    }

    public function postChangeorder()
    {
        $ordering = $_POST['featured-listing'];
        //$order = explode(',',$ordering);
        $i = 1;
        foreach($ordering as $o):
            $property = \Property::findOrFail($o);
            $data['featured_order'] = $i;
            $property->update($data);
            echo "test";
            $i++;
        endforeach;
        $return['success'] = "Success";
        return \Response::json($return);
    }
}