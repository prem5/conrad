<?php
namespace Admin;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class AreasController extends \AdminBaseController {
    function __construct() {
        parent::__construct();
    }
	/**
	 * Display a listing of the resource.
	 * GET /areas
	 *
	 * @return Response
	 */
    public function getIndex() {
        $roles = \Area::all();
        $this->layout->content = View::make('areas.index', compact('roles'));
    }

	/**
	 * Show the form for creating a new resource.
	 * GET /areas/create
	 *
	 * @return Response
	 */
    public function getCreate() {
        $this->layout->content = View::make('areas.create');
    }

	/**
	 * Store a newly created resource in storage.
	 * POST /areas
	 *
	 * @return Response
	 */
    public function postStore() {
        $validator = Validator::make($data = \Input::all(), \Area::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \Area::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/areas/');
    }

	/**
	 * Display the specified resource.
	 * GET /areas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function getUpdate($id) {
        $data = \Area::findOrFail($id);
        $this->layout->content = View::make('areas.edit', compact('data'));
    }

	/**
	 * Show the form for editing the specified resource.
	 * GET /areas/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function postSave($id) {
        $status = \Input::get('status');
        $validator = Validator::make($data = \Input::all(), \Area::$rules);
        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        $area = \Area::findOrFail($id);
        $area->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
    }

	/**
	 * Update the specified resource in storage.
	 * PUT /areas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function getDelete($id) {
        $area = \Property::where('area_id','=',$id)->count();
        if($area>0){
            Session::flash('message', "Cannot be deleted as it is already in use.");
            return Redirect::to('/administrator/areas/');
        }
        $data = \Area::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/areas/');
    }

	/**
	 * Remove the specified resource from storage.
	 * DELETE /areas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}