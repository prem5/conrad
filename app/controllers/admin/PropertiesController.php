<?php

namespace Admin;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Property;

class PropertiesController extends \AdminBaseController {

    function __construct() {
        parent::__construct();
    }

	/**
	 * Display a listing of properties
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		/*$properties = Property::getAll()->get();
		$this->layout->content = View::make('properties.list', compact('properties'));*/
		$properties = Property::getAll();
        if(Input::get('published') == '1'){
            $properties->where('published',1);
        }
        elseif (Input::get('published') == '0'){
            $properties->where('published',0);
        }
        $this->layout->noindex = true;
        $properties = $properties->orderBy('reference_code','DESC')->get();
		$this->layout->content = View::make('properties.list', compact('properties'));
	}

	/**
	 * Show the form for creating a new property
	 *
	 * @return Response
	 */
	public function getCreate()
	{
        $data = Property::inst([]);
        $currences = ['THB' => 'THB','USD' =>'USD'];
        $this->layout->noindex = true;
        $this->layout->content =  View::make('properties.create',compact('data','currences'));
	}

	/**
	 * Store a newly created property in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{
        $data = Input::all();
        $status = \Input::get('published');
        $rul = \Property::$rules;

        if(\Input::get('monthly_rental_price_from') ){
            $rul['monthly_rental_price_from'] = 'required|max:8';
            $rul['rent'] = 'required';
            $data['rent'] = 1;
        }else{
            $data['monthly_rental_price_from'] = '';
            $data['monthly_rental_price_to'] = '';
            $data['rent'] = 0;
        }
        if(\Input::get('daily_rental_price_from') || \Input::get('daily_rental_price_to')){
            $rul['daily_rental_price_from'] = 'required|max:8';
            $rul['daily_rental_price_to'] = 'required|max:8';
            $rul['rent'] = 'required';
            $data['rent'] = 1;
        }else{
            $data['daily_rental_price_from'] = '';
            $data['daily_rental_price_to'] = '';
            $data['rent'] = 0;
        }

        if($data['sale_price']){
            $rul['sale_price'] = 'required|max:10';
            $data['sale'] = 1;
        }
        $validator = Validator::make($data, $rul);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

        //$data['reference_code'] = strtoupper($data['reference_code']);
        $min = ($data['min_bedroom'] > 0)? $data['min_bedroom'] : 0;
        if($data['max_bedroom'] > 0){
            $data['bedrooms'] = $min .'-'. $data['max_bedroom'];
        }
        else{
            $data['bedrooms'] = $min;
        }


        if ($status != 1) {
            $data['published'] = 0;
        }
        if($data['ribbon']==7)
	        $data['is_sold'] = 1;
	else
		$data['is_sold'] = 0;

		$lastinserted_id = Property::create($data);

        $formatted_value = sprintf("%04d",$lastinserted_id->id);
        if(\Input::get('meta_title')){
            $lastinserted_id->meta_title =\ Input::get('meta_title');
        }
        if(\Input::get('meta_description')){
            $lastinserted_id->meta_description = \Input::get('meta_description');
        }
        $code = "CP".$formatted_value;
        $lastinserted_id->reference_code = $code;
        $lastinserted_id->save();
		return Redirect::to('administrator/listings');
	}

	/**
	 * Show the form for editing the specified property.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		$data = Property::find($id);

        if(!$data){
            return Redirect::to('/administrator/properties/');
        }
        $currences = ['THB' => 'THB','USD' =>'USD'];
        $this->layout->noindex = ($data->published)? false : true;
        $this->layout->content =  View::make('properties.update', compact('data','currences'));
	}

	/**
	 * Update the specified property in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postSave($id)
	{
		$property = Property::findOrFail($id);
        $data = Input::all();
        $status = \Input::get('published');
        $rul = \Property::$rules;

        if(\Input::get('monthly_rental_price_from')){
            $rul['monthly_rental_price_from'] = 'required|max:8';
            $rul['rent'] = 'required';
        }else{
            $data['monthly_rental_price_from'] = '';
            $data['rent'] = 0;
        }
        if(\Input::get('daily_rental_price_from') || \Input::get('daily_rental_price_to')){
            $rul['daily_rental_price_from'] = 'required|max:8';
            $rul['daily_rental_price_to'] = 'required|max:8';
            $rul['rent'] = 'required';
        }else{
            $data['daily_rental_price_from'] = '';
            $data['daily_rental_price_to'] = '';
            $data['rent'] = 0;
        }

        if(\Input::get('monthly_rental_price_from') || (\Input::get('daily_rental_price_from') && \Input::get('daily_rental_price_to'))){
            $data['rent'] = 1;
        }
        if($data['sale_price']){
            $rul['sale_price'] = 'required|max:10';
            $rul['sale'] = 'required';
            $data['sale'] = 1;
        }else{
            $data['sale'] = 0;
        }

        $validator = Validator::make($data, $rul);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

        //$data['reference_code'] = strtoupper($data['reference_code']);
         $min = ($data['min_bedroom'] > 0)? $data['min_bedroom'] : 0;
        if($data['max_bedroom'] > 0){
            $data['bedrooms'] = $min .'-'. $data['max_bedroom'];
        }
        else{
            $data['bedrooms'] = $min;
        }
        if ($status != 1) {
            $data['published'] = 0;
        }
        if($data['ribbon']==7)
	        $data['is_sold'] = 1;
	else
		$data['is_sold'] = 0;

        /*if($ref_code)
        {
            unset ($data['reference_code']);
        }*/
        if(\Input::get('meta_title')){
            $property->meta_title =\ Input::get('meta_title');
        }
        if(\Input::get('meta_description')){
            $property->meta_description = \Input::get('meta_description');
        }
		$property->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
	}

    public function getDelete($id)
    {
        $data = \Property::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/listings/');
    }

	/**
	 * Remove the specified property from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Property::destroy($id);

		return Redirect::route('properties.index');
	}

    public function getStatus($id)
    {
        $property = \Property::findOrFail($id);
        if($property->published==1)
        {
            $data['published']=0;
        }
        else{
            $data['published']=1;
        }
        $property->update($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/listings/');

    }

}
