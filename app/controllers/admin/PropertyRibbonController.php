<?php
namespace Admin;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class PropertyRibbonController extends \AdminBaseController{
    function __construct() {
        parent::__construct();
    }
    public function getIndex() {
        $roles = \PropertyRibbon::all();
        $this->layout->content = View::make('propertiesribbon.list', compact('roles'));
    }

    public function getCreate() {
        $this->layout->content = View::make('propertiesribbon.create');
    }

    public function postStore() {
        $validator = Validator::make($data = \Input::all(), \PropertyRibbon::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \PropertyRibbon::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/propertiesribbon/');
    }

    public function getUpdate($id) {
        $data = \PropertyRibbon::findOrFail($id);
        $this->layout->content = View::make('propertiesribbon.update', compact('data'));
    }

    public function postSave($id) {
        $status = \Input::get('status');
        $validator = Validator::make($data = \Input::all(), \PropertyRibbon::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        $propertyribbon = \PropertyRibbon::findOrFail($id);
        $propertyribbon->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
        /*Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/propertiesribbon/');*/
    }

    public function getDelete($id) {
        $ribbon = \Property::where('ribbon','=',$id)->count();
        if($ribbon>0){
            Session::flash('message', "Cannot be deleted as it is already in use.");
            return Redirect::to('/administrator/propertiesribbon/');
        }
        $data = \PropertyRibbon::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/propertiesribbon/');
    }


} 