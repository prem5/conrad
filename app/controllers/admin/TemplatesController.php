<?php
namespace Admin;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\File;

class TemplatesController extends \AdminBaseController {
    function __construct() {
        parent::__construct();
    }

	/**
	 * Display a listing of the resource.
	 * GET /templates
	 *
	 * @return Response
	 */
	public function getIndex()
	{
        $data = \Template::all();
        $this->layout->content = View::make('templates.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /templates/create
	 *
	 * @return Response
	 */
	public function getCreate()
	{
        $this->layout->content = View::make('templates.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /templates
	 *
	 * @return Response
	 */
	public function postStore()
	{
        $validator = Validator::make($data = \Input::all(), \Template::$rules);
        if ($validator->fails())
        {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \Template::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/templates/');
	}

	/**
	 * Display the specified resource.
	 * GET /templates/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /templates/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
        $data = \Template::findOrFail($id);
        $this->layout->content = View::make('templates.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /templates/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
        $rules = array(
            'name' => 'required|unique:templates,name,'.$id,
            'title' => 'required',
            'subject' => 'required',
            'message' => 'required',
        );

        $validator = Validator::make($data = \Input::all(), $rules);
        if ($validator->fails())
        {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $template = \Template::findOrFail($id);
        $template->update($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/templates/');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /templates/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
        $data = \Template::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/templates/');
	}

}