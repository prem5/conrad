<?php

namespace Admin;

use Districts;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class DistrictsController extends \AdminBaseController {

    function __construct() {
        parent::__construct();
    }

    public function getIndex() {
        $roles = \District::all();
        $this->layout->content = View::make('districts.list', compact('roles'));
    }

    public function getCreate() {
        $this->layout->content = View::make('districts.create');
    }

    public function postStore() {
        $validator = Validator::make($data = \Input::all(), \District::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \District::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/districts/');
    }

    public function getUpdate($id) {
        $data = \District::findOrFail($id);
        $this->layout->content = View::make('districts.update', compact('data'));
    }

    public function postSave($id) {
        $status = \Input::get('status');
        $validator = Validator::make($data = \Input::all(), \District::$rules);
        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        $district = \District::findOrFail($id);
        $district->update($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/districts/');
    }

    public function getDelete($id) {
        $data = \District::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/districts/');
    }

}
