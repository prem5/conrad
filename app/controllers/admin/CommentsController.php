<?php
namespace Admin;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class CommentsController extends \AdminBaseController {

    public function __construct(){
        parent::__construct();
    }

	/**
	 * Display a listing of the resource.
	 * GET /comments
	 *
	 * @return Response
	 */
	public function getIndex()
	{
        $roles = \Comment::all();
        $this->layout->content = View::make('comments.index', compact('roles'));
    }


	/**
	 * Show the form for creating a new resource.
	 * GET /comments/create
	 *
	 * @return Response
	 */
	public function getView($id)
	{
        $data = \Comment::findOrFail($id);
        $this->layout->content = View::make('comments.show', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /comments
	 *
	 * @return Response
	 */
	public function getPublish($id)
	{
        $comment = \Comment::findOrFail($id);
       if($comment->status == 1)
       {
           $comment->status= '0';
       }
        else{
            $comment->status= '1';
        }

        $comment->save();
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/comments/');


	}

	/**
	 * Display the specified resource.
	 * GET /comments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function getUpdate($id) {
        $data = \Comment::findOrFail($id);
        $this->layout->content = View::make('comments.edit', compact('data'));
    }

	/**
	 * Show the form for editing the specified resource.
	 * GET /comments/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postSave($id)
	{
        $status = \Input::get('status');
        $validator = Validator::make($data = \Input::all(), \Comment::$rules);
        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        $city = \Comment::findOrFail($id);
        $city->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
      /*  Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/comments/');*/
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /comments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /comments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}