<?php

namespace Admin;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Provinces;
use Illuminate\Support\Facades\View;

class ProvincesController extends \AdminBaseController {

    /**
     * Display a listing of the resource.
     * GET /provinces
     *
     * @return Response
     */
    function __construct() {
        parent::__construct();
    }

    public function getIndex() {
        $roles = \Province::all();
        $this->layout->content = View::make('provinces.list', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /provinces/create
     *
     * @return Response
     */
    public function getCreate() {
        $this->layout->content = View::make('provinces.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /provinces
     *
     * @return Response
     */
    public function postStore() {
        $validator = Validator::make($data = \Input::all(), \Province::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \Province::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/provinces/');
    }

    /**
     * Display the specified resource.
     * GET /provinces/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function getUpdate($id) {
        $data = \Province::findOrFail($id);
        $this->layout->content = View::make('provinces.update', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     * GET /provinces/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function postSave($id) {
        $status = \Input::get('status');
        $validator = Validator::make($data = \Input::all(), \Province::$rules);
        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        $country = \Province::findOrFail($id);
        $country->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
       /* Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/provinces/');*/
    }

    /**
     * Update the specified resource in storage.
     * PUT /provinces/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function getDelete($id) {

        $province_id = \Property::where('province_id','=',$id)->count();
        if($province_id>0){
            Session::flash('message', "Cannot be deleted as it is already in use.");
            return Redirect::to('/administrator/provinces/');
        }
        $data = \Province::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/provinces/');
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /provinces/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
