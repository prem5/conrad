<?php

namespace Admin;
use Gallery;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class GalleriesController  extends \AdminBaseController{
    function __construct() {
        parent::__construct();
    }
    public function getEdit($id)
    {
        $data = \Property::findOrFail($id);
        $this->layout->content = View::make('galleries.update', compact('data'));
    }

    public function postUpload($id)
    {

        $file = \Input::file('photo');
        $pro  = \Property::findOrFail($id);
        $slug = $pro->slug."-".str_random(32);
        $validator = Validator::make($data = \Input::all(), \Gallery::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if($file !=null){
            $path = Config::get('conrad.properties');
            $destinationPath = public_path() ."/".$path.$id."/";

            if (!is_dir($destinationPath)) {
                mkdir($destinationPath);
                mkdir($destinationPath."/thumb");
            }
            $filename =$slug. '.' . $file->getClientOriginalExtension();
            \Input::file('photo')->move($destinationPath, $filename);
            \CustomHelper::genThumbs($destinationPath.$filename,$destinationPath."/thumb/",$slug,'property');
            $data['photo'] = $filename;
        }
        else
        {
            unset($data['photo']);
        }
        $gallery = new Gallery(array('photo' => $filename , 'status' => 1, 'property_id' => $id));
        $gallery->save();
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/galleries/edit/'.$id);
    }
    public function getDelete($id) {
        $gall = \Gallery::findOrFail($id);
        // Delete multiple files
        $gall->deleteFiles();
        $gall->delete();
        return Redirect::back();
    }

    public function getPrimary($id) {
        $gall = \Gallery::findOrFail($id);
        $affectedRows = \Gallery::where('property_id', '=', $gall->property_id)->update(array('primary' => 0));
        $gall->primary = 1;
        $gall->save();

        Session::flash('message', "Primary Image Successfully Updated !");
        return Redirect::back();
    }

    public function postChangeorder()
    {
        $ordering = $_POST['gallery-listing'];

        //$order = explode(',',$ordering);
        $i = 1;
        foreach($ordering as $o):
            $gallery = \Gallery::findOrFail($o);
            $data['order'] = $i;
            $gallery->update($data);
            $i++;
        endforeach;
        $return['success'] = "Success";
        return \Response::json($return);
    }
} 