<?php

namespace Admin;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class TestimonialsController extends \AdminBaseController{
    function __construct() {
        parent::__construct();
    }

    public function getIndex() {
        $roles = \Testimonial::all();
        $this->layout->content = View::make('testimonials.list', compact('roles'));
    }

    public function getCreate() {
        $this->layout->content = View::make('testimonials.create');
    }

    public function postStore() {
        $validator = Validator::make($data = \Input::all(), \Testimonial::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \Testimonial::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/testimonials/');
    }

    public function getUpdate($id) {
        $data = \Testimonial::findOrFail($id);
        $this->layout->content = View::make('testimonials.update', compact('data'));
    }

    public function postSave($id) {
        $status = \Input::get('status');
        $validator = Validator::make($data = \Input::all(), \Testimonial::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        $testimoial = \Testimonial::findOrFail($id);
        $testimoial->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
        /*Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/testimonials/');*/
    }

    public function getDelete($id) {
        $data = \Testimonial::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/testimonials/');
    }


} 