<?php

namespace Admin;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class NewsLetterController extends \AdminBaseController
{

    function __construct()
    {
        parent::__construct();
    }

    public function getIndex()
    {
       $roles = \Newsletter::orderBy('id','ASC')->get();
        $this->layout->content = View::make('newsletters.list', compact('roles'));
    }
    public function getCreate()
    {
        $this->layout->content = View::make('newsletters.create');
    }

    public function postStore()
    {
        $validator = Validator::make($data = \Input::all(), \Newsletter::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \Newsletter::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/newsletter/');
    }

    public function getCsv()
    {
        $table = \Newsletter::all();
        $filename = 'subscriber.csv';
        $handle = fopen($filename, 'w+');

        fputcsv($handle, array('Sno', 'Name', 'Email'));

        foreach ($table as $row) {
            fputcsv($handle, array($row['id'], $row['name'], $row['email']));
        }

        fclose($handle);

        $headers = array(

            'Content-Encoding: UTF-8',

            'Content-Type' => 'text/csv',
        );

        return Response::download($filename, 'subscriber.csv', $headers);

    }

    public function getUpdate($id)
    {
        $data = \Newsletter::findOrFail($id);
        $this->layout->content = View::make('newsletters.edit', compact('data'));
    }

    public function postSave($id)
    {
        $validator = Validator::make($data = \Input::all(), \Newsletter::$rules);
        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $news = \Newsletter::findOrFail($id);
        $news->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
    }
    public function postMuldelete()
    {
        $all = \Input::get('id');
        if(!isset($all)){
            $err = Session::flash('message', "Checkbox must be checked");
            return Redirect::back()->withErrors($err);
        }
        $ids = \Newsletter::destroy($all);
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/newsletter/');





    }


}