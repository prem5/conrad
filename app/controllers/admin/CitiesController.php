<?php

namespace Admin;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class CitiesController extends \AdminBaseController {

	/**
	 * Display a listing of the resource.
	 * GET /cities
	 *
	 * @return Response
	 */
    public function getIndex() {
        $roles = \City::all();
        $this->layout->content = View::make('cities.index', compact('roles'));
    }

	/**
	 * Show the form for creating a new resource.
	 * GET /cities/create
	 *
	 * @return Response
	 */
    public function getCreate() {
        $this->layout->content = View::make('cities.create');
    }

	/**
	 * Store a newly created resource in storage.
	 * POST /cities
	 *
	 * @return Response
	 */
    public function postStore() {
        $validator = Validator::make($data = \Input::all(), \City::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        \City::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/cities/');
    }

	/**
	 * Display the specified resource.
	 * GET /cities/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function getUpdate($id) {
        $data = \City::findOrFail($id);
        $this->layout->content = View::make('cities.edit', compact('data'));
    }

	/**
	 * Show the form for editing the specified resource.
	 * GET /cities/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function postSave($id) {
        $status = \Input::get('status');
        $validator = Validator::make($data = \Input::all(), \City::$rules);
        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        $city = \City::findOrFail($id);
        $city->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
        /*Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/cities/');*/
    }
	/**
	 * Update the specified resource in storage.
	 * PUT /cities/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function getDelete($id) {
        $city_id = \Property::where('city_id','=',$id)->count();
        if($city_id>0){
            Session::flash('message', "Cannot be deleted as it is already in use.");
            return Redirect::to('/administrator/cities/');
        }
        $data = \City::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/cities/');
    }


    /**
	 * Remove the specified resource from storage.
	 * DELETE /cities/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}