<?php

namespace Admin;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\File;

class CarouselsController extends \AdminBaseController {
    function __construct() {
        parent::__construct();
    }

    function getIndex()
    {
        $data = \Carousel::orderBy('order','asc')->get();
        $this->layout->content = View::make('carousel.index',compact('data'));
    }

    public function getCreate()
    {
        $data = [''=>'Select Property'] + \Property::getImageWidth();
        $this->layout->content = View::make('carousel.create',compact('data'));
    }

    public function postStore()
    {
        $data = \Input::all();
        $imname = \Input::get('image');
        $property = explode('_',$data['property_id']);
        $data['property_id'] = $property[0];/*
        $data['image'] = $property[1]."-".$imname;*/
        $validator = Validator::make($data, \Carousel::$rules);

        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }

        \Carousel::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/carousels/');
    }

    public function getUpdate($id)
    {
        $data['value'] = \Carousel::findOrFail($id);
        $data['dropdown'] = [''=>'Select Property'] + \Property::getImageWidth();
        $this->layout->content = View::make('carousel.update', compact('data'));
    }
    public function postSave($id)
    {
        $data = \Input::all();
        $property = explode('_',$data['property_id']);
        $data['property_id'] = $property[0];
        /*$data['image'] = $property[1];*/
        $rules = array(
            'property_id' => 'required',
            'order' => 'required'
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $carousel = \Carousel::findOrFail($id);
        $carousel->update($data);
        $msg = Session::flash('message', "Successfully Updated !");
        return Redirect::back()->withErrors($msg);
        /*Session::flash('message', "Successfully Updated !");
        return Redirect::to('/administrator/carousels/');*/
    }
    public function getDelete($id)
    {
        $data = \Carousel::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/administrator/carousels/');
    }

    public function postChangeorder()
    {
        $ordering = $_POST['carousel-listing'];

        //$order = explode(',',$ordering);
        $i = 1;
        foreach($ordering as $o):
            $carousel = \Carousel::findOrFail($o);
            $data['order'] = $i;
            $carousel->update($data);
        $i++;
        endforeach;
        $return['success'] = "Success";
        return \Response::json($return);
    }


    /* get Property images */
    public function getImage()
    {
        $id = \Input::get('property_id');
        $img = \Input::get('proimg');
        $property = \Property::findOrFail($id);
        $image ='';
        foreach($property->getBannerImageWidth($id) as $res):
           // $image .="<input type='radio' name='image' value='".$res['imgname']."'><br>"."<img src='".$res['path']."'><br>";
            $image .=' <div class="col-sm-1 col-md-2">
                              <img src="'.$res["path"].'" class="img-responsive">
                              <div class="caption">
                              <p><input type="radio" name="image" value="'.$res["imgname"].'" class="carouselimg"></p>
                              </div>
                         </div>';
            endforeach;
        return $image;



    }

}