<?php

class ErrorController extends \FrontBaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($code)
    {
        switch ($code) {
            case 404:
                $this->layout->content = View::make('errors.404');
                break;

            default:
                $this->layout->content = View::make('errors.500');
                break;
        }
    }

}
