<?php

namespace Writer;


use Blog;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class BlogController  extends \AdminBaseController{
    function __construct() {
        parent::__construct();
    }

    public function getIndex() {
        $roles = \Blog::getMyBlogs()->get();
        $this->layout->content = View::make('blogs.writer.list', compact('roles'));
    }

    public function getCreate() {
        $data = Blog::inst([]);
        $this->layout->content = View::make('blogs.writer.create',compact('data'));
    }
    public function postStore() {
        $file = \Input::file('photo');

        $slug = \Input::get('slug');
        $validator = Validator::make($data = \Input::all(), \Blog::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if($file !=null){
            $destinationPath = public_path() . '/uploads/blogs/';
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath);
            }
            $filename =$slug. '.' . $file->getClientOriginalExtension();
            \Input::file('photo')->move($destinationPath, $filename);
            \CustomHelper::genThumbs(public_path() .'/uploads/blogs/'.$filename,public_path() .'/uploads/blogs/',$slug,'blog');
            $data['photo'] = $filename;
        }
        else
        {
            unset($data['photo']);
        }
        $data['status']= '0';
        $blog = \Blog::create($data);
        $blog->tags()->attach($data['tags']);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/writer/blogs/');
    }

    public function getUpdate($id) {
        $data = \Blog::findOrFail($id);
        $this->layout->content = View::make('blogs.writer.update', compact('data'));
    }

    public function postSave($id) {
        $status = \Input::get('status');
        $file = \Input::file('photo');
        $slug = \Input::get('slug');
        $validator = Validator::make($data = \Input::all(), \Blog::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if ($status != 1) {
            $data['status'] = 0;
        }
        /*To Do */
        if($file !=null){
            $destinationPath = public_path() . '/uploads/blogs/';
            $filename =$slug. '.' . $file->getClientOriginalExtension();
            \Input::file('photo')->move($destinationPath, $filename);
            \CustomHelper::genThumbs(public_path() .'/uploads/blogs/'.$filename,public_path() .'/uploads/blogs/',$slug,'blog');
            $data['photo'] = $filename;
        }
        else
        {
            unset($data['photo']);
        }
        $blogs = \Blog::findOrFail($id);
        $tag = \Input::get('tags');
        if(!isset($tag)){
            $blogs->tags()->sync([]);
        }
        else{
            $blogs->tags()->sync($tag);
        }

        $blogs->update($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/writer/blogs/');
    }

    public function getDelete($id) {
        $data = \Blog::findOrFail($id);
        $data->delete();
        Session::flash('message', "Successfully Deleted !");
        return Redirect::to('/writer/blogs/');
    }

} 