<?php

namespace Writer;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View as View;

class UserController extends \UserBaseController
{
    public function getDashboard()
    {
        $this->layout->content = View::make('users.dashboard');
    }

    public function getProfile()
    {
        $id = \Auth::user()->id;
        $data = \User::findOrFail($id);
        $this->layout->content = View::make('users.profile',compact('data'));
    }

    public function postProfile()
    {
    }

    public function postSave($id)
    {

        $validator = Validator::make($data = \Input::all(), \User::$prules);
        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $user = \User::findOrFail($id);
        $user->update($data);

        Session::flash('message', "Successfully Updated !");
        return Redirect::back();
    }

    public function postPass($id)
    {
        $user = \User::findOrFail($id);
        $validator = Validator::make($data = \Input::all(), \User::$edit_rules);
        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if (Hash::check(\Input::get('old_pass'), \Auth::user()->password)) {
            $data['password'] = \Hash::make($data['password']);
            unset($data['password_confirmation']);
            $user->update($data);

            Session::flash('message', "Successfully Updated !");
            return Redirect::back();

        }
        else {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
    }
}
