<?php
namespace Front;
use App;
use Guide;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Page;

class GuidesController extends \FrontBaseController {

    function __construct() {
        parent::__construct();
        $this->layout = 'layouts.front.page';
        if (Route::getCurrentRoute()->getUri() == 'buyers-guide/{slug?}') {
            //$this->layout = 'layouts.front.page';
            $this->layout = 'layouts.front.sidebar';
        }
    }

	/**
	 * Display a listing of the resource.
	 * GET /guides
	 *
	 * @return Response
	 */
	public function getIndex($slug)
	{
        $data['guide'] = Guide::where('slug', '=', $slug)->first();
        if (!$data['guide']) {
            return App::abort(404, '');
        }
        $this->layout->head_title = $data['guide']->meta_title;
	$this->layout->meta_description = $data['guide']->meta_description;

        $this->layout->content = View::make('guides.view', compact('data'));

    }

	/**
	 * Show the form for creating a new resource.
	 * GET /guides/create
	 *
	 * @return Response
	 */
	public function getSearch()
	{
        $guides= Guide::getActive();
        $page = Page::where('id', '=', '2')->first();
        $data['content'] = '';
        if($page){
            $data['content'] = $page->content;
            $this->layout->head_title = $page->meta_title;
            $this->layout->title =  $page->name;
            $this->layout->meta_description = $page->meta_description;
        }

        $data['guides'] = $guides->orderBy('id');
        $this->layout->content = View::make('guides.all',compact('data'));
	}

}