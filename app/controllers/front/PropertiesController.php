<?php
namespace Front;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use PropertySubmission;
use Illuminate\Http\Request;

class PropertiesController extends \BaseController {

    function __construct() {
        parent::__construct();
        $this->layout = 'layouts.front.all';
        if (Route::getCurrentRoute()->getUri() == 'properties/sell') {
            $this->layout = 'layouts.front.page';
        }

        if (Route::getCurrentRoute()->getUri() == 'properties/{keyboard?}') {
            $this->layout = 'layouts.front.layout';
        }
    }

    public function getIndex($slug){
        $data['property'] = \Property::where('slug', '=', $slug)->where('published',1)->first();
        if (!$data['property']) {
            return Redirect::to('',301);
//            $this->layout = 'layouts.front.home';
//            $this->setupLayout();
//
//            $this->layout->home = true;
//            $this->layout->content = '';
//            return http_redirect()

//            return \App::abort(404, '');
        }else{
            $data['property']->recent_log = date('Y-m-d H:i:s');
            $data['property']->save();
            /*End Store recently view property*/
            $this->layout->head_title =$data['property']->title." in ".$data['property']->getArea();
            $mta = preg_replace('/(.*?[?!.](?=\s|$)).*/', '\\1', strip_tags($data['property']->overview));
            $dot = explode('.',$mta);
//        $this->layout->meta_description = $dot[0];
            $this->layout->meta_title = $data['property']->meta_title;
            $this->layout->meta_description = $data['property']->meta_description;
            $this->layout->og_data = [
                'title'=>$data['property']->meta_title,
                'description'=> $data['property']->meta_description,
                'url'=>$data['property']->getUrl(),
                'image'=>$data['property']->getPrimaryImageSmall()
            ];

            $this->layout->script = '<script type="text/javascript" src="/assets/js/map-individual.js"></script>';
            $this->layout->content = View::make('properties.view',compact('data'));
        }
        /*Start Store recently view property*/

    }


    public function getSearch($country=Null, $province=Null, $city=Null, $place = null,$action=null, $type=null)
    {

        $properties = \Property::getActive();
        $data['type'] =  Session::get('type'); // $type;
        $data['action'] = Session::get('action');
        $meta_description_status = false;
        if(strpos("kkoh-samui-luxury-villas",\Request::segment('1'))){
            Session::put('type','laxury');
            Session::put('action','');
            $data['type'] = "laxury";
            $this->layout->meta_title = "Luxury Koh Samui Holiday Villas | Thailand Real Estate";
            $this->layout->meta_description = "Conrad Properties offer some of the most amazing luxury villas for sale in Koh Samui, whether you want panoramic sea-views, an infinity swimming pool, or beachfront, we have the property for you.";
            $meta_description_status = true;
        }elseif (strpos("kkoh-samui-luxury-villa-for-sale",\Request::segment('1'))){
            $data['type'] = "laxury";
            $data['action'] = "buy";
            Session::put('action','buy');
            $this->layout->meta_title = "Koh Samui Luxury Villas For Sale | Buy Property Thailand";
            $this->layout->meta_description = "Koh Samui Real Estate has experienced a strong demand in recent years for luxury villas and holiday homes, Conrad Properties have exclusive and extensive listings for sale. ";
            $meta_description_status = true;

        }
        elseif (strpos("kkoh-samui-luxury-villa-for-rent",\Request::segment('1'))){
            $data['type'] = "laxury";
            $data['action'] = "rent";
            Session::put('action','rent');
            $this->layout->meta_title = "Koh Samui Luxury Villas For Rent | Investment Property Thailand";
            $this->layout->meta_description = "Koh Samui Real Estate has experienced a strong rental demand in recent years for luxury villas and holiday homes, Conrad Properties have exclusive and extensive listings to rent";
            $meta_description_status = true;
        }
        $data['place'] = "";
        $data['place1'] = "";
        $data['bedroom'] = Session::get('bedroom');
        $data['price'] = Session::get('price');

        $a = $data['action'];
        if(isset($a)){
            $action = Session::get('action');
            $data['action'] = $action;
        }

        $type = $data['type'];


        // for luxury Villas

        if( (isset($type) && ($type == "laxury")) ){
            if(($action == 'rent') ){
//                $properties->whereRaw('IF daily_rental_price_from > 0 THEN  END IF');
                $properties->whereRaw('daily_rental_price_from > IF( daily_rental_price_from > 0,8000, ?)',[-0]);
                $properties->whereRaw('monthly_rental_price_from > IF( monthly_rental_price_from > 0,80000, ?)',[-0]);
            }
            elseif(($action == 'buy') ){
                $properties->where('sale_price', '>=', 10 * 1000000);
            }

            else{
                $properties->whereRaw('daily_rental_price_from > IF( daily_rental_price_from > 0,8000, ?)',[-0]);
                $properties->whereRaw('monthly_rental_price_from > IF( monthly_rental_price_from > 0,80000, ?)',[-0]);
                $properties->orWhere('sale_price', '>=', 10 * 1000000);
//                    ->whereRaw('monthly_rental_price_from > IF( monthly_rental_price_from > 0,80000, ?)',[-0])->whereRaw('sale_price >= IF(sale_price > 0 , 10000000,?)', [-0]);
//                $properties->whereRaw('monthly_rental_price_from > IF( monthly_rental_price_from > 0,80000, ?)',[-0]);
//                $properties->whereRaw('sale_price > IF(sale_price > 0 , 10000000,?)', [-0]);
            }
            $properties->where('type',6);
        }
        //$desc ='';

        // if the url passes country in search
        if(isset($country)|| Session::get('country')){
           if(Session::get('country'))
               $country = Session::get('country');
            $country = \Country::where('slug','=', $country);
            if($country->count() > 0){
                $cnt = $country->first();
                $properties->where('country_id','=',$cnt->id);
                $data['place'] = $cnt->name;

            }
        }

        // if the url passes provinces in search
        if(isset($province) || Session::get('province')){
            if(Session::get('province'))
                $province = Session::get('province');
            $province = \Province::where('slug','=', $province);
            if($province->count() > 0){
                $cnt = $province->first();
                $properties->where('province_id','=',$cnt->id);
                $data['place'] = $cnt->name;
                //$desc .=$data['place'];
            }
        }

        // if the url passes city in search
        if(isset($city)||Session::get('city')){
            if(Session::get('city'))
                $city = Session::get('city');
            $city = \City::where('slug','=', $city);
            if($city->count() > 0){
                $cnt = $city->first();
                $properties->where('city_id','=',$cnt->id);
                $data['place'] = $cnt->name;
                //$desc .=$data['place'];

            }
        }

        // if area is sent by selection from search from
        $p = Session::get('place');
        if(isset($p) && $p && $p != " "){
            $place = Session::get('place');
            $placeArr = explode('_',$place);
            if(isset($placeArr[1])){
                $placeField = $placeArr[0]."_id";
                $placeId = $placeArr[1];

                $properties->where($placeField, '=', $placeId);
                $name = '';
                switch($placeArr[0]){
                    case 'area':
                        $area = \Area::find($placeId);
                        $prov = $area->city()->first()->province()->first();
                        $name = $area->name;
                        $ci = $area->city()->first();
                       // $desc .= $prov->name.",".$ci->name.",".$area->name;
                        $name1 = ', <a href="'.url('search/'.$prov->getCountrySlug().'/'.$prov->slug).'">'.$prov->name.'</a>';
                        break;
                    case 'city':
                        $city = \City::find($placeId);
                        $name = $city->name;
                        //$desc .= $city->name;
                        $name1 = ', <a href="'.url('search/'.$city->province()->first()->getCountrySlug().'/'.$city->province()->first()->slug).'">'.$city->province()->first()->name.'</a>';
                        break;
                }
                $data['place'] = $name;
                $data['place1'] = $name1;
            }
            else{
                $place = Session::get('place');
            }
        }

        // if the url passes area in search
        if(isset($place) && $place != 'all'){
            $area = \Area::whereSlug($place);
            if($area->count() > 0){
                $cnt = $area->get();
                $properties->where('area_id','=',$cnt[0]->id);
                $data['place'] = $cnt[0]->name;
               //$desc .= $data['place'];
            }
        }




//        if(isset($t)){
//            $type = Session::get('type');
//            $data['type'] = $type;
//        }

        if($action == 'buy'  && $action != 'all'){
            $properties->where('sale','=',1);
            $data['action'] = 'sale';
        }else if($action == 'rent'  && $action != 'all'){
            $properties->where('rent','=',1);
            $data['action'] = 'rent';
        }
// getting url segment


        //
        if(isset($type) && ($type != "laxury")){
            if(is_numeric($type)){
                $typeId = $type;
            }else{
                $typeId = \PropertyType::getIdBySlug($type);
            }
            if($typeId){
                $properties->where('type','=',$typeId);
            }
        }

        if($data['price'] && $data['price']!=" "){


            if($action == 'rent'){
                $prices = explode('-',$data['price']);

                if($prices[0]=="monthly")
                {
                    if(isset($prices[1])) {
                        if ($prices[1] == '<30') {
                            $prices[1] = 0;
                            $prices[2] = 30;
                            //unset($prices[1]);
                        } else if ($prices[1] == '>150') {
                            $prices[1] = 150;
                            unset($prices[2]);
                        }
                        if (isset($prices[2])) {
                            $properties->where($prices[0] . '_rental_price_from', '<=', $prices[2] * 1000);
                        }
                        if (isset($prices[1]) && ($prices[1] != 0)) {
                            $properties->where($prices[0] . '_rental_price_from', '>=', $prices[1] * 1000);
                        }else{
                            $properties->where($prices[0] . '_rental_price_from', '>', $prices[1]);
                        }
                    }
                    else{
                        $properties->where($prices[0] . '_rental_price_from', '>', 0);
                    }
                }
                else if($prices[0]=="daily")
                {
                    if(isset($prices[1])) {
                        if ($prices[1] == '<5') {
                            $prices[1] = 1;
                            $prices[2] = 10;
                            //unset($prices[1]);
                        } else if ($prices[1] == '>50') {
                            $prices[1] = 50;
                            unset($prices[2]);
                        }
                        if (isset($prices[2])) {
                            //$properties->where($prices[0] . '_rental_price_to', '<=', $prices[2] * 1000)->where($prices[0] . '_rental_price_to','!=',0);
                            $properties->where($prices[0] . '_rental_price_from', '<=', $prices[2] * 1000)->where($prices[0] . '_rental_price_from','!=',0);
                        }
                        if (isset($prices[1]) && ($prices[1] != 0)) {
                            $properties->where($prices[0] . '_rental_price_from', '>=', $prices[1] * 1000);
                        }else{
                            $properties->where($prices[0] . '_rental_price_from', '>', $prices[1]);
                        }
                    }
                    else{
                        $properties->where($prices[0] . '_rental_price_from', '>', 0);
                    }
                }
            }
            else if($action == 'buy' || $action==""){


                    if($data['price'] == '<5'){
                        $prices[1] = 5;
                    }else if($data['price'] == '>30'){
                        $prices[0] = 30;
                    }
                    else{
                        $prices = explode('-',$data['price']);
                    }
                    if(isset($prices[0])) {
                        $properties->where('sale_price', '>=', $prices[0] * 1000000);
                    }
                    if(isset($prices[1])){
                        $properties->where('sale_price','<=', $prices[1] * 1000000);
                    }

            }

        }

         if($data['bedroom'] && $data['bedroom']!=" "){
            $beds = explode('-',$data['bedroom']);


            if(isset($beds[1])){
                //$properties->where('bedrooms','>',$beds[0]);
                $properties->where('min_bedroom','<=',$beds[0]);
                $properties->where('min_bedroom','!=','NA');
                //$properties->where('bedrooms','<',$beds[1]);
                $properties->where('max_bedroom','>=',$beds[1]);

            }
            else if($beds[0] == '>5'){
                //$properties->whereRaw(\DB::raw('bedrooms > 5'));
                //$properties->whereRaw(\DB::raw('min_bedroom > 5'));
                                $properties->where('min_bedroom','!=','NA');
                $properties->where('max_bedroom','!=','NA');
                $properties->where(function($query){

                $query->where('min_bedroom','>',5);
                $query->orWhere('max_bedroom','>',5);
                });
                /*$properties->where('min_bedroom','!=','NA');
                $properties->where('max_bedroom','!=','NA');
                $properties->where('min_bedroom','>',5);
                $properties->orWhere('max_bedroom','>',5);*/
            }else{
                $number = $beds[0];
                $properties->where(function($query)use ($number){
                $query->where('min_bedroom','<=',$number);
                $query->where('max_bedroom','>=',$number);
                $query->orWhere('min_bedroom','=',$number);
                });
                /*$properties->where('min_bedroom','<=',$beds[0]);
                $properties->where('max_bedroom','>=',$beds[0]);
                $properties->orWhere('min_bedroom','=',$beds[0]);*/
//                $properties->where(\DB::raw("min_bedroom <= $number AND max_bedroom >= $number"));
//                $properties->where(\DB::raw("if(`max_bedroom` != '', max_bedroom >= $beds[0] , null) AND  min_bedroom <= $beds[0]"));
            }
        }
       
        /*if($data['bedroom'] && $data['bedroom']!=" "){
            $beds = explode('-',$data['bedroom']);

            if(isset($beds[1])){
                $properties->where('bedrooms','>',$beds[0]);
                $properties->where('bedrooms','<',$beds[1]);

            }
            else if($beds[0] == '>5'){
                $properties->whereRaw(\DB::raw('bedrooms > 5'));
            }else{
                $properties->where('bedrooms','=',$beds[0]);
                // $properties->orWhere('bedrooms','LIKE',"%-" . $beds[0]);
                //$properties->orWhere('bedrooms','LIKE',$beds[0] . "-%");
            }
        }*/
        $data['count'] = $properties->count();
        $place1 = \Request::segment(2);
        $place2 = \Request::segment(3);
        $place3 = \Request::segment(4);
        $place4 = \Request::segment(5);
        $place = '';
        if($place1!=null){
            $place = $place1;
        }
        if($place2!=null){
            $place = $place2;
        }
        if($place3!=null){
            $place = $place3;
        }
        if($place4!=null){
            $place = $place4;
        }
        if($place!=''){
            $place = str_replace('-', ' ', $place);
            $this->layout->head_title = "Property for Sale and Rent at Conrad Properties - ".ucfirst($place);
            if(!$meta_description_status)
                $this->layout->meta_description = "Properties for sale and rent in Thailand at Conrad Properties. Search for villas, condos, land, and commercial property - ".ucfirst($place);
        } else {
            $this->layout->head_title = "Property for Sale and Rent at Conrad Properties.";
            if(!$meta_description_status)
                $this->layout->meta_description = "Properties for sale and rent in Thailand at Conrad Properties. Search for villas, condos, land, and commercial property.";
        }
        $noOrderBy = true;
        if(Input::has('sortBy')){
            if(Input::get('sortBy') == array_search('Recommended',\Config::get('conrad.short_options'))){
                $properties->whereHas('ribbon',function($q){
                    $q->where('label','Recommended')->orderBy('published');
                });
            }
            elseif(Input::get('sortBy') == array_search('Price high - low',\Config::get('conrad.short_options'))){
                if(!($action == 'rent')){
                   // $properties->select(\DB::row('id,if(sale_price > 0, sale_price ,if(monthly_rental_price_from > 0,monthly_rental_price_from/30,daily_rental_price_from)) as price_1'));
                    //$properties->orderBy('sale_price','DESC');
                    $properties->addSelect(\DB::raw('reference_code, if( sale_price > 0 ,sale_price  / (if(default_currency IS NULL,1,(select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1) )), if(monthly_rental_price_from > 0,  monthly_rental_price_from/(30 * (if(default_currency IS NULL,1,(select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1) )) ),(daily_rental_price_from / if(default_currency IS NULL,1,(select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1) )))) as sale_price1,id'))->orderBy('sale_price1','DESC');
                }
                else{
                    $properties->addSelect(\DB::raw('reference_code, if(default_currency IS NULL,1,(select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1) ) as temp_rate,
if (monthly_rental_price_from=0,daily_rental_price_from / (if(default_currency IS NULL,1,(select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1) )), monthly_rental_price_from/ ( 30 * if(default_currency IS NULL,1,(select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1) ))) as dailyprice,id'))->orderBy('dailyprice','DESC');
                }
            }
            elseif(Input::get('sortBy') == array_search('Price low - high',\Config::get('conrad.short_options'))){
                if(!($action == 'rent')){
                    //$properties->orderBy('sale_price','ASC');
                    $properties->addSelect(\DB::raw('reference_code, if( sale_price > 0 ,sale_price  / (if(default_currency IS NULL,1,(select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1) )), if(monthly_rental_price_from > 0,  monthly_rental_price_from/(30 * (if(default_currency IS NULL,1,(select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1) )) ),(daily_rental_price_from / if(default_currency IS NULL,1,(select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1) )))) as sale_price1,id'))->orderBy('sale_price1','ASC');
                   // $properties->addSelect(\DB::raw('reference_code, if( sale_price > 0 ,sale_price, if(monthly_rental_price_from > 0, monthly_rental_price_from/30,daily_rental_price_from) )as sale_price1,id'))->orderBy('if(default_currency IS NULL,sale_price1,sale_price1 * (select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1))','ASC');
                }
                else{
                    $properties->addSelect(\DB::raw('reference_code, if(default_currency IS NULL,1,(select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1) ) as temp_rate,
if (monthly_rental_price_from=0,daily_rental_price_from / (if(default_currency IS NULL,1,(select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1) )), monthly_rental_price_from/ ( 30 * if(default_currency IS NULL,1,(select rate FROM exchange_rates  where exchange_code = default_currency ORDER BY date DESC limit 1) ))) as dailyprice,id'))->orderBy('dailyprice','ASC');
                }

            }
            elseif(Input::get('sortBy') == array_search('Featured',\Config::get('conrad.short_options'))){
                $properties->whereHas('ribbon',function($q){
                    $q->where('label','Featured')->orderBy('published');
                });
            }
            $noOrderBy = false;
        }
        if($noOrderBy){

//         $properties->orderBy('is_sold','ASC');
//            $properties->orderBy('created_at','DESC');
            $properties->orderByRaw('FIELD(ribbon,"12","7")')->orderBy('created_at','DESC');
        }
        if(Input::has('display')){
            $data['properties'] = $properties->paginate(Input::get('display'));
        }else{
            $data['properties'] = $properties->paginate(12);

        }
        if($data['properties']->toArray()['current_page'] >= 1){

            $array = $data['properties']->toArray();
            if($array['current_page'] * $array['per_page'] < $array['total']){
                $next = $array['current_page']+1;
                $data['next_url'] = url('search?page='.$next);
                View::share('next_url',$data['next_url']);
            }
            if($data['properties']->toArray()['current_page'] != 1){
                $next = $array['current_page'] - 1;
                $data['previous_url'] = url('search?page='.$next);
                View::share('previous_url',$data['previous_url']);
            }

        }

       // if()
         \ExchangeRate::getExchangeRate();

        $this->setMetaTitle();

        $this->layout->content = View::make('properties.all',compact('data'));
        $this->layout->script = '<script type="text/javascript" src="/assets/js/map-sales.js"></script>';
    }

    public function postSearchSession()
    {
        $forminput=Input::all();
           foreach($forminput as $key =>$value):
            Session::put($key, $value);
           endforeach;
        if(Input::get('type') == 'laxury'){
            if(Input::get('action') == 'rent'){
                return Redirect::to('koh-samui-luxury-villa-for-rent');
            }
            elseif (Input::get('action') == 'buy'){
                return Redirect::to('koh-samui-luxury-villa-for-sale');
            }
            else{
                return Redirect::to('koh-samui-luxury-villas');
            }

        }

        return Redirect::to('search');
    }

    public function getSell()
    {
        $data = '';
        $this->layout->content = View::make('properties.sell',compact('data'));
    }

    public function postSubmissionSave()
    {

        $status = \Input::get('news');

        $file = \Input::file('image');
        $name =\Input::get('name');
        $validator = Validator::make($data = \Input::all(), \PropertySubmission::$rules);
        if ($validator->fails()) {
            Session::flash('error', "Failed validation !");
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if($file !=null){
            $destinationPath = public_path() . '/uploads/submissions/';
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath);
            }
            $filename =$name. '.' . $file->getClientOriginalExtension();
            \Input::file('image')->move($destinationPath, $filename);
            $data['image'] = $filename;
        }
        else
        {
            unset($file);
        }
        if($status=='1')
        {
            $data['name'] = \Input::get('name');
            $data['email'] = \Input::get('email');
            \Newsletter::create($data);
            try {
                $templateDataUser = \Template::getTemplates('property-sell-user')->first();
                //$asd = $templateData->first();
                $templateMessageUser = $templateDataUser->message;
                $templateSubjectUser = $templateDataUser->subject;

                $templateDataAdministrator = \Template::getTemplates('property-sell-administrator')->first();
                //$asd = $templateData->first();
                $templateMessageAdministrator = $templateDataAdministrator->message;
                $templateSubjectAdministrator = $templateDataAdministrator->subject;

                $keywordToReplace = array('[NAME]','[EMAIL]','[PHONE]');
                $valueToReplace = array($data['name'],$data['email'],$data['phone']);
                $templateMessageAdministrator = str_replace($keywordToReplace,$valueToReplace,$templateMessageAdministrator);


                $settings = \Setting::find(1);
                // auto response message for booking user
                $adminEmail = ['email' =>$settings->primary_email,'name'=>''];
                $contactEmail = ['email' =>$settings->primary_email,'name'=>''];
                //mail to notify the admin for the contact request
                Mail::send('emails.email', array('body' => $templateMessageAdministrator, 'style'=>''), function ($message) use ($adminEmail, $contactEmail, $templateSubjectAdministrator, $templateMessageAdministrator) {
                    $message->from($adminEmail['email'], $adminEmail['name'])
                        ->to($contactEmail['email'], $contactEmail['name'])
                        ->subject( $templateSubjectAdministrator);
                    //->setBody($templateMessageAdministrator);
                });

                //thankyou mail to the user
                Mail::send('emails.email', array('body' => $templateMessageUser, 'style'=>''), function ($message) use ($data, $adminEmail, $templateMessageUser,$templateSubjectUser) {
                    $message->from($adminEmail['email'], $adminEmail['name'])
                        ->to($data['email'], $data['name'])
                        ->subject($templateSubjectUser);
                    //->setBody($templateMessage);
                });
            } catch (Exception $e) {
                print $e->getMessage();exit;
            }
        }

        PropertySubmission::create($data);
        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/properties/list/');

    }


    public function getSetfavourites()
    {
        $property_id = $_GET['property_id'];
        $propertyData = \Property::findOrFail($property_id);
        $cookieData = \Cookie::get('favourites');
        $cookie = \CustomHelper::checkCookie($propertyData->slug);
        if($cookie):
            unset($cookieData[$property_id]);
            $return['status'] = 0;
        else:
            $cookieData[$propertyData->id] = $propertyData->slug;
            $return['status'] = 1;
        endif;
        \Cookie::queue('favourites',$cookieData,10080);
        $count = count($cookieData);
        $return['success'] = "success";
        $return['count'] = $count;
        return \Response::json($return);
    }

    public function getRemoveFavourites()
    {
        $property_id = $_GET['property_id'];
        $cookieData = \Cookie::get('favourites');
        unset($cookieData[$property_id]);
        \Cookie::queue('favourites',$cookieData,10080);
        $count = count($cookieData);
        $return['success'] = "success";
        $return['count'] = $count;
        return \Response::json($return);
    }

    public function postEnquire()
    {
        $validator = Validator::make($data = \Input::all(), \Enquiry::$rules);
        if ($validator->fails()) {
            return "Failed validation !";
        }
        $succ = \Enquiry::create($data);
        if(isset($succ->id))
        {
            $propertyData = \Property::find($data['property_id']);
            $templateDataUser = \Template::getTemplates('property-enquiry-user')->first();
            //$asd = $templateData->first();
            $templateMessageUser = $templateDataUser->message;
            $templateSubjectUser = $templateDataUser->subject;

            $templateDataAdministrator = \Template::getTemplates('property-enquiry-administrator')->first();
            //$asd = $templateData->first();
            $templateMessageAdministrator = $templateDataAdministrator->message;
            $templateSubjectAdministrator = $templateDataAdministrator->subject;

            $keywordToReplace = array('[PROPERTYNAME]','[NAME]','[EMAIL]','[PHONE]','[SUBJECT]','[MESSAGE]');
            $valueToReplace = array($propertyData->title.' ['.$propertyData->reference_code.']',$data['name'],$data['email'],$data['phone'],$data['subject'],$data['message']);
            $templateMessageAdministrator = str_replace($keywordToReplace,$valueToReplace,$templateMessageAdministrator);
            try {


                $settings = \Setting::find(1);
                // auto response message for booking user
                $adminEmail = ['email' =>$settings->primary_email,'name'=>''];
                $contactEmail = ['email' =>$settings->primary_email,'name'=>''];
                //mail to notify the admin for the contact request
                Mail::send('emails.email', array('body' => $templateMessageAdministrator, 'style'=>''), function ($message) use ($adminEmail, $contactEmail, $templateSubjectAdministrator, $templateMessageAdministrator) {
                    $message->from($adminEmail['email'], $adminEmail['name'])
                        ->to($contactEmail['email'], $contactEmail['name'])
                        ->subject( $templateSubjectAdministrator);
                    //->setBody($templateMessageAdministrator);
                });

                //thankyou mail to the user
                Mail::send('emails.email', array('body' => $templateMessageUser, 'style'=>''), function ($message) use ($data, $adminEmail, $templateMessageUser,$templateSubjectUser) {
                    $message->from($adminEmail['email'], $adminEmail['name'])
                        ->to($data['email'], $data['name'])
                        ->subject($templateSubjectUser);
                    //->setBody($templateMessage);
                });
            } catch (Exception $e) {
                print $e->getMessage();exit;
            }
            echo  "<script language='javascript'>
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                    
                    ga('create', 'UA-65003715-1', 'auto');
                    ga('send', 'pageview');
                    ga('send', 'event', 'Enquiry Form', 'submit', 'Properties');
                    </script>";
            return "Thank you for your enquiry.";
        }
        else{
            return  "Something went wrong";
        }


    }

    public function getLuxury()
    {
        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);
        dd($uri_segments);

        echo $uri_segments[0];
    }
    public function getChange()
    {
    dd('error');
	$properties = \Property::all();
	foreach($properties as $p):
	$prop = \Property::find($p->id);
		$bed = explode('-',$p->bedrooms);
		if(isset($bed[0]))
		$prop->min_bedroom = trim($bed[0]);
		if(isset($bed[1]))
		$prop->max_bedroom = trim($bed[1]);
		$prop->save();
	endforeach;
    }

    public function setupLayout()
    {
        $this->layout = View::make($this->layout);

        $this->layout->content = Null;
        $this->layout->script = Null;
        $this->layout->og_data = Null;

        if ($this->settings) {
            $this->layout->title = $this->settings->title;
            $this->layout->head_title = $this->settings->title;
            $this->layout->meta_description = $this->settings->description;
        } else {
            $this->layout->title = 'Conrad Properties Bangkok';
            $this->layout->head_title = 'Conrad Properties Bangkok';
            $this->layout->meta_description = "Conrad Properties - Thailand's leading independent property agency";
        }

        if ($this->role == 'administrator') {
            //dd(Route::currentRouteName());
            $controller = \Request::segment(2); // somecontroller
            $action = \Request::segment(3); // someaction
            if($controller || $action){
                if ($controller == "propertiestype")
                    $controller = "Properties Type";
                if ($controller == "propertiesribbon")
                    $controller = "Properties Ribbon";
                if ($controller == "property-submission")
                    $controller = "Property Submission";
                if ($controller == "property-enquiry")
                    $controller = "Property Enquiry";

                if ($action == "")
                    $controller = $controller . ' : List';
                if ($action == "create")
                    $controller = $controller . ' : Add';
                if ($action == "update" || $action == "edit")
                    $controller = $controller . ' : Edit';
                if ($action == "changepassword")
                    $controller = $controller . ' : Change Password';

                if ($action == "dashboard")
                    $controller = '';
            }


            $this->layout->title = ($controller) ? ucwords($controller) : Null;
            $this->layout->head_title = 'Conrad Properties Bangkok';
            $this->layout->meta_description = "Conrad Properties - Thailand's leading independent property agency";
        }

    }

    public function setMetaTitle()
    {
        $segments = explode('/search/',$_SERVER['REQUEST_URI']);

        if(isset($segments[1])){
            switch ($segments[1]){
                case 'thailand/koh-samui/north-west/bophut':
                    $this->layout->meta_title = "Properties in Bophut, North West Koh Samui | Conrad Properties premsinggh";
                    $this->layout->meta_description = "Browse our collection of stunning properties for sale and rent in Bophut, North West Koh Samui, one of Thailand's most idyllic islands. Our range of villas and apartments offer a perfect retreat, with properties ranging from 1 bedroom flats to multi-bedroom secluded houses with private pools.";
                    break;
                case "thailand/koh-samui/north-east/bophut":
                    $this->layout->meta_title = "Properties in Bophut, North East Koh Samui | Conrad Properties";
                    $this->layout->meta_description = "We have a wide selection of properties for sale and rent in Bophut, North East Koh Samui from luxury villas, apartments, condos and real estate investment.";
                    break;
                case "thailand/koh-samui/north-west/chaweng-noi":
                    $this->layout->meta_title = "Properties in Chaweng Noi, North West Koh Samui | Conrad Properties";
                    $this->layout->meta_description = "With a stunning selection of properties for sale and rent in Chaweng Noi, North West Koh Samui with breathtaking panoramic sea views, tranquil sandy beaches, making Koh Samui amongst one of the finest locations for property investment in Thailand.";
                    break;
                case 'thailand/koh-samui/north-east/chaweng-noi':
                    $this->layout->meta_title= 'Properties in Chaweng Noi, North East Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We have a phenomenal selection of properties  in Chaweng Noi, North East Koh Samui with breathtaking scenic landscapes, white sandy beaches making Koh Samui amongst one of the finest locations  for property investment or vacation destinations.';
                    break;

                case 'thailand/koh-samui/north-west/bangrak':
                    $this->layout->meta_title= 'Properties in Bangrak, North West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We can help you find your dream propert in Bangrak, North West Koh Samui; from luxury villas, apartments, condos and real estate investment.';
                    break;
                case 'thailand/koh-samui/north-east/bangrak':
                    $this->layout->meta_title= 'Properties in Bangrak, North East Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We help you find your dream property in Bangrak, North East Koh Samui from luxury villas and houses, to apartments and condos, perfect for a real estate investment.';
                    break;
                case 'thailand/koh-samui/north-west/cheong-mon':
                    $this->layout->meta_title= 'Properties in Choeng Mon, North West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We have the best collection of properties for sale and rent in Choeng Mon, North West Koh Samui that suits every buyer, from breathtaking panoramic sea views, tranquil sandy beaches, making Koh Samui amongst one of the top locations for property investment in Thailand.';
                    break;
                case 'thailand/koh-samui/north-east/cheong-mon':
                    $this->layout->meta_title= 'Properties in Choeng Mon, North East Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We offer property for sale and rent in Choeng Mon, North East Koh Samui ranging from beachfront luxury villas to modern apartments and land for sale, which is ideal for anyone looking to make an investment in Koh Samui. ';
                    break;
                case 'thailand/koh-samui/north-west/maenam':
                    $this->layout->meta_title= 'Properties in Maenam, North West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We help in finding your dream property in Maenam, North West Koh Samui from luxury villas, apartments, condos and real estate investment.';
                    break;
                case 'thailand/koh-samui/north-east/maenam':
                    $this->layout->meta_title= 'Properties in Maenam, North East Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We  have a wide selection of properties for sale and rent in Maenam, North East Koh Samui with stunning panoramic sea views, tranquil beaches, one of the best location for property investment in Thailand.';
                    break;
                case 'thailand/koh-samui/north-west/plai-laem':
                    $this->layout->meta_title= 'Properties in Plai Laem, North West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We have a great selection of properties in Plai Laem, North West Koh Samui with exotic panoramic sea views, tranquil sandy beaches, making Koh Samui amongst one of the finest locations for property investment in Thailand.';
                    break;
                case 'thailand/koh-samui/north-east/plai-laem':
                    $this->layout->meta_title= 'Properties in Plai Laem, North East Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We offer property for sale and rent in Plai Laem, North East Koh Samui ranging from beachfront luxury villas to modern apartments and land for sale, which is a fantastic opportunity for anyone looking to make an investment in Koh Samui. ';
                    break;
                case 'thailand/koh-samui/north-east/bang-por':
                    $this->layout->meta_title= 'Properties in Bang Por, North East Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'Property for sale and rent in Bang Por, North East Koh Samui from luxury villas, apartments, condos and land, suitable for real estate investment.';
                    break;
                case 'thailand/koh-samui/north-west/bang-por':
                    $this->layout->meta_title= 'Properties in Bang Por, North West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We  have a wide selection of properties for sale and rent in Bang Por, North West Koh Samui, ranging  from luxury villas, houses, apartments, condos perfect for real estate investment.';
                    break;
                case 'thailand/koh-samui/south-west/ban-makham':
                    $this->layout->meta_title= 'Properties in Ban Makham, South West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We have a unique selection of properties in Ban Makham, South West Koh Samui with beautiful sea views, tranquil sandy beaches, making Koh Samui one of the perfect location for real estate investment in Thailand.';
                    break;
                case 'thailand/koh-samui/north-east/ban-makham':
                    $this->layout->meta_title= 'Properties in Ban Makham, North East Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We offer property for sale and rent in Ban Makham, North East Koh Samui from beachfront luxury villas to modern apartments and land for sale, which is a fantastic opportunity for anyone looking to make an investment in Koh Samui. ';
                    break;
                case 'thailand/koh-samui/north-west/ban-makham':
                    $this->layout->meta_title= 'Properties in Ban Makham, North West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'Property for sale and rent in Ban Makham, North West Koh Samui from luxury villas and houses, to apartments, condos and land.';
                    break;
                case 'thailand/koh-samui/north-west/chaweng':
                    $this->layout->meta_title= 'Properties in Chaweng, North West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'Property for sale and rent in Chaweng  from luxury villas and houses, to apartments, condos and land, suitable for real estate investment.';
                    break;
                case 'thailand/koh-samui/north-east/chaweng':
                    $this->layout->meta_title= 'Properties in Chaweng, North East Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We offer property for sale and rent in Chaweng in North East of Koh Samui and help you find your dream property, from villas and houses, to apartments, condos and land for sale in some of the best locations in Koh Samui.';
                    break;
                case 'thailand/koh-samui/north-west/namuang':
                    $this->layout->meta_title= 'Properties in Namuang, North West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We offer property for sale and rent in Namuang, North West Koh Samui ranging from beachfront luxury villas, to modern apartments and land for sale - a great opportunity for anyone looking to make a real estate investment in Koh Samui.';
                    break;
                case 'thailand/koh-samui/south-west/namuang':
                    $this->layout->meta_title= 'Properties in Namuang, South West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We offer property for sale and rent in Namuang, South West Koh Samui from luxury villas and houses, to apartments, condos and beachfront land - ideal for a property investment or permanent residence.';
                    break;
                case 'thailand/koh-samui/north-west/ban-tai':
                    $this->layout->meta_title= 'Properties in Ban Tai, North West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We have a wide selection of properties for sale and rent in Ban Tai, North West Koh Samui from luxury villas and houses, to apartments, condos and land.';
                    break;
                case 'thailand/koh-samui/north-east/ban-tai':
                    $this->layout->meta_title= 'Properties in Ban Tai, North East Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We have a huge selection of properties for sale and rent in Ban Tai, North East Koh Samui from panoramic sea-views, sandy tropical beaches - making Koh Samui one of the finest locations for property investment in Thailand.';
                    break;
                case 'thailand/koh-samui/south-east/laem-yai':
                    $this->layout->meta_title= 'Properties in Laem Yai, South East Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We offer a stunning selection of properties for sale and rent in Laem Yai, South East Koh Samui with beachfront luxury villas, apartments and land for sale; unique opportunity for anyone looking to invest in Koh Samui Real Estate.';
                    break;
                case 'thailand/koh-samui/north-west/laem-yai':
                    $this->layout->meta_title= 'Properties in Laem Yai, North West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We offer a unique selection of properties for sale and rent in Laem Yai, North West Koh Samui and can assist you finding your dream home, from luxury villas and houses, to apartments, condos and real estate investments.';
                    break;
                case 'thailand/koh-samui/north-west/big-buddha':
                    $this->layout->meta_title= 'Properties in Big Buddha, North West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We have a wide selection of properties in Big Buddha, North West Koh Samui from handpicked luxury villas and houses, to modern apartments, condos and land options - a great opportunity for real estate investment.';
                    break;
                case 'thailand/koh-samui/north-east/big-buddha':
                    $this->layout->meta_title= 'Properties in Big Buddha, North East Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We have a stunning selection of properties for sale and rent  in Big Buddha, North East Koh Samui from breathtaking panoramic sea views, to tranquil sandy beaches, Koh Samui is one of the finest locations for property investment.';
                    break;
                case 'thailand/koh-samui/south-west/laem-sor':
                    $this->layout->meta_title= 'Properties in Laem Sor, South West Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We provide unique property investments for sale and rent  in Laem Sor, South West Koh Samui ranging from luxury beachfront villas to modern apartments and land for sale - a fantastic opportunity for anyone buying Real Estate in Koh Samui.';
                    break;
                case 'thailand/koh-samui/south-east/laem-sor':
                    $this->layout->meta_title= 'Properties in Laem Sor, South East Koh Samui | Conrad Properties';
                    $this->layout->meta_description = 'We offer a spectacular selection of properties for sale and rent in Laem Sor, South East Koh Samui from luxury villas, apartments, condos and to unique land options, suiting all your requirements for real estate investment.';
                    break;
                case 'all/buy':
                    $this->layout->meta_title= 'Property for Sale in Koh Samui  | Conrad Properties';
                    $this->layout->meta_description = 'Koh Samui Property for sale; luxury villas , houses, condos, apartments, land & real estate investment.condos and to unique land options, suiting all your requirements for real estate investment.';
                    break;
                case 'all/rent':
                    $this->layout->meta_title= 'Property for Rent in Koh Samui  | Conrad Properties';
                    $this->layout->meta_description = 'Koh Samui Property for rent, luxury villas, houses, condos, apartments, land & real estate investment.';
                    break;
                default:

            }
        }
    }

}
