<?php

namespace Front;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use UserBaseController;

class UserController extends UserBaseController
{

    function __construct()
    {
        $this->layout = 'layouts.front.layout';
        $this->role = '';
        $this->beforeFilter('auth', array('except' => array('getLogin', 'postSignin', 'getLogout')));
    }

    public function getDashboard()
    {

        if (Auth::user()->isAdmin()) {
            return Redirect::to('/administrator/users/dashboard');
        }
        if (Auth::user()->isWriter()) {
            return Redirect::to('/writer/users/dashboard');
        }
        $role = Auth::user()->role();
        $this->layout->content = "Dashboard " . $role->name;
    }

    public function getEditprofile()
    {
        $user = Auth::user();
        $this->layout->content = View::make('users/account-profile-edit', compact('user'));
    }

    public function postEditprofile()
    {
        $post = Input::all();
        $user = Auth::user();

        if ($post['password']) {
            $validator = Validator::make($post, \User::$edit_rules);
            if (!$validator->fails()) {
                $user->password = Hash::make($post['password']);
            }
        } else {
            $validator = Validator::make($post, \User::$profilerules);
        }

        if ($validator->fails()) {
            Session::flash('message', "Validation Failed !");
            return Redirect::back()->withErrors($validator)->withInput();
        }


        $user->firstname = $post['firstname'];
        $user->lastname = $post['lastname'];
        $user->mobile = $post['mobile'];
        $user->phone = $post['phone'];
        $user->save();

        Session::flash('message', "Successfully Updated !");
        return Redirect::to('/users/editprofile');
    }

}
