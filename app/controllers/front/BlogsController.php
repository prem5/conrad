<?php
namespace Front;
use App;
use Blog;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class BlogsController extends \FrontBaseController {

    function __construct() {
        parent::__construct();
        $this->layout = 'layouts.front.sidebar';
    }
	/**
	 * Display a listing of the resource.
	 * GET /blogs
	 *
	 * @return Response
	 */
    public function getIndex($slug) {
        // Changing the slug back to title
        //$name = str_replace("-", " ", \Str::title($slug));
        //dd($name);
        $data['blog'] = Blog::where('slug', '=', $slug)->first();
        if (!$data['blog']) {
            return App::abort(404, '');
        }

        $this->layout->meta_description = strip_tags($data['blog']->meta_description);//strip_tags($data['blog']->teaser);
        $this->layout->og_data = [
            'title'=>$data['blog']->meta_title,
            'description'=> strip_tags($data['blog']->meta_description),
            'url'=>$data['blog']->getUrl(),
            'image'=>$data['blog']->getBlogMainImagePath()
        ];
        $this->layout->head_title = $data['blog']->meta_title;
        $this->layout->content = View::make('blogs.view', compact('data'));
    }

    /**
     * Display a listing of the resource.
     * GET /blogs
     *
     * @return Response
     */
    public function getSearch()
    {
        $blogs = Blog::getBlogListing();

        $data['blogs'] = $blogs->paginate(10);
        $this->layout->meta_title = "Koh Samui Property News | Thailand Real Estate";
        $this->layout->meta_description = "Latest property news from leading independent real estate agency, Conrad Properties, based in Koh Samui, Thailand. Read through our informative blogs here. ";

        $this->layout->content = View::make('blogs.all',compact('data'));
    }

    public function postSearchResult()
    {
        $value = \Input::all();
        $keyword = $value['keyword'];

        $blogs = \Blog::where('status','=',1)->where('name','LIKE','%'.$keyword.'%')
            ->orWhere('content','LIKE','%'.$keyword.'%')
            ->orWhereHas('tags', function($q) use ($keyword)
            {
                $q->where('name','LIKE','%'.$keyword.'%');

            });
        $data['blogs'] = $blogs->orderBy('id','Desc')->paginate(10);

        $this->layout->content = View::make('blogs.all',compact('data'));
    }

    public function getBlogPerTag($slug)
    {
        $tag = \Tag::where('slug', '=', $slug)->first();
        if (!$tag) {
            return App::abort(404, '');
        }
        $data['blogs'] = $tag->blog()->paginate(10);
        $this->layout->noindex = true;
        $this->layout->content = View::make('blogs.all',compact('data'));

    }

    public function postComment()
    {

        $validator = Validator::make($data = \Input::all(), \Comment::$rules);
        if ($validator->fails()) {
            return "Failed validation !";
        }
        $succ = \Comment::create($data);
        if(isset($succ->id))
        {
            return "Thank you for your comment. We will review it and if it is approved it will be published with 24 hours.";
        }
        else
        {
            return "Something might wrong";

        }


    }
}