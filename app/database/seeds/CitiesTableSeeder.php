<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CitiesTableSeeder extends Seeder {

	public function run()
	{


			City::create([
                'name'=>'North West',
                'province_id' => 1,
                'slug'=>\Str::slug('North West'),
                'status'=>1
			]);
        City::create([
            'name'=>'North East',
            'province_id' => 1,
            'slug'=>\Str::slug('North East'),
            'status'=>1
        ]);
        City::create([
            'name'=>'South West',
            'province_id' => 1,
            'slug'=>\Str::slug('South West'),
            'status'=>1
        ]);
        City::create([
            'name'=>'South East',
            'province_id' => 1,
            'slug'=>\Str::slug('South East'),
            'status'=>1
        ]);
	}

}