<?php

class PropertyTypesTableSeeder extends Seeder {
    public function run()
    {
       $property = array(
           array(
               'label' => 'Villa',
               'status'=>'1'
           ),
           array(
               'label' => 'Apartment/Condo',
               'status'=>'1'
           ),
           array(
               'label' => 'House/Townhouse',
               'status'=>'1'
           )

       );

        foreach ($property as $pro) {
            PropertyType::create($pro);
        }
    }

} 