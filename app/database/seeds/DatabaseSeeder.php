<?php

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        Eloquent::unguard();

        $this->call('RolesTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('DistrictsTableSeeder');
        $this->call('PropertyTypesTableSeeder');
        $this->call('PropertyRibbonsTableSeeder');
        $this->call('SettingsTableSeeder');
        $this->call('GuideCategoriesTableSeeder');

        $this->call('CurrenciesTableSeeder');
        $this->call('CountriesTableSeeder');
        $this->call('ProvincesTableSeeder');
        $this->call('CitiesTableSeeder');
        $this->call('AreasTableSeeder');

        $this->call('TemplateTableSeeder');
    }

}
