<?php

class DistrictsTableSeeder extends Seeder
{

    public function run()
    {
        $data = array(
            array(
                'id' => 1,
                'name' => 'Phuket',
                'province_id' => 1
            ),
            array(
                'id' => 2,
                'name' => 'Thalang',
                'province_id' => 1
            ),
            array(
                'id' => 3,
                'name' => 'Kathu',
                'province_id' => 1
            )
        );



        foreach ($data as $d) {
            District::create($d);
        }
    }

    /*function csv_to_array($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = array(
            'id',
            'name',
            'parent_id',
            'province_id'
        );
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                $data[] = $this->array_fill_keys($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    function array_fill_keys($array, $values)
    {
        $dist = array(8 => 1, 9 => 2, 10 => 3);
        $values[] = 1;
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if ($key == 2) {
                    $arraydisplay[$value] = $dist[$values[$key]];
                } else {
                    $arraydisplay[$value] = $values[$key];
                }
            }
        }
        return $arraydisplay;
    }*/

}
