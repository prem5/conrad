<?php

class RolesTableSeeder extends Seeder
{

    public function run()
    {
        $roles = array(
            array(
                'id' => '1',
                'name' => 'Administrator'
            ),
            array(
                'id' => '2',
                'name' => 'Writer'
            )
        );
        foreach ($roles as $role) {
            Role::create($role);
        }
    }

}
