<?php

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        User::create([
            'firstname' => 'Sanjiv',
            'lastname' => 'Kuikel',
            'email' => 'usersanjib@gmail.com',
            'password' => Hash::make('admin123'),
            'role_id' => '1',
            'status' => '1'
        ]);
    }

}
