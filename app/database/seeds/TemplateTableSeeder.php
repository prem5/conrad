<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class TemplateTableSeeder extends Seeder {

	public function run()
	{
        $message = '<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title></title>
<style type="text/css">body {margin: 0; padding: 0; min-width: 100%!important;}
  img {height: auto;}
  p.browser {font-size: 13px; text-align: center; }
  p.browser a {color: #333; }
  .content {width: 100%; max-width: 600px;}
  .header {padding: 20px 30px 20px 30px;}
  .innerpadding {padding: 30px 30px 30px 30px;}
  .borderbottom {border-bottom: 1px solid #f2eeed;}
  .subhead {font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;}
  .h1, .h2, .bodycopy {color: #153643; font-family: sans-serif;}
  .h1 {font-size: 33px; line-height: 38px; font-weight: bold;}
  .h2 {padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;}
  .bodycopy {font-size: 16px; line-height: 22px;}
  .button {text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;}
  .button a {color: #ffffff; text-decoration: none;}
  .footer {padding: 20px 30px 15px 30px;}
  .footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
  .footercopy a {color: #ffffff; text-decoration: underline;}

  @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
  body[yahoo] .hide {display: none!important;}
  body[yahoo] .buttonwrapper {background-color: transparent!important;}
  body[yahoo] .button {padding: 0px!important;}
  body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important;}
  body[yahoo] .unsubscribe {display: block; margin-top: 20px; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none!important; font-weight: bold;}
  }

  /*@media only screen and (min-device-width: 601px) {
    .content {width: 600px !important;}
    .col425 {width: 425px!important;}
    .col380 {width: 380px!important;}
    }*/
</style>
<table bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
			<p class="browser">&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td><!--[if (gte mso 9)|(IE)]>
      <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>
    <![endif]-->
			<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="content">
				<tbody>
					<tr>
						<td bgcolor="#575757" class="header">
						<table align="left" border="0" cellpadding="0" cellspacing="0" width="70">
							<tbody>
								<tr>
									<td height="62" style="padding: 0 20px 0px 0;"><img alt="" border="0" class="fix" height="62" src="http://app.conradproperties.asia/assets/images/logo.png" width="225" /></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td class="innerpadding borderbottom">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td class="h2">Thanks for subscribing to Conrad Properties!</td>
								</tr>
								<tr>
									<td class="bodycopy">We are an independent property agency focusing on professional, reliable real estate services in Thailand and the United Kingdom.<br />
									<br />
									With over a decade of experience in real estate, we offer sale and secure advice, and a huge variety of listings; from condos and apartments, to villas, townhouses &amp; land investments.<br />
									<br />
									Every week you will also receive our weekly newsletter direct to your email, ﬁlled with property highlights &amp; events, and course some stunning properties available for you to purchase your very own piece of paradise!<br />
									<br />
									Kind regards,<br />
									<br />
									<strong>Conrad Krawezyk</strong><br />
									Managing Director<br />
									Mob: 092-959-1299<br />
									Email: Conrad@conradproperties.asia<br />
									Website: www.conradproperties.asia</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#575757" class="footer">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td align="center" class="footercopy">&reg; <a href="http://www.conradproperties.asia">Conrad Properties</a> 2015<br />
									<a class="unsubscribe" href="#"><font color="#ffffff">Unsubscribe</font></a> <span class="hide">from this newsletter instantly</span></td>
								</tr>
								<tr>
									<td align="center" style="padding: 20px 0 0 0;">
									<table border="0" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td style="text-align: center; padding: 0 10px 0 10px;" width="37"><a href="https://www.facebook.com/conradproperties.asia"><img alt="Facebook" border="0" height="37" src="images/facebook.png" width="37" /> </a></td>
												<td style="text-align: center; padding: 0 10px 0 10px;" width="37"><a href="https://twitter.com/Property_Conrad"><img alt="Twitter" border="0" height="37" src="images/twitter.png" width="37" /> </a></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			<!--[if (gte mso 9)|(IE)]>
          </td>
        </tr>
    </table>
    <![endif]--></td>
		</tr>
	</tbody>
</table>
';
			Template::create([
                'name' =>'contact-us',
                'title' => 'Contact Us',
                'subject' => 'Contact Us',
                'message' =>$message
			]);

        Template::create([
            'name' =>'property-sell',
            'title' => 'Property Sell',
            'subject' => 'Property Sell',
            'message' =>$message
        ]);

        Template::create([
            'name' =>'subscriber',
            'title' => 'Subscriber',
            'subject' => 'Subscriber',
            'message' =>$message
        ]);

        Template::create([
            'name' =>'property-enquiry',
            'title' => 'Property Enquiry',
            'subject' => 'Property Enquiry',
            'message' =>$message
        ]);

	}

}