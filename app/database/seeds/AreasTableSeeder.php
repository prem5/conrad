<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AreasTableSeeder extends Seeder {

	public function run()
	{
			Area::create([
                'name' =>'Ban Makham & Nathon',
                'slug'=>\Str::slug('Ban Makham & Nathon'),
                'city_id' => 1,
                'status' => 1
			]);
        Area::create([
            'name' =>'Bang Por & Laem Yai',
            'slug'=>\Str::slug('Bang Por & Laem Yai'),
            'city_id' => 1,
            'status' => 1
        ]);

        Area::create([
            'name' =>'Bangrak & Big Buddha',
            'slug'=>\Str::slug('Bangrak & Big Buddha'),
            'city_id' => 2,
            'status' => 1
        ]);

        Area::create([
            'name' =>'Bophut',
            'slug'=>\Str::slug('Bophut'),
            'city_id' => 2,
            'status' => 1
        ]);

        Area::create([
            'name' =>'Chaweng & Chaweng Noi',
            'slug'=>\Str::slug('Chaweng & Chaweng Noi'),
            'city_id' => 2,
            'status' => 1
        ]);

        Area::create([
            'name' =>'Cheong Mon & Plai Laem',
            'slug'=>\Str::slug('Cheong Mon & Plai Laem'),
            'city_id' => 2,
            'status' => 1
        ]);

        Area::create([
            'name' =>'Maenam & Ban Tai',
            'slug'=>\Str::slug('Maenam & Ban Tai'),
            'city_id' => 2,
            'status' => 1
        ]);

        Area::create([
            'name' =>'Lipa Noi & Ban Saket ',
            'slug'=>\Str::slug('Lipa Noi & Ban Saket '),
            'city_id' => 3,
            'status' => 1
        ]);
        Area::create([
            'name' =>'Namuang',
            'slug'=>\Str::slug('Namuang'),
            'city_id' => 3,
            'status' => 1
        ]);
        Area::create([
            'name' =>'Pang Ka bay & Thong Krut',
            'slug'=>\Str::slug('Pang Ka bay & Thong Krut'),
            'city_id' => 3,
            'status' => 1
        ]);
        Area::create([
            'name' =>'Taling Ngam',
            'slug'=>\Str::slug('Taling Ngam'),
            'city_id' => 3,
            'status' => 1
        ]);
        Area::create([
            'name' =>'Bang Kao, Laem Sor & Laem Set',
            'slug'=>\Str::slug('Bang Kao, Laem Sor & Laem Set'),
            'city_id' => 4,
            'status' => 1
        ]);
        Area::create([
            'name' =>'Lamai & Hua Thanon',
            'slug'=>\Str::slug('Lamai & Hua Thanon'),
            'city_id' => 4,
            'status' => 1
        ]);



    }

}