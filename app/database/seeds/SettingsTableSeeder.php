<?php
class SettingsTableSeeder extends Seeder {
    public function run()
    {
        Setting::create([
            'title'=>'Conrad Prperties Bangkok',
            'description'=>'Donec rutrum congue leo eget malesuada',
            'primary_email'=>'info@conradproperties.asia',
            'display_email'=>'info@conradproperties.asia',
            'display_phone'=>'09 1234567',
            'additional_email'=>'info@conradproperties.asia',
            'address'=>'12 soi soi, moo 4, Sukhumvit, Bankok, Thailand',
            'analtyics_code'=>'UA-65003715-1',
            'twitter_url'=>'https://twitter.com/Property_Conrad',
            'facebook_url'=>'https://www.facebook.com/conradproperties.asia',
            'instagram_url'=>'http://instagram.com/conrad_properties',
            'pinterest_url'=>'http://uk.pinterest.com/conradprop/',
            'google_plus_url'=>'https://plus.google.com/b/113659035049733309252/113659035049733309252',
            'linkedin_url'=>'https://www.linkedin.com/company/conradproperties.asia?trk=biz-companies-cym'
        ]);
    }

} 