<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProvincesTableSeeder extends Seeder {

	public function run()
	{
Province::create([
                'name' =>'Koh Samui',
                'slug' =>\Str::slug('Koh Samui'),
                'country_id' => 1,
                'status' =>1

			]);

	}

}