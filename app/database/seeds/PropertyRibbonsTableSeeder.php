<?php

class PropertyRibbonsTableSeeder extends Seeder {
    public function run()
    {
        $ribbon = array(
            array(
                'label' => 'Recommended',
                'color'=>'F99200',
                'status'=>'1'
            ),
            array(
                'label' => 'New',
                'color'=>'F99200',
                'status'=>'1'
            ),
            array(
                'label' => 'Hot Deal',
                'color'=>'F99200',
                'status'=>'1'
            ),
            array(
                'label' => 'Bargain',
                'color'=>'F99200',
                'status'=>'1'
            ),
            array(
                'label' => 'Negotiable',
                'color'=>'F99200',
                'status'=>'1'
            ),
            array(
                'label' => 'Reduced',
                'color'=>'F99200',
                'status'=>'1'
            ),
            array(
                'label' => 'Sold',
                'color'=>'F99200',
                'status'=>'1'
            ),
            array(
                'label' => 'Updated',
                'color'=>'F99200',
                'status'=>'1'
            )

        );

        foreach ($ribbon as $rib) {
            PropertyRibbon::create($rib);
        }
    }

} 