<?php

class GuideCategoriesTableSeeder extends Seeder {
    public function run()
    {
        $data = array(
            array(
                'id' => 1,
                'name' => 'guide-icon1',
                'image' => 'guide-icon1.png'
            ),
            array(
                'id' => 2,
                'name' => 'guide-icon2',
                'image' => 'guide-icon2.png'
            ),
            array(
                'id' => 3,
                'name' => 'guide-icon3',
                'image' => 'guide-icon3.png'
            ),
            array(
                'id' => 4,
                'name' => 'guide-icon4',
                'image' => 'guide-icon4.png'
            ),
            array(
                'id' => 5,
                'name' => 'guide-icon5',
                'image' => 'guide-icon5.png'
            ),
            array(
                'id' => 6,
                'name' => 'guide-icon6',
                'image' => 'guide-icon2.png'
            ),
            array(
                'id' => 7,
                'name' => 'guide-icon7',
                'image' => 'guide-icon7.png'
            ),
            array(
                'id' => 8,
                'name' => 'guide-icon8',
                'image' => 'guide-icon8.png'
            )
        );



        foreach ($data as $d) {
            GuideCategory::create($d);
        }
    }
} 