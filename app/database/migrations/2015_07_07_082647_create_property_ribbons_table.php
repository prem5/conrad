<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyRibbonsTable extends Migration {
    use UpdatedByFieldTrait;
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('property_ribbons', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('label');
            $table->string('color');
            $table->integer('status');
            $this->updater($table);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('property_ribbons');
	}

}
