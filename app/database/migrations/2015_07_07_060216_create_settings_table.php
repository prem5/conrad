<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {
    use UpdatedByFieldTrait;
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->string('primary_email');
            $table->string('display_email');
            $table->string('display_phone');
            $table->string('additional_email');
            $table->string('address');
            $table->integer('primary_currency')->default('1');
            $table->string('analtyics_code');
            $table->string('twitter_url');
            $table->string('facebook_url');
            $table->string('instagram_url');
            $table->string('pinterest_url');
            $table->string('google_plus_url');
            $table->string('linkedin_url');
            $this->updater($table);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}

}
