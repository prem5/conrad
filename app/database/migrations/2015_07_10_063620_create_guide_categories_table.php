<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuideCategoriesTable extends Migration {
    use UpdatedByFieldTrait;
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guide_categories', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->text('image');
            $this->updater($table);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('guide_categories');
	}

}
