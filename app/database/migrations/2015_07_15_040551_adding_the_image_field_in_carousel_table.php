<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingTheImageFieldInCarouselTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('carousels', function(Blueprint $table)
        {
            $table->text('image')->after('property_id');
        });
	}




}
