<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalleryTable extends Migration {

    use UpdatedByFieldTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('galleries', function(Blueprint $table) {
            $table->increments('id');
            $table->string('photo');
            $table->integer('primary');
            $table->integer('status');
            $table->integer('property_id');
            $this->updater($table);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('galleries');
    }

}
