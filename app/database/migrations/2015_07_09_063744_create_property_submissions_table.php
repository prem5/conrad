<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreatePropertySubmissionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_submissions', function (Blueprint $table) {
            $table->create();
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone')->nullable();
            $table->integer('property_type')->unsigned();
            $table->foreign('property_type')->references('id')->on('property_types');
            $table->string('terms');
            $table->string('location');
            $table->string('price');
            $table->string('image');
            $table->text('extra_information');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_submissions', function (Blueprint $table) {
            $table->drop();
        });
    }

}
