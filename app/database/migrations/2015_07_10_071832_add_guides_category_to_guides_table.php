<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddGuidesCategoryToGuidesTable extends Migration {
    use UpdatedByFieldTrait;
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('guides', function(Blueprint $table)
		{
            $table->integer('guide_category_id')->after('slug');
		});
	}
}
