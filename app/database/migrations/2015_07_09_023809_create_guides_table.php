<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuidesTable extends Migration {
    use UpdatedByFieldTrait;
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guides', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('content');
            $table->text('teaser');
            $table->string('meta_title');
            $table->text('meta_description');
            $table->string('photo');
            $table->integer('status')->default(1);
            $this->updater($table);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('guides');
	}

}
