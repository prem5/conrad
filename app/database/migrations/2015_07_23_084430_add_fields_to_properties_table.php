<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFieldsToPropertiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('properties', function(Blueprint $table)
		{
/*            $table->boolean('daily_rent')->after('sale');
            $table->boolean('monthly_rent')->after('daily_rent');*/

            $table->integer('daily_rental_price_from')->after('rent');
            $table->integer('daily_rental_price_to')->after('daily_rental_price_from');

            $table->integer('monthly_rental_price_from')->after('daily_rental_price_to');
            $table->integer('monthly_rental_price_to')->after('monthly_rental_price_from');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('properties', function(Blueprint $table)
		{
            /*$table->dropColumn('daily_rent');
            $table->dropColumn('monthly_rent');*/

            $table->dropColumn('daily_rental_price_from');
            $table->dropColumn('daily_rental_price_to');

            $table->dropColumn('monthly_rental_price_from');
            $table->dropColumn('monthly_rental_price_to');
		});
	}

}
