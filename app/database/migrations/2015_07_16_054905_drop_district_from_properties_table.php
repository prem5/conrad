<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DropDistrictFromPropertiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('properties', function(Blueprint $table)
        {
            /*$table->dropColumn('district_id');
            $table->dropColumn('subdistrict_id');*/
            $table->integer('country_id')->after('sale_price');
            $table->integer('province_id')->after('country_id');
            $table->integer('city_id')->after('province_id');
            $table->integer('area_id')->after('city_id');

        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('properties', function(Blueprint $table)
		{
            $table->dropColumn('country_id');
            $table->dropColumn('province_id');
            $table->dropColumn('city_id');
            $table->dropColumn('area_id');
		});
	}

}
