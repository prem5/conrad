<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertiesTable extends Migration {

    use UpdatedByFieldTrait;
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('properties', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('title',255);
            $table->string('slug',255);
            $table->text('overview');
            $table->boolean('sale');
            $table->boolean('rent');
            $table->float('rental_price_from');
            $table->float('rental_price_to');
            $table->float('sale_price');
            $table->integer('district_id');
            $table->integer('subdistrict_id');
            $table->text('latitude');
            $table->text('longitude');
            $table->text('key_features');
            $table->integer('type');
            $table->integer('ribbon');
            $table->string('bedrooms');
            $table->string('bathrooms');
            $table->string('view');
            $table->string('parking');
            $table->string('swimming_pool');
            $table->string('land_area');
            $table->string('building_area');
            $table->string('living_room');
            $table->string('dining_room');
            $table->string('family_room');
            $table->string('kitchen');
            $table->string('study_office');
            $table->string('year_build');
            $table->string('floors');
            $table->string('pets');
            $table->string('terrace_rooftop');
            $table->text('features');
            $table->string('community_features');
            $table->string('library');
            $table->string('cinema_room');
            $table->string('security');
            $table->string('tennis');
            $table->string('furnished');
            $table->string('rental_program');
            $table->string('restaurant');
            $table->string('concierge');
            $table->integer('gallery_id');
            $table->boolean('published');
            $table->text('meta_title');
            $table->text('meta_description');
            $this->updater($table);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('properties');
	}

}
