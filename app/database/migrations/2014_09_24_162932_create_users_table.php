<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{

    use UpdatedByFieldTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('street_address1');
            $table->string('street_address2');
            $table->string('city');
            $table->integer('province');
            $table->integer('post_code');
            $table->string('phone');
            $table->string('mobile');
            $table->string('remember_token');
            $table->integer('role_id');
            $table->integer('status');
            $table->string('forgot_password');
            $this->updater($table);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }

}
