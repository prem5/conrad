<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSlugFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('countries', function(Blueprint $table)
        {
            $table->string('slug')->after('name');
        });
        Schema::table('provinces', function(Blueprint $table)
        {
            $table->string('slug')->after('name');
        });
        Schema::table('cities', function(Blueprint $table)
        {
            $table->string('slug')->after('name');
        });
        Schema::table('areas', function(Blueprint $table)
        {
            $table->string('slug')->after('name');
        });
        Schema::table('tags', function(Blueprint $table)
        {
            $table->string('slug')->after('name');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('countries', function(Blueprint $table)
        {
            $table->dropColumn('slug');
        });
        Schema::table('provinces', function(Blueprint $table)
        {
            $table->dropColumn('slug');
        });
        Schema::table('cities', function(Blueprint $table)
        {
            $table->dropColumn('slug');
        });
        Schema::table('areas', function(Blueprint $table)
        {
            $table->dropColumn('slug');
        });
        /*Schema::table('tags', function(Blueprint $table)
        {
            $table->dropColumn('slug');
        });*/
	}

}
