<?php

trait UpdatedByTrait
{

    public static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            if (Auth::user()) {
                $post->created_by = Auth::user()->id;
                $post->updated_by = Auth::user()->id;
            } else {
                $post->created_by = 0;
                $post->updated_by = 0;
            }
        });

        static::updating(function ($post) {
            if (Auth::user()) {
                $post->updated_by = Auth::user()->id;
            } else {
                $post->updated_by = 0;
            }
        });
    }

}
