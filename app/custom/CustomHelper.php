<?php

class CustomHelper
{
    public static function genPdfThumbnail($source, $target)
    {
        //dd($source,$target);
        try {
            $im = new Imagick($source . "[0]"); // 0-first page, 1-second page
            $im->setImageColorspace(255); // prevent image colors from inverting
            $im->setimageformat("jpeg");
            $im->thumbnailimage(200, 200); // width and height
            $im->writeimage($target);
            $im->clear();
            $im->destroy();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public static function genThumbs($src, $des, $filename, $area)
    {
        $sizes = Config::get("conrad.image_sizes_$area");

        $size = getimagesize($src);
        list($width, $height) = $size;
        $aspectRatio = $width / $height;

        $ext = pathinfo($src, PATHINFO_EXTENSION);
        foreach ($sizes as $name_part => $size) {

            $dimensions = explode("x", $size);
            $width_new = $dimensions[0];
            $height_new = $dimensions[1];

            if ($width >= $width_new && $height >= $height_new) {
                $image = new SimpleImage();
                $image->load($src);

                // What will height be after resizing width?
                $temp_height = $width_new / $aspectRatio;
                if ($temp_height < $height_new) {
                    // calculate required width to give the correct height
                    $temp_width = $height_new * $aspectRatio;
                    $image->resizeToWidth($temp_width);
                } else {
                    $image->resizeToWidth($width_new);
                }
                $image->crop($width_new, $height_new);

                $filename_this = $filename . "-" . $name_part . '.' . $ext;
                $targetFile_this = $des . $filename_this;
                $image->save($targetFile_this);
            }
        }
    }

    public static function checkCookie($slug)
    {
        $cookieData = \Cookie::get('favourites');
        if (!empty($cookieData)):
            if (in_array($slug, $cookieData))
                return True;
            else
                return False;

        else:
            return False;
        endif;
    }

    public static function countFavorites()
    {
        $cookieData = \Cookie::get('favourites');
        $data = array();
        if (!empty($cookieData)):
            foreach ($cookieData as $key => $val):
                $available = \Property::find($key);
                if ($available)
                    $data[$key] = $val;
            endforeach;
        endif;
        Cookie::queue('favourites', $data, 10080);
        return count($data);
    }

    public static  function format_price($number)
    {
        $arr = explode(".",$number);
        if ($arr[1]=="00") { //no elements after the decimal point
            return $arr[0] ; //only show the first part with M
        } else { // we have some numbers after the decimal
            if (substr($arr[1], 1)==0) {
                return $arr[0].".".substr($arr[1], 0,1); //see if the second digit after the decimal exists or is a zero
            } else {
                return $arr[0] . "." . $arr[1] ;
            }
        }
    }

    public static function  getImageUrl($path = Null)
    {
        $link = Config::get("conrad.imageserver");
        if(!is_null($path)){
            $link .= $path;
        }
        return $link;
    }
    public static function dataUri($image, $mime = ''){
        return 'data: '.(function_exists('mime_content_type') ? mime_content_type($image) : $mime).';base64,'.base64_encode(file_get_contents($image));
    }
}