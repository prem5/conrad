<?php

trait UpdatedByFieldTrait
{

    /**
     * Add creation and update timestamps to the table.
     *
     * @return void
     */
    public function updater($table)
    {
        $table->integer('created_by')->default(1);

        $table->integer('updated_by')->default(1);
    }

}
