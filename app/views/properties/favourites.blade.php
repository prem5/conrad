<!-- begin propertie block -->
<div class="propertie-block">
    <div class="centering">
        <h2>Your Favourite Properties</h2>
        <ul class='tabs clearfix'>
            <li><a href='#buy'>Buy</a></li>
            <li><a href='#rent'>Rent</a></li>
            <div class="clear"></div>
        </ul>

        <div id="buy" class="tabOuter property-info tabouter">
                @if(!empty($data))
                @if(!$data['sell']->isEmpty())
                <ul>
                    @foreach($data['sell'] as $f)
                    <li>
                        <div class="cols favourite-{{$f->id}}" id="favourite-{{$f->id}}">
                            <div class="image">
                                @if($f->getRibbon())
                                <span class="tag">{{$f->getRibbon()}}</span>
                                @endif
                                <a href="{{$f->getUrl()}}">
                                    <img src="{{$f->getPrimaryImageSmall()}}" alt="{{$f->title}}"/>
                                </a>
                                <?php $cookie = CustomHelper::checkCookie($f->slug); ?>
                                <span
                                    class="remove property-{{$f->id}} {{'heart_class'}} <?php echo ($cookie) ? 'selected' : ''; ?>"
                                    id="featureproperty-{{$f->id}}" id="featuredproperty-{{$f->id}}"> X </span>
                            </div>

                            <div class="text">

                                <h3>{{$f->title}}</h3>

                                <div class="bottom">
                                    <a href="{{$f->getAddressUrl()}}">{{$f->getFullAddress()}}</a>
                                    <ul>
                                        @if($f->bedrooms)
                                        <li><span><i class="icon-bedroom"></i>{{$f->bedrooms}}</span></li>
                                        @endif
                                        @if($f->bathrooms)
                                        <li><span><i class="icon-bath"></i>{{$f->bathrooms}}</span></li>
                                        @endif

                                       @if($f->land_area)
                                                                                   <li><img src="/assets/images/unit-icon.svg" alt="area" />{{$f->land_area}}</li>
                                                                                   @endif
                                    </ul>
                                </div>

                                <strong>{{$f->getPrice()}}</strong>
                                <a href="{{$f->getUrl()}}" class="btn">View DETAILS</a>

                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>

                @endif
                    <div class="clear"></div>
         </div>
        <div id="rent" class="tabOuter reviewList tabouter">
                    @if(!$data['rent']->isEmpty())
                    <ul>
                        @foreach($data['rent'] as $f)
                        <li>
                            <div class="cols favourite-{{$f->id}}" id="favourite-{{$f->id}}">
                                <div class="image">
                                    @if($f->getRibbon())
                                    <span class="tag">{{$f->getRibbon()}}</span>
                                    @endif
                                    <a href="{{$f->getUrl()}}">
                                        <img src="{{$f->getPrimaryImageSmall()}}" alt="{{$f->title}}"/>
                                    </a>
                                    <?php $cookie = CustomHelper::checkCookie($f->slug); ?>
                                    <span
                                        class="remove property-{{$f->id}} {{'heart_class'}} <?php echo ($cookie) ? 'selected' : ''; ?>"
                                        id="featureproperty-{{$f->id}}" id="featuredproperty-{{$f->id}}"> X </span>
                                </div>

                                <div class="text">

                                    <h3>{{$f->title}}</h3>

                                    <div class="bottom">
                                        <a href="{{$f->getAddressUrl()}}">{{$f->getFullAddress()}}</a>
                                        <ul>
                                            @if($f->bedrooms)
                                            <li><span><i class="icon-bedroom"></i>{{$f->bedrooms}}</span></li>
                                            @endif
                                            @if($f->bathrooms)
                                            <li><span><i class="icon-bath"></i>{{$f->bathrooms}}</span></li>
                                            @endif
                                            @if($f->land_area)
                                            <li><img src="/assets/images/unit-icon.svg" alt="area" />{{$f->land_area}}</li>
                                            @endif
                                        </ul>
                                    </div>

                                    <strong>{{$f->getPrice('rent')}}</strong>
                                    <a href="{{$f->getUrl()}}" class="btn">View DETAILS</a>

                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    @endif
                    @else
                    {{'You have no favourites property'}}
                    @endif
            <div class="clear"></div>
        </div>

    </div>
    {{----}}
</div>

<!-- finish propertie block -->