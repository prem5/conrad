<div class="row row-fluid">
    <div class="col-md-7">
        <div class="panel panel-default">
            <div class="panel-heading">General</div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('title')){echo 'has-error';}?>">
                    {{Form::label('title', 'Title', array('class'=>'control-label'))}}
                    {{ Form::text('title', $data->title, array('onchange'=>empty($data->slug)?'getListingSlug("#title","add");return true;':NULL,
                    'class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('slug')){echo 'has-error';}?>">
                    <div class="row">
                        <div class="col-md-4">{{Form::label('slug', 'Slug', array('class'=>'control-label'))}}</div>
                            <div class="col-md-9"> {{ Form::text('slug', $data->slug,array('class'=>'form-control')) }}</div>
                        <div class="col-md-3">{{Form::button('Update Slug' ,array('onclick'=>'getListingSlug("#title","edit");return true;','class'=>'btn btn-success'))}}</div>
                    </div>

                </div>
                <div class="form-group <?php if( $errors->first('reference_code')){echo 'has-error';}?>">
                    @if(isset($data->reference_code))
                    {{Form::label('reference_code', 'Reference Code', array('class'=>'control-label'))}}
                    {{(isset($data->reference_code))?": ".$data->reference_code:''}}
                    @endif
                </div>
                <div class="form-group <?php if( $errors->first('overview')){echo 'has-error';}?>">
                    {{Form::label('overview','Overview', array('class'=>'control-label'))}}
                    {{ Form::textarea('overview', $data->overview,array('class'=>'form-control','id'=>'editor1')) }}
                </div>
                <script>
                    CKEDITOR.replace('editor1', {
                        toolbar: [
                            {name: 'document', items: ['Bold', 'Italic', "Underline", "Link", "Maximize", "Source"]},
                            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] }
                        ],
                        filebrowserBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html',
                        filebrowserImageBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Images',
                        filebrowserFlashBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Flash',
                        filebrowserUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                        filebrowserImageUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                        filebrowserFlashUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                    });
                </script>
                <div class="form-group <?php if( $errors->first('key_features')){echo 'has-error';}?>">
                    {{Form::label('key_features','Key Features', array('class'=>'control-label'))}}
                    {{ Form::textarea('key_features',
                    $data->key_features,array('class'=>'form-control','id'=>'editor2')) }}
                </div>
                <script>
                    CKEDITOR.replace('editor2', {
                        toolbar: [
                            {name: 'document', items: ['Bold', 'Italic', "Underline", "Link", "Maximize", "Source","BulletedList"]},
                            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] }
                        ],
                        filebrowserBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html',
                        filebrowserImageBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Images',
                        filebrowserFlashBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Flash',
                        filebrowserUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                        filebrowserImageUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                        filebrowserFlashUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                    });
                </script>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Sale / Rent</div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('province_id')){echo 'has-error';}?>">
                    {{Form::label('default_currency','Default Currency', array('class'=>'control-label'))}}
                    {{ Form::select('default_currency',$currences,$data->default_currency,array('class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('sale_price')){echo 'has-error';}?>">
                    {{Form::label('sale_price','Sale Price', array('class'=>'control-label'))}}
                    {{ Form::text('sale_price', $data->sale_price,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('daily_rental_price_from')){echo 'has-error';}?>">
                    {{Form::label('daily_rental_price_from','Daily Rental Price From', array('class'=>'control-label'))}}
                    {{ Form::text('daily_rental_price_from', $data->daily_rental_price_from,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('daily_rental_price_to')){echo 'has-error';}?>">
                    {{Form::label('daily_rental_price_to','Daily Rental Price To', array('class'=>'control-label'))}}
                    {{ Form::text('daily_rental_price_to', $data->daily_rental_price_to,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('monthly_rental_price_from')){echo 'has-error';}?>">
                    {{Form::label('monthly_rental_price_from','Monthly Rental Price', array('class'=>'control-label'))}}
                    {{ Form::text('monthly_rental_price_from', $data->monthly_rental_price_from,array('class'=>'form-control')) }}
                </div>


                <div class="form-group <?php if( $errors->first('country_id')){echo 'has-error';}?>">
                    {{Form::label('country_id','Location Country', array('class'=>'control-label'))}}
                    {{ Form::select('country_id',
                    Country::getCountryForSelect(),$data->country_id,array('class'=>'form-control')) }}
                </div>

                @if($data->country_id == '')
                {{--*/$disable='disabled'/*--}}
                @else
                {{--*/$disable=''/*--}}
                @endif

                <div class="form-group <?php if( $errors->first('province_id')){echo 'has-error';}?>">
                    {{Form::label('province_id','Location Province', array('class'=>'control-label'))}}
                    {{ Form::select('province_id',
                    Province::getProvinceForSelect(),$data->province_id,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('city_id')){echo 'has-error';}?>">
                    {{Form::label('city_id','Location City', array('class'=>'control-label'))}}
                    {{ Form::select('city_id', City::getCitiesForSelect(),$data->city_id,array('class'=>'form-control','onchange'=>'citychange(this)')) }}
                </div>

                <div class="form-group <?php if( $errors->first('area_id')){echo 'has-error';}?>">
                    {{Form::label('area_id','Location Area', array('class'=>'control-label'))}}
                    {{ Form::select('area_id', Area::getAreaForSelect(),$data->area_id,
                    array('class'=>'form-control')) }}
                </div>


                {{--
                <div class="form-group">
                    {{Form::label('subdistrict_id','Location Secondary', array('class'=>'control-label'))}}
                    {{ Form::select('subdistrict_id',
                    District::getSubdistrictForSelect(),$data->subdistrict_id,array('class'=>'form-control')) }}
                </div>
                --}}
            </div>
        </div>
    </div>
</div>
<div class="col-md-7">
    <div class="panel panel-default">
        <div class="panel-heading">Mandatory Fields</div>
        <div class="panel-body">
            <div class="form-group <?php if( $errors->first('type')){echo 'has-error';}?>">
                {{Form::label('type','Type', array('class'=>'control-label'))}}
                {{ Form::select('type', PropertyType::getTypeForSelect(),(isset($data->type))? $data->type:'',
                array('class'=>'form-control')) }}
            </div>

           <!-- <div class="form-group <?php if( $errors->first('bedrooms')){echo 'has-error';}?>">
                {{Form::label('bedrooms','Bedrooms', array('class'=>'control-label'))}}
                {{ Form::text('bedrooms', $data->bedrooms,array('class'=>'form-control')) }}
            </div> -->
             <div class="form-group <?php if( $errors->first('min_bedroom')){echo 'has-error';}?>">
                {{Form::label('bedrooms','Bedrooms', array('class'=>'control-label'))}}<br />
                {{Form::label('min_bedroom','Minimum', array('class'=>'control-label'))}}
                {{ Form::text('min_bedroom', $data->min_bedroom,array('class'=>'form-control')) }}
                {{Form::label('max_bedroom','Maximum', array('class'=>'control-label'))}}
                {{ Form::text('max_bedroom', $data->max_bedroom,array('class'=>'form-control')) }}
            </div>

            <div class="form-group <?php if( $errors->first('bathrooms')){echo 'has-error';}?>">
                {{Form::label('bathrooms','Bathrooms', array('class'=>'control-label'))}}
                    {{ Form::text('bathrooms', $data->bathrooms,array('class'=>'form-control')) }}
            </div>

            <div class="form-group <?php if( $errors->first('view')){echo 'has-error';}?>">
                {{Form::label('view','View', array('class'=>'control-label'))}}
                {{ Form::select('view', \Config::get('conrad.property_view'),(isset($data->view))? $data->view:'',array('class'=>'form-control')) }}
            </div>

            <div class="form-group <?php if( $errors->first('parking')){echo 'has-error';}?>">
                {{Form::label('parking','Parking', array('class'=>'control-label'))}}
                {{ Form::text('parking', $data->parking,array('class'=>'form-control')) }}
            </div>

            <div class="form-group <?php if( $errors->first('swimming_pool')){echo 'has-error';}?>">
                {{Form::label('swimming_pool','Swimming Pool', array('class'=>'control-label'))}}
                {{ Form::text('swimming_pool', $data->swimming_pool,array('class'=>'form-control')) }}
            </div>

            <div class="form-group <?php if( $errors->first('land_area')){echo 'has-error';}?>">
                {{Form::label('land_area','Land Size', array('class'=>'control-label'))}}
                {{ Form::text('land_area', $data->land_area,array('class'=>'form-control')) }}
            </div>

            <div class="form-group <?php if( $errors->first('building_area')){echo 'has-error';}?>">
                {{Form::label('building_area','Size', array('class'=>'control-label'))}}
                {{ Form::text('building_area', $data->building_area,array('class'=>'form-control')) }}
            </div>

            <div class="form-group <?php if( $errors->first('ribbon')){echo 'has-error';}?>">
                {{Form::label('ribbon','Ribbon', array('class'=>'control-label'))}}
                {{ Form::select('ribbon',PropertyRibbon::getRibbonForSelect(),(isset($data->ribbon))? $data->ribbon :''
                ,array('class'=>'form-control')) }}
            </div>

            <div class="form-group <?php if( $errors->first('features')){echo 'has-error';}?>">
                {{Form::label('features','Features', array('class'=>'control-label'))}}
                {{ Form::textarea('features', $data->features,array('class'=>'form-control','id'=>'editor3')) }}
            </div>

            <script>
                CKEDITOR.replace('editor3', {
                    toolbar: [
                        {name: 'document', items: ['Bold', 'Italic', "Underline", "Link", "Maximize", "Source"]},
                        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] }
                    ],
                    height:400,
                    filebrowserBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html',
                    filebrowserImageBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Images',
                    filebrowserFlashBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Flash',
                    filebrowserUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    filebrowserImageUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                    filebrowserFlashUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                });
            </script>


            <div class="form-group <?php if( $errors->first('community_features')){echo 'has-error';}?>">
                {{Form::label('community_features','Community Features', array('class'=>'control-label'))}}
                {{ Form::textarea('community_features',
                $data->community_features,array('class'=>'form-control','id'=>'editor4')) }}
            </div>
            <script>
                CKEDITOR.replace('editor4', {
                    toolbar: [
                        {name: 'document', items: ['Bold', 'Italic', "Underline", "Link", "Maximize", "Source"]},
                        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] }
                    ],
                    height:400,
                    filebrowserBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html',
                    filebrowserImageBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Images',
                    filebrowserFlashBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Flash',
                    filebrowserUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    filebrowserImageUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                    filebrowserFlashUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                });
            </script>
        </div>
    </div>
</div>
<div class="row row-fluid">
    <div class="col-md-5">
        <div class="panel panel-default">
            <div class="panel-heading">Optional Fields</div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('living_room')){echo 'has-error';}?>">
                    {{Form::label('living_room','Living Room', array('class'=>'control-label'))}}
                    {{ Form::text('living_room', $data->living_room,array('class'=>'form-control')) }}
                </div>


                <div class="form-group <?php if( $errors->first('dining_room')){echo 'has-error';}?>">
                    {{Form::label('dining_room','Dining Room', array('class'=>'control-label'))}}
                    {{ Form::text('dining_room', $data->dining_room,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('family_room')){echo 'has-error';}?>">
                    {{Form::label('family_room','Family Room', array('class'=>'control-label'))}}
                    {{ Form::text('family_room', $data->family_room,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('kitchen')){echo 'has-error';}?>">
                    {{Form::label('kitchen','Kitchen', array('class'=>'control-label'))}}
                    {{ Form::text('kitchen', $data->kitchen,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('study_office')){echo 'has-error';}?>">
                    {{Form::label('study_office','Study/Office', array('class'=>'control-label'))}}
                    {{ Form::text('study_office', $data->study_office,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('floors')){echo 'has-error';}?>">
                    {{Form::label('floors','Floors', array('class'=>'control-label'))}}
                    {{ Form::text('floors', $data->floors,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('pets')){echo 'has-error';}?>">
                    {{Form::label('pets','Pets', array('class'=>'control-label'))}}
                    {{ Form::text('pets', $data->pets,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('terrace_rooftop')){echo 'has-error';}?>">
                    {{Form::label('terrace_rooftop','Terrace/Rooftop', array('class'=>'control-label'))}}
                    {{ Form::text('terrace_rooftop', $data->terrace_rooftop,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('library')){echo 'has-error';}?>">
                    {{Form::label('library','Library', array('class'=>'control-label'))}}
                    {{ Form::text('library', $data->library,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('cinema_room')){echo 'has-error';}?>">
                    {{Form::label('cinema_room','Cinema Room', array('class'=>'control-label'))}}
                    {{ Form::text('cinema_room', $data->cinema_room,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('security')){echo 'has-error';}?>">
                    {{Form::label('security','Security', array('class'=>'control-label'))}}
                    {{ Form::text('security', $data->security,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('tennis')){echo 'has-error';}?>">
                    {{Form::label('tennis','Tennis', array('class'=>'control-label'))}}
                    {{ Form::text('tennis', $data->tennis,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('furnished')){echo 'has-error';}?>">
                    {{Form::label('furnished','Furnished', array('class'=>'control-label'))}}
                    {{ Form::text('furnished', $data->furnished,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('rental_program')){echo 'has-error';}?>">
                    {{Form::label('rental_program','Rental Program', array('class'=>'control-label'))}}
                    {{ Form::text('rental_program', $data->rental_program,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('restaurant')){echo 'has-error';}?>">
                    {{Form::label('restaurant','Restaurant', array('class'=>'control-label'))}}
                    {{ Form::text('restaurant', $data->restaurant,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('concierge')){echo 'has-error';}?>">
                    {{Form::label('concierge','Concierge', array('class'=>'control-label'))}}
                    {{ Form::text('concierge', $data->concierge,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('year_build')){echo 'has-error';}?>">
                    {{Form::label('year_build','Build Year', array('class'=>'control-label'))}}
                    {{ Form::text('year_build', $data->year_build,array('class'=>'form-control')) }}
                </div>
            </div>

        </div>
    </div>
</div>
<div class="row row-fluid">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">Map</div>
            <fieldset class="gllpLatlonPicker">
                <input type="text" class="gllpSearchField">
                <input type="button" value="search" class="gllpSearchButton">

                <div class="gllpMap" style="  height: 250px">Google Maps</div>


                <div class="panel-body   col-md-6">
                    <label for="latitude">Latitude</label>

                    <input type="text" value='{{(isset($data->latitude))?"$data->latitude":"9.496386936743432"}}'
                           id="latitude" class="form-control gllpLatitude" name="latitude">
                </div>

                <div class="panel-body   col-md-6">
                    <label for="map_lon">Longitude</label>

                    <input type="text" value='{{(isset($data->longitude))?"$data->longitude":"99.99369621276855"}}'
                           id="longitude" class="form-control gllpLongitude" name="longitude">
                </div>

                <input type="hidden" value="11" class="gllpZoom">
            </fieldset>
            <div class="panel-footer">Double click to place a marker, or drag a current marker.</div>
        </div>
    </div>
</div>
<div class="row row-fluid">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">Meta Data</div>
            <fieldset class="gllpLatlonPicker">



                <div class="panel-body   col-md-12">
                    <label for="meta_title">Meta Title</label>

                    <input type="text" value='{{(isset($data->meta_title))?"$data->meta_title":""}}'
                           id="meta_title" class="form-control" name="meta_title">
                </div>

                <div class="panel-body   col-md-12">
                    <label for="meta_description">Meta Description</label>

                    {{ Form::textarea('meta_description',
               $data->meta_description,array('class'=>'form-control','id'=>'meta_description')) }}

                </div>

                <input type="hidden" value="11" class="gllpZoom">
            </fieldset>
        </div>
    </div>
</div>
<div class="panel panel-default">


    <div class="panel-body">
        {{Form::label('published', 'Publish', array('class'=>'control-label'))}}
        <div class="form-group">
            {{ Form::checkbox('published', 1,$data->published) }}
        </div>
    </div>
</div>

