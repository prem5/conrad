<span style="float: right; margin-bottom: 10px;">{{HTML::link('/administrator/features/create','Add Featured Property',['class'=>"btn btn-primary"])}}</span>
{{ Form::open(array('url'=>'', 'class'=>'','id'=>'featured-index-form')) }}
{{Form::Close()}}
<table id="featured-listing" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Property</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    {{--*/$i=1/*--}}
    @foreach($data as $d)
    <tr id="{{$d->id}}">
        <td>{{$i}}</td>
        <td>{{$d->title}}</td>
        <td>
            {{HTML::link('/administrator/features/removefeature/'.$d->id,'Remove Featured Property',array('class'=>'btn
            btn-warning btn-small', 'onClick'=>"return confirm('Do you really want to remove from the feature property ?')"))}}
        </td>
    </tr>
    {{--*/$i++/*--}}
    @endforeach
    </tbody>
</table>