<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Subject</th>
        <th>Property Name</th>
        <th>Reference Code</th>
        <th>Message</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach($roles as $d)
    <tr id="{{$d->id}}">
        <td>{{$i}}</td>
        <td>{{$d->name}}</td>
        <td>{{$d->email}}</td>
        <td>{{$d->phone}}</td>
        <td>{{$d->subject}}</td>
        <td>{{ ($d->getPropertyName()->first())? $d->getPropertyName()->first()->title : ' '}}</td>
        <td>{{ ($d->getPropertyName()->first())? $d->getPropertyName()->first()->reference_code : ' '}}</td>
        <td>{{$d->message}}</td>
        <td>
            {{HTML::link('/administrator/property-enquiry/show/'.$d->id,'View Detail',array('class'=>'btn btn-warning btn-small'))}}
            {{HTML::link('/administrator/property-enquiry/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning
            btn-small', 'onClick'=>"return confirm('Do you really want to delete ?')"))}}
        </td>
        
    </tr>
    <?php $i++; ?>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="9">{{$roles->links()}}</td>
        </tr>
    </tfoot>
</table>