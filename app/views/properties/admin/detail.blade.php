<table>
    <thead>
    <tr>
        <td>Name : {{$data->name}}</td>
    </tr>
    <tr>
        <td>Email : {{$data->email}}</td>
    </tr>
    <tr>
        <td>Phone : {{$data->phone}}</td>
    </tr>
    <tr>
        <td>Property Type : {{$data->propertyTypes->label}}</td>
    </tr>
    <tr>
        <td>Terms : {{$data->terms}}</td>
    </tr>
    <tr>
        <td>Location : {{$data->location}}</td>
    </tr>
    <tr>
        <td>Price : {{$data->price}}</td>
    </tr>
    <tr>
        <td>Extra Information : {{$data->extra_information}}</td>
    </tr>
    </thead>
    <tbody>

    <tr id=>
    </tr>
    </tbody>
</table>
<span>Image</span>
<div class="">
    <img src="/uploads/submissions/{{$data->image}}" alt="{{$data->name}}" >
</div>