{{ Form::open(array('url'=>'/administrator/features/store', 'class'=>'')) }}
<div class="row row-fluid">
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    {{Form::label('property_id', 'Property', array('class'=>'control-label'))}}
                    {{ Form::select('property_id',$data) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                </div>
            </div>
        </div>
    </div>
</div>

{{Form::close()}}