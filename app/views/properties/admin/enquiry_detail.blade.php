<div class="row row-fluid">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Enquiry</h3>
            </div>
            <div class="panel-body">
                <div class="form-group  <?php if( $errors->first('name')){echo 'has-error';}?>">
                    {{Form::label('name', 'Name', array('class'=>'control-label'))}}
                    {{$data->name}}
                </div>
                <div class="form-group  ">
                    <label  class="control-label">Email:</label>
                    {{$data->email}}
                </div>
                <div class="form-group  <?php if( $errors->first('email')){echo 'has-error';}?>">
                    {{Form::label('email', 'Phone:', array('class'=>'control-label'))}}
                    {{$data->phone}}
                </div>
                <div class="form-group  ">
                    <label  class="control-label">Subject:</label>
                    {{$data->subject}}
                </div>
                <div class="form-group  ">
                    <label  class="control-label">Property Name:</label>
                    {{ ($data->getPropertyName()->first())? $data->getPropertyName()->first()->title : ' '}}
                </div>
                <div class="form-group  ">
                    <label  class="control-label">Reference Code:</label>
                    {{ ($data->getPropertyName()->first())? $data->getPropertyName()->first()->reference_code : ' '}}
                </div>
                <div class="form-group  ">
                    <label  class="control-label">Message:</label>
                    {{$data->message }}
                </div>

                <a href="{{url('/administrator/property-enquiry')}}" class="btn btn-warning btn-small">Back</a>
            </div>
        </div>
    </div>
</div>

