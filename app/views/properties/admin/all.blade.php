<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    @foreach($roles as $d)
    <tr id="{{$d->id}}">
        <td>{{$d->id}}</td>
        <td>{{$d->name}}</td>
        <td>{{$d->email}}</td>
        <td>{{$d->phone}}</td>
        <td>
            {{HTML::link('/administrator/property-submission/view/'.$d->id,'More',array('class'=>'btn btn-warning
            btn-small'))}}
            {{HTML::link('/administrator/property-submission/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning
            btn-small', 'onClick'=>"return confirm('Do you really want to delete ?')"))}}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>