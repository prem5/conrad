<?php
include(app_path() . '/custom/mobile_detect.php');
$detect = new mobile_detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
?>
<!-- begin luxury block -->
<div class="luxury-block">

    <div class="centering">

        <div class="display">

            {{--<div class="left"><span>Displaying property {{$data['property']->getType()}} of {{$data['property']->getProvince()}}</span>--}}
            {{--</div>--}}
            <div class="right">
                <span>
                    <a class="prev" href="{{$data['property']->getPrevious()->getUrl()}}">&lt; &lt; Previous</a>
                    <a class="back" href="{{url('search')}}">Back to search Results</a>
                    <a class="next" href="{{$data['property']->getNext()->getUrl()}}">Next &gt;&gt;</a>
                </span>
            </div>

        </div>

        <h1>{{$data['property']->title}}</h1>

        @if ($data['property']->gallery()->get()->count())

            <div class="slider">

                <div class="wrap">

                    <div class="control1">
                        <a href="#" class="cycle-prev1">&laquo; prev</a>
                        <a href="#" style="color: #red;" class="cycle-next1">next &raquo;</a>
                    </div>

                    <div class="show cycle-slideshow asd" data-cycle-swipe="true" data-cycle-swipe-fx="scrollHorz"
                         id="cycle-1" data-cycle-slides="> div" data-cycle-timeout="0" data-cycle-prev=".cycle-prev1"
                         data-cycle-next=".cycle-next1" data-cycle-log="false">
                        {{-- */$count = 1;/* ---}}
                        @foreach($data['property']->propertyViewGallery()->get() as $gall)
                            <div class="showslide">
                                {{ HTML::image($gall->getPhotoSliderPath(),$data['property']->title."-".$count) }}
                                {{--<img src="/assets/images/luxury-img1.jpg" alt=""/>--}}
                                <?php $cookie = CustomHelper::checkCookie($data['property']->slug); ?>
                                <span
                                        class="heart property-{{$data['property']->id}} <?php echo ($cookie) ? 'selected' : ''; ?>"
                                        id="listingproperty-{{$data['property']->id}}">&nbsp;</span>
                            </div>
                            {{-- */$count++;/* ---}}
                        @endforeach
                    </div>

                </div>
                <div class="thumb cycle-slideshow <?php echo ($data['property']->gallery()->count() > 5) ? 'asd' : '' ?>"
                     data-cycle-swipe="true" data-cycle-swipe-fx="scrollHorz" id="cycle-2" data-cycle-slides="> div"
                     data-cycle-timeout="0" data-cycle-fx="carousel" data-cycle-carousel-visible="5"
                     data-cycle-carousel-vertical="true" data-cycle-carousel-fluid="true" data-allow-wrap="false"
                     data-cycle-prev=".cycle-prev" data-cycle-next=".cycle-next"
                     data-cycle-log="false">

                    @foreach($data['property']->propertyViewGallery()->get() as $gall)
                        <div class="thumbslide">
                            {{ HTML::image($gall->getPhotoSliderThumbPath(),$data['property']->title."-".$count) }}
                        </div>
                        {{-- */$count++;/* ---}}
                    @endforeach

                </div>

                @if($data['property']->gallery()->count() > 5)
                    <div class="control">
                        <a href="javascript:void(0)" class="cycle-prev">&laquo; prev</a>
                        <a href="javascript:void(0)" class="cycle-next">next &raquo;</a>
                    </div>
                @endif
            </div>
        @endif
        <div class="bottom">

            <ul>
                <li>
                    <span class="loacation location_new pull-left"><i class="icon-location2" ></i>{{$data['property']->getFullAddress()}}</span>
                    @if($deviceType != 'computer')
                        <span class="loacation location_new pull-right">Ref No. {{strtoupper($data['property']->reference_code)}}</span>
                    @endif
                </li>
                <li class="icon">
                    @if($data['property']->bedrooms)
                        <span><i class="icon-bedroom"></i>{{$data['property']->bedrooms}}</span>
                    @endif
                    @if($data['property']->bathrooms)
                        <span><i class="icon-bath"></i>{{$data['property']->bathrooms}}</span>
                    @endif
                    @if($data['property']->swimming_pool)
                        <span><i class="icon-swimming-pool"></i>{{$data['property']->swimming_pool}}</span>
                    @endif
                    @if($data['property']->getType()=="Villa")
                        <span class="villa"></span>
                    @endif
                </li>
                @if($deviceType == 'computer')
                    @if($data['property']->reference_code)
                        <li class="ref">Reference No. {{strtoupper($data['property']->reference_code)}}</li>
                    @endif
                @endif
            </ul>

        </div>

        <div class="detail">

            <div class="col-1 " >

                <table class="prcc floatLeft @if($deviceType != 'computer') sale_price_new pull-left @endif">
                    <?php $ac = Session::get('action');
                    $d = Session::get('price');
                    $p = explode("-",$d);
                    $type = $p[0];
                    ?>
                    @if($data['property']->getAllPrices('sale') && $ac != "rent")
                        <tr>
                            @if($ac == "buy")
                                <td><h5>Price:</h5></td>
                            @else
                                <td><h5>Sales Price:</h5></td>
                            @endif
                            <td><h5>{{$data['property']->getAllPrices('sale')}}</h5></td>
                        </tr>
                    @endif
                    @if(($data['property']->getAllPrices('daily') || $data['property']->getAllPrices('monthly')) && $ac != "buy")
                        <tr>

                            @if($ac == "rent")
                                <td>
                                    <h5>Price:</h5>
                                </td>
                            @else
                                <td><h5>Rental Price:</h5></td>
                            @endif

                            @if($type == "daily" || $type == "monthly")
                                <td>
                                    <h5>
                                        @if($data['property']->getAllPrices('daily') && $type == "daily")
                                            {{$data['property']->getAllPrices('daily')}}<br/>
                                        @endif
                                        @if($data['property']->getAllPrices('monthly') && $type == "monthly")
                                            {{$data['property']->getAllPrices('monthly')}}
                                        @endif
                                    </h5>
                                </td>
                            @else
                                <td>
                                    <h5>
                                        @if($data['property']->getAllPrices('daily'))
                                            {{$data['property']->getAllPrices('daily')}}<br/>
                                        @endif
                                        @if($data['property']->getAllPrices('monthly'))
                                            {{$data['property']->getAllPrices('monthly')}}
                                        @endif
                                    </h5>
                                </td>
                            @endif
                        </tr>
                    @endif
                    @if($data['property']->getAllPrices('sale') && $ac != "rent")

                    @endif
                </table>
                <select class="currency-selector floatRight  @if($deviceType != 'computer') pull-right @endif " name="currency" onchange="updateSearchPage()" id="currency">
                    <option value="">Select</option>
                    @foreach(Config::get('conrad.currencies') as $index => $currency)
                        <option <?php if((Input::has('currency') && Input::get('currency') == $index) || (Session::has('exchange_rate') && Session::get('exchange_rate')['exchange_code'] == $index)){ echo 'selected' ; } ?> value="{{$index}}">{{$index}}</option>
                    @endforeach
                </select>

                <div class="clear"></div>

                @if($deviceType != 'computer')
                    @include('partials.front.property_detail')
                @endif
                <div class="clear"></div>


                {{--<h5>{{$data['property']->getPrice()}}</h5>--}}

                <h3>Overview</h3>

                <p>
                    {{$data['property']->overview}}
                </p>

                <h3>Key Features</h3>

                <p>
                    {{$data['property']->key_features}}
                </p>

            </div>

            @if($deviceType == 'computer')
                @include('partials.front.property_detail')
            @endif


            <div class="clear"></div>

            <?php if($data['property']->latitude && $data['property']->longitude!="" ){?>
            <h3>Location</h3>
            <div class="map">
                <div class="acf-map">
                    <div class="marker" data-lat="{{$data['property']->latitude}}"
                         data-lng="{{$data['property']->longitude}}"></div>
                </div>
            </div>
            <?php }?>

            <h2 class="link"><a href="#">Interested in this Property?</a></h2>

            <span class="fone"><i class="icon-phone-icon"></i><a href="tel:{{$settings->display_phone}}">{{$settings->display_phone}}</a></span>
            <span class="email"><i class="icon-email"></i><a href="mailto:{{$settings->display_email}}">{{$settings->display_email}}</a></span>

            <a class="btn fancybox " href="#enqiry">ENQUIRE NOW</a>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

</div>
<!-- finish luxury block -->
@include('partials.front.home.recent')

<div class="fancy-block" id="enqiry">

    <h2>Enquire Now!</h2>

    <div class="succemsg"></div>

    <!--<form action="/xxxxxx" class="required-form">-->
    {{ Form::open(array('url'=>'/property-enquire/', 'class'=>'required-form','id'=>'MyForm','onsubmit'=>'return sendEnquery();')) }}
    <fieldset>

        <div class="field">

            <ul>
                <li>
                    <div class="control-group">
                        <div class="controls">
                            {{ Form::text('name', null,array('class'=>'required','placeholder'=>'Name*','id'=>'name'))}}
                            {{Form::hidden('property_id',$data['property']->id)}}
                            {{Form::hidden('reference_code',$data['property']->reference_code)}}
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            {{ Form::text('phone', null,array('class'=>'required',
                            'placeholder'=>'Phone*','id'=>'phone')) }}
                        </div>
                    </div>
                </li>
                <li>
                    <div class="controls">
                        {{ Form::text('email', null,array('class'=>'required', 'placeholder'=>'Email*','id'=>'email'))
                        }}
                    </div>
                </li>
                <li>
                    <div class="controls">
                        {{ Form::text('subject', null,array('class'=>'required',
                        'placeholder'=>'Subject*','id'=>'subject')) }}
                    </div>
                </li>
                <li>
                    <div class="controls">
                        {{ Form::textarea('message', null,array('class'=>'required',
                        'placeholder'=>'Please type your property enquiry here, including as much information as possible. i.e. property type, size, location, budget or any other specific requirements you may have.','id'=>'message')) }}
                    </div>
                </li>
            </ul>

        </div>

        {{ Form::button('Send',array('type'=>'submit','id'=>'enquire_now_new'))}}

    </fieldset>
    {{ Form::close() }}
    <script>
        function sendEnquery() {
            debugger;
            if(jQuery("#MyForm").validate({
                rules:{name:"required",email:{required:!0,email:!0},phone:"required",subject:"required",message:"required"},
                messages:{name:"Name is required.",email:{required:"Email is required.",email:"Please enter valid email."},phone:"Phone is required",subject:"Subject is required",message:"Message is required"}}),
                !jQuery("#MyForm").valid())return!1;
            jQuery('#enquery_now').prop('disabled', true);
            var e=$("#MyForm").serialize(),r=$("#MyForm").attr("action");
            $.ajax({url:r,type:"POST",data:e,success:function(e){jQuery("#MyForm").hide(),jQuery(".succemsg").html(e),jQuery('#enquery_now').prop('disabled', false); }})
            return false;
        }
    </script>

</div>