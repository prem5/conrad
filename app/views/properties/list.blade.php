<span style="margin-bottom: 10px;">
    <select name="" id="propertyFilter" onchange="fileterPropertyList()">
        <option  value="">Select Filter</option>
        <option <?php if(Input::get('published') == '1'){echo 'selected';} ?> value="1">Published</option>
        <option <?php if(Input::get('published') == '0'){echo 'selected';} ?> value="0">Unpublished</option>
    </select>
    <input style="width:200px;" type="text" id="keyword" onkeyup="searchPropertyByRefNo()" placeholder="Search for property by ref no.">
</span>
<span style="float: right; margin-bottom: 10px;">{{HTML::link('/administrator/listings/create','Add Property',['class'=>"btn btn-primary"])}}</span>
<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Name</th>
        <th>Property Status</th>
        <th>Property Ref No</th>
        <th>City</th>
        <th>Area</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    @foreach($properties as $d)
    <tr id="{{$d->id}}">
        <td>{{$d->title}}</td>
        <?php
        if($d->published==1)
        {
            $p = "Published";
            $ic = "btn btn-success";
        }else{
            $p = "Unpublished";
            $ic = "btn btn-primary";
        }
        ?>
        <td>  {{HTML::link('/administrator/listings/status/'.$d->id,$p,array('class'=>$ic))}}</td>
        <td>{{$d->reference_code}}</td>
        <td>{{(isset($d->city->name))?$d->city->name:''}}</td>
        <td>{{(isset($d->area->name))?$d->area->name:''}}</td>
        <td>
            {{HTML::link('/administrator/listings/edit/'.$d->id,'Edit',array('class'=>'btn btn-warning btn-small'))}}
            {{HTML::link('/administrator/listings/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning btn-small',
            'onClick'=>"return confirm('Do you really want to delete ?')"))}}
            {{HTML::link('/administrator/galleries/edit/'.$d->id,'Gallery',array('class'=>'btn btn-warning
            btn-small'))}}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
<script>
    function searchPropertyByRefNo() {
        // Declare variables
        var input, filter, table, tr, td;
        input = document.getElementById("keyword");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (var i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>