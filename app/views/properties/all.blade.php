<!-- begin sales block -->

<div class="sales-block">
    <div  class="searchOpen">
        <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE2LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkNhcGFfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiCgkgd2lkdGg9IjQ1LjE5N3B4IiBoZWlnaHQ9IjQ1LjE5N3B4IiB2aWV3Qm94PSI4NS4zOTkgNTAuNDAxIDQ1LjE5NyA0NS4xOTciIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgODUuMzk5IDUwLjQwMSA0NS4xOTcgNDUuMTk3IgoJIHhtbDpzcGFjZT0icHJlc2VydmUiPgo8cGF0aCBmaWxsPSIjMkZCRkJEIiBkPSJNMTI5LjU5Miw4OS42NzdsLTkuMzE2LTkuMzE2YzIuMjQ2LTMuMjQxLDMuMzY5LTYuODU0LDMuMzY5LTEwLjgzN2MwLTIuNTg5LTAuNTAzLTUuMDY2LTEuNTA5LTcuNDI5CgljLTEuMDA0LTIuMzYzLTIuMzYyLTQuNC00LjA3My02LjExMmMtMS43MTItMS43MTEtMy43NDgtMy4wNjktNi4xMTEtNC4wNzRzLTQuODQtMS41MDctNy40My0xLjUwN3MtNS4wNjUsMC41MDItNy40MjksMS41MDcKCXMtNC40LDIuMzYzLTYuMTExLDQuMDc0Yy0xLjcxMSwxLjcxMS0zLjA3LDMuNzQ5LTQuMDc0LDYuMTEyYy0xLjAwNSwyLjM2My0xLjUwOCw0LjgzOS0xLjUwOCw3LjQyOXMwLjUwMiw1LjA2NSwxLjUwOCw3LjQyOQoJYzEuMDA0LDIuMzYyLDIuMzYzLDQuMzk5LDQuMDc0LDYuMTFjMS43MTEsMS43MTEsMy43NDksMy4wNjksNi4xMTEsNC4wNzRjMi4zNjMsMS4wMDYsNC44MzksMS41MDgsNy40MjksMS41MDgKCWMzLjk4NCwwLDcuNTk3LTEuMTIzLDEwLjgzOC0zLjM2OGw5LjMxNiw5LjI4OGMwLjY1MSwwLjY4OCwxLjQ2NiwxLjAzMywyLjQ0NCwxLjAzM2MwLjk0MSwwLDEuNzU3LTAuMzQ1LDIuNDQ0LTEuMDMzCgljMC42ODgtMC42ODgsMS4wMzItMS41MDIsMS4wMzItMi40NDNDMTMwLjU5Nyw5MS4xNjEsMTMwLjI2Myw5MC4zNDcsMTI5LjU5Miw4OS42Nzd6IE0xMTMuMTE4LDc4LjExOQoJYy0yLjM4MSwyLjM4MS01LjI0NywzLjU3MS04LjU5NywzLjU3MWMtMy4zNSwwLTYuMjE1LTEuMTktOC41OTctMy41NzFjLTIuMzgxLTIuMzgyLTMuNTcyLTUuMjQ3LTMuNTcyLTguNTk3CgljMC0zLjM1LDEuMTkxLTYuMjE2LDMuNTcyLTguNTk3YzIuMzgxLTIuMzgxLDUuMjQ3LTMuNTcyLDguNTk3LTMuNTcyYzMuMzUsMCw2LjIxNSwxLjE5MSw4LjU5NywzLjU3MnMzLjU3Miw1LjI0NywzLjU3Miw4LjU5NwoJQzExNi42OSw3Mi44NzMsMTE1LjQ5OSw3NS43MzcsMTEzLjExOCw3OC4xMTl6Ii8+Cjwvc3ZnPgo=" alt="">
    </div>

    <div class="centering">
        <?php
        include(app_path().'/custom/mobile_detect.php');
        $detect = new mobile_detect;
        $deviceType =($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
        if($deviceType!='phone'){?>
        <h3 onclick="showMap()" class="mapshow"><img width="40" src="https://i.stack.imgur.com/XtpTm.png" alt=""></h3>
        @if($data['type'] && ($data['type'] == 'laxury'))
            <h3  onclick="showFilter()" class="filter-show">
                {{--<img width="40" src="https://i.stack.imgur.com/XtpTm.png" alt="">--}}
                <i class="icon-search"></i>
            </h3>
        @endif
        <?php }
        ?>
        @if($data['action'] && ($data['action'] == 'rent') && ($data['type'] == 'laxury') )
            <h1> Luxury Villas for Rent </h1>
        @elseif($data['action'] && ($data['action'] == 'sale') && ($data['type'] == 'laxury'))
            <h1> Luxury Villas for Sale </h1>
        @elseif($data['type'] == 'laxury')
            <h1> Luxury Villas </h1>
        @elseif($data['action'])
            <h1>Properties for {{ucwords($data['action'])}} </h1>
        @elseif($data['place'])
            <h1>Properties in {{ucwords($data['place'])}} </h1>
        @else
            <h1>All Properties</h1>
        @endif
        @if($data['count'] > 0)
            <h2>
                {{$data['count']}}
                {{(!$data['action'] && !$data['place']) ? 'available':'';}} {{Lang::choice('conrad.search.properties', $data['count'])}}{{($data['action']) ? ' for '.ucwords($data['action']) : ''}}
                {{($data['place']) ? ($data['place'] != 'all') ? " in ". ucwords($data['place']).$data['place1'] : '' : ''}}

            </h2>
        @else
            <h2>No Property Found</h2>
        @endif
    </div>

    <script>
        // Defin your locations: HTML content for the info window, latitude, longitude
                @if ($data['count'] > 0)
                {{--*/$i=1/*--}}
            <?php  $totalmarker = \Config::get('conrad.property-marker-count');?>
        var locations = [
                        @foreach($data['properties'] as $res)
                        @if($i>$totalmarker)
                        {{--*/break/*--}}
                        @endif
                ['<h4><a href="{{$res->getUrl()}}" target="" >{{$res->title}}</a></h4>', {{$res->latitude}}, {{$res->longitude}}],

                    {{--*/$i++/*--}}
                    @endforeach
            ];
                @else
                {{--*/$i=1/*--}}
            <?php $totalmarker = \Config::get('property-marker-count');?>
        var locations = [
                        @foreach(Property::getActive()->get() as $res)
                        @if( $i> $totalmarker)
                        {{--*/break/*--}}
                        @endif
                ['<h4>{{$res->title}}</h4>', {{$res->latitude}}, {{$res->longitude}}],

                    {{--*/$i++/*--}}
                    @endforeach
            ];
        //var locations = [];
        @endif
    </script>
    <?php
    if($deviceType!='phone'){?>
    <div id="mapToggle" >
        <div id="map" ></div>
    </div>
    <div class="centering ">
        @include('partials.front.search',compact($data))
    </div>
    <?php
    }
    else{
    ?>
    <div class="centering ">
        @include('partials.front.search_mobile',compact($data))
    </div>
    <?php
    }
    ?>



</div>
<!-- finish sales block -->
@if($data['count'] > 0)
    <div class="bg-propertie">
        <!-- begin propertie block -->
        <div class="propertie-block notag">
            <div class="centering">

                <div class="list-filter">
                    <div class="col-xs-3">
                        <label>Display:</label>
                        <select name="display" onchange="updateSearchPage()" id="display">
                            <option value="">Select</option>
                            <option <?php if( !Input::has('Display') || (Input::get('display') == 12 )){ echo 'selected' ; } ?> value="12">12</option>
                            <option <?php if(Input::has('display') && Input::get('display') == 24){ echo 'selected' ; } ?> value="24">24</option>
                            <option <?php if(Input::has('display') && Input::get('display') == 48){ echo 'selected' ; } ?> value="48">48</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <label>Sort By:</label>
                        <select name="sortBy" onchange="updateSearchPage()" id="sortBy">
                            <option value="">Select</option>
                            @foreach(Config::get('conrad.short_options') as $index => $option)
                                <option <?php if(Input::has('sortBy') && Input::get('sortBy') == $index){ echo 'selected' ; } ?> value="{{$index}}">{{$option}}</option>
                            @endforeach


                        </select>
                    </div>
                    <div class="col-xs-3">
                        <label>Currency:</label>
                        <select name="currency" onchange="updateSearchPage()" id="currency">
                            <option value="">Select</option>
                            @foreach(Config::get('conrad.currencies') as $index => $currency)
                                <option <?php if((Input::has('currency') && Input::get('currency') == $index) || (Session::has('exchange_rate') && Session::get('exchange_rate')['exchange_code'] == $index)){ echo 'selected' ; } ?> value="{{$index}}">{{$index}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <ul>
                    @foreach($data['properties'] as $T)

                        <?php
                        $f = Property::find($T->id) ;
                        $currency = ($f->default_currency)? $f->default_currency : 'THB' ;
                        ?>


                        <li>
                            <div class="image">
                                @if($f->getRibbon())
                                    <span class="tag">{{$f->getRibbon()}}</span>
                                @endif
                                {{--<a href="{{$f->getUrl()}}?currency={{ $currency }}">--}}
                                <a href="{{$f->getUrl()}}">
                                    <img src="{{$f->getPrimaryImageSmall()}}" alt="{{$f->title}}"/>
                                </a>
                                <?php $cookie = CustomHelper::checkCookie($f->slug); ?>
                                <span class="heart property-{{$f->id}}<?php echo ($cookie) ? 'selected' : ''; ?>"
                                      id="featureproperty-{{$f->id}}"></span>
                            </div>

                            <div class="text">

                                <h3>{{$f->title}}</h3>

                                <div class="bottom">
                                    <i class="icon-location2"></i>
                                    {{--<a href="{{$f->getAddressUrl()}}?currency={{ $currency }}">{{$f->getFullAddress()}}</a>--}}
                                    <a href="{{$f->getAddressUrl()}}">{{$f->getFullAddress()}}</a>
                                    <ul>
                                        @if($f->bedrooms)
                                            <li><span><i class="icon-bedroom"></i>{{$f->bedrooms}}</span></li>
                                        @endif
                                        @if($f->bathrooms)
                                            <li><span><i class="icon-bath"></i>{{$f->bathrooms}}</span></li>
                                        @endif

                                        @if($f->land_area)
                                            <li><img src="/assets/images/unit-icon.png" alt="area" />{{$f->land_area ." sqm"}}</li>
                                        @endif
                                    </ul>
                                </div>

                                <strong>{{$f->getPrice($data['action'])}}</strong>

{{--                                <a href="{{$f->getUrl()}}?currency={{ $currency }}" class="btn">View DETAILS</a>--}}
                                <a href="{{$f->getUrl()}}" class="btn">View DETAILS</a>

                            </div>
                            {{--<a href="{{$f->getUrl()}}" class="view">view detail</a>--}}
                        </li>
                    @endforeach
                </ul>

                <div class="clear"></div>

                <div class="pagination">
                    <?php
                    $link = [];
                    if(Input::has('display')){
                        $link = array_merge(['display'=> Input::get('display')],$link);
                    }
                    if(Input::has('sortBy')){
                        $link = array_merge(['sortBy'=> Input::get('sortBy')],$link);
                    }
                    if(Input::has('currency')){
                        $link = array_merge(['currency'=> Input::get('currency')],$link);
                    }
                    ?>
                    {{ $data['properties']->appends($link)->links() }}
                    {{--
                    <ul>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">&nbsp;</a></li>
                    </ul>
                    --}}

                </div>
            </div>
        </div>
        <!-- finish propertie block -->

    </div>
@endif