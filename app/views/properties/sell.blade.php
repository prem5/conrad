<!-- begin sell block -->
<div class="sell-block">
    <div class="centering">

        <h1>List your Koh Samui Property with Us</h1>

        <p>Do you own property or land in Thailand that you would like to rent or sell? Simply fill out the brief form
            below and one of our property advisors will contact you.</p>

        <p>Or feel free to call us on <a href="tel:+66 92 959 1299">+66 92 959 1299</a> or email us at <a href="mailto:info@conradproperties.asia">info@conradproperties.asia</a></p>
        @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        <div class="detail">
            {{ Form::open(array('url'=>'/properties/submissionsave', 'class'=>'"required-form form-horizontal',
            'files'=>true)) }}
            <fieldset>

                <div class="left">
                    <h2>Personal Details</h2>
                </div>

                <div class="right">

                    <ul>
                        <li>
                            {{ Form::text('name', null,array('class'=>'form-control','placeholder'=>'Name')) }}
                            {{$errors->first('name', '<p class="error">:message</p>')}}
                        </li>
                        <li>
                            {{ Form::text('email', null,array('class'=>'form-control','placeholder'=>'Email')) }}
                            {{$errors->first('email', '<p class="error">:message</p>')}}
                        </li>
                        <li>
                            {{ Form::text('phone', null,array('class'=>'form-control','placeholder'=>'Phone')) }}
                            <!--{{$errors->first('phone', '<p class="error">:message</p>')}}-->
                        </li>
                        <li class="last">
                            <span>Get Our Newsletter?</span>
                            {{ Form::checkbox('news', 1, 1 ,array('class'=>'styled','checked'=>'checked')) }}
                        </li>
                    </ul>

                </div>

                <div class="clear"></div>
                <hr/>

                <div class="left">
                    <h2>Property Details</h2>
                </div>

                <div class="right">

                    <ul>
                        <li>
                            <div class="left" style="width:227px; margin-right: 20px;">
                                {{Form::select('terms',Config::get('conrad.terms_select'),'',array('class'=>'select'))}}
                                {{$errors->first('terms', '<p class="error">:message</p>')}}
                            </div>
                            <div class="left" style="width:227px;">
                                {{ Form::select('property_type',PropertyType::getTypeForSelect(),'',array('class'=>'select')) }}
                                {{$errors->first('property_type', '<p class="error">:message</p>')}}
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li>
                            <div class="prolocation">
                                {{ Form::text('location', null,array('class'=>'form-control','placeholder'=>'Location*')) }}
                               {{-- {{ Form::select('location', City::getSearchSelect(),'', array('class'=>'select')) }}--}}
                                {{$errors->first('location', '<p class="error">:message</p>')}}
                                <div class="clear"></div>
                            </div>
                        </li>
                        <li>
                            {{ Form::text('price', null,array('class'=>'form-control','placeholder'=>'Price (THB)*')) }}
                            {{$errors->first('price', '<p class="error">:message</p>')}}

                        </li>
                            {{ Form::file('image', null,array('class'=>'form-control','placeholder'=>'Upload Property Photo')) }}
                            {{$errors->first('image', '<p class="error">:message</p>')}}
                        <div class="clear"></div>
                        <li>
                            {{ Form::textarea('extra_information',null,array('class'=>'form-control','placeholder'=>'Additional Information')) }}
                            {{$errors->first('extra_information', '<p class="error">:message</p>')}}

                        </li>
                        <li>
                            {{ Form::button('Submit', array('type'=>'submit'))}}
                        </li>
                        <li class="req">*Required Fields</li>
                    </ul>

                </div>

            </fieldset>
            {{ Form::close() }}

        </div>

    </div>
</div>
<!-- finish sell block -->