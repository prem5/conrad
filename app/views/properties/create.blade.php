{{ Form::open(array('url'=>'/administrator/listings/store', 'class'=>'form form-horizontal')) }}
@include('properties.form')

{{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
<a href="{{url('/administrator/listings/')}}" class="btn btn-warning btn-small">Cancel</a>
</div>
</div>
{{ Form::close() }}