<div class="panel-heading"><h2>Property Name : {{$data->title}}</h2></div>
<h3 class="heading line">Upload your Pictures </h3>
<div class="span5">
    {{Form::open(array('id'=>'upload','file'=>true,'method'=>'POST','url' =>
    url("/administrator/galleries/upload/$data->id")))}}
    {{--
    <form id="upload" enctype="multipart/form-data" method="POST" action=''
    {{url("/administrator/galleries/upload/$data->id")}}'>--}}
    <div id="drop"><!-- multiple -->
        Drop Here <a>choose file</a>
        <input type="file" action='{{url("/administrator/galleries/upload/$data->id")}}' name="photo">

        <p class="tip-text">Min Image size should be 400*400.<br>
            File size limit 4 MB.</p>
    </div>
    <ul class="up-img">
        <!-- The file uploads will be shown here -->
    </ul>
    <div style="display:none" class="add-galley-image-in">
        <ul>
            <li class="last-chd">
                <label class="selected-label">No file chosen</label>
                <input type="submit" value="Upload" name="upload">
            </li>
        </ul>
    </div>
    {{--</form>--}}
    {{ Form::close() }}
</div>
<div class="span6">
    <div class="tips tip-1">
        <div class="tips-message">
        </div>
    </div>
</div>
<div class="clearfix"></div>


@if ($data->gallery()->count())
<div class="galalry-tip line-upper">
    <div class="tip-text">
        <span></span>
    </div>
</div>
{{ Form::open(array('url'=>'', 'class'=>'','id'=>'gallery-index-form')) }}
{{Form::Close()}}
<table id="gallery-listing" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
       <!-- <th><i class="fa fa-move"></i>ID</th>-->
        <th>Image</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>

    {{-- */$count = 1;/* ---}}
    @foreach($data->gallery()->orderBy('order', 'ASC')->get() as $gall)
    <?php $class = '<i class="fa fa-check-square-o"></i>' ?>
    <tr id="{{$gall->id}}">
        <!--<td>{{$count}}</td>-->
        <td>{{ HTML::image($gall->getPhotoThumbPath(),$data->name."-".$count) }}</td>
        <td>
            <!-- <a href="{{url('administrator/galleries/primary/'.$gall->id)}}">
            Primary Image <?php /* if($gall->primary=='1'){echo " ".$class;}else{echo '<i class="fa fa-square-o"></i>';}*/?>
        </a>-->
            <a onclick="return confirm('Are you sure want to delete this image?')"
               href="{{url('administrator/galleries/delete/'.$gall->id)}}">x
                <span>Delete Image</span>
            </a>

        </td>
        {{-- */$count++;/* ---}}
    </tr>
    @endforeach

    </tbody>
</table>
@else
<div class="galalry-tip line-upper">
    <div class="tip-text">
        <span>No picture uploaded.</span>
    </div>
</div>
@endif