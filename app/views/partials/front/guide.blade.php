<div class="guide">

    <span class="title">BUYER'S GUIDE</span>

    <ul>
        <li>Index</li>
        <li>Why Buy Property on Koh Samui?</li>
        <li>10-Step Property Purchasing Checklist</li>
        <li>Property Ownership Rights</li>
        <li>Thailand Land Titles</li>
        <li>Thailand Property Taxes</li>
        <li>Land measurement in Thailand</li>
    </ul>

</div>