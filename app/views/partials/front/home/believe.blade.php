<!-- begin believe block -->
<div class="believe-block ">
    <div class="centering wow slideInDown">

        <h2>We believe our <strong>passion</strong>, <strong>creativity</strong> and <strong>commitment</strong> are the
            keys to helping you find your perfect property. <a href="#center-wrap">Let's get started!</a></h2>

    </div>
</div>
<!-- finish believe block -->