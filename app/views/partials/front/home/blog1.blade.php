<div class="centering">
    <h2><span class="blk">Blog News Updates</span><a href="{{'blog-news'}}"><span class="vw">View all </span></a></h2>
    <div class="clear"></div>

    <ul>
        @foreach(Blog::getActive()->take(4)->get() as $bg)
        <li>
            <h3>{{$bg->name}}</h3>
            <img src="{{$bg->getBlogThumbImagePath()}}" alt="{{$bg->name}}"/>

            {{$bg->teaser}}
            <a class="detail" href="{{$bg->getUrl()}}">detail</a>
            <a class="more" href="{{$bg->getUrl()}}">Continue reading -></a>
        </li>
        @endforeach
        {{--
        <li>
            <h3>Top 10 Island Destinations in Asia</h3>
            <img src="/assets/images/blog_img.jpg" alt=""/>

            <p>Thailand has enough unique attractions to keep any visitor satisfied. So where do you get started? Our
                introductory list includes the essential s to keep any visitor satisfied. So where do you get started?
                Our introducto .... </p>
            <a class="detail" href="#">detail</a>
            <a class="more" href="#">Continue reading -></a>
        </li>
        <li>
            <h3>Top 10 Island Destinations in Asia</h3>
            <img src="/assets/images/blog_img.jpg" alt=""/>

            <p>Thailand has enough unique attractions to keep any visitor satisfied. So where do you get started? Our
                introductory list includes the essential s to keep any visitor satisfied. So where do you get started?
                Our introducto .... </p>
            <a class="detail" href="#">detail</a>
            <a class="more" href="#">Continue reading -></a>
        </li>
        <li>
            <h3>Top 10 Island Destinations in Asia</h3>
            <img src="/assets/images/blog_img.jpg" alt=""/>

            <p>Thailand has enough unique attractions to keep any visitor satisfied. So where do you get started? Our
                introductory list includes the essential s to keep any visitor satisfied. So where do you get started?
                Our introducto .... </p>
            <a class="detail" href="#">detail</a>
            <a class="more" href="#">Continue reading -></a>
        </li>
        <li>
            <h3>Top 10 Island Destinations in Asia</h3>
            <img src="/assets/images/blog_img.jpg" alt=""/>

            <p>Thailand has enough unique attractions to keep any visitor satisfied. So where do you get started? Our
                introductory list includes the essential s to keep any visitor satisfied. So where do you get started?
                Our introducto .... </p>
            <a class="detail" href="#">detail</a>
            <a class="more" href="#">Continue reading -></a>
        </li>
        --}}
    </ul>

</div>