<div class="centering">
    <div class="h2"><span class="blk">Blog News Updates</span><a href="{{'blog-news'}}"><span class="vw">View all </span></a></div>
    <div class="clear"></div>

    <ul>
        @foreach(Blog::getActive()->take(4)->get() as $bg)
        <li>
            <h3>{{$bg->name}}</h3>
            <img src="{{$bg->getBlogThumbImagePath()}}" alt="{{$bg->name}}" width="225" height="135"/>

            {{$bg->teaser}}
            <a class="detail" href="{{$bg->getUrl()}}">detail</a>
            <a class="more" href="{{$bg->getUrl()}}">Continue reading -></a>
        </li>
        @endforeach
        </ul>

</div>