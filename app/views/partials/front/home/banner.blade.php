<!-- begin slider wrap -->
<?php $banner = \Carousel::getBanner();

?>
<style>
    select {

        /* styling */
        background-color: white;
        border: thin solid blue;
        /*border-radius: 4px;*/
        display: inline-block;
        font: inherit;
        line-height: 1.5em;
        padding: 0.5em 3.5em 0.5em 1em;

        /* reset */

        margin: 0;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-appearance: none;
        -moz-appearance: none;

        text-decoration: none;
        display: block;
        font-size: 16px;
        color: #000;
        text-transform: uppercase
    }
    select.minimal {
        background-image:
                linear-gradient(45deg, transparent 50%, #58cbca 50%),
                linear-gradient(135deg, #58cbca 50%, transparent 50%),
                linear-gradient(to right, #ccc, #ccc);
        background-position:
                calc(100% - 20px) calc(1em + 2px),
                calc(100% - 15px) calc(1em + 2px),
                calc(100% - 2.5em) 0.5em;
        background-size:
                5px 5px,
                5px 5px,
                1px 1.5em;
        background-repeat: no-repeat;
    }
</style>
<section id="slider-wrap">

    <!-- begin slider block -->
    <div class="slider-block">
        <?php if($deviceType != 'phone'){?>
        <div class="single-item">
            @foreach($banner as $b)
                <div class="show">
                    <img src="{{$b['images']}}" alt="{{$b['title']}}" width="1400" height="665"/>
                    <div class="text">
                        <div style="width: 37px;height: 37px;display: block;float:left;background: url('/assets/images/png.png') no-repeat;overflow: hidden;text-indent: -9999px;text-align: left;background-position: -0px -80px;"></div>

                        <div class="around">
                            <div>
                                <strong>{{$b['title']}}</strong>
                                <span class="banner-price">{{$b['price']}}</span>
                            </div>
                            <a href="{{$b['url']}}">View Project</a>

                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            @endforeach
        </div>
        <?php }?>

        <div class="overlay">

            <div class="title">
                <div class="inner">
                    {{--<h3>Find your perfect <strong id="text-type">Property</strong> in <strong id="text-place">Koh
                            Samui</strong></h3>--}}
                    <h3>Find your perfect <span><strong>Property</strong> in <strong>Koh Samui</strong></span></h3>
                </div>
            </div>

            <div class="search">
                {{--*/$data = Input::all();/*--}}
                {{Form::open(['url'=> url('searchresult/'), 'id'=>'property-search-form','method'=>'POST'])}}
                <fieldset>
                    @if($deviceType == 'phone')
                        {{--{{Form::select('action',\Config::get('conrad.buy_sell_select'),((count($data) > 0) && (isset($data['action']))) ? $data['action'] : '' ,array('class'=>'search_buy_rent','id'=>'search-buy-rent'))}}--}}
                        {{Form::select('action',\Config::get('conrad.buy_sell_select'),((count($data) > 0) && (isset($data['action']))) ? $data['action'] : '' ,array('class'=>'search_buy_rent1 minimal heapBox','id'=>'action','onchange'=>'searchbuyrentchange()'))}}

                        <select class="select1 minimal heapBox" name="type" id="search-type1">
                            <?php $first_types = PropertyType::orderBy('order', 'ASC')->where('status', '=', '1')->limit(2)->get(); ?>
                            <?php $second_types = PropertyType::orderBy('order', 'ASC')->where('status', '=', '1')->offset(2)->limit(20)->get(); ?>
                            <option value="">TYPE</option>
                            <option value="">ANY</option>
                            @if((array_key_exists('type', $data) && count($data) > 0))
                                @foreach($first_types as $type)
                                    <option value="{{$type->id}}"
                                            @if($data['type'] == $type->id) selected @endif>{{$type->label}}</option>
                                @endforeach
                                <option value="laxury" @if($data['type'] == 'laxury') selected @endif>LUXURY VILLA
                                </option>
                                @foreach($second_types as $type)
                                    <option value="{{$type->id}}"
                                            @if($data['type'] == $type->id) selected @endif>{{$type->label}}</option>
                                @endforeach
                            @else
                                @foreach($first_types as $type)
                                    <option value="{{$type->id}}">{{$type->label}}</option>
                                @endforeach
                                <option value="laxury">LUXURY VILLA</option>
                                @foreach($second_types as $type)
                                    <option value="{{$type->id}}">{{$type->label}}</option>
                                @endforeach
                            @endif

                        </select>
                        {{-- {{Form::select('type',\PropertyType::getTypeForSelect(),(count($data) > 0 && (isset($data['type']))) ? $data['type'] : '',array('class'=>'property-type','id'=>'search-type'))}} --}}
                        {{Form::select('place',\City::getSearchSelect(),(count($data) > 0 && (isset($data['place']))) ? $data['place'] : '',array('class'=>'property-place1 minimal heapBox','id'=>'search-place1'))}}
                        {{Form::select('bedroom',\Config::get('conrad.bedrooms_select'),(count($data) > 0 && (isset($data['bedroom']))) ? $data['bedroom'] : '',array('class'=>'select1 minimal heapBox'))}}

                        {{Form::select('price',\Config::get('conrad.price_buy_select'),(count($data) > 0 && (isset($data['price']))) ? $data['price'] : '',array('class'=>'select1 minimal heapBox','id'=>'property-price'))}}

                    @else
                        {{Form::select('action',\Config::get('conrad.buy_sell_select'),((count($data) > 0) && (isset($data['action']))) ? $data['action'] : '' ,array('class'=>'search_buy_rent','id'=>'search-buy-rent'))}}

                        <select class="select" name="type" id="search-type">
                            <?php $first_types = PropertyType::orderBy('order', 'ASC')->where('status', '=', '1')->limit(2)->get(); ?>
                            <?php $second_types = PropertyType::orderBy('order', 'ASC')->where('status', '=', '1')->offset(2)->limit(20)->get(); ?>
                            <option value="">TYPE</option>
                            <option value="">ANY</option>
                            @if((array_key_exists('type', $data) && count($data) > 0))
                                @foreach($first_types as $type)
                                    <option value="{{$type->id}}"
                                            @if($data['type'] == $type->id) selected @endif>{{$type->label}}</option>
                                @endforeach
                                <option value="laxury" @if($data['type'] == 'laxury') selected @endif>LUXURY VILLA
                                </option>
                                @foreach($second_types as $type)
                                    <option value="{{$type->id}}"
                                            @if($data['type'] == $type->id) selected @endif>{{$type->label}}</option>
                                @endforeach
                            @else
                                @foreach($first_types as $type)
                                    <option value="{{$type->id}}">{{$type->label}}</option>
                                @endforeach
                                <option value="laxury">LUXURY VILLA</option>
                                @foreach($second_types as $type)
                                    <option value="{{$type->id}}">{{$type->label}}</option>
                                @endforeach
                            @endif

                        </select>
                        {{-- {{Form::select('type',\PropertyType::getTypeForSelect(),(count($data) > 0 && (isset($data['type']))) ? $data['type'] : '',array('class'=>'property-type','id'=>'search-type'))}} --}}
                        {{Form::select('place',\City::getSearchSelect(),(count($data) > 0 && (isset($data['place']))) ? $data['place'] : '',array('class'=>'property-place','id'=>'search-place'))}}
                        {{Form::select('bedroom',\Config::get('conrad.bedrooms_select'),(count($data) > 0 && (isset($data['bedroom']))) ? $data['bedroom'] : '',array('class'=>'select'))}}

                        {{Form::select('price',\Config::get('conrad.price_buy_select'),(count($data) > 0 && (isset($data['price']))) ? $data['price'] : '',array('class'=>'select','id'=>'property-price'))}}
                    @endif

                    {{Form::button('<i class="icon-search"></i> SEARCH',['type'=>'submit'])}}


                </fieldset>
                {{Form::close()}}
            </div>

        </div>

        <div class="clear"></div>

        <div class="strip">
            <div class="centering">
                <h2>Setting the standard for Koh Samui property, Thailand property - villas, apartments,
                    condos, real estate investments</h2>
            </div>
        </div>

    </div>
    <!-- finish slider block -->

</section>
<script>
    function clickSelect() {
        debugger
    }
</script>
<!-- finish leftwrap
