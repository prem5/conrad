<div class="centering">

    <h3>4 THINGS <strong>YOU SHOULD KNOW</strong> ABOUT US</h3>

    <ul>
        <li>
            <div class="image wow  1">
                <i class="icon-C1"></i>
            </div>
            <h3>Committed</h3>

            <p>We are an independent property agency committed to providing professional, reliable real estate services, to help you find your dream property in Thailand or Asia.</p>
        </li>
        <li>
            <div class="image wow ">
                <i class="icon-C2"></i>
            </div>
            <h3>Customers</h3>

            <p>We focus on your needs as a customer first, to help you find the right property. With a friendly team of staff able to speak many languages, we can take care of you even after your purchase.</p>
        </li>
        <li>
            <div class="image wow ">
                <i class="icon-C3"></i>
            </div>
            <h3>Choice</h3>

            <p>We have an extensive choice of properties for sale and rent in Thailand and Asia, from private villas and luxury apartments, to freehold condominiums, and land investments.</p>
        </li>
        <li>
            <div class="image wow ">
                <i class="icon-C4"></i>
            </div>
            <h3>Competitive</h3>

            <p>Our services are free for both property buyers and renters. Our commissions only come directly from property owners, so our buyers can get the most competitive price available on the market!</p>
        </li>
    </ul>

</div>