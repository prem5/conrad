<!-- begin propertie block -->
<div class="propertie-block">
    <div class="centering">
        <?php
        $featured = Property::featured();
        ?>
        <h2><span>Featured Properties for Sale and Rent</span>

            <div class="line"></div>
            <a href="{{url('search')}}">View all properties</a>

            <div class="clear"></div>
        </h2>



            <div class="featured_list">

            @foreach($featured->get() as $f)

            <li>
                <a href="{{$f->getUrl()}}">
                    <div class="image">
                        @if($f->getRibbon())
                            <span class="tag">{{$f->getRibbon()}}</span>
                        @endif
                        <img src="{{$f->getPrimaryImageSmall()}}" alt="{{$f->title}}" width="312" height="192" />
                        <?php $cookie = CustomHelper::checkCookie($f->slug); ?>
                        <span class="heart property-{{$f->id}} {{'heart_class'}} <?php echo ($cookie) ? 'selected' : ''; ?>"
                              id="featureproperty-{{$f->id}}"></span>
                    </div>
                </a>
                <div class="text">

                    <h3>{{$f->title}}</h3>

                    <div class="bottom">
                        <a href="{{$f->getAddressUrl()}}"><i class="icon-location2"></i>{{$f->getFullAddress()}}</a>
                        <ul>
                            @if($f->bedrooms)
                                <li><span><i class="icon-bedroom"></i>{{$f->bedrooms}}</span></li>
                            @endif
                            @if($f->bathrooms)
                                <li><span><i class="icon-bath"></i>{{$f->bathrooms}}</span></li>
                            @endif

                            @if($f->land_area)
                                <li>
                                    <span><i class="icon-unit-icon"></i>{{$f->land_area. " sqm"}}</span>
                                </li>
                            @endif
                        </ul>
                    </div>

                    <strong>{{$f->getPrice()}}</strong>
                    <a href="{{$f->getUrl()}}" class="btn">View DETAILS</a>

                </div>
                {{--<a href="{{$f->getUrl()}}?currency=THB">--}}
                {{--<div class="image">--}}
                    {{--<span class="tag">Hot Deal</span>--}}
                        {{--<img src="http://localhost/conradproperties/public_html/assets/images/default_img.jpg" alt="Modern 2 Bed Luxury Sea View Pool Villas in Bophut">--}}
                    {{--<span class="heart property-943" id="featureproperty-943"></span>--}}
                {{--</div>--}}
                {{--</a>--}}
                {{--<div class="text">--}}

                    {{--<h3>Modern 2 Bed Luxury Sea View Pool Villas in Bophut</h3>--}}

                    {{--<div class="bottom">--}}
                        {{--<i class="icon-location2"></i>--}}
                        {{--<a href="https://localhost/conradproperties/public_html/search/thailand/koh-samui/north-east/bophut?currency=THB">Bophut, Koh Samui</a>--}}
                        {{--<ul>--}}
                            {{--<li><span><i class="icon-bedroom"></i>2</span></li>--}}
                            {{--<li><span><i class="icon-bath"></i>2</span></li>--}}

                            {{--<li><img src="/assets/images/unit-icon.png" alt="area">242 sqm</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}

                    {{--<strong>THB 10.5 M </strong>--}}

                    {{--<a href="https://localhost/conradproperties/public_html/properties/koh-samui-modern-luxury-villa-sea-view-pool-bophut?currency=THB" class="btn">View DETAILS</a>--}}

                {{--</div>--}}
            </li>
        @endforeach
            </div>

            <div class="slideshow">
            @foreach($featured->get() as $f)

            @endforeach
        </div>
        <div class="control" id="slickarrow">
        </div>
    </div>
</div>
<!-- finish propertie block -->