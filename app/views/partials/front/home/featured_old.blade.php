<!-- begin propertie block -->
<div class="propertie-block">
    <div class="centering">
        <?php
        $featured = Property::featured();
        ?>
        <h2><span>Featured Properties for Sale and Rent</span>

            <div class="line"></div>
            <a href="{{url('search')}}">View all properties</a>

            <div class="clear"></div>
        </h2>





            <div class="slideshow">
            @foreach($featured->get() as $f)
                <div class="cols">
                    <a href="{{$f->getUrl()}}?currency=THB">
                        <div class="image">
                            @if($f->getRibbon())
                                <span class="tag">{{$f->getRibbon()}}</span>
                            @endif
                            <img src="{{$f->getPrimaryImageSmall()}}" alt="{{$f->title}}" width="312" height="192" />
                            <?php $cookie = CustomHelper::checkCookie($f->slug); ?>
                            <span class="heart property-{{$f->id}} {{'heart_class'}} <?php echo ($cookie) ? 'selected' : ''; ?>"
                                  id="featureproperty-{{$f->id}}"></span>
                        </div>
                    </a>

                    <div class="text">

                        <h3>{{$f->title}}</h3>

                        <div class="bottom">
                            <a href="{{$f->getAddressUrl()}}?currency=THB"><i class="icon-location2"></i>{{$f->getFullAddress()}}</a>
                            <ul>
                                @if($f->bedrooms)
                                    <li><span><i class="icon-bedroom"></i>{{$f->bedrooms}}</span></li>
                                @endif
                                @if($f->bathrooms)
                                    <li><span><i class="icon-bath"></i>{{$f->bathrooms}}</span></li>
                                @endif

                                @if($f->land_area)
                                    <li>
                                        <span><i class="icon-unit-icon"></i>{{$f->land_area. " sqm"}}</span>
                                    </li>
                                @endif
                            </ul>
                        </div>

                        <strong>{{$f->getPrice()}}</strong>
                        <a href="{{$f->getUrl()}}?currency=THB" class="btn">View DETAILS</a>

                    </div>
                </div>
            @endforeach
        </div>
        <div class="control" id="slickarrow">
        </div>
    </div>
</div>
<!-- finish propertie block -->