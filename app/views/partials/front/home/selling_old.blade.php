<div id="home-content">
	<h1>Unrivalled support upon entering the Koh Samui real estate market&nbsp;</h1>

	<p>Welcome to Conrad Properties, Koh Samui’s favoured, independent property agency. We are wholeheartedly devoted to providing you with personalised, yet professional services and advice in order to help you discover your perfect, dream property in Thailand.</p>

	<p>Conrad Properties’ reputation is built upon a strict code of ethics and sincere business acumen, so you can expect nothing other than genuine, <a href="https://www.conradproperties.asia/buyers-guide" style="color:#2fbfbd;">honest and well-informed advice</a>. We are truly dedicated to you, putting your needs and desires at the very forefront of our efforts, even after completion of the sale. We are always <a href="https://www.conradproperties.asia/contact-us" style="color:#2fbfbd;">happy to hear from you</a>, our valued customer.&nbsp;</p>

	<p>We boast a solid team of experienced and professional, multilingual advisors who are passionate about finding the perfect property for our customers. There are many real estate agencies who seek only to write numbers on the board. Conrad Properties on the other hand, operate in a very different manner; treating each individual case with a precise and unique level of care and attention.</p>

	<h2>As though our portfolio of Koh Samui properties were built especially for you</h2>

	<p>Based on your desires and the information provided by you, we will compile a list of properties that we believe to be most suitable. We are not simply trying to move you into the first property that falls within your budget, but to present you with a selection of ideas that would each serve you appropriately, in the long run.&nbsp;</p>

	<p>Our services are completely free for both property <a href="https://www.conradproperties.asia/search/all/buy" style="color:#2fbfbd;">buyers</a> and <a href="https://www.conradproperties.asia/search/all/rent" style="color:#2fbfbd;">renters</a>. Our commissions only come directly from <a href="https://www.conradproperties.asia/properties/list" style="color:#2fbfbd;">property owners</a>, which is why we are able to offer you the most competitive prices available on the market. We have an extensive choice of properties for sale and rent in Thailand, from private villas and luxury apartments, to freehold condominiums, and land investments.</p>

	<p>It doesn’t matter if you are looking to discover your new home in paradise, or if you are simply seeking a fruitful and confidential investment opportunity; you will be afforded the same level of care and respect that all of our customers receive – paramountcy.&nbsp;</p>
</div>

<div class="row" id="subscribe">

    <div class="col-3">
        <div class="icon">

            <img src="/assets/images/selling_icon.png" alt="ad" width="98" height="98" />

        </div>
    </div>

    <div class="col-3 two">
        <div class="block">

            <h2>Selling Your Property?</h2>

            <p>If you would like to have your property listed for <strong>FREE</strong>, please contact us with the
                details and we will be happy to assist.</p>

            <img src="/assets/images/selling_icon2.png" alt="selling" width="94" height="127" />

        </div>
    </div>

    <div class="col-6">
        {{ Form::open(array('url'=>'/newsletter/', 'class'=>'required-form','id'=>'homenews')) }}

        <div class="left">

            <h3>Subscribe to our newsletter</h3>

            <p>For all your property news, promotions and new project developments please enter your email below to subscribe to our property newsletter.</p>

        </div>

        <div class="right">
            <div class="homemsg"></div>
            {{ Form::text('name', null,['class'=>'required', 'placeholder'=>"Your Name",'id'=>'hname']) }}
            {{ Form::email('email', null,['class'=>'required email', 'placeholder'=>"Email Address",'id'=>'hemail']) }}

            {{ Form::submit('SUBSCRIBE', array('class'=>'btn btn-primary','id'=>'homenewsbtn'))}}
        </div>

        {{ Form::close() }}
    </div>

</div>