<div id="home-content">
	<h1>Koh Samui Real Estate with Conrad Properties&nbsp;</h1>

	<p>Welcome to Conrad Properties: Koh Samui’s favoured, luxuryreal estate agency. We are wholeheartedly
        devoted to providing you with personalised, yet professional real estate services and advice to help you

        discover your dream property in Thailand.</p>

    <p>We have an extensive choice of properties for sale and rent in Thailand, from private <a href="{{url('koh-samui-luxury-villas')}}">luxury villas</a> &amp;houses,
        to freehold condos, apartments and land investments. Our services are completely free for both
        property <a href="{{url('search/all/buy')}}">buyers</a> and <a href="{{url('search/all/rent')}}">renters</a> , as our commissions come directly from
        <a href="{{url('properties/list')}}"> property owners</a> - we are able to offer
        you the most competitive prices available in the popular Koh Samui Real Estate Market.</p>

	<p>Our reputation is built upon a strict code of ethics and sincere business acumen, so you can
        expect genuine, <a href="{{url('/buyers-guide')}}">honest and reliable advice </a> . We have a wide selection of <a >Koh Samui</a> </p>

	<p>
        <a href="">Properties</a> for <a href="">sale</a> or <a href="">rent</a> in Koh Samui and our dedicated team of property advisors are available to answer
        any questions you may have regarding legislation and <a href="{{url('blog-news/can-foreigners-buy-real-estate-in-thailand')}}">foreign ownership</a>.
    </p>

	<p>Whether you are looking to discover your new home in paradise or seeking a fruitful and confidential
        investment opportunity; you will be offered the same level of care and respect that all of our customers

        receive.</p>

</div>
<div class="row" id="subscribe">

    <div class="col-3">
        <div class="icon">

            <img src="/assets/images/selling_icon.png" alt="ad" width="98" height="98" />

        </div>
    </div>

    <div class="col-3 two">
        <div class="block">

            <h2>Selling Your Property?</h2>

            <p>If you would like to have your property listed for <strong>FREE</strong>, please contact us with the
                details and we will be happy to assist.</p>

            <img src="/assets/images/selling_icon2.png" alt="selling" width="94" height="127" />

        </div>
    </div>

    <div class="col-6">
        {{ Form::open(array('url'=>'/newsletter/', 'class'=>'required-form','id'=>'homenews')) }}

        <div class="left">

            <h3>Subscribe to our newsletter</h3>

            <p>For all your property news, promotions and new project developments please enter your email below to subscribe to our property newsletter.</p>

        </div>

        <div class="right">
            <div class="homemsg"></div>
            {{ Form::text('name', null,['class'=>'required', 'placeholder'=>"Your Name",'id'=>'hname']) }}
            {{ Form::email('email', null,['class'=>'required email', 'placeholder'=>"Email Address",'id'=>'hemail']) }}

            {{ Form::submit('SUBSCRIBE', array('class'=>'btn btn-primary','id'=>'homenewsbtn'))}}
        </div>

        {{ Form::close() }}
    </div>

</div>