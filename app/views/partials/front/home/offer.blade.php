<div class="centering">
	
	<div class="left">
		<div class="off">
			<h3>Koh Samui Property for Sale</h3>
			<p>From exclusive real estate developments in private gated communities, to foreign freehold condominiums and commercial real estate, Conrad Properties Co., Ltd. offers a carefully selected collection of lifestyle properties for sale in Koh Samui. With competitive prices, premium quality, and prime locations, our multi-lingual staff will be happy to assist you <a class="link" href="{{url('search/all/buy')}}">find your dream property</a> in paradise.
		</div>
		<div class="off">
			<h3>Koh Samui Property for Rent</h3>
			<p>We also offer a wide selection of Samui property for rent, from holiday homes, to luxury sea-view villas and lifestyle condominiums in projects with full resort facilities, we have an array of Koh Samui property for long term or short term rent, in variety of locations; either by the beach, hillside, or city center. <a class="link" href="{{url('#subs')}}" class="link">Join our mailing list</a> today to receive our latest property rental promotions and last minute special offers.</p>
			</div>
		<div class="off">
			<h3>Luxury Koh Samui Holiday Villas</h3>
			<p>Koh Samui Real Estate has experienced a strong rental demand in recent years for luxury villas and holiday homes, with new upmarket holidaymakers from Hong Kong, Singapore and China. Wherever you are located, we offer a wide range property for rent, with optional VIP services, and in-room amenities. Feel free to browse our handpicked collection of Holiday Villas for rent in Koh Samui.</p>
		</div>
	</div>
	
	<div class="mid">
		<i class="icon-logo"></i>
		<h2>Services <br/>we <br/>offer </h2>
		<p>At Conrad Properties we are committed to delivering the best possible service, to ensure you are completely
		satisfied with your property choice.</p>
		<a href="{{url('contact-us')}}" class="btn">Contact Us Today</a>
	</div>
	
	<div class="right">
		<div class="off">
			<h3>Koh Samui Land For Sale</h3>
			<p>We also specialize in sourcing land for sale in Koh Samui, and other surrounding areas. From prime beachfront land plots, to pristine sea-view serviced land on hillsides, and affordable flat land for private bespoke villas, all the land we list is inspected and quality approved for buyers safety, however the land available on our website is only a fraction of the land we have available. Please <a href="mailto:info@conradproperties.asia?subject=Please Send Samui Land Options" class="link">Email us for a full list</a>.</p>
		</div>
		<div class="off">
			<h3>Koh Samui Real Estate Consultation</h3>
			<p>As an independent real estate agency we are not tied to any specific projects or owners, and can therefore offer our clients completely unbiased FREE property consultation and advice. Whether you are a new, referral or regular customer, we will treat you with the same level of patience and respect, and <a href="https://www.samui-business-lawyer.com/" target="_blank" class="link">recommend reliable lawyers</a> to assist you with your property purchase, to ensure it transfers as smooth as possible.</p>
		</div>
		<div class="off">
			<h3>Thailand Real Estate Buyer's Guide</h3>
			<p>How can foreigners buy real estate in Thailand securely? If you are looking for impartial and informal real estate advice on Koh Samui property, we offer a dedicated hub of accurate information, including: property taxes, legal fees  & estimates, and our own user friendly 10 step buyer’s guides, to help navigate your property investment successfully. Visit our: <a href="mailto:info@conradproperties.asia?subject=Please Send Samui Land Options" class="link">Real Estate Buyer's Guide</a> for top property buying tips. </p>
		</div>
	</div>
	
	<div class="clear"></div>

</div>