<!-- begin recently block -->
<div class="recently-block">
    <div class="centering">
        <?php
        $recents = Property::recent();
        ?>
        <h3>Recently viewed properties</h3>
        <ul>
            @foreach($recents->get() as $f)
                <li>
                    <div class="image">
                        <a href="{{$f->getUrl()}}"><img src="{{$f->getPrimaryImageSmall()}}" alt="{{$f->title}}" width="174" height="105" />
                            <?php $cookie = CustomHelper::checkCookie($f->slug); ?>
                            <span
                                    class="heart property-{{$f->id}} {{'heart_class'}} <?php echo ($cookie) ? 'selected' : ''; ?>"
                                    id="recentproperty-{{$f->id}}"></span>
                        </a>
                    </div>
                    <div class="text">
                        <h4><a class="more" href="{{$f->getUrl()}}">{{$f->title}}</a></h4>

                        <div class="bottom">
                            <a class="locat" href="{{$f->getAddressUrl()}}"><i class="icon-location2"></i>{{$f->getFullAddress()}}</a>

                            <div class="clear"></div>
                            {{--<ul>
                                  @if($f->bedrooms)
                                  <li><span><i class="icon-bedroom"></i>{{$f->bedrooms}}</span></li>
                                  @endif
                                  @if($f->bathrooms)
                                  <li><span><i class="icon-bath"></i>{{$f->bathrooms}}</span></li>
                                  @endif
                                  @if($f->land_area)
                                                                  <li><img src="/assets/images/unit-icon.svg" alt="area" />{{$f->land_area}}</li>
                                                                  @endif
                              </ul>--}}
                            <div class="clear"></div>
                            <strong>{{$f->getPrice()}}</strong>
                        </div>
                    </div>
                    <a class="detail" href="{{$f->getUrl()}}">detail</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
<!-- finish recently block -->