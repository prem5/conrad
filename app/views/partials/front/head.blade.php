<meta charset="UTF-8">
@if(isset($meta_title))
    <meta name="title" content="{{$meta_title}}">
@endif
<?php if($_SERVER['REQUEST_URI'] == '/properties/list'){ ?>
<meta name="description" content="List for Sale and Rent real estate with Conrad Properties; receive a reliable and honest service - matching the available properties with the current market.">
<?php }elseif (isset($meta_description)){
?>
<meta name="description" content=" {{$meta_description}}">
<?php
} else { ?>
<meta name="description" content="{{preg_replace('/\s+/', ' ', $meta_description)}}">
<?php } ?>
<meta name=viewport content="width=device-width, initial-scale=1">
<?php xdebug_break(); ?>
@if( (isset($noindex) && $noindex) || (strpos($_SERVER['REQUEST_URI'],'currency') !== false) || (strpos($_SERVER['REQUEST_URI'],'display') !== false) || (strpos($_SERVER['REQUEST_URI'],'sortBy') !== false) )
    <meta name="robots" content="noindex" />
@else
    <meta name="robots" content="index, follow"/>
@endif

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
{{--*/$url = \Request::url();/*--}}
<link rel="canonical" href="{{str_replace("http://", "https://",Request::url())}}">
{{--@if(strpos($url,'/buy') !== false)--}}
{{--<link rel="canonical" href="/search/all/buy?page=all">--}}
{{--@elseif(strpos($url,'/rent') !== false)--}}
{{--<link rel="canonical" href="/search/all/rent?page=all">--}}
{{--@else--}}
{{--<link rel="canonical" href="{{str_replace("http://", "https://",Request::url())}}">--}}
{{--@endif--}}
<?php if($_SERVER['REQUEST_URI'] == '/properties/list'){ ?>
<title>Sell &amp; Rent your Koh Samui Real Estate | List Property Here </title>
<?php } elseif (isset($meta_title)){
?>
<title>{{$meta_title}}</title>
<?php
} else { ?>
<title>{{$head_title}}</title>
<?php } ?>
<?php
$og = str_replace("http://", "https://",Request::fullUrl());
if(isset($_COOKIE['lan'])){
    if(strpos($og,'?') === false){
        $og .= "?lang=".$_COOKIE['lan'];
    }
    elseif(strpos($og,'lang') === false){
        $og .= "&lang=".$_COOKIE['lan'];
    }
}
else{
    if(strpos($og,'?') === false){
        $og .= "?lang=eng";
    }
    elseif(strpos($og,'lang') === false){
        $og .= "&lang=eng";
    }
}
?>

@if($og_data)
    <!-- Open Graph data -->
    <meta property="og:site_name" content="Conrad Properties"/>
    <meta property="og:title" content="{{$og_data['title']}}"/>
    <meta property="og:description" content="{{$og_data['description']}}"/>
    <meta property="og:url" content="{{$og}}"/>
    {{--<meta property="og:url" content="{{$og_data['url']}}"/>--}}
    <meta property="og:image" content="{{$og_data['image']}}"/>
@else
    <!-- Open Graph data -->
    <meta property="og:site_name" content="Conrad Properties"/>
    <meta property="og:title" content="{{$head_title}}"/>
    <meta property="og:description" content="{{$meta_description}}"/>
    <meta property="og:url" content="{{$og}}"/>
    <meta property="og:image" content="{{str_replace("http://", "https://",Request::url())}}/images/logo-header.png"/>
@endif
@if(isset($previous_url))
    <link rel="prev" href="{{$previous_url}}" />
@endif
@if(isset($next_url))
    <link rel="next" href="{{$next_url}}" />
@endif
