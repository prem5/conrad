<!-- begin recently block -->
<div class="recently-block sub">
    <div class="centering">

        <h3>Recently viewed properties</h3>

        <ul>
            @foreach([] as $f)
            <li>
                <div class="image">
                    <a href="{{$f->getUrl()}}"><img src="{{$f->getThumbImage()}}" alt="{{$f->title}}" width="174" height="105" />
                        <span class="heart {{'heart_class'}}"></span></a>
                </div>
                <div class="text">
                    <h4><a class="more" href="{{$f->getThumbImage()}}">{{$f->title}}</a></h4>

                    <div class="bottom">
                        <a class="locat" href="{{$f->getUrl()}}"><i class="icon-location2"></i>{{$f->getFullAddress()}}</a>
                        <strong>{{$f->sale_price}}</strong>
                    </div>
                </div>
                <a class="detail" href="{{$f->getUrl()}}">detail</a>
            </li>
            @endforeach
        </ul>

    </div>
</div>
<!-- finish recently block -->