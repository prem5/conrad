<div id="subscribe" class="subscribe">
    <h4>Sign up for Newsletter</h4>
    <div class="sidemsg"></div>
    {{ Form::open(array('url'=>url('newsletter'), 'class'=>'form-horizontal','id'=>'sidenewfrm')) }}
    <div class="control-group">

        <div class="controls">
            {{ Form::text('name', null,array('class'=>'required','placeholder'=>'Your Name','id'=>'sname')) }}
        </div>
    </div>
    <div class="control-group">

        <div class="controls">
            {{ Form::text('email', null,array('class'=>'required','placeholder'=>'Email Address','id'=>'semail')) }}
        </div>
    </div>
    <div class="form-actions">
        {{ Form::submit('SUBSCRIBE', array('class'=>'btn btn-primary' ,'id'=>'newsfrm'))}}
    </div>
    {{ Form::close() }}
</div>

