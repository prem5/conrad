<!-- CSS Styles -->
<link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css"/>

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<style type="text/css">
    .heapbox{display: none;}

    .things-block .centering > h3{
        margin-bottom: 0px !important;
    }
    .things-block p {
        font-size: 12px !important;
    }
    /*.luxury-block .detail p{*/
    /*padding-bottom: 21px;*/
    /*font-size: 16px;*/
    /*line-height: 23px;*/
    /*text-align: justify;*/
    /*color: #454545;*/
    /*}*/
    #header-wrap .top .centering {
        padding: 5px 0;
    }
    .date-block .tele{
        padding: 2px 5px 1px 42px;;
    }
    .slider-block .strip{
        padding: 10px 0;
    }
    .slider-block .centering h2{
        font-size: 24px;
        line-height: 30px;
    }
    .banner-price{
        color: #2fbfbd !important;
        padding: 10px 5px 0 0;
        display: inline-block;
        font-size: 14px ! important;
        text-transform: uppercase;
        font-family: 'GothamBlack' !important;
    }
    .slider-block .text strong {
        padding: 12px 5px 0 0;
        display: inline-block;
        font-size: 14px;
        text-transform: uppercase;
        font-family: 'GothamBlack';
    }
    #header-wrap .top{
        /*background: url('/assets/images/header-bg.png') repeat !important;*/
        background-color: #202121;
    }
    .home-top{
        opacity: 0.85;
    }
    .footer-block{
        background-color: #202121;
    }
    .copyright-block{
        background-color: #131414;
    }
    .featured_list li{
        list-style: none;
        margin-bottom: 30px;
    }
    .filter-show{
        float: right;
        cursor: pointer;
        margin-left: 15px;
    }
    .filter-show .icon-search{
        color: #2fbfbd;
        font-size: 40px;
    }
    .laxury-message{
        margin: 23px 0 20px 0;
        display: none;
    }
    .laxury-message p{
        font-size: 16px;
        color: #454545;
    }
    .slider-block .heapBox .heapOption a{
        color: #000;
    }
    .pull-left{
        float: left !important;
    }
    .pull-right{
        float: right !important;
    }
    .office-pic{
        position: absolute;
        top: 0%;
        left: 53%;
    }
    .office-pic img{
        width: 80%;
    }
    .sales-block .heapBox .heapOption a{
        color: #000;
    }
    .bold-option{
        font-family: 'GothamBold' !important;
    }
    #home-content a {
        color: #2fbfbd;
    }
    @media only screen and (max-width: 767px){
        #header-wrap .logo {
            float: none;
            width: 100%;
            padding-left: 10px;
        }

        .home-top{
            opacity: 1;
        }

        #header-wrap .top{
            min-height: 75px !important;
        }
        #slickarrow a{
            background-position: center;
        }
        .selling-block .left{
            text-align: center;
        }
        .adams-block .h2{
            text-align: center;
            max-width: 100%;
        }
        .contact-block .overlay{
            margin-top: 280px;
        }
        .office-pic{
            width: 100%;
            left: 4%;
            z-index: 2;
        }
        .office-pic img{
            width: 100% !important;
        }
        .map{
            margin-top: -200px;
        }
        .blog-block li img{
            display: block;
            width: 100%;
        }
    }

    @media only screen and (max-width: 800px){
        .filter-show{
            display: none;
        }

        .luxury-block .bottom span.location_new{
            display: inline-block;
            font-size: 15px;
            padding-bottom: 15px;
        }
        .sale_price_new{
            width: 76%;
        }

        .luxury-block .detail .prcc h5{
            font-size: 18px;
        }
        .enquire{
            width: 100%;
            text-align: center;
        }
        .luxury-block .detail .col-1 .no-bg{
            background: none;
        }
    }
</style>
@include('partials.google-analytics')