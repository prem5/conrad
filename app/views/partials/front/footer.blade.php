<!-- begin footer block -->
<div class="footer-block">
    <div class="centering">

        <div class="first bx">

            <h3>Information</h3>
            <div class="cntnts">
                <ul>
                    <li><a href="{{url('aboutus')}}">About us</a></li>
                    <li><a href="{{url('contact-us')}}">Contact us</a></li>
                    <li><a href="{{url('buyers-guide')}}">Guides</a></li>
                    <li><a href="{{url('blog-news')}}">Blog & News Updates</a></li>
                    <li><a href="{{url('search/all/rent')}}">Property for Rent</a></li>
                    <li><a href="{{url('search/all/buy')}}">Property for Sale</a></li>
                    <li><a href="{{url('koh-samui-luxury-villa-for-sale')}}">Luxury Villas for Sale</a></li>
                    <li><a href="{{url('koh-samui-luxury-villa-for-rent')}}">Luxury Villas for Rent</a></li>
                </ul>
            </div>




        </div>

        <div class="second bx">

            <h3>Search Areas in Samui</h3>
            <div class="cntnts">
                <?php $area =Area::getRandArea();
                $i = 1;
                echo "<p>";
                foreach($area as $a):
                    $city = $a->city()->first();
                    $province = $city->province()->first();
                    $country = $province->country()->first();
                    $url = url('search/'.$country->slug.'/'.$province->slug.'/'.$city->slug.'/'.$a->slug);
                    if($i==1)
                        echo  "<a href='".$url."'>".$a->name."</a>";
                    else
                        echo ', '."<a href='".$url."'>".$a->name."</a>";
                    $i++;
                endforeach;
                echo "</p>";
                ?>
            </div>
            <!-- <p>Chaweng, Chaweng Noi, Lamai, Hua Thanon Bangrak, Big Buddha, Choeng Mon, Plai Lam, Bophut, Maenam, Ban
                 Tai, Ban Por, Laem Yai, Ban Makham, Nathon, Lipa Noi, Ban Saket, Taling Ngam Pang Ka, Thong Krut, Na
                 Muang, Ban Kao, Laem Sor, Laem set, </p>-->

        </div>

        <div class="third bx">

            <h3>Property Guides</h3>
            <div class="cntnts">
                <ul><?php $guide=Guide::getBuyerGuide();
                    foreach($guide as $res):?>
                    <li><a href="{{$res->getUrl()}}"><?php echo $res->name;?></a></li>
                    <?php  endforeach;
                    ?>
                </ul>
            </div>


        </div>
<style>
    ::placeholder{
        color: #ffffff;
    }
</style>

        <div class="social">
            <div class="footmsg"></div>
            {{ Form::open(array('url'=>url('newsletter'), 'class'=>'','id'=>'subs')) }}
            <div class="controls">
                {{ Form::hidden('name', 'footernewslettersubscription') }}
                {{ Form::email('email',null,array('class'=>'required','placeholder'=>'EMAIL ADDRESS','id'=>'femail')) }}

            </div>
            <div class=" col-md-7">
                {{ Form::submit('Subscribe', array('class'=>'btn btn-primary','id'=>'emailsub'))}}
            </div>
            {{ Form::close() }}


            <div class="clear"></div>
            <ul>
                <?php if ($settings->twitter_url): ?>
                <li><a href="{{$settings->twitter_url}}" target="_blank"><i class="icon-twitter"></i></a></li>
                <?php endif; ?>
                <?php if ($settings->facebook_url): ?>
                <li><a href="{{$settings->facebook_url}}" target="_blank"><i class="icon-facebook"></i></a></li>
                <?php endif; ?>
                <?php if ($settings->instagram_url): ?>
                <li><a href="{{$settings->instagram_url}}" target="_blank"><i class="icon-instagram"></i></a></li>
                <?php endif; ?>
                <?php if ($settings->pinterest_url): ?>
                <li><a href="{{$settings->pinterest_url}}" target="_blank"><i class="icon-pinterest"></i></a></li>
                <?php endif; ?>
                <?php if ($settings->google_plus_url): ?>
                <li><a href="{{$settings->google_plus_url}}" target="_blank"><i class="icon-gplus"></i></a></li>
                <?php endif; ?>
                <?php if ($settings->linkedin_url): ?>
                <li><a href="{{$settings->linkedin_url}}" target="_blank"><i class="icon-linkedin"></i></a></li>
                <?php endif; ?>
                <?php if ($settings->primary_email): ?>
                <li><a href="mailto:{{$settings->primary_email}}"><i class="icon-envelope"></i></a></li>
                <?php endif; ?>
            </ul>
            <div class="clear"></div>
                    {{ Form::open(array('url'=>url('find'), 'class'=>'','id'=>'subs')) }}
                    <div class="controls">
                        {{ Form::hidden('name', 'footernewslettersubscription') }}
                        <input class="required" placeholder="Ref ID:" id="ref" style="padding: 12px 10px;color: #999898;font-size: 12px;float: left;border: none;height: 30px;width: 40%;background: #464747;text-transform:uppercase; border-radius: 0" name="ref" type="text" required >
                    </div>
                    <div class=" col-md-7">
                        <button style="    padding-left: 10px;
    padding-right: 10px;
    cursor: pointer;
    border: none;
    height: 30px;
    line-height: 36px;
    position: absolute;
    border-radius: 0;
    -webkit-appearance: none;
    /* right: 0; */
    top: 0;
    font-size: 12px;
    font-family: GothamBlack;
    color: #fff;
    text-transform: uppercase;
    background-color: #464747;
    background-size: 15px;
    width: 20%;" type="submit"><i class="icon-search"></i></button>
                    </div>
                    {{ Form::close() }}
        </div>
        <div class="clear"></div>
    </div>
</div>
<!-- finish footer block -->

<!-- begin copyright block -->
<div class="copyright-block">
    <div class="centering">
        <p>Copyright &copy; {{date('Y')}} <strong>Conrad Properties Co., Ltd.</strong> All rights Reserved.</p>

        <p><a href="{{url('privacy')}}">Privacy Policy</a> <a href="{{url('terms-conditions')}}">Terms &amp;
                Conditions</a> {{--<a href="#">Sitemap</a>--}}</p>

        <div class="clear"></div>
    </div>
</div>
<!-- finish copyright block -->
<div id="inset_form" style="display: none;">

</div>