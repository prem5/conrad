<div class="guide tag">
    <span class="title">TAGS</span>

    <p>
        <?php
        $tags = \Tag::take(25)->get();
        $rnd = array('<span>#link#</span>', '#link#');
        $count = 1;
        ?>
        @foreach($tags as $tg)
        <?php
        $link = HTML::link($tg->getUrl(), $tg->name);
        ?>
        @if(($count % 2) == 0)
        {{str_replace('#link#', $link, $rnd[0]);}}
        @else
        {{str_replace('#link#', $link, $rnd[1]);}}
        @endif
        <?php $count++;?>
        @endforeach

    </p>

</div>