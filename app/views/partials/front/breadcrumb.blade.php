<div class="np-block">

    <div class="centering">
        <?php
        $segment1 = \Request::segment(1);
        $segment2 = \Request::segment(2);
        if ($segment1 == "search" ){
            if(Request::segment(3))
                $navData = ucwords(\Request::segment(3));
            else
                $navData = ucwords(\Request::segment(1));
        } else if ($segment1 == "blogs")
            $navData = "Blogs";
        else if ($segment1 == "blog-news")
            $navData = "Blog & News";
        else if ($segment1 == "properties")
            $navData = "List";
        else
            $navData = ucwords($segment1);
        $navData = str_replace('-', ' ', $navData);
        ?>
        <ul>
            <li>YOU ARE HERE:</li>
            <li><a href="/">Home &gt;</a></li>
            <li><?php echo $navData; ?></li>
        </ul>

        <div id="share">
            <a href="#link" class="share"><i class="icon-share"></i></a>

            <div id="link">

                <div class="social social-sharing">

                    <a class="share-facebook" href="https://www.facebook.com/sharer.php?u=conradproperties.com"
                       target="_blank">
                        <span aria-hidden="true" class="icon icon-facebook"></span>
                        <span class="share-title">Like</span>

                    </a>
                    <a class="share-twitter"
                       href="https://twitter.com/share?url=conradproperties.com&amp;text=Description of page"
                       target="_blank">
                        <span aria-hidden="true" class="icon icon-twitter"></span>
                        <span class="share-title">Tweet</span>
                        {{--<span class="share-count is-loaded">6200</span>--}}
                    </a>
                    <a class="share-pinterest"
                       href="https://pinterest.com/pin/create/button/?url=conradproperties.com&amp;description=Description of page"
                       target="_blank">
                        <span aria-hidden="true" class="icon icon-pinterest"></span>
                        <span class="share-title">Pinterest</span>

                    </a>
                    <a class="share-google" href="https://plus.google.com/share?url=conradproperties.com"
                       target="_blank">
                        <!-- Cannot get Google+ share count with JS yet -->
                        <span aria-hidden="true" class="icon icon-gplus"></span>
                        <span class="share-title">Google+</span>
                        {{--<span class="share-count is-loaded">+1</span>--}}
                    </a>

                </div>

            </div>

        </div>

        <div class="clear"></div>

        <hr>
        <span class="border"></span>
    </div>

</div>