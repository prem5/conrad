<div id="filter-form" class="search schfrm">
    {{--*/$data = Session::all();/*--}}
    {{Form::open(['url'=> url('searchresult/'), 'id'=>'property-search-form','method'=>'POST'])}}
    <fieldset>
        <div class="inner">
            {{Form::select('action',\Config::get('conrad.buy_sell_select'),(array_key_exists('action', $data) && count($data) > 0) ? $data['action'] : ''
            ,array('class'=>'search_buy_rent','id'=>'search-buy-rent'))}}

            <select class="select" name="type" id="search-type" >
                <?php $first_types = PropertyType::orderBy('order','ASC')->where('status','=','1')->limit(2)->get(); ?>
                <?php $second_types = PropertyType::orderBy('order','ASC')->where('status','=','1')->offset(2)->limit(20)->get(); ?>
                <option value="">TYPE</option>
                <option value="">ANY</option>
                    @if((array_key_exists('type', $data) && count($data) > 0))
                        @foreach($first_types as $type)
                            <option value="{{$type->id}}" @if($data['type'] == $type->id) selected @endif>{{$type->label}}</option>
                        @endforeach
                        <option value="laxury" @if($data['type'] == 'laxury') selected @endif>LUXURY VILLA</option>
                        @foreach($second_types as $type)
                            <option value="{{$type->id}}" @if($data['type'] == $type->id) selected @endif>{{$type->label}}</option>
                        @endforeach
                    @else
                        @foreach($first_types as $type)
                            <option value="{{$type->id}}" >{{$type->label}}</option>
                        @endforeach
                        <option value="laxury" >LUXURY VILLA</option>
                        @foreach($second_types as $type)
                            <option value="{{$type->id}}" >{{$type->label}}</option>
                        @endforeach
                    @endif
            </select>
            {{Form::select('place',\City::getSearchSelect(),(array_key_exists('place', $data) && count($data) > 0) ? $data['place'] :
            '',array('class'=>'property-place','id'=>'search-place'))}}
            {{Form::select('bedroom',\Config::get('conrad.bedrooms_select'),(array_key_exists('bedroom', $data) && isset($data['bedroom']) > 0) ?
            $data['bedroom'] : '',array('class'=>'select'))}}

            {{--*/$priceSel = (array_key_exists('action', $data) && count($data) > 0) ? ($data['action'] == 'rent') ?
            \Config::get('conrad.price_rent_select') : \Config::get('conrad.price_buy_select') :
            \Config::get('conrad.price_buy_select');/*--}}
            {{--<select class="select" id="property-price">--}}
                {{--@foreach($priceSel as $key)--}}
                {{--<option value="{{$key}}">{!! $key !!} </option>--}}
                {{--@endforeach--}}
            {{--</select>--}}

            {{Form::select('price',$priceSel,(array_key_exists('price', $data) && isset($data['price']) > 0) ? $data['price'] :
            '',array('class'=>'select','id'=>'property-price'))}}
        </div>
        {{Form::button('SEARCH',['type'=>'submit'])}}
    </fieldset>
    {{Form::close()}}
</div>
<div id="laxury-message" class="laxury-message">
    @if( (isset($data['type'])) && $data['type'] == 'laxury')
    <p>
        @if($data['action'] && ($data['action'] == 'rent') )
            We offer some of the most stunning luxury villas for rent in Koh Samui with infinity pools, BBQ, gyms and jacuzzi. Whether you are planning for a quiet holiday or long term rental , our specialized real estate team can help you pick your luxury villa for rent.
        @else
        We offer some of the most amazing luxury villas for sale in Koh Samui. whether you want panoramic sea-views, an infinity swimming pool, or beachfront, all our properties have been hand-picked and our specialized real estate team will find exactly what you’re looking for.
        @endif
    </p>
    @endif
</div>