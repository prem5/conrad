@if(Session::has('message'))
<p class="alert alert-success ">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    {{ Session::get('message') }}
</p>

@endif
<h1>Contact Us</h1>

<div class="left">
    {{ Form::open(array('url'=>'contact', 'class'=>'')) }}
    <fieldset>
        <ul>
            <li>
                <div class="control-group">
                    {{ Form::text('name',null,array('placeholder'=>'Name')) }}
                    {{$errors->first('name', '<p class="error">:message</p>')}}
                </div>
                <div class="control-group">
                    {{ Form::text('email',null,array('placeholder'=>'Email')) }}
                    {{$errors->first('email', '<p class="error">:message</p>')}}
                </div>
            </li>
            <li>
                {{ Form::text('phone', null,array('placeholder'=>'Phone')) }}
                {{$errors->first('phone', '<p class="error">:message</p>')}}
            </li>
            <li>
            <li>
                {{Form::select('subject',\Config::get('conrad.contact_subject_select'),'',array('class'=>'select'))}}
                {{$errors->first('subject', '<p class="error">:message</p>')}}
            </li>
            </li>
            <li>
                {{Form::textarea('message',null,array('placeholder'=>'Please type your property enquiry here, including as much information as possible. i.e. property type, size, location, budget or any other specific requirements you may have.'))}}
                {{$errors->first('message', '<p class="error">:message</p>')}}
            </li>
            <li>

                {{ Form::button('Send', array('type'=>'submit'))}}

            </li>
        </ul>
    </fieldset>
    {{Form::close()}}
</div>
