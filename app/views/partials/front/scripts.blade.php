<script type="text/javascript">

</script>
{{--<script src="/assets/js/jquery.js" type="text/javascript"></script>--}}
<script src="{{url('assets/js/my.js')}}" type="text/javascript"></script>

<!-- <script src="/assets/js/heapbox.js" type="text/javascript"></script>
<script src="/assets/js/cycle2.js" type="text/javascript"></script>
<script src="/assets/js/wow.js" type="text/javascript"></script>
<script src="/assets/js/slick.js" type="text/javascript"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false" type="text/javascript"></script>
<!--<script src="/assets/js/fancybox.js" type="text/javascript"></script> -->
{{--<script src="/assets/js/custom-form-elements.js" type="text/javascript"></script>--}}
<!-- <script src="/assets/js/nicefileinput.js" type="text/javascript"></script>
<script src="/assets/js/retina.js" type="text/javascript"></script>
<script src="/assets/js/simpleValidate.js" type="text/javascript"></script>
<script src="/assets/js/social-buttons.js" type="text/javascript"></script>
<script src="/assets/js/jquery.validate.js" type="text/javascript"></script>
<script src="/assets/js/custom.js" type="text/javascript"></script>
<script src="/assets/js/main.js" type="text/javascript"></script> -->
<script type="text/javascript">
    function showFilter() {
        $('#filter-form').slideToggle("slow");
        $('#laxury-message').slideToggle("slow");

    }
    jQuery(document).ready(function () {

        if($(window).width() > 800){
            if(($('.filter-show').length > 0)) {
                showFilter();
            }
        }else{
            $('#laxury-message').slideToggle("slow");
           // searchbuyrentchange();
        }
        $('#property-search-form option').each(function(){
            if(!($(this).attr('value').length > 0)){
                $(this).addClass("bold-option");
            }
            $(this).text($(this).text().toUpperCase());
        });
    })

    function senden(){
        if(jQuery("#MyForm").validate({
            rules:{name:"required",email:{required:!0,email:!0},phone:"required",subject:"required",message:"required"},
            messages:{name:"Name is required.",email:{required:"Email is required.",email:"Please enter valid email."},phone:"Phone is required",subject:"Subject is required",message:"Message is required"}}),
            !jQuery("#MyForm").valid())return!1;
        jQuery('#enquery_now_new').prop('disabled', true);
        var e=$("#MyForm").serialize(),r=$("#MyForm").attr("action");
        $.ajax({url:r,type:"POST",data:e,success:function(e){jQuery("#MyForm").hide(),jQuery(".succemsg").html(e),jQuery('#enquery_now').prop('disabled', false); }})

    }

    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
    }
    function searchbuyrentchange(){
        if($('#action').val() == 'rent'){
            $('#property-price').html('<option value="" selected="selected">PRICE</option><option value="daily">DAILY</option><option value="daily-5-10">5-10K THB</option><option value="daily-10-20">10-20K THB</option><option value="daily-20-30">20-30K THB</option><option value="daily-30-50">30-50K THB</option><option value="daily->50">&gt;50K THB</option><option value="monthly">MONTHLY</option><option value="monthly-30-50">30-50K THB</option><option value="monthly-50-70">50-70K THB</option><option value="monthly-70-100">70 -100K THB</option><option value="monthly-100-150">100 -150K THB</option><option value="monthly->150">&gt;150K THB</option>');
        }
        else{
            $('#property-price').html('<option value="">PRICE</option><option value=" ">ANY</option><option value="<5"><5M THB</option><option value="5-10">5 - 10M THB</option><option value="10-20">10 - 20M THB</option><option value="20-30">20 - 30M THB</option><option value=">30">&gt;30M THB</option>');
        }
    }
</script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>

<script type="text/javascript">

    // function add_chatinline(){var hccid=45433843;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
    // add_chatinline();

</script>