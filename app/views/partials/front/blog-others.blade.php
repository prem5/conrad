<?php
$blogOther = \Blog::getRandomBlogData();

?>
<div class="guide">

    <span class="title">OTHER POSTs</span>
    @foreach($blogOther as $bo)
    <h3><a href="{{$bo->getUrl()}}">{{$bo->name}}</a></h3>

    <p>{{$bo->trimTeaser(150)}}</p>
    @endforeach

</div>