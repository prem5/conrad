<style>

    select {
        /* styling */
        background-color: white;
        border: 1px solid #d6d6d6;
        /*border-radius: 4px;*/
        display: inline-block;
        font: inherit;
        line-height: 1.5em;
        padding: 0.5em 3.5em 0.5em 1em;

        /* reset */

        margin: 0;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-appearance: none;
        -moz-appearance: none;

        text-decoration: none;
        display: block;
        font-size: 16px;
        color: #000;
        text-transform: uppercase
    }
    select.minimal {
        background-image:
                linear-gradient(45deg, transparent 50%, #58cbca 50%),
                linear-gradient(135deg, #58cbca 50%, transparent 50%),
                linear-gradient(to right, #ccc, #ccc);
        background-position:
                calc(100% - 20px) calc(1em + 2px),
                calc(100% - 15px) calc(1em + 2px),
                calc(100% - 2.5em) 0.5em;
        background-size:
                5px 5px,
                5px 5px,
                1px 1.5em;
        background-repeat: no-repeat;
        width: 100%;
        float: none;
        margin-left: 0;
        height: 40px;
        margin-bottom: 10px;
    }
</style>
<div id="filter-form" class="search schfrm">
    {{--*/$data = Session::all();/*--}}
    {{Form::open(['url'=> url('searchresult/'), 'id'=>'property-search-form','method'=>'POST'])}}
    <fieldset>
        <div class="inner">

            {{--$(".searchOpen").click(function(){--}}
            {{--$(".schfrm").slideToggle(),$(".schfrm .heapBox").slideToggle();--}}
            {{--$('#laxury-message').slideToggle("slow");--}}

            {{--})--}}
            {{--class="select" name="type" id="search-type"--}}
            {{Form::select('action',\Config::get('conrad.buy_sell_select'),(array_key_exists('action', $data) && count($data) > 0) ? $data['action'] : ''
           ,array('class'=>'minimal','onchange'=>'searchbuyrentchange()','id'=>'action'))}}
            <select class="minimal" name="type" id="search-type1" onchange="searchbuyrentchange()">
                <?php $first_types = PropertyType::orderBy('order','ASC')->where('status','=','1')->limit(2)->get(); ?>
                <?php $second_types = PropertyType::orderBy('order','ASC')->where('status','=','1')->offset(2)->limit(20)->get(); ?>
                <option value="">TYPE</option>
                <option value="">ANY</option>
                @if((array_key_exists('type', $data) && count($data) > 0))
                    @foreach($first_types as $type)
                        <option value="{{$type->id}}" @if($data['type'] == $type->id) selected @endif>{{$type->label}}</option>
                    @endforeach
                    <option value="laxury" @if($data['type'] == 'laxury') selected @endif>LUXURY VILLA</option>
                    @foreach($second_types as $type)
                        <option value="{{$type->id}}" @if($data['type'] == $type->id) selected @endif>{{$type->label}}</option>
                    @endforeach
                @else
                    @foreach($first_types as $type)
                        <option value="{{$type->id}}" >{{$type->label}}</option>
                    @endforeach
                    <option value="laxury" >LUXURY VILLA</option>
                    @foreach($second_types as $type)
                        <option value="{{$type->id}}" >{{$type->label}}</option>
                    @endforeach
                @endif

            </select>
            {{Form::select('place',\City::getSearchSelect(),(array_key_exists('place', $data) && count($data) > 0) ? $data['place'] :
            '',array('class'=>'minimal'))}}
            {{Form::select('bedroom',\Config::get('conrad.bedrooms_select'),(array_key_exists('bedroom', $data) && isset($data['bedroom']) > 0) ?
            $data['bedroom'] : '',array('class'=>'minimal'))}}

            {{--*/$priceSel = (array_key_exists('action', $data) && count($data) > 0) ? ($data['action'] == 'rent') ?
            \Config::get('conrad.price_rent_select') : \Config::get('conrad.price_buy_select') :
            \Config::get('conrad.price_buy_select');/*--}}
            {{Form::select('price',$priceSel,(array_key_exists('price', $data) && isset($data['price']) > 0) ? $data['price'] :
            '',array('class'=>'minimal','id'=>'property-price'))}}
        </div>
        {{Form::button('SEARCH',['type'=>'submit'])}}
    </fieldset>
    {{Form::close()}}
</div>
<div id="laxury-message" class="laxury-message">
    @if( (isset($data['type'])) && $data['type'] == 'laxury')
        <p>
            @if($data['action'] && ($data['action'] == 'rent') )
                We offer some of the most stunning luxury villas for rent in Koh Samui with infinity pools, BBQ, gyms and jacuzzi. Whether you are planning for a quiet holiday or long term rental , our specialized real estate team can help you pick your luxury villa for rent.
            @else
                We offer some of the most amazing luxury villas for sale in Koh Samui. whether you want panoramic sea-views, an infinity swimming pool, or beachfront, all our properties have been hand-picked and our specialized real estate team will find exactly what you’re looking for.
            @endif
        </p>
    @endif
</div>