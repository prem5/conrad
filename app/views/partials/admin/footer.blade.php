<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="/assets/admin/js/jquery.min.js"></script>
<script src="/assets/admin/js/jquery-ui.js"></script>
<script src="/assets/admin/js/jquery-gmaps-latlon-picker.js"></script>
<script src="/assets/admin/js/selectize.min.js"></script>
<script src="/assets/admin/js/selectTag.js"></script>
<script src="/assets/admin/js/bootstrap.min.js"></script>
<script src="/assets/admin/js/jquery.ui.widget.js"></script>
<script src="/assets/admin/js/jquery.iframe-transport.js"></script>
<script src="/assets/admin/js/jquery.knob.js"></script>
<script src="/assets/admin/js/jquery.fileupload.js"></script>
<script src="/assets/admin/js/uploader.js"></script>
<script src="/assets/admin/js/templatemo_script.js"></script>
<script src="/assets/admin/js/main.js"></script>
<script src="/assets/admin/js/jquery-sortable.js"></script>
<script src="/assets/admin/js/jquery.tablednd.js"></script>
<script>
    function citychange(btn) {
        if($(btn).val()!='')
        {
            var token = $("form input[name=_token]").val();
            $.ajax({
                url:'/ajax/area',
                data:'city_id='+$(btn).val()+'&_token='+token,
                type:'post',
                dataType:'json',
                success:function(response)
                {
                    $('#area_id').attr('disabled',false);
                    $('#area_id').html(response['data']);
                }
            });
        }
        else
        {
            $('#city_id').attr('disabled',true);
            $('#area_id').attr('disabled',true);
        }
    }
</script>

