<?php $segment = \Request::segment(2);
$segment1 = \Request::segment(3);
?>
<ul class="templatemo-sidebar-menu">
    @if(Auth::user()->isAdmin())
    <!--<li><a href="{{url('administrator/users/dashboard')}}"><i class="fa fa-home"></i>Dashboard</a></li>-->
    <li class="sub <?php echo ($segment == "setting" || $segment == 'currencies' || $segment == 'propertiestype' || $segment == 'propertiesribbon' || $segment == 'templates') ? 'active open' : ''; ?>">
        <a href="javascript:;" class="cat">
            <i class="fa fa-gears"></i> Site Settings
            <div class="pull-right"><span class="caret"></span></div>
        </a>
        <ul class="templatemo-submenu">
            <li><a href="{{url('administrator/setting/')}}">Settings</a></li>
            <li <?php echo ($segment == 'currencies') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/currencies/')}}">Currency</a></li>
            <li <?php echo ($segment == 'propertiestype') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/propertiestype/')}}">Property Types</a></li>
            <li <?php echo ($segment == 'propertiesribbon') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/propertiesribbon/')}}">Property Ribbons</a></li>
            <li <?php echo ($segment == 'templates') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/templates/')}}">Mail Templates</a></li>
        </ul>
    </li>

    <li class="sub <?php echo ($segment == 'countries' || $segment == 'provinces' || $segment == 'districts' || $segment == 'cities' || $segment == 'areas') ? 'active open' : ''; ?>">
        <a href="javascript:;" class="cat">
            <i class="fa fa-database"></i> Locations
            <div class="pull-right"><span class="caret"></span></div>
        </a>
        <ul class="templatemo-submenu">

            <li <?php echo ($segment == 'countries') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/countries/')}}">Countries</a></li>
            <li <?php echo ($segment == 'provinces') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/provinces/')}}">Provinces</a></li>
            <!--<li <?php /*echo ($segment=='districts')?'class="active"':''; */?>><a href="{{url('administrator/districts/')}}">Districts</a></li>-->
            <li <?php echo ($segment == 'cities') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/cities/')}}">Cities</a></li>
            <li <?php echo ($segment == 'areas') ? 'class="active"' : ''; ?>><a href="{{url('administrator/areas/')}}">Areas</a>
            </li>
        </ul>
    </li>

    <li class="sub <?php echo ($segment == "carousels" || $segment == "features" || $segment == 'testimonials') ? 'active open' : ''; ?>">
        <a href="javascript:;" class="cat">
            <i class="fa fa-home"></i>Home Page
            <div class="pull-right"><span class="caret"></span></div>
        </a>
        <ul class="templatemo-submenu">
            <li <?php echo ($segment == 'carousels') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/carousels/')}}">Top Carousel</a></li>
            <li <?php echo ($segment == 'features') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/features/')}}">Featured Properties</a></li>
            <li <?php echo ($segment == 'testimonials') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/testimonials/')}}"></i>Testimonials</a></li>
        </ul>
    </li>


    <li class="sub <?php echo ($segment == "guides" || $segment == "pages") ? 'active open' : ''; ?>">
        <a href="javascript:;" class="cat">
            <i class="fa fa-file-text"></i>Pages
            <div class="pull-right"><span class="caret"></span></div>
        </a>
        <ul class="templatemo-submenu">

            <li <?php echo ($segment == 'guides' && $segment1 == "") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/guides/')}}">Guides Listing</a>
            </li>
            <li <?php echo ($segment == 'guides' && $segment1 == "create") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/guides/create')}}"></i>Add Guides</a></li>
            <li <?php echo ($segment == 'pages' && $segment1 == "") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/pages/')}}">Pages Listing</a>
                    </li>
            <li <?php echo ($segment == 'pages' && $segment1 == "create") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/pages/create')}}">Add Pages</a></li>

        </ul>
    </li>

    <li class="sub <?php echo ($segment == "listings") ? 'active open' : ''; ?>">
        <a href="javascript:;" class="cat">
            <i class="fa fa-th-list"></i>Listings
            <div class="pull-right"><span class="caret"></span></div>
        </a>
        <ul class="templatemo-submenu">
            <li <?php echo ($segment == 'listings' && $segment1 == "") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/listings/')}}">Listings</a></li>
            <li <?php echo ($segment == 'listings' && $segment1 == "create") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/listings/create')}}">Add Listings</a></li>
        </ul>
    </li>

    <li class="sub <?php echo ($segment == "blogs" || $segment == "keyword" || $segment == 'comments') ? 'active open' : ''; ?>">
        <a href="javascript:;" class="cat">
            <i class="fa fa-cubes"></i>Blog
            <div class="pull-right"><span class="caret"></span></div>
        </a>
        <ul class="templatemo-submenu">
            <li <?php echo ($segment == 'blogs' && $segment1 == "") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/blogs/')}}">Blog Listing</a></li>
            <li <?php echo ($segment == 'blogs' && $segment1 == "create") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/blogs/create')}}">Add Blog</a></li>
            <li <?php echo ($segment == 'comments') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/comments/')}}"><span class="badge pull-right">{{Comment::getUnpublish()->get()->count()}}</span>Blog
                    Comment</a></li>
            <li <?php echo ($segment == 'keyword') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/keyword/')}}">Keywords</a></li>
        </ul>
    </li>


    <li class="sub <?php echo ($segment == "newsletter" || $segment == "property-submission" || $segment == 'property-enquiry') ? 'active open' : ''; ?>">
        <a href="javascript:;" class="cat">
            <i class="fa fa-envelope"></i>Contacts
            <div class="pull-right"><span class="caret"></span></div>
        </a>
        <ul class="templatemo-submenu">
            <li <?php echo ($segment == 'newsletter') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/newsletter/')}}">Subscribers</a></li>
            <li <?php echo ($segment == 'property-submission') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/property-submission/')}}">Sales Requests</a></li>
            <li <?php echo ($segment == 'property-enquiry') ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/property-enquiry/')}}">Property Enquries</a></li>
        </ul>
    </li>

    <li class="sub <?php echo ($segment == 'users' || $segment == "roles") ? 'active open' : ''; ?>">
        <a href="javascript:;" class="cat">
            <i class="fa fa-users"></i> Manage Users
            <div class="pull-right"><span class="caret"></span></div>
        </a>
        <ul class="templatemo-submenu">
            <li <?php echo ($segment == 'users' && $segment1 == "") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/users/')}}">Users Listing</a></li>
            <li <?php echo ($segment == 'users' && $segment1 == "create") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/users/create')}}">Add Users</a></li>
            <li <?php echo ($segment == 'roles' && $segment1 == "") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/roles/')}}">Roles Listing</a></li>
            <li <?php echo ($segment == 'roles' && $segment1 == "create") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/roles/create')}}">Add Roles</a></li>
        </ul>
    </li>
    @endif
    @if(Auth::user()->isWriter())
    <li class="sub <?php echo ($segment == "listings") ? 'active open' : ''; ?>">
        <a href="javascript:;" class="cat">
            <i class="fa fa-th-list"></i>Listings
            <div class="pull-right"><span class="caret"></span></div>
        </a>
        <ul class="templatemo-submenu">
            <li <?php echo ($segment == 'listings' && $segment1 == "") ? 'class="active"' : ''; ?>><a
                        href="{{url('writer/listings/')}}">Listings</a></li>
            <li <?php echo ($segment == 'listings' && $segment1 == "create") ? 'class="active"' : ''; ?>><a
                        href="{{url('writer/listings/create')}}">Add Listings</a></li>
        </ul>
    </li>
    <li class="sub <?php echo ($segment == "blogs") ? 'active open' : ''; ?>">
        <a href="javascript:;" class="cat">
            <i class="fa fa-cubes"></i>Blog
            <div class="pull-right"><span class="caret"></span></div>
        </a>
        <ul class="templatemo-submenu">
            <li <?php echo ($segment == 'blogs' && $segment1 == "") ? 'class="active"' : ''; ?>><a
                    href="{{url('writer/blogs/')}}">Blog Listing</a></li>
            <li <?php echo ($segment == 'blogs' && $segment1 == "create") ? 'class="active"' : ''; ?>><a
                    href="{{url('writer/blogs/create')}}">Add Blog</a></li>
                    
        </ul>


    </li>
    <li class="sub <?php echo ($segment == "guides") ? 'active open' : ''; ?>">

     <a href="javascript:;" class="cat">
            <i class="fa fa-users"></i> Manage Guides
            <div class="pull-right"><span class="caret"></span></div>
        </a>
        <ul class="templatemo-submenu">
            <li <?php echo ($segment == 'guides' && $segment1 == "") ? 'class="active"' : ''; ?> >
            <a
                    href="{{url('administrator/guides/')}}">Guides Listing
                    </a></li>
            <li <?php echo ($segment == 'guides' && $segment1 == "create") ? 'class="active"' : ''; ?>><a
                    href="{{url('administrator/guides/create')}}"></i>Add Guides</a></li>
        </ul>
        
    </li>
    <li class="sub <?php echo ($segment == 'users') ? 'active open' : ''; ?>">
        <a href="javascript:;" class="cat">
            <i class="fa fa-users"></i> Manage Users
            <div class="pull-right"><span class="caret"></span></div>
        </a>
        <ul class="templatemo-submenu">
            <li <?php echo ($segment == 'users' && $segment1 == "") ? 'class="active"' : ''; ?>><a
                    href="{{url('writer/users/profile')}}">Profile</a></li>
        </ul>
    </li>
    @endif

    <li><a href="{{url('users/logout/')}}" data-toggle="modal" data-target=""><i class="fa fa-sign-out"></i>Sign
            Out</a></li>

</ul>