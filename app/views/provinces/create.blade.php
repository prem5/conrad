{{ Form::open(array('url'=>'/administrator/provinces/store', 'class'=>'form form-horizontal', 'files'=>true)) }}
<div class="row row-fluid">
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('name')){echo 'has-error';}?>">
                    {{Form::label('name', 'Province Name', array('class'=>'control-label'))}}
                    {{ Form::text('name', null,array('onchange'=>'getProvinceSlug(this);return
                    true;','class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('slug')){echo 'has-error';}?>">
                    {{Form::label('slug', 'Slug', array('class'=>'control-label'))}}
                    {{ Form::text('slug', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('code')){echo 'has-error';}?>">
                    {{Form::label('code', 'Code', array('class'=>'control-label'))}}
                    {{ Form::text('code', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group">
                    {{Form::label('country_id', 'Country', array('class'=>'control-label'))}}
                    {{ Form::select('country_id', Country::getcountryForSelect(), array('class'=>'form-control')) }}
                </div>

                <div class="form-group">
                    {{Form::label('status', 'Status', array('class'=>'control-label'))}}
                    {{ Form::checkbox('status', 1, array('checked'=>'checked')) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/provinces/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}