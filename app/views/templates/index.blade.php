<!--<span style="float: right; margin-bottom: 10px;">{{HTML::link('/administrator/templates/create','Add Template',['class'=>"btn btn-primary"])}}</span>-->
<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    {{--*/$i=1/*--}}
    @foreach($data as $d)
    <tr id="{{$d->id}}">
        <td>{{$i}}</td>
        <td>{{$d->title}}</td>
        <td>
            {{HTML::link('/administrator/templates/edit/'.$d->id,'Edit',array('class'=>'btn btn-warning btn-small'))}}
            {{--HTML::link('/administrator/templates/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning btn-small',
            'onClick'=>"return confirm('Do you really want to delete ?')"))--}}
        </td>
    </tr>
    {{--*/$i++/*--}}
    @endforeach
    </tbody>
</table>