{{Form::model($data,array('url' => array('administrator/templates/update',$data->id), 'method' => 'POST'))}}
<div class="row row-fluid">
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('name')){echo 'has-error';}?>">
                    {{Form::label('name', 'Name', array('class'=>'control-label'))}}
                    {{ Form::text('name', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('title')){echo 'has-error';}?>">
                    {{Form::label('title', 'Title', array('class'=>'control-label'))}}
                    {{ Form::text('title', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('subject')){echo 'has-error';}?>">
                    {{Form::label('subject', 'Subject', array('class'=>'control-label'))}}
                    {{ Form::text('subject', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('message')){echo 'has-error';}?>">
                    {{Form::label('message', 'Message', array('class'=>'control-label'))}}
                    {{ Form::textarea('message', null, array('class'=>'form-control','id'=>'editor1')) }}
                </div>
                <script>
                    CKEDITOR.replace('editor1', {
                        filebrowserBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html',
                        filebrowserImageBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Images',
                        filebrowserFlashBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Flash',
                        filebrowserUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                        filebrowserImageUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                        filebrowserFlashUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                    });
                </script>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}