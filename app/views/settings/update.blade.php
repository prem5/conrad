{{ Form::open(array('url'=>"/administrator/setting/save/$data->id", 'class'=>'form form-horizontal')) }}
<div class="row row-fluid">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">General</h2>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('title')){echo 'has-error';}?>">
                    {{ Form::label('title', 'Title', array('class'=>'form-label')) }}
                    {{ Form::text('title', $data->title,array('class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('description')){echo 'has-error';}?>">
                    {{ Form::label('description', 'Description', array('class'=>'control-label')) }}
                    {{ Form::textarea('description', $data->description, array('class'=>'form-control','id'=>'editor1')) }}
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">Default Currency</h2>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('primary_currency')){echo 'has-error';}?>">
                    {{ Form::label('primary_currency', 'Title', array('class'=>'form-label')) }}
                    {{ Form::select('primary_currency', Currency::getDefaultCurrency(),$data->primary_currency,array('class'=>'form-control')) }}
                </div>

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Map</div>
            <fieldset class="gllpLatlonPicker">
                <input type="text" class="gllpSearchField">
                <input type="button" value="search" class="gllpSearchButton">

                <div class="gllpMap" style="  height: 250px">Google Maps</div>


                <div class="panel-body   col-md-6">
                    {{Form::label('latitude','Latitude',array('class'=>'form-control'))}}
                    {{Form::text('latitude',$data->latitude,array('class'=>'form-control gllpLatitude','id'=>'latitude'))}}
                </div>

                <div class="panel-body   col-md-6">
                    {{Form::label('longitude','Longitude',array('class'=>'form-control'))}}
                    {{Form::text('longitude',$data->longitude,array('class'=>'form-control gllpLongitude','id'=>'longitude'))}}
                </div>
                {{Form::hidden('zoom','6',array('class'=>'gllpZoom'))}}
            </fieldset>
            <div class="panel-footer">Double click to place a marker, or drag a current marker.</div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">Contacts</h2>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('primary_email')){echo 'has-error';}?>">
                    {{ Form::label('primary_email', 'Primary Email (for contact forms) :',
                    array('class'=>'control-label')) }}
                    {{ Form::text('primary_email', $data->primary_email,array('class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('display_email')){echo 'has-error';}?>">
                    {{ Form::label('display_email', 'Display Email (for header and footer) :',
                    array('class'=>'control-label')) }}
                    {{ Form::text('display_email', $data->display_email,array('class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('display_name')){echo 'has-error';}?>">
                    {{ Form::label('display_phone', 'Display Phone (for header and footer) :',
                    array('class'=>'control-label')) }}
                    {{ Form::text('display_phone', $data->display_phone,array('class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('additional_email')){echo 'has-error';}?>">
                    {{ Form::label('additional_email', 'Additional Email (to also receive contacts)',
                    array('class'=>'control-label')) }}
                    {{ Form::text('additional_email', $data->additional_email,array('class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('address')){echo 'has-error';}?>">
                    {{ Form::label('address', 'Address :', array('class'=>'control-label')) }}
                    {{ Form::textarea('address', $data->address,array('class'=>'form-control')) }}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row row-fluid">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">Social Media Links</h2>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('analtyics_code_')){echo 'has-error';}?>">
                    {{Form::label('analtyics_code', 'Analtyics Code', array('class'=>'control-label'))}}
                    {{ Form::text('analtyics_code', $data->analtyics_code,array('class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('facebook_url')){echo 'has-error';}?>">
                    {{Form::label('facebook_url', 'Facebook URL:', array('class'=>'control-label'))}}
                    {{ Form::text('facebook_url', $data->facebook_url,array('class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('twitter_url')){echo 'has-error';}?>">
                    {{Form::label('twitter_url', 'Twitter URL:', array('class'=>'control-label'))}}
                    {{ Form::text('twitter_url', $data->twitter_url,array('class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('linkedin_url')){echo 'has-error';}?>">
                    {{Form::label('linkedin_url', 'LinkedIn URL:', array('class'=>'control-label'))}}
                    {{ Form::text('linkedin_url', $data->linkedin_url,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('instagram_url')){echo 'has-error';}?>">
                    {{Form::label('instagram_url', 'Instagram URL:', array('class'=>'control-label'))}}
                    {{ Form::text('instagram_url', $data->instagram_url,array('class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('pinterest_url')){echo 'has-error';}?>">
                    {{Form::label('pinterest_url', 'Pinterest URL:', array('class'=>'control-label'))}}
                    {{ Form::text('pinterest_url', $data->pinterest_url,array('class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('google_plus_url')){echo 'has-error';}?>">
                    {{Form::label('google_plus_url', 'Google Plus URL:', array('class'=>'control-label'))}}
                    {{ Form::text('google_plus_url', $data->google_plus_url,array('class'=>'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}