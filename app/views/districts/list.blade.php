<span style="float: right; margin-bottom: 10px;">{{HTML::link('/administrator/districts/create','Add District',['class'=>"btn btn-primary"])}}</span>
<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    @foreach($roles as $d)
    <tr id="{{$d->id}}">
        <td>{{$d->id}}</td>
        <td>{{$d->name}}</td>
        <td>
            {{HTML::link('/administrator/districts/update/'.$d->id,'Edit',array('class'=>'btn btn-warning btn-small'))}}
            {{HTML::link('/administrator/districts/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning btn-small',
            'onClick'=>"return confirm('Do you really want to delete ?')"))}}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>