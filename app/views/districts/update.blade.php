{{ Form::open(array('url'=>"/administrator/districts/save/$data->id", 'class'=>'form form-horizontal', 'files'=>true)) }}
<div class="row row-fluid">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('name')){echo 'has-error';}?>">
                    {{Form::label('name', 'Name', array('class'=>'control-label'))}}
                    {{ Form::text('name', $data->name,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('province_id')){echo 'has-error';}?>">
                    {{Form::label('province_id', 'Province', array('class'=>'control-label'))}}
                    {{ Form::select('province_id', Province::getProvinceForSelect(),$data->province_id,
                    array('class'=>'form-control')) }}
                </div>

                <div class="form-group">
                    {{Form::label('parent_id', 'District', array('class'=>'control-label'))}}
                    {{ Form::select('parent_id', District::getDistrictForSelect(),$data->parent_id,
                    array('class'=>'form-control')) }}
                </div>

                <div class="form-group">
                    {{Form::label('status', 'Status', array('class'=>'control-label'))}}
                    {{ Form::checkbox('status', 1,$data->status) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                </div>
            </div>
        </div>
    </div>
</div>


{{ Form::close() }}