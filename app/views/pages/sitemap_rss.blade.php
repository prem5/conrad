<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0" charset="UTF-8">
<urlset
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <document>
        <agent_name>{{$agent['agent_name']}}</agent_name>
        <agent_id>{{$agent['agent_id']}}</agent_id>
        <language>en</language>
        <phone>{{$agent['phone']}}</phone>
        <email>{{$settings->display_email}}</email>
        <website>{{$agent['website']}}</website>
        <updated_at>{{$agent['updated_at']}}</updated_at>
        <country>{{$agent['country']}}</country>
        <province>{{$agent['province']}}</province>
        <properties>
            @foreach($properties as $property)
                <property>
                    <proerty_id>{{$property->reference_code}}</proerty_id>
                    <prices>
                        @if($property->sale_price > 0)
                            <price tenure="sale" currency="{{$property->default_currency}}">{{$property->sale_price}}</price>
                        @endif
                        @if($property->daily_rental_price_from > 0 )
                        <price tenure="rent" currency="{{$property->default_currency}}" period="daily" >{{$property->daily_rental_price_from}}</price>
                        @endif
                        @if($property->monthly_rental_price_from > 0 )
                        <price tenure="rent" currency="{{$property->default_currency}}" period="monthly" >{{$property->monthly_rental_price_from}}</price>
                        @endif
                    </prices>
                    <address>
                        @if($property->province)
                        <province>{{ $property->province->name}}</province>
                        @endif
                        <city>{{ $property->city? $property->city->name : ' '}}</city>
                        <area>{{ $property->area? $property->area->name : ' '}}</area>
                        <street>{{ $property->street? $property->street->name : ' '}}</street>
                        <zip> </zip>
                        <transport> </transport>
                        <gps_lat>{{ $property->latitude }}</gps_lat>
                        <gps_lon>{{ $property->longitude }}</gps_lon>
                    </address>
                    <project ><![CDATA[ {{ $property->title }} ]]> </project>
                    <detail>
                        <type>{{ $property->getType() }} </type>
                        <sub_type> </sub_type>
                        <bedrooms> {{ $property->bedrooms }}</bedrooms>
                        <bathrooms>{{ $property->bathrooms }}</bathrooms>
                        <floor_size unit="sqm">{{ $property->building_area }}</floor_size>
                        @if($property->land_area)
                        <land_size unit="sqm">{{ $property->land_area }}</land_size>
                        @endif
                        <titles>
                            <title lang="en"><![CDATA[ {{ $property->title }} ]]></title>
                        </titles>
                        <descriptions>
                            <description lan="en"><![CDATA[ {{ $property->overview }} ]]></description>
                        </descriptions>
                        <facilities>
                            <facility> </facility>
                        </facilities>
                    </detail>
                    <images>
                        <?php $count = 1; ?>
                        @foreach($property->propertyViewGallery as $g)
                        <image number='{{$count}}'>{{ $g->getPhotoSliderPath() }}</image>
                        <?php $count++ ; ?>
                        @endforeach
                    </images>
                    <datasource>https://www.conradproperties.asia/properties/{{$property->slug}}</datasource>
                </property>
            @endforeach
        </properties>

    </document>
</urlset>
</rss>