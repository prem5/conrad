{{ Form::open(array('url'=>'/administrator/pages/store', 'class'=>'form form-horizontal')) }}
<div class="row row-fluid">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('name')){echo 'has-error';}?>">
                    {{ Form::text('name', null,array('onchange'=>'getPageSlug(this);return
                    true;','class'=>'form-control')) }}
                    {{Form::label('slug', 'Slug', array('class'=>'control-label'))}}
                </div>


                <div class="form-group <?php if( $errors->first('slug')){echo 'has-error';}?>">
                    {{ Form::text('slug', null,array('class'=>'form-control')) }}
                    {{Form::label('content', 'Content', array('class'=>'control-label'))}}
                </div>

                <div class="form-group <?php if( $errors->first('content')){echo 'has-error';}?>">
                    {{ Form::textarea('content', null, array('class'=>'form-control','id'=>'editor1')) }}
                    <script>
                        CKEDITOR.replace('editor1', {
                            filebrowserBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html',
                            filebrowserImageBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Images',
                            filebrowserFlashBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Flash',
                            filebrowserUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                            filebrowserImageUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                            filebrowserFlashUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',

                            height : 500

                        });
                    </script>
                </div>

                <div class="form-group <?php if( $errors->first('meta_title')){echo 'has-error';}?>">
                    {{Form::label('meta_title', 'Meta Title', array('class'=>'control-label'))}}
                    {{ Form::text('meta_title', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('meta_description')){echo 'has-error';}?>">
                    {{Form::label('meta_description', 'Meta Description', array('class'=>'control-label'))}}
                    {{ Form::textarea('meta_description', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group">
                    {{Form::label('status', 'Status', array('class'=>'control-label'))}}
                    {{ Form::checkbox('status', 1, array('checked'=>'checked')) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/pages/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}