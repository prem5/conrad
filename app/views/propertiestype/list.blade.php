<span style="float: right; margin-bottom: 10px;">{{HTML::link('/administrator/propertiestype/create','Add Property Type',['class'=>"btn btn-primary"])}}</span>
<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Label</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    @foreach($roles as $d)
    <tr id="{{$d->id}}">
        <td>{{$d->id}}</td>
        <td>{{$d->label}}</td>
        <td>
            {{HTML::link('/administrator/propertiestype/update/'.$d->id,'Edit',array('class'=>'btn btn-warning
            btn-small'))}}
            {{HTML::link('/administrator/propertiestype/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning
            btn-small', 'onClick'=>"return confirm('Do you really want to delete ?')"))}}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>