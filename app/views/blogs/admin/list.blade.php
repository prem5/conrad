<span style="float: right; margin-bottom: 10px;">{{HTML::link('/administrator/blogs/create','Add Blog',['class'=>"btn btn-primary"])}}</span>
<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Main Image</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    @foreach($roles as $d)
    <tr id="{{$d->id}}">
        <td>{{$d->id}}</td>
        <td>{{$d->name}}</td>
        <td>{{ HTML::image($d->getBlogThumbImagePath(), $d->name, array('id' => 'blah')) }}</td>
        <td>
            {{HTML::link('/administrator/blogs/update/'.$d->id,'Edit',array('class'=>'btn btn-warning btn-small'))}}
            {{HTML::link('/administrator/blogs/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning btn-small',
            'onClick'=>"return confirm('Do you really want to delete ?')"))}}
            {{HTML::link('/administrator/blogs/image/'.$d->id,'Blog Image',array('class'=>'btn btn-warning
            btn-small'))}}
        </td>

    </tr>
    @endforeach
    </tbody>
</table>