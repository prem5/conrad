{{ Form::open(array('url'=>'/writer/blogs/store', 'class'=>'form form-horizontal', 'files'=>true)) }}
@include('blogs.form')
{{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
<a href="{{url('/writer/blogs')}}" class="btn btn-warning btn-small">Cancel</a>
</div>

</div>
{{ Form::close() }}