<div class="buy-block about blogPost">
    <?php
    $today =
    $date = $data['blog']->created_at;
    $ts = strtotime($date);
    $curr = date('F j, Y', $ts);
    ?>
    <h1>{{$data['blog']->name}}</h1>

    <div class="social social-sharing">

        <ul>
            <li>
                <a class="share-facebook" href="https://www.facebook.com/sharer.php?u=" target="_blank">
                    <span aria-hidden="true" class="icon icon-facebook"></span>
                    <span class="share-title">Like</span>

                </a>
            </li>
            <li>
                <a class="share-twitter" href="https://twitter.com/share?url=" target="_blank">
                    <span aria-hidden="true" class="icon icon-twitter"></span>
                    <span class="share-title">Tweet</span>
                    {{--<span class="share-count is-loaded">6203</span>--}}
                </a>
            </li>
            <li>
                <a class="share-pinterest" href="https://pinterest.com/pin/create/button/?url=" target="_blank">
                    <span aria-hidden="true" class="icon icon-pinterest"></span>
                    <span class="share-title">Pinterest</span>

                </a>
            </li>
            <li>
                <a class="share-google" href="https://plus.google.com/share?url=" target="_blank">
                    <!-- Cannot get Google+ share count with JS yet -->
                    <span aria-hidden="true" class="icon icon-gplus"></span>
                    <span class="share-title">Google+</span>
                    {{--<span class="share-count is-loaded">+1</span>--}}
                </a>
            </li>
            <li>
                <a class="share-fancy" href="https://www.thefancy.com/fancyit?ItemURL=" target="_blank">
                    <span aria-hidden="true" class="icon icon-fancy"></span>
                    <span class="share-title">Share+</span>
                </a>
            </li>
        </ul>

    </div>

    <span class="date">{{$curr}}</span>

    <div class="pic">
        <a href="{{$data['blog']->getUrl()}}"><img alt="" src="{{$data['blog']->getBlogMainImagePath()}}"></a>
    </div>
    {{$data['blog']->content}}
</div>
<div class="comment-block">
    <div class="centering">
        <span class="comRes"></span>
        @if($data['blog']->getActiveComments()->count()>0)
            <div class="comment">

                <h4>Comments:</h4>
                <!--  <a class="pic" href="#"><img src="images/comment-img1.jpg" alt="" /></a>-->
                <div class="text">
                    @if($data['blog']->has('comments'))
                        @foreach($data['blog']->getActiveComments()->get() as $comment)
                            <?php
                            $create = $comment->created_at;
                            $ts = strtotime($create);
                            $curr = date('F j,Y ,g:i', $ts);
                            $at = explode(',', $curr);
                            ?>
                            <span class="date">{{$at[0].",".$at[1]."AT ". $at[2]}} </span>
                            <p>{{$comment->message}}</p>
                        @endforeach
                        @else
                        <p>0 Commtents for this blog</p>
                    @endif

                </div>

            </div>
        @endif

        <div class="comment_respone">
            <h3>Leave a reply</h3>

            {{ Form::open(array('url'=>'/blogs/comments', 'class'=>'form form-horizontal','id'=>'comment_form')) }}
            <fieldset>
                {{ Form::hidden('blog_id',$data['blog']->id) }}
                {{ Form::text('name', null,array('class'=>'form-control required','id'=>'name','placeholder'=>'Name*'))
                }}
                {{ Form::text('email', null,array('class'=>'form-control email required','placeholder'=>'Email*')) }}
                {{ Form::textarea('message', null,array('class'=>'form-control
                required','id'=>'message','placeholder'=>'Message*')) }}
                {{ Form::button('Submit Comment', array('type'=>'submit','class'=>'subcomment'))}}

            </fieldset>
            {{ Form::close() }}

        </div>
    </div>
</div>
