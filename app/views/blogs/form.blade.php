<?php
$tags = Tag::getAllTag();
$blogstags = $data->tags()->lists('tag_id');
$ts = strtotime($data->created_at);
$curr = date('Y-m-d', $ts);

?>
<div class="row row-fluid">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group  <?php if( $errors->first('name')){echo 'has-error';}?>">
                    {{Form::label('name', 'Name', array('class'=>'control-label'))}}
                    {{ Form::text('name', $data->name,array('onchange'=>'getBlogSlug(this);return
                    true;','class'=>'form-control')) }}
                </div>

                <div class="form-group  <?php if( $errors->first('slug')){echo 'has-error';}?>">
                    {{Form::label('slug', 'Slug', array('class'=>'control-label'))}}
                    {{ Form::text('slug', $data->slug,array('class'=>'form-control')) }}
                </div>

                <div class="form-group  <?php if( $errors->first('created_at')){echo 'has-error';}?>">
                    {{Form::label('created_at', 'Date', array('class'=>'control-label'))}}
                    {{ Form::text('created_at',$curr,array('class'=>'form-control','id'=>'datepicker')) }}
                </div>

                <div class="form-group  <?php if( $errors->first('content')){echo 'has-error';}?>">
                    {{Form::label('content', 'Content', array('class'=>'control-label'))}}
                    {{ Form::textarea('content', $data->content, array('class'=>'form-control','id'=>'editor1')) }}
                </div>
                <script>
                    CKEDITOR.replace('editor1', {
                        filebrowserBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html',
                        filebrowserImageBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Images',
                        filebrowserFlashBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Flash',
                        filebrowserUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                        filebrowserImageUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                        filebrowserFlashUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                        height:300
                    });
                </script>

                <div class="form-group  <?php if( $errors->first('teaser')){echo 'has-error';}?>">
                    {{Form::label('teaser', 'Teaser', array('class'=>'control-label'))}}
                    {{ Form::textarea('teaser', $data->teaser,array('class'=>'form-control','id'=>'editor2')) }}
                </div>
                <script>
                    CKEDITOR.replace('editor2', {
                        
                        filebrowserBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html',
                        filebrowserImageBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Images',
                        filebrowserFlashBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Flash',
                        filebrowserUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                        filebrowserImageUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                        filebrowserFlashUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                    });
                </script>

            </div>
        </div>
    </div>


</div>


<div class="row row-fluid">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <div class="form-group  <?php if( $errors->first('tags')){echo 'has-error';}?>">
                    {{Form::label('tags', 'Tags', array('class'=>'control-label'))}}
                    {{Form::select('tags[]',$tags,$blogstags,['id'=>'select-state','class'=>'Information-box
                    no_dedivvery_type', 'multiple'=>'multiple'])}}
                    <div class="vadivdation-box"></div>
                </div>

                <div class="form-group  <?php if( $errors->first('meta_title')){echo 'has-error';}?>">
                    {{Form::label('meta_title', 'Meta Title', array('class'=>'control-label'))}}
                    {{ Form::text('meta_title', $data->meta_title,array('class'=>'form-control')) }}
                </div>

                <div class="form-group  <?php if( $errors->first('meta_description')){echo 'has-error';}?>">
                    {{Form::label('meta_description', 'Meta Description', array('class'=>'control-label'))}}
                    {{ Form::textarea('meta_description', $data->meta_description,array('class'=>'form-control')) }}
                </div>


            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    @if(Auth::user()->isAdmin())
    <div class="panel-body">
        {{Form::label('status', 'Status', array('class'=>'control-label'))}}
        {{ Form::checkbox('status', 1,$data->status) }}
    </div>
    @endif
    <div class="panel-body">
