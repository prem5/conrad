<div class="buy-block about update">
    <h1>Blog & News Updates </h1>
    @if($data['blogs']->count())
    @foreach($data['blogs'] as $blog)
    <div class="row">
        <div class="pic">
            <a href="{{$blog->getUrl()}}"><img alt="" src="{{$blog->getBlogMainImagePath()}}"></a>
        </div>
        <h3><a href="{{$blog->getUrl()}}">{{$blog->name}}</a></h3>
            {{$blog->teaser}}


        <a href="{{$blog->getUrl()}}" class="more">read more</a>

        <div class="social social-sharing">

            <ul>
                <li>
                    <a class="share-facebook" href="https://www.facebook.com/sharer.php?u={{$blog->getUrl()}}"
                       target="_blank">
                        <span aria-hidden="true" class="icon icon-facebook"></span>
                        <span class="share-title">Like</span>

                    </a>
                </li>
                <li>
                    <a class="share-twitter"
                       href="https://twitter.com/share?url={{$blog->getUrl()}}&amp;text=jQuery%20social%20media%20buttons%20with%20share%20counts%20on%20GitHub&amp;via=test"
                       target="_blank">
                        <span aria-hidden="true" class="icon icon-twitter"></span>
                        <span class="share-title">Tweet</span>
                        {{--<span class="share-count is-loaded">6203</span>--}}
                    </a>
                </li>
                <li>
                    <a class="share-pinterest"
                       href="https://pinterest.com/pin/create/button/?url={{$blog->getUrl()}}&amp;description=Description of page"
                       target="_blank">
                        <span aria-hidden="true" class="icon icon-pinterest"></span>
                        <span class="share-title">Pinterest</span>

                    </a>
                </li>
                <li>
                    <a class="share-google" href="https://plus.google.com/share?url={{$blog->getUrl()}}" target="_blank">
                        <!-- Cannot get Google+ share count with JS yet -->
                        <span aria-hidden="true" class="icon icon-gplus"></span>
                        <span class="share-title">Google+</span>
                        {{--<span class="share-count is-loaded">+1</span>--}}
                    </a>
                </li>
                <li>
                    <a class="share-fancy"
                       href="https://www.thefancy.com/fancyit?ItemURL={{$blog->getUrl()}}&amp;Title={{$blog->name}}"
                       target="_blank">
                        <span aria-hidden="true" class="icon icon-fancy"></span>
                        <span class="share-title">Share+</span>
                    </a>
                </li>
            </ul>

        </div>
    </div>
    @endforeach
    <?php $links = $data['blogs']->appends(Request::except(['page','_token']))->links();?>
    @if($links!="")
    <div class="pagination">
        {{ $links }}
    </div>
    @endif
    @else
        <p>Sorry we couldn’t find any news or articles that match your search. Try looking at our most recent articles <a href="{{url('/blog-news')}}"> here.</a></p>
    @endif
</div>