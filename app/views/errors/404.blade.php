@extends('layouts.front.layout')
@section('header')
@stop
@section('content')
    <section >
        <div>
            <h1>We Couldn't Find the Page You Were Looking For.</h1>
            <p style="font-size: 28px; padding: 26px 0 26px 0;">
                The page you’re trying to open doesn't exist. Either check the url in the address bar, or use the menu options above.
            </p>
        </div>
    </section>
@stop