<section id="middle-wrap">
    <div class="default">
        <div class="after-scroll">


        </div>
    </div>
    <div class="mainpagenotfound container">
        <img src="{{url('assets/images/404.jpg')}}">

        <p>
            Woops! It looks like that page doesn't exist.
            Please go back to our<br> <a href="/"> Home Page </a> to find what you're looking for.
        </p>
    </div>
</section>