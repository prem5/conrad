<div class="guide-block">
    <div class="centering">
        {{$data['content']}}
        <div class="group">
            @if($data['guides']->count())
            <?php $data['guides']->chunk(2, function ($row) {
                foreach ($row as $guide) {
                    ?>
                    <div class="left">
                        <ul>
                            <li>
                                <div class="icon">
                                    <a href="{{$guide->getUrl()}}"><img src="/assets/images/{{$guide->category->image}}"
                                                                        alt=""></a>
                                </div>
                                <div class="text">
                                    <h2><a href="{{$guide->getUrl()}}">{{$guide->name}}</a></h2>

                                    <p>{{$guide->teaser}} <a href="{{$guide->getUrl()}}">Read More</a></p>
                                </div>
                            </li>

                        </ul>
                    </div>
                <?php } ?>
                <div class="clear"></div>
            <?php
            });
            ?>
            @endif
        </div>
    </div>
</div>