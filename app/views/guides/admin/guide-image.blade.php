<div class="span5">
    {{Form::open(array('id'=>'upload','file'=>true,'method'=>'POST','url' =>
    url("/administrator/guides/upload/$data->id")))}}
    {{--
    <form id="upload" enctype="multipart/form-data" method="POST" action=''
    {{url("/administrator/guides/upload/$data->id")}}'>--}}
    <div id="drop"><!-- multiple -->
        Drop Here <a>choose file</a>
        <input type="file" action='{{url("/administrator/blogs/guides/$data->id")}}' name="photo">
        <input type="hidden" name="slug" value="{{$data->slug}}">

        <p class="tip-text">Min Image size should be 400*400.<br>
            File size limit 4 MB.</p>
    </div>
    <ul class="up-img">
        <!-- The file uploads will be shown here -->
    </ul>
    <div style="display:none" class="add-galley-image-in">
        <ul>
            <li class="last-chd">
                <label class="selected-label">No file chosen</label>
                <input type="submit" value="Upload" name="upload">
            </li>
        </ul>
    </div>
    {{--</form>--}}
    {{ Form::close() }}
</div>
<div class="span6">
    <div class="tips tip-1">
        <div class="tips-message">
        </div>
    </div>
</div>
<div class="clearfix"></div>


<div class="galalry-tip line-upper">
    <div class="tip-text">
        <span>Hover over picture to delete or make your main picture.</span>
    </div>
</div>

<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th><i class="fa fa-move"></i>ID</th>
        <th>Image</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{{$data->id}}</td>
        <td>{{ HTML::image($data->getGuideThumbImagePath(),$data->name) }}</td>
        <td></td>
    </tr>


    </tbody>
</table>

<div class="galalry-tip line-upper">
    <div class="tip-text">
        <span>No picture uploaded.</span>
    </div>
</div>
