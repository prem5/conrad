<span style="float: right; margin-bottom: 10px;">{{HTML::link('/administrator/guides/create','Add Guide',['class'=>"btn btn-primary"])}}</span>
<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    @foreach($roles as $d)
    <tr id="{{$d->id}}">
        <td>{{$d->id}}</td>
        <td>{{$d->name}}</td>
        <td>
            {{HTML::link('/administrator/guides/update/'.$d->id,'Edit',array('class'=>'btn btn-warning btn-small'))}}
            {{HTML::link('/administrator/guides/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning btn-small',
            'onClick'=>"return confirm('Do you really want to delete ?')"))}}
            <!--{{HTML::link('/administrator/guides/image/'.$d->id,'Guide Image',array('class'=>'btn btn-warning
            btn-small'))}}-->
        </td>
    </tr>
    @endforeach
    </tbody>
</table>