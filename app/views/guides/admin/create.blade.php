{{ Form::open(array('url'=>'/administrator/guides/store', 'class'=>'form form-horizontal', 'files'=>true)) }}
<div class="row row-fluid">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('name')){echo 'has-error';}?>">
                    {{Form::label('name', 'Name', array('class'=>'control-label'))}}
                    {{ Form::text('name', null,array('onchange'=>'getGuideSlug(this);return
                    true;','class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('slug')){echo 'has-error';}?>">
                    {{Form::label('slug', 'Slug', array('class'=>'control-label'))}}
                    {{ Form::text('slug', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('guide_category_id')){echo 'has-error';}?>">
                    {{Form::label('guide_category_id', 'Guide Category', array('class'=>'control-label'))}}
                    {{ Form::select('guide_category_id', GuideCategory::getGuideCategories(),
                    array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('content')){echo 'has-error';}?>">
                    {{Form::label('content', 'Content', array('class'=>'control-label'))}}
                    {{ Form::textarea('content', null, array('class'=>'form-control','id'=>'editor1')) }}
                    <script>
                        CKEDITOR.replace('editor1', {
                            filebrowserBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html',
                            filebrowserImageBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Images',
                            filebrowserFlashBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Flash',
                            filebrowserUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                            filebrowserImageUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                            filebrowserFlashUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                            height :300
                        });
                    </script>
                </div>


                <div class="form-group <?php if( $errors->first('teaser')){echo 'has-error';}?>">
                    {{Form::label('teaser', 'Teaser', array('class'=>'control-label'))}}
                    {{ Form::textarea('teaser', null,array('class'=>'form-control','id'=>'editor2')) }}
                </div>
                <script>
                    CKEDITOR.replace('editor2', {
                        toolbar: [
                            {name: 'document', items: ['Bold', 'Italic', "Underline", "Link", "Maximize", "Source"]},
                            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] }
                        ],
                        filebrowserBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html',
                        filebrowserImageBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Images',
                        filebrowserFlashBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Flash',
                        filebrowserUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                        filebrowserImageUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                        filebrowserFlashUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                    });
                </script>
            </div>
        </div>


    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="form-group <?php if( $errors->first('meta_title')){echo 'has-error';}?>">
                        {{Form::label('meta_title', 'Meta Title', array('class'=>'control-label'))}}
                        {{ Form::text('meta_title', null,array('class'=>'form-control')) }}
                    </div>

                    <div class="form-group <?php if( $errors->first('meta_description')){echo 'has-error';}?>">
                        {{Form::label('meta_description', 'Meta Description', array('class'=>'control-label'))}}
                        {{ Form::textarea('meta_description', null,array('class'=>'form-control')) }}
                    </div>

                    <div class="form-group">
                        {{Form::label('status', 'Status', array('class'=>'control-label'))}}
                        {{ Form::checkbox('status', 1, array('checked'=>'checked')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                        <a href="{{url('/administrator/guides/')}}" class="btn btn-warning btn-small">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}
