<!-- begin buy block -->
<div class="buy-block">

    <h1>{{$data['guide']->name}}</h1>

    <div class="social social-sharing">

        <ul>
            <li>
                <a target="_blank" href="https://www.facebook.com/sharer.php?u=<?php echo $data['guide']->getUrl(); ?>"
                   class="share-facebook">
                    <span class="icon icon-facebook" aria-hidden="true"></span>
                    <span class="share-title">Like</span>
                    {{--<span class="share-count">0</span>--}}
                </a>
            </li>
            <li>
                <a target="_blank" href="https://twitter.com/share?url=<?php echo $data['guide']->getUrl(); ?>"
                   class="share-twitter">
                    <span class="icon icon-twitter" aria-hidden="true"></span>
                    <span class="share-title">Tweet</span>
                    {{--<span class="share-count">0</span>--}}
                </a>
            </li>
            <li>
                <a target="_blank"
                   href="https://pinterest.com/pin/create/button/?url=<?php echo $data['guide']->getUrl(); ?>"
                   class="share-pinterest">
                    <span class="icon icon-pinterest" aria-hidden="true"></span>
                    <span class="share-title">Pinterest</span>
                   {{-- <span class="share-count">0</span>--}}
                </a>
            </li>
            <li>
                <a target="_blank" href="https://plus.google.com/share?url=<?php echo $data['guide']->getUrl(); ?>"
                   class="share-google">
                    <!-- Cannot get Google+ share count with JS yet -->
                    <span class="icon icon-google none" aria-hidden="true"></span>
                    <span class="share-title">Google+</span>
                    {{--<span class="share-count">+1</span>--}}
                </a>
            </li>
            <li>
                <a target="_blank"
                   href="https://www.thefancy.com/fancyit?ItemURL=<?php echo $data['guide']->getUrl(); ?>"
                   class="share-fancy">
                    <span class="icon icon-fancy none" aria-hidden="true"></span>
                    <span class="share-title">Share+</span>
                </a>
            </li>
        </ul>

    </div>

   <!-- <div class="pic">
        <a href="{{$data['guide']->getUrl()}}"><img alt="" src="{{$data['guide']->getGuideMainImagePath()}}"></a>
    </div> -->
    <p>{{$data['guide']->content}}</p>

 {{--*/$count = \Guide::getActive()->count();/*--}}
 @if($count > 1)
    <div class="link">
        @if($count > 2)
            <a class="prev" href="{{$data['guide']->getPrevious()->getUrl()}}">Previous Page</a>
        @endif
        <a class="next" href="{{$data['guide']->getNext()->getUrl()}}">Next Page</a>
    </div>
 @endif

</div>
<!-- finish buy block -->
