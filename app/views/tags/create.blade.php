{{ Form::open(array('url'=>'/administrator/keyword/store', 'class'=>'form form-horizontal', 'files'=>true)) }}
<div class="row row-fluid">
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('name')){echo 'has-error';}?>">
                    {{Form::label('name', 'Tag Name', array('class'=>'control-label'))}}
                    {{ Form::text('name', null,array('onchange'=>'getTagSlug(this);return
                    true;','class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('slug')){echo 'has-error';}?>">
                    {{Form::label('slug', 'Slug', array('class'=>'control-label'))}}
                    {{ Form::text('slug', null,array('class'=>'form-control')) }}
                </div>


                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/keyword/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}