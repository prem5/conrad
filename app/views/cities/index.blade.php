<span style="float: right; margin-bottom: 10px;">{{HTML::link('/administrator/cities/create','Add City',['class'=>"btn btn-primary"])}}</span>
<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>City Name</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    @foreach($roles as $d)
    <tr id="{{$d->id}}">
        <td>{{$d->id}}</td>
        <td>{{$d->name}}</td>
        <td>
            {{HTML::link('/administrator/cities/update/'.$d->id,'Edit',array('class'=>'btn btn-warning btn-small'))}}
            {{HTML::link('/administrator/cities/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning btn-small',
            'onClick'=>"return confirm('Do you really want to delete ?')"))}}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>