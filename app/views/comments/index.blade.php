<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Blog</th>
        <th>Commmented By</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>

    @foreach($roles as $d)
    <tr id="{{$d->id}}">
        <td>{{$d->id}}</td>
        <td>{{$d->blog()->first()->name}}</td>
        <td>{{$d->name}} ( {{$d->email}} )</td>
        <td>

            <?php
            if ($d->status == 0) {
                $status = "Publish";
            } else {
                $status = "Unpublish";

            }
            ?>
            {{HTML::link('/administrator/comments/update/'.$d->id,'Edit',array('class'=>'btn btn-warning btn-small'))}}
            {{HTML::link('/administrator/comments/view/'.$d->id,'View',array('class'=>'btn btn-warning btn-small'))}}
            {{HTML::link('/administrator/comments/publish/'.$d->id,$status,array('class'=>'btn btn-warning
            btn-small'))}}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>