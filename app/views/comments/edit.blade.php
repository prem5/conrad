{{ Form::open(array('url'=>"/administrator/comments/save/$data->id", 'class'=>'form form-horizontal', 'files'=>true)) }}
<div class="row row-fluid">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('name')){echo 'has-error';}?>">
                    {{Form::label('name', 'Name', array('class'=>'control-label'))}}
                    {{ Form::text('name', $data->name,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('email')){echo 'has-error';}?>">
                    {{Form::label('email', 'Email', array('class'=>'control-label'))}}
                    {{ Form::text('email', $data->email,array('class'=>'form-control')) }}
                </div>
                <div class="form-group <?php if( $errors->first('message')){echo 'has-error';}?>">
                    {{Form::label('message', 'Message', array('class'=>'control-label'))}}
                    {{ Form::textarea('message', $data->message, array('class'=>'form-control','id'=>'editor1')) }}
                </div>
                <script>
                    CKEDITOR.replace('editor1', {
                        toolbar: [
                            {name: 'document', items: ['Bold', 'Italic', "Underline", "Link", "Maximize", "Source"]},
                            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] }
                        ],
                        filebrowserBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html',
                        filebrowserImageBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Images',
                        filebrowserFlashBrowseUrl: '<?php echo url('assets/admin') ?>/ckfinder/ckfinder.html?type=Flash',
                        filebrowserUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                        filebrowserImageUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                        filebrowserFlashUploadUrl: '<?php echo url('assets/admin') ?>/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                    });
                </script>

                <div class="form-group">
                    {{Form::label('status', 'Status', array('class'=>'control-label'))}}
                    {{ Form::checkbox('status', 1,$data->status) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/comments/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>


            </div>
        </div>
    </div>
</div>
{{ Form::close() }}