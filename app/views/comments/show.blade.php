<table>
    <thead>
    <tr>
        <td>Name : {{$data->name}}</td>
    </tr>
    <tr>
        <td>Email : {{$data->email}}</td>
    </tr>
    <tr>
        <td>message : {{$data->message}}</td>
    </tr>
    <tr>
        <td>Blog Name : {{$data->blog()->first()->name}}</td>
    </tr>
    <tr>
        <td>Status :
            @if($data->status=='1')
            Publish
            @else
            Unpublish
            @endif
        </td>
    </tr>

    </thead>
</table>
<div class="">
    <?php
    if ($data->status == 0) {
        $status = "Publish";
    } else {
        $status = "Unpublish";

    }
    ?>
    {{HTML::link('/administrator/comments/publish/'.$data->id,$status,array('class'=>'btn btn-warning btn-small'))}}
</div>
