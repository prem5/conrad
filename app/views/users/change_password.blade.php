{{ Form::open(array('url'=>'/administrator/users/savepassword/'.$uid, 'class'=>'form form-horizontal form-striped')) }}
<div class="row row-fluid">
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('password')){echo 'has-error';}?>">
                    {{Form::label('password', 'Password', array('class'=>'control-label'))}}
                    {{ Form::password('password',array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('password_confirmation')){echo 'has-error';}?>">
                    {{Form::label('password_confirmation', 'Confirm Password', array('class'=>'control-label'))}}
                    {{ Form::password('password_confirmation',array('class'=>'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}