<div class="row row-fluid">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Basic Information</h3>
            </div>
            {{ Form::open(array('url'=>"/administrator/users/save/$data->id", 'class'=>'form form-horizontal form-striped')) }}
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('firstname')){echo 'has-error';}?>">
                    {{Form::label('firstname', 'First Name', array('class'=>'control-label'))}}
                    {{ Form::text('firstname', $data->firstname,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('lastname')){echo 'has-error';}?>">
                    {{Form::label('lastname', 'Last Name', array('class'=>'control-label'))}}
                    {{ Form::text('lastname', $data->lastname,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('email')){echo 'has-error';}?>">
                    {{Form::label('email', 'Email', array('class'=>'control-label'))}}
                    {{ Form::text('email', $data->email,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('role_id')){echo 'has-error';}?>">
                    {{Form::label('role_id', 'Role', array('class'=>'control-label'))}}
                    {{ Form::select('role_id', Role::getRoleForSelect(),$data->role_id,array('class'=>'form-control')) }}
                </div>

                <div class="form-group">
                    {{Form::label('status', 'Status', array('class'=>'control-label'))}}
                    {{ Form::checkbox('status',1,$data->status ) }}
                </div>


                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/users/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>