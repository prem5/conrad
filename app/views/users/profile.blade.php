<div class="row row-fluid">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Basic Information</h3>
            </div>
            {{ Form::open(array('url'=>"/writer/users/save/$data->id", 'class'=>'form form-horizontal form-striped')) }}
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('firstname')){echo 'has-error';}?>">
                    {{Form::label('firstname', 'First Name', array('class'=>'control-label'))}}
                    {{ Form::text('firstname', $data->firstname,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('lastname')){echo 'has-error';}?>">
                    {{Form::label('lastname', 'Last Name', array('class'=>'control-label'))}}
                    {{ Form::text('lastname', $data->lastname,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('email')){echo 'has-error';}?>">
                    {{Form::label('email', 'Email', array('class'=>'control-label'))}}
                    {{ Form::text('email', $data->email,array('class'=>'form-control')) }}
                </div>


                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Password Information</h3></div>
            {{ Form::open(array('url'=>"/writer/users/pass/$data->id", 'class'=>'form form-horizontal form-striped')) }}
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('old_pass')){echo 'has-error';}?>">
                    {{Form::label('old_pass', 'Old Password', array('class'=>'control-label'))}}
                    {{ Form::password('old_pass',array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('password')){echo 'has-error';}?>">
                    {{Form::label('password', 'Password', array('class'=>'control-label'))}}
                    {{ Form::password('password',array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('password_confirmation')){echo 'has-error';}?>">
                    {{Form::label('password_confirmation', 'Confirm Password', array('class'=>'control-label'))}}
                    {{ Form::password('password_confirmation',array('class'=>'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/users/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    {{ Form::close() }}
</div>
