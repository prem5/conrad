{{ Form::open(array('url'=>'/administrator/users/store', 'class'=>'form form-horizontal form-striped')) }}
<div class="row row-fluid">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Basic Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('firstname')){echo 'has-error';}?>">
                    {{Form::label('firstname', 'First Name', array('class'=>'control-label'))}}
                    {{ Form::text('firstname', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('lastname')){echo 'has-error';}?>">
                    {{Form::label('lastname', 'Last Name', array('class'=>'control-label'))}}
                    {{ Form::text('lastname', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('email')){echo 'has-error';}?>">
                    {{Form::label('email', 'Email', array('class'=>'control-label'))}}
                    {{ Form::text('email', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('password')){echo 'has-error';}?>">
                    {{Form::label('password', 'Password', array('class'=>'control-label'))}}
                    {{ Form::password('password',array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('password+confirmation')){echo 'has-error';}?>">
                    {{Form::label('password_confirmation', 'Confirm Password', array('class'=>'control-label'))}}
                    {{ Form::password('password_confirmation',array('class'=>'form-control')) }}
                </div>

                <?php
                $role = \Role::all();
                $roles[] = 'Select ';
                foreach ($role as $r) {
                    //dd($r->id);
                    $roles[$r->id] = $r->name;
                }
                ?>

                <div class="form-group <?php if( $errors->first('role_id')){echo 'has-error';}?>">
                    {{Form::label('role_id', 'Role', array('class'=>'control-label'))}}
                    {{Form::select('role_id',
                    $roles,Null,array('onchange'=>'selectRole(this);','class'=>'form-control'))}}
                </div>

                <div class="form-group">
                    {{Form::label('status', 'Status', array('class'=>'control-label'))}}
                    {{ Form::checkbox('status',1,array('checked'=>'checked')) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/users/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}