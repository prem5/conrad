<div class="form-horizontal templatemo-signin-form">
    <h1 class='sectionTitle'>Sign in</h1>

    {{ Form::open(array('url'=>'users/signin', 'class'=>'form-signin')) }}

    <div class="form-group">
        <div class="col-md-12">
            {{Form::label('email', 'Email',array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                {{ Form::text('email', null, array('class'=>'form-control','placeholder'=>"Email")) }}
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            {{Form::label('password', 'Password',array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                {{ Form::password('password', array('class'=>'form-control','placeholder'=>"Password")) }}
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <div class="col-sm-offset-2 col-sm-10">
                {{ Form::submit('Login', array('class'=>'btn btn-default'))}}
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="/users/forgot-password">Forgot your password?</a>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>