<span style="float: right; margin-bottom: 10px;">{{HTML::link('/administrator/users/create','Add User',['class'=>"btn btn-primary"])}}</span>
<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Lastname Name</th>
        <th>Email</th>
        <!--<th>Value</th>-->
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $d)
    <tr id="{{$d->id}}">
        <td>{{$d->id}}</td>
        <td>{{$d->firstname}}</td>
        <td>{{$d->lastname}}</td>
        <td>{{$d->email}}</td>
        <td>
            {{HTML::link('/administrator/users/profile/'.$d->id,'Edit',array('class'=>'btn btn-warning
            btn-small'))}}
            @if(Auth::user()->id!=$d->id && $d->id!=1 )
             {{HTML::link('/administrator/users/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning
            btn-small', 'onClick'=>"return confirm('Do you really want to delete ?')"))}}
            @endif
            {{HTML::link('/administrator/users/changepassword/'.$d->id,'Change Password',array('class'=>'btn btn-warning
            btn-small'))}}
        </td>
        <!--<td>{{$d->password}}</td>-->
    </tr>
    @endforeach
    </tbody>
</table>