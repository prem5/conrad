{{ Form::open(array('url'=>"/administrator/newsletter/muldelete", 'class'=>'form form-horizontal')) }}
<span style="float: right; margin-bottom: 10px;">
     {{ Form::submit('Delete', array('id'=>"deluser",'class'=>'btn btn-danger'))}}
   {{HTML::link('/administrator/newsletter/create','Add Subscriber',['class'=>"btn btn-primary"])}}
    {{HTML::link('/administrator/newsletter/csv','Export CSV',['class'=>"btn btn-primary"])}}
</span>
<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Select</th>
        <th>Name</th>
        <th>Email</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    @foreach($roles as $d)
    <tr id="{{$d->id}}">
        <td>{{$d->id}}</td>
        <td><input type="checkbox" name="id[]" value="{{$d->id}}" class="muldelete"></td>
        <td>{{$d->name}}</td>
        <td>{{$d->email}}</td>
        <td>{{HTML::link('/administrator/newsletter/update/'.$d->id,'Edit',array('class'=>'btn btn-warning btn-small'))}}</td>
    </tr>
    @endforeach
    </tbody>
</table>
{{ Form::close() }}