<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.front.head')

    <!-- CSS Styles -->
    <link rel="stylesheet" href="/assets/admin/css/jquery-ui.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open Sans:300,400,700">
    <link rel="stylesheet" href="/assets/admin/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/admin/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/admin/css/templatemo_main.css">
    <link rel="stylesheet" href="/assets/admin/css/selectize.bootstrap3.css">
    <!--<link rel="stylesheet" href="/assets/admin/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="/assets/admin/css/style.css">
    <script src="/assets/admin/ckeditor/ckeditor.js"></script>
    <script src="/assets/admin/ckfinder/ckfinder.js"></script>

</head>
<body>
<div id="main-wrapper">
    <div class="mobile-menu">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <div class="navbar navbar-inverse" role="navigation">
        @include('partials.admin.header')
    </div>
    <div class="navbar-collapse collapse templatemo-sidebar">
        @include('partials.admin.navigation')
    </div>
    <div class="template-page-wrapper">
        <div class="templatemo-content">
            @if(Session::has('message'))
            <p class="alert alert-success ">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                {{ Session::get('message') }}
            </p>

            @endif
            @if($errors->all())
            <p class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                @foreach($errors->all() as $val)
                {{ $val ."<br/>" }}
                @endforeach
            </p>
            @endif
            <h1>{{$title}}</h1>
            {{$content}}

        </div>
    </div>
</div>
@include('partials.admin.footer')
</body>
</html>