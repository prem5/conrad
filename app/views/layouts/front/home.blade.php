<?php
include(app_path() . '/custom/mobile_detect.php');
$detect = new mobile_detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.front.head')
    @include('partials.front.styles')
</head>
<body>
<div class="loader" >
    <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48c3ZnIHdpZHRoPScxMjBweCcgaGVpZ2h0PScxMjBweCcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgMTAwIDEwMCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ieE1pZFlNaWQiIGNsYXNzPSJ1aWwtcmluZy1hbHQiPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAiIGhlaWdodD0iMTAwIiBmaWxsPSJub25lIiBjbGFzcz0iYmsiPjwvcmVjdD48Y2lyY2xlIGN4PSI1MCIgY3k9IjUwIiByPSI0MCIgc3Ryb2tlPSIjZmZmZmZmIiBmaWxsPSJub25lIiBzdHJva2Utd2lkdGg9IjEwIiBzdHJva2UtbGluZWNhcD0icm91bmQiPjwvY2lyY2xlPjxjaXJjbGUgY3g9IjUwIiBjeT0iNTAiIHI9IjQwIiBzdHJva2U9IiM1Y2ZmZDYiIGZpbGw9Im5vbmUiIHN0cm9rZS13aWR0aD0iNiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIj48YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJzdHJva2UtZGFzaG9mZnNldCIgZHVyPSIycyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiIGZyb209IjAiIHRvPSI1MDIiPjwvYW5pbWF0ZT48YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJzdHJva2UtZGFzaGFycmF5IiBkdXI9IjJzIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIgdmFsdWVzPSIxNTAuNiAxMDAuNDsxIDI1MDsxNTAuNiAxMDAuNCI+PC9hbmltYXRlPjwvY2lyY2xlPjwvc3ZnPg==">
</div>
<!-- begin section -->
<section id="section">

    <!-- begin page-wrap -->
    <section id="page-wrap">
        <!-- begin header -->
        <header id="header-wrap">
            @include('partials.front.header')
        </header>
        <!-- finish header -->
    {{--  @if(!Agent::isMobile())--}}
    @include('partials.front.home.banner')
    {{--@endif--}}

    <!-- begin content -->
        <section id="content-wrap">
            <div id="center-wrap">
                <div class="bg-propertie">
                    @include('partials.front.home.featured')
                    @if($deviceType == 'computer')
                    @include('partials.front.home.recent')
                    @endif
                </div>
                {{--<div class="selling-block wow bounceInRight">--}}
                <div class="selling-block">
                    @include('partials.front.home.selling')
                </div>
                <div class="things-block">
                    @include('partials.front.home.things')
                </div>
                <div class="offer-block">
                    @include('partials.front.home.offer')
                </div>
                <!-- begin adams block -->
                <div class="adams-block wow bounceInLeft">
                    <div class="h2">“To give <strong>real service</strong>, you must add something which cannot be bought or
                        measured with money, and that is <strong>sincerity</strong> and <strong>integrity</strong>.”
                    </div>
                    <span>- Douglas Adams</span>
                </div>
                <!-- finish adams block -->
                <div class="blog-block">
                    @include('partials.front.home.blog')
                </div>

                @include('partials.front.home.believe')
            </div>

        </section>

        <!-- finish content -->
        <!-- begin footer -->
        <footer id="footer-wrap">
            @include('partials.front.footer')
        </footer>
        <!-- finish footer -->
    </section>
    <!-- finish page wrap -->

</section>
<!-- finish section -->

@include('partials.front.styles-bottom')

@include('partials.front.scripts')
</body>
</html>