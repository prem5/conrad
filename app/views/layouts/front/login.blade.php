<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.front.head')

    <!-- CSS Styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open Sans:300,400,700">
        <link rel="stylesheet" href="/assets/admin/css/font-awesome.min.css">
        <link rel="stylesheet" href="/assets/admin/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/admin/css/templatemo_main.css">
</head>
<body>
<div id="main-wrapper">
    <div class="navbar navbar-inverse" role="navigation">
        @include('partials.admin.header')
    </div>

    <div class="template-page-wrapper">
        @if(Session::has('message'))
        <p class="alert">{{ Session::get('message') }}</p>
        @endif
        @if($errors->all())
        <p class="alert" role="alert">
            @foreach($errors->all() as $val)
            {{ $val ."<br/>" }}
            @endforeach
        </p>
        @endif
        {{$content}}
    </div>
</div>
</body>
</html>