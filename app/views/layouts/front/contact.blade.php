<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.front.head')

    @include('partials.front.styles')
    <script>
        var settings = {{json_encode(Setting::getAddressForContact())}};
    </script>
</head>
<body>
<div class="loader">
    <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48c3ZnIHdpZHRoPScxMjBweCcgaGVpZ2h0PScxMjBweCcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgMTAwIDEwMCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ieE1pZFlNaWQiIGNsYXNzPSJ1aWwtcmluZy1hbHQiPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAiIGhlaWdodD0iMTAwIiBmaWxsPSJub25lIiBjbGFzcz0iYmsiPjwvcmVjdD48Y2lyY2xlIGN4PSI1MCIgY3k9IjUwIiByPSI0MCIgc3Ryb2tlPSIjZmZmZmZmIiBmaWxsPSJub25lIiBzdHJva2Utd2lkdGg9IjEwIiBzdHJva2UtbGluZWNhcD0icm91bmQiPjwvY2lyY2xlPjxjaXJjbGUgY3g9IjUwIiBjeT0iNTAiIHI9IjQwIiBzdHJva2U9IiM1Y2ZmZDYiIGZpbGw9Im5vbmUiIHN0cm9rZS13aWR0aD0iNiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIj48YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJzdHJva2UtZGFzaG9mZnNldCIgZHVyPSIycyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiIGZyb209IjAiIHRvPSI1MDIiPjwvYW5pbWF0ZT48YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJzdHJva2UtZGFzaGFycmF5IiBkdXI9IjJzIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIgdmFsdWVzPSIxNTAuNiAxMDAuNDsxIDI1MDsxNTAuNiAxMDAuNCI+PC9hbmltYXRlPjwvY2lyY2xlPjwvc3ZnPg==">
</div>
<!--<@if(Session::has('message'))
<p class="alert alert-success ">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    {{ Session::get('message') }}
</p>
@endif -->
<!-- begin section -->
<section id="section">

    <!-- begin page-wrap -->
    <section id="page-wrap">
        <!-- begin header -->
        <header id="header-wrap">
            @include('partials.front.header')
        </header>
        <!-- finish header -->
        <!-- begin content -->
        <section id="content-wrap">

            @include('partials.front.breadcrumb')
            <div class="contact-block">
                <div class="map">
                    <div id="map-container"></div>
                </div>
                <div class="office-pic">
                    <img src='https://www.conradproperties.asia/uploads/CPofficepic.png'/>
                </div>
                <div class="overlay">
                    <span class="shadow"></span>

                    <div class="centering">
                        <h1 class="cntmob">Contact Us</h1>
                        <div class="right">
                            {{$content}}
                        </div>
                        @include('partials.front.contact-form')


                        <div class="clear"></div>
                    </div>
                </div>
            </div>

        </section>

        @include('partials.front.home.believe')

        <!-- finish content -->
        <!-- begin footer -->
        <footer id="footer-wrap">
            @include('partials.front.footer')
        </footer>
        <!-- finish footer -->
    </section>
    <!-- finish page wrap -->

</section>
<!-- finish section -->

@include('partials.front.styles-bottom')

@include('partials.front.scripts')
{{$script}}
</body>
</html>