<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.front.head')

    @include('partials.front.styles')
</head>
<body>
<div class="loader">
    <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48c3ZnIHdpZHRoPScxMjBweCcgaGVpZ2h0PScxMjBweCcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgMTAwIDEwMCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ieE1pZFlNaWQiIGNsYXNzPSJ1aWwtcmluZy1hbHQiPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAiIGhlaWdodD0iMTAwIiBmaWxsPSJub25lIiBjbGFzcz0iYmsiPjwvcmVjdD48Y2lyY2xlIGN4PSI1MCIgY3k9IjUwIiByPSI0MCIgc3Ryb2tlPSIjZmZmZmZmIiBmaWxsPSJub25lIiBzdHJva2Utd2lkdGg9IjEwIiBzdHJva2UtbGluZWNhcD0icm91bmQiPjwvY2lyY2xlPjxjaXJjbGUgY3g9IjUwIiBjeT0iNTAiIHI9IjQwIiBzdHJva2U9IiM1Y2ZmZDYiIGZpbGw9Im5vbmUiIHN0cm9rZS13aWR0aD0iNiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIj48YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJzdHJva2UtZGFzaG9mZnNldCIgZHVyPSIycyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiIGZyb209IjAiIHRvPSI1MDIiPjwvYW5pbWF0ZT48YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJzdHJva2UtZGFzaGFycmF5IiBkdXI9IjJzIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIgdmFsdWVzPSIxNTAuNiAxMDAuNDsxIDI1MDsxNTAuNiAxMDAuNCI+PC9hbmltYXRlPjwvY2lyY2xlPjwvc3ZnPg==">
</div>
<!-- begin section -->
<section id="section">
    <!-- begin page-wrap -->
    <section id="page-wrap">
        <!-- begin header -->
        <header id="header-wrap">
            @include('partials.front.header')
        </header>
        <!-- finish header -->
        <!-- begin content -->
        <section id="content-wrap" class="bg">
            <div id="center-wrap">
                @include('partials.front.breadcrumb')
                <div class="centering">
                    <div id="leftside">
                        {{$content}}
                    </div>
                    <div id="rightside">
                        @include('partials.front.sidebar')
                    </div>
                    <div class="clear"></div>
                </div>
                @include('partials.front.home.believe')
            </div>
        </section>
        <!-- finish content -->
        <!-- begin footer -->
        <footer id="footer-wrap">
            @include('partials.front.footer')
        </footer>
        <!-- finish footer -->
    </section>
    <!-- finish page wrap -->
</section>
<!-- finish section -->

@include('partials.front.styles-bottom')

@include('partials.front.scripts')
{{$script}}
</body>
</html>