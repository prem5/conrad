{{ Form::open(array('url'=>'/administrator/testimonials/store', 'class'=>'form form-horizontal')) }}
<div class="row row-fluid">
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('quote')){echo 'has-error';}?>">
                    {{Form::label('quote', 'Quote', array('class'=>'control-label'))}}
                    {{ Form::textarea('quote', null,array('class'=>'form-control','id'=>'editor1')) }}
                </div>
                <script>
                    CKEDITOR.replace('editor1', {
                        toolbar: [
                            {name: 'document', items: ['Italic', 'Bold']},
                            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] }
                        ]
                    });
                </script>

                <div class="form-group <?php if( $errors->first('name')){echo 'has-error';}?>">
                    {{Form::label('name', 'Name', array('class'=>'control-label'))}}
                    {{ Form::text('name', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group">
                    {{Form::label('status', 'Status', array('class'=>'control-label'))}}
                    {{ Form::checkbox('status', 1, array('checked'=>'checked')) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/testimonials/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}