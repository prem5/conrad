{{ Form::open(array('url'=>'/administrator/carousels/store', 'class'=>'','method'=>'POST','id'=>'carousel-index-form')) }}
<div class="row row-fluid">
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('property_id')){echo 'has-error';}?>">
                    {{Form::label('property_id', 'Property', array('class'=>'control-label'))}}
                    {{ Form::select('property_id',$data,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('order')){echo 'has-error';}?>">
                    {{Form::label('order', 'Order', array('class'=>'control-label'))}}
                    {{ Form::text('order', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group" id="choose" style="display: none">
                    {{Form::label('Image', 'Choose Image', array('class'=>'control-label'))}}
                    <div class="row">
                            <div id="Sthumbs"></div>
                    </div>
                    </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/carousels/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>
            </div>
        </div>
    </div>

</div>

{{Form::close()}}