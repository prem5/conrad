{{Form::model($data['value'],array('url' => array('administrator/carousels/save',$data['value']->id), 'method' => 'POST'))}}

<div class="row row-fluid">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('property_id')){echo 'has-error';}?>">
                    {{Form::label('property_id', 'Property', array('class'=>'control-label'))}}
                    {{
                    Form::select('property_id',$data['dropdown'],$data['value']->property_id.'_'.$data['value']->image,array('class'=>'form-control'))
                    }}
                </div>

                <div class="form-group <?php if( $errors->first('order')){echo 'has-error';}?>">
                    {{Form::label('order', 'Order', array('class'=>'control-label'))}}
                    {{ Form::text('order', $data['value']->order) }}
                </div>

                <div class="form-group" id="choose" style="display: none">
                    {{Form::label('Image', 'Choose Image', array('class'=>'control-label'))}}
                    <div class="row">
                            <div id="Sthumbs"></div>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/carousels/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Carousel Image</h3>
            </div>
            <div class="panel-body">
                <img src="{{$data['value']->getCarouselImage($data['value']->id)}}" height="300" width="500">
            </div>
        </div>
    </div>
</div>
{{Form::close()}}