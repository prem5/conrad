<span style="float: right; margin-bottom: 10px;">{{HTML::link('/administrator/carousels/create','Assign Property in Carousels',['class'=>"btn btn-primary"])}}</span>
{{ Form::open(array('url'=>'', 'class'=>'','id'=>'carousel-index-form')) }}
{{Form::Close()}}
<table id="carousel-listing" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
       <!-- <th>ID</th>-->
        <th>Property</th>
        <th>Carousel Image</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    {{--*/$i=1/*--}}

    @foreach($data as $d)
    <tr id="{{$d->id}}">
       <!-- <td><i class="fa fa-move"></i>{{$d->id}}</td>-->
        <td>{{\Property::find($d->property_id)->title}}</td>
	    <td><img src="{{$d->getCarouselImage($d->id)}}" height="100" width="165"></td>
         <td>
            {{HTML::link('/administrator/carousels/update/'.$d->id,'Edit',array('class'=>'btn btn-warning btn-small'))}}
            {{HTML::link('/administrator/carousels/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning btn-small',
            'onClick'=>"return confirm('Do you really want to delete ?')"))}}
        </td>
    </tr>
    {{--*/$i++/*--}}
    @endforeach
    </tbody>
</table>