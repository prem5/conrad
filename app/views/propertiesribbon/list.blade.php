<span style="float: right; margin-bottom: 10px;">{{HTML::link('/administrator/propertiesribbon/create','Add Property Ribbons',['class'=>"btn btn-primary"])}}</span>
<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Label</th>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    @foreach($roles as $d)
    <tr id="{{$d->id}}">
        <td>{{$d->id}}</td>
        <td>{{$d->label}}</td>
        <td>
            {{HTML::link('/administrator/propertiesribbon/update/'.$d->id,'Edit',array('class'=>'btn btn-warning
            btn-small'))}}
            {{HTML::link('/administrator/propertiesribbon/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning
            btn-small', 'onClick'=>"return confirm('Do you really want to delete ?')"))}}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>