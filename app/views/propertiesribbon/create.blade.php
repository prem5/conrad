{{ Form::open(array('url'=>'/administrator/propertiesribbon/store', 'class'=>'form form-horizontal')) }}
<div class="row row-fluid">
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('label')){echo 'has-error';}?>">
                    {{Form::label('label', 'Ribbons Name:', array('class'=>'control-label'))}}
                    {{ Form::text('label', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('color')){echo 'has-error';}?>">
                    {{Form::label('color', 'Colour:', array('class'=>'control-label'))}}
                    {{ Form::text('color', null,array('class'=>'form-control')) }}
                </div>

                <div class="form-group">
                    {{Form::label('status', 'Status', array('class'=>'control-label'))}}
                    {{ Form::checkbox('status', 1, array('checked'=>'checked')) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/propertiesribbon/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}