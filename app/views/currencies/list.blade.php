<span style="float: right; margin-bottom: 10px;">{{HTML::link('/administrator/currencies/create','Add Curriency',['class'=>"btn btn-primary"])}}</span>
<table id="myTable" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Symbol</th>
        <td>Rate</td>
        <th>Operation</th>
    </tr>
    </thead>
    <tbody>
    @foreach($roles as $d)
    <tr id="{{$d->id}}">
        <td>{{$d->id}}</td>
        <td>{{$d->symbol}}</td>
        <td>{{$d->rate}}</td>
        <td>
            @if ($d->id==1)

            @else
            {{HTML::link('/administrator/currencies/update/'.$d->id,'Edit',array('class'=>'btn btn-warning
            btn-small'))}}
            {{HTML::link('/administrator/currencies/delete/'.$d->id,'Delete',array('class'=>'btn btn-warning btn-small',
            'onClick'=>"return confirm('Do you really want to delete ?')"))}}
            @endif

        </td>
    </tr>
    @endforeach
    </tbody>
</table>