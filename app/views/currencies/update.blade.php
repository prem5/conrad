{{ Form::open(array('url'=>"/administrator/currencies/save/$data->id", 'class'=>'form form-horizontal')) }}
<div class="row row-fluid">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group  <?php if( $errors->first('symbol')){echo 'has-error';}?>">
                    {{Form::label('symbol', 'Symbol', array('class'=>'control-label'))}}
                    {{ Form::text('symbol', $data->symbol,array('class'=>'form-control')) }}
                </div>

                <div class="form-group  <?php if( $errors->first('rate')){echo 'has-error';}?>">
                    {{Form::label('rate', 'Rate', array('class'=>'control-label'))}}
                    {{ Form::text('rate', $data->rate,array('class'=>'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::checkbox('status', 1, $data->status) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/currencies/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}