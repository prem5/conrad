{{ Form::open(array('url'=>"/administrator/areas/save/$data->id", 'class'=>'form form-horizontal')) }}
<div class="row row-fluid">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group <?php if( $errors->first('name')){echo 'has-error';}?>">
                    {{Form::label('name', 'Name', array('class'=>'control-label'))}}
                    {{ Form::text('name', $data->name,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('slug')){echo 'has-error';}?>">
                    {{Form::label('slug', 'Slug', array('class'=>'control-label'))}}
                    {{ Form::text('slug', $data->slug,array('class'=>'form-control')) }}
                </div>

                <div class="form-group <?php if( $errors->first('city_id')){echo 'has-error';}?>">
                    {{Form::label('city_id', 'City', array('class'=>'control-label'))}}
                    {{ Form::select('city_id', City::getCitiesForSelect(),$data->city_id,array('class'=>'form-control'))
                    }}
                </div>

                <div class="form-group">
                    {{Form::label('status', 'Status', array('class'=>'control-label'))}}
                    {{ Form::checkbox('status', 1, $data->status) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Save', array('class'=>'btn btn-primary'))}}
                    <a href="{{url('/administrator/areas/')}}" class="btn btn-warning btn-small">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}