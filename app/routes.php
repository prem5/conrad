<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
#Ajax Controller
Route::controller('/ajax', 'AjaxController');
Route::get('/', 'HomeController@showWelcome');

Route::controller('users', 'Front\UserController');
Route::get('dotproperty-feed.xml', 'HomeController@sitemap');





#Owner or Manager CRUD interfaces
Route::group(array('before' => 'auth'), function () {
    Route::get('get-area-slug/{title}', function ($title) {
        $slug = Str::slug($title);
        $slugCount = Guide::where('slug', 'like %' . $slug . '%')->get()->count();
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    });

    Route::get('get-city-slug/{title}', function ($title) {
        $slug = Str::slug($title);
        $slugCount = Guide::where('slug', 'like %' . $slug . '%')->get()->count();
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    });

    Route::get('get-country-slug/{title}', function ($title) {
        $slug = Str::slug($title);
        $slugCount = Guide::where('slug', 'like %' . $slug . '%')->get()->count();
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    });

    Route::get('get-province-slug/{title}', function ($title) {
        $slug = Str::slug($title);
        $slugCount = Guide::where('slug', 'like %' . $slug . '%')->get()->count();
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    });

    Route::get('get-tag-slug/{title}', function ($title) {
        $slug = Str::slug($title);
        $slugCount = Guide::where('slug', 'like %' . $slug . '%')->get()->count();
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    });

    Route::get('get-guide-slug/{title}', function ($title) {
        $slug = Str::slug($title);
        $slugCount = Guide::where('slug', 'like %' . $slug . '%')->get()->count();
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    });
    Route::get('get-blog-slug/{title}', function ($title) {
        $slug = Str::slug($title);
        $slugCount = Blog::where('slug', 'like %' . $slug . '%')->get()->count();
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    });
    Route::get('get-page-slug/{title}', function ($title) {
        $slug = Str::slug($title);
        $slugCount = Page::where('slug', 'like %' . $slug . '%')->get()->count();
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    });
    Route::get('get-listing-slug/{title}', function ($title) {
        $slug = Str::slug($title);
        $slugCount = Property::where('slug', 'like %' . $slug . '%')->get()->count();
        $slugStr = ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
        $refStr = str_random(5);
        return json_encode(array('slug' => $slugStr));
    });
});

Route::group(array('prefix' => 'writer', 'before' => 'auth|writer'), function () {
    Route::get('/', 'Writer\UserController@getDashboard');
    Route::controller('users', 'Writer\UserController');
    Route::controller('blogs', 'Writer\BlogController');
    Route::controller('listings', 'Admin\PropertiesController');
});

Route::group(array('prefix' => 'administrator', 'before' => 'auth|admin'), function () {
    Route::get('/', 'Admin\UserController@getDashboard');
    Route::controller('users', 'Admin\UserController');
    Route::controller('roles', 'Admin\RolesController');

    Route::controller('pages', 'Admin\PagesController');
        Route::controller('listings', 'Admin\PropertiesController');
    Route::controller('countries', 'Admin\CountriesController');
    Route::controller('provinces', 'Admin\ProvincesController');
    Route::controller('districts', 'Admin\DistrictsController');
    Route::controller('currencies', 'Admin\CurrenciesController');
    Route::controller('testimonials', 'Admin\TestimonialsController');
    Route::controller('setting', 'Admin\SettingsController');
    Route::controller('propertiestype', 'Admin\PropertyTypeController');
    Route::controller('propertiesribbon', 'Admin\PropertyRibbonController');
    Route::controller('newsletter', 'Admin\NewsLetterController');
        Route::controller('blogs', 'Admin\BlogController');
    Route::controller('guides', 'Admin\GuidesController');
    Route::controller('keyword', 'Admin\TagsController');
    Route::controller('galleries', 'Admin\GalleriesController');
    Route::controller('templates', 'Admin\TemplatesController');
    Route::controller('carousels', 'Admin\CarouselsController');
    Route::controller('features', 'Admin\FeaturesController');
    Route::controller('property-submission', 'Admin\PropertySubmissionsController');
    Route::controller('property-enquiry', 'Admin\PropertyEnquriesController');
    Route::controller('cities', 'Admin\CitiesController');
    Route::controller('areas', 'Admin\AreasController');
    Route::controller('comments', 'Admin\CommentsController');
});


Route::post('property-enquire', 'Front\PropertiesController@postEnquire');

# Blogs - match slug
Route::get('blog-news/', 'Front\BlogsController@getSearch');
Route::post('blogs/comments', 'Front\BlogsController@postComment');
Route::get('blog-news/{slug?}', 'Front\BlogsController@getIndex');
Route::post('blog-news/search-result', 'Front\BlogsController@postSearchResult');
Route::get('blog-news/tag/{slug?}', 'Front\BlogsController@getBlogPerTag');

//
//Route::get('send-mail', function () {
//    try {
//        // auto notification message for site admin
//        Mail::send('emails.email', ['bdy' => 'sdgsg'], function ($message) {
//            //$message->alwaysFrom("mailforabhinav@gmail.com", "mailforabhinav@gmail.com");
//            $message->to("mailforabhinav@gmail.com", "mailforabhinav@gmail.com")->subject("Newsletter Signup from mailforabhinav@gmail.com.");
//        });
//    } catch (Exception $e) {
//        print $e->getMessage();
//    }
//});

# Guides - match slug
Route::get('buyers-guide/', 'Front\GuidesController@getSearch');
Route::get('buyers-guide/{slug?}', 'Front\GuidesController@getIndex');

Route::get('pad','HomeController@getPad');
# sitemat
Route::get('test','HomeController@test');
# Route::get('sitemap.xml','HomeController@sitemap');
# Listings - match slug
Route::get('properties/list', 'Front\PropertiesController@getSell');
Route::get('properties/setfavourites', 'Front\PropertiesController@getSetfavourites');
Route::get('properties/removefavourites', 'Front\PropertiesController@getRemoveFavourites');
Route::post('properties/submissionsave', 'Front\PropertiesController@postSubmissionSave');
//Route::get('properties/change','Front\PropertiesController@getChange');
Route::get('properties/{keyboard?}', 'Front\PropertiesController@getIndex');

Route::any('search/all/buy', function () {
    $request = Request::create('/search/all/all/all/all/buy/', 'GET');
    Session::set('action','buy');
    Session::forget('type');
    Session::forget('place');
    Session::forget('price');
    Session::forget('bedroom');
    return Route::dispatch($request)->getContent();
});

Route::any('search/all/rent', function () {
    $request = Request::create('/search/all/all/all/all/rent/', 'GET');
    Session::set('action','rent');
    Session::forget('type');
    Session::forget('place');
    Session::forget('price');
    Session::forget('bedroom');
    return Route::dispatch($request)->getContent();
});
Route::post('searchresult','Front\PropertiesController@postSearchSession');
Route::any('search/{country?}/{province?}/{city?}/{area?}/{action?}/{type?}/', 'Front\PropertiesController@getSearch');

Route::post('contact', 'HomeController@postContact');
Route::post('newsletter', 'HomeController@postNews');
Route::get('favorites', 'HomeController@getFavourites');




/** Property Search From Reference code*/
Route::get('code/{code?}', function ($code) {
    $property = Property::where('reference_code', $code)->first();
    if ($property != null) {
        return Redirect::to('/properties/' . $property->slug);
    }
    return \App::abort(404, '');
});
Route::post('find','HomeController@find');
# Pages - match slug<?php

Route::group(array('prefix' => 'administrator', 'before' => 'auth|admin_writer'),function(){
    Route::controller('listings', 'Admin\PropertiesController');
    Route::controller('blogs', 'Admin\BlogController');
    Route::controller('guides', 'Admin\GuidesController');
});


//Route::get('properties/change','Front\PropertiesController@getChange');

Route::any('koh-samui-luxury-villa-for-sale', 'Front\PropertiesController@getSearch');
Route::any('koh-samui-luxury-villa-for-rent', 'Front\PropertiesController@getSearch');
Route::any('koh-samui-luxury-villas', 'Front\PropertiesController@getSearch');

#Ajax Controller
Route::controller('ajax', 'AjaxController');


# Pages - match slug
Route::get('{postSlug}', 'HomeController@getPage');





// Custom 404 page
App::error(function ($exception, $code) {

    if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
        Log::error('NotFoundHttpException Route: ' . Request::url());
    }

    Log::error($exception);
    $title = 'Conrad Properties Bangkok';
    $head_title = 'Conrad Properties Bangkok';
    $meta_description = "Conrad Properties - Thailand's leading independent property agency";
    $script = "";
    $content = "";
    $og_data = [];
    return Response::make(View::make('errors.404',compact('title','head_title','meta_description','content','og_data','script')), 404);
   //return App::make("ErrorController")->callAction("index", ['code'=> 404]);
    //return View::make('404');

});

/*App::missing(function($exception) {
    return View::make('404');
});*/
